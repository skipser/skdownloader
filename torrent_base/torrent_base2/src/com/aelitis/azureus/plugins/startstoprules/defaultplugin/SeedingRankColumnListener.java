/*
 * File    : SeedingRankColumnListener.java
 * Created : Sep 27, 2005
 * By      : TuxPaper
 *
 * Copyright (C) 2005, 2006 Aelitis SAS, All rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details ( see the LICENSE file ).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * AELITIS, SAS au capital de 46,603.30 euros,
 * 8 Allee Lenotre, La Grille Royale, 78600 Le Mesnil le Roi, France.
 */

package com.aelitis.azureus.plugins.startstoprules.defaultplugin;

import java.util.Map;

import org.gudy.azureus2.core3.config.COConfigurationListener;
import org.gudy.azureus2.core3.config.COConfigurationManager;
import org.gudy.azureus2.core3.internat.MessageText;
import org.gudy.azureus2.core3.util.Constants;
import org.gudy.azureus2.core3.util.SystemTime;
import org.gudy.azureus2.core3.util.TimeFormatter;
import org.gudy.azureus2.plugins.PluginConfig;
import org.gudy.azureus2.plugins.download.Download;

/** A "My Torrents" column for displaying Seeding Rank.
 */
public class SeedingRankColumnListener implements
		 COConfigurationListener {
	private Map downloadDataMap;

	private PluginConfig pluginConfig;

	private int minTimeAlive;

	private int iRankType;

	private boolean bDebugLog;

	public SeedingRankColumnListener(Map _downloadDataMap, PluginConfig pc) {
		downloadDataMap = _downloadDataMap;
		pluginConfig = pc;
		COConfigurationManager.addListener(this);
		configurationSaved();
	}


	/* (non-Javadoc)
	 * @see org.gudy.azureus2.core3.config.COConfigurationListener#configurationSaved()
	 */
	public void configurationSaved() {
		minTimeAlive = pluginConfig
				.getUnsafeIntParameter("StartStopManager_iMinSeedingTime") * 1000;
		iRankType = pluginConfig.getUnsafeIntParameter("StartStopManager_iRankType");
		bDebugLog = pluginConfig.getUnsafeBooleanParameter("StartStopManager_bDebugLog");
	}
}
