/*
 * Created on 07-May-2004
 * Created by Paul Gardner
 * Copyright (C) 2004, 2005, 2006 Aelitis, All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * AELITIS, SAS au capital de 46,603.30 euros
 * 8 Allee Lenotre, La Grille Royale, 78600 Le Mesnil le Roi, France.
 *
 */

package org.gudy.azureus2.platform.macosx;

/**
 * @author parg
 *
 */

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.net.*;

import org.gudy.azureus2.core3.util.*;
import org.gudy.azureus2.core3.html.HTMLUtils;
import org.gudy.azureus2.core3.logging.*;
import org.gudy.azureus2.platform.*;
import org.gudy.azureus2.plugins.utils.resourcedownloader.*;
import org.gudy.azureus2.pluginsimpl.local.utils.resourcedownloader.*;
import org.gudy.azureus2.plugins.*;

public class 
PlatformManagerUpdateChecker
	implements Plugin
{
	private static final LogIDs LOGID = LogIDs.CORE;
	public static final String UPDATE_NAME	= "Platform-specific support";
	
	public static final int	RD_SIZE_RETRIES	= 3;
	public static final int	RD_SIZE_TIMEOUT	= 10000;
	
	protected PluginInterface			plugin_interface;
	
	public void
	initialize(
		PluginInterface	_plugin_interface)
	{
		plugin_interface	= _plugin_interface;
		
		plugin_interface.getPluginProperties().setProperty( "plugin.name", "Platform-Specific Support" );

		String	version = "1.0";	// default version if plugin not present
		
		PlatformManager platform	= PlatformManagerFactory.getPlatformManager();

		if ( platform.getPlatformType() == PlatformManager.PT_MACOSX ){

			if ( platform.hasCapability( PlatformManagerCapabilities.GetVersion )){
				
				try{
					version = platform.getVersion();
					
				}catch( Throwable e ){
					
					Debug.printStackTrace(e);
				}
			}
			
			
		}else{
			
			plugin_interface.getPluginProperties().setProperty( "plugin.version.info", "Not required for this platform" );
			
		}
		
		plugin_interface.getPluginProperties().setProperty( "plugin.version", version );
	}
	
	public String
	getName()
	{
		return( UPDATE_NAME );
	}
	
	public int
	getMaximumCheckTime()
	{
		return(( RD_SIZE_RETRIES * RD_SIZE_TIMEOUT )/1000);
	}
	

	protected List
	splitMultiLine(
		String		indent,
		String		text )
	{
		int		pos = 0;
		
		String	lc_text = text.toLowerCase();
		
		List	lines = new ArrayList();
		
		while( true ){
			
			String	line;
			
			int	p1 = lc_text.indexOf( "<br>", pos );
			
			if ( p1 == -1 ){
				
				line = text.substring(pos);
				
			}else{
				
				line = text.substring(pos,p1);
				
				pos = p1+4;
			}
			
			lines.add( indent + line );
			
			if ( p1 == -1 ){
				
				break;
			}
		}
		
		return( lines );
	}
}
