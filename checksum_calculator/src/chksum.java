import java.io.*;
import java.util.zip.Checksum;

public class chksum {
	public static void main(String[] args) {
		File startingDirectory= new File(args[0]);
		File outputFile = new File(args[1]);
		String version = args[2];
		//File startingDirectory= new File(fileName);
		try {
			getFileListing(startingDirectory, outputFile, version);
		} catch(Exception e) {
			e.printStackTrace();
		}

	}
  
  
  /**
  * Recursively walk a directory tree and return a List of all
  * Files found; the List is sorted using File.compareTo().
  *
  * @param aStartingDir is a valid directory, which can be read.
  */
	static public void getFileListing(File aStartingDir, File outputFile, String version) throws FileNotFoundException {
		validateDirectory(aStartingDir);
		BufferedWriter out=null;
		try {
			out = new BufferedWriter(new FileWriter(outputFile));
			out.write("<skdownloader>");
			out.newLine();
			out.write("<version>"+version+"</version>");
			out.newLine();
			out.write("</skdownloader>");
			out.newLine();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Directory is valid if it exists, does not represent a file, and can be read.
	 */
	static private void validateDirectory (File aDirectory) throws FileNotFoundException {
		if (aDirectory == null) {
			throw new IllegalArgumentException("Directory should not be null.");
		}
		if (!aDirectory.exists()) {
			throw new FileNotFoundException("Directory does not exist: " + aDirectory);
		}
		if (!aDirectory.isDirectory()) {
			throw new IllegalArgumentException("Is not a directory: " + aDirectory);
		}
		if (!aDirectory.canRead()) {
			throw new IllegalArgumentException("Directory cannot be read: " + aDirectory);
		}
	}

	public static long getChecksumValue(Checksum checksum, String fname) {
		try {
			BufferedInputStream is = new BufferedInputStream(new FileInputStream(fname));
			byte[] bytes = new byte[1024];
			int len = 0;

			while ((len = is.read(bytes)) >= 0) {
				checksum.update(bytes, 0, len);
			}
			is.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return checksum.getValue();
	}
}