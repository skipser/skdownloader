SKDownloader is an open source torrent client and download accelerator.
![Alt text](http://www.skdownloader.skipser.com/img/screenshot_home_full.png)

### What is this repository for? ###

* SKDownloader source code
* 6.0.0

### How do I get set up? ###

* clone the entire repository.
* Start eclipse
* Add all directories (except firefox_addon) as projects.
* Add jars in lib/ to classpath. For swt, there will be 4 jars, so add only one according to your java version.
* You should now be able to compile fine. To run the code -
* * Install SKDownloader in your computer
* * Get the installation folder path
* * For the SKDownloader project in eclipse, add "-instdir [folder]" as arguments.
* * Run as java project.

### Contribution guidelines ###

* For new features please contact me (arun@skipser.com) to reach an agreement.
* For bug fixes, please do them and send me pull requests.

### Who do I talk to? ###
=======
