import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class DownloadAdder {
	private static int listenPort = 6235;
	
	public static void main(String[] Args) {
		String instDir = Args[0];
		String url = Args[1];
		
		//System.out.println("Instdir:"+instDir+", url"+url);

		//Check if skdownloader is running and start it if not.
		if(! CheckSkdownloaderAlive()) {
			try {
				Runtime rt = Runtime.getRuntime();
				if(System.getProperty("os.name").toLowerCase().indexOf("win")>=0) {
					rt.exec(instDir+"/skdownloader.exe -instdir \""+instDir+"\"");
				}
				else{
					rt.exec(instDir+"/skdownloader");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//Wait for some time for the process to be up.
			for(int i=0; i<15; i++) {
				if(CheckSkdownloaderAlive()) {
					break;
				}
				
				//Sleep for 1 sec
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
				
				if(i==12) {
					//The process did not come up after 10 seconds. Exit.
					return;
				}
			}
		}
		
		try {
			Socket soc = new Socket("localhost", listenPort);
			PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
			out.println("skdownloader_addnew-"+url);
			out.close();
			soc.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static boolean CheckSkdownloaderAlive() {
		boolean alive=false;
		try {
			Socket soc = new Socket("localhost", listenPort);
			PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
			out.println("skdownloader_alive");
			BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
			String line = in.readLine();
			if(line.equals("is_alive")) {
				alive=true;
			}
			out.close();
			in.close();
			soc.close();
		}
		catch(Exception e){}
		return alive;
	}
}
