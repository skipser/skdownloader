var cm = require("sdk/context-menu");
var {Cc, Ci} = require("chrome");
const data = require("sdk/self").data;
//console.log("Got chrome ");

cm.Item({
	label: "Download with SKDownloader",
	context: cm.SelectorContext("a[href]"),
	image: data.url("skdownloader16.png"),
	insertbefore: "context-savelink",
	contentScript: 'self.on("click", function (node) {' +
                 '  self.postMessage(node.href)' +
                 '});',
	onMessage: function (url) {
		//console.log("Url- "+url);
		var osString = Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULRuntime).OS; 
		//console.log(osString);
		if (osString.toLowerCase().indexOf("win")!=-1) {
			var wrk = Cc["@mozilla.org/windows-registry-key;1"].createInstance(Ci.nsIWindowsRegKey);
			var instdir="";

			// If 64 bit windows, check both 64 bit as well as 32 bit registries.
			try {
				wrk.open(wrk.ROOT_KEY_LOCAL_MACHINE, "SOFTWARE\\skdownloader", wrk.ACCESS_READ);
				instdir = wrk.readStringValue("path");
			}
			catch (e) {
				wrk.open(wrk.ROOT_KEY_LOCAL_MACHINE, "SOFTWARE\\skdownloader", wrk.ACCESS_READ|wrk.WOW64_64);
				instdir = wrk.readStringValue("path");
			}
			
			wrk.close();
			//console.log("INstdir "+instdir);

			if (instdir=="") {
				return;
			}

			var file =Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
			file.initWithPath(instdir+"\\download_adder.exe");
			//console.log("Got path "+instdir+"\\download_adder.exe");

			// create an nsIProcess
			var process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess);
			process.init(file);
			//console.log("Starting skdownloader");
	
			var args = [instdir, url];
			process.run(false, args, args.length);
		} else {
			var instdir = "/usr/bin"

			var file = Ccs["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
			file.initWithPath(instdir+"/download_adder");

			// create an nsIProcess
			var process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess);
			process.init(file);

			var args = [url];
			process.run(false, args, args.length);
		}
	}
});
