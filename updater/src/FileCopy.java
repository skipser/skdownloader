/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    XML updater for prefs.xml
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/18/09 - prefs.xml should not be replaced. New tags should be 
 *                           copied over and version overritten.
 *
 ****************************************************************************************/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.dom.DOMSource; 
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

public class FileCopy 
{
	public void Copy(String fromFileName, String toFileName) {
		File fromFile = new File(fromFileName);
		File toFile = new File(toFileName);

		if (!fromFile.exists()){
			return;
		}
		if (!fromFile.isFile()){
			return;
		}
        if (!fromFile.canRead()){
        	return;
        }

        if (toFile.exists()){
        	if (!toFile.canWrite()){
        		return;
        	}
        }

        
        //First create directory for toFile
        File parentDir = new File(toFile.getParent());
        if(! parentDir.exists()){
        	if(toFile.getParent() != null)
        		new File(toFile.getParent()).mkdir();
        }

        FileInputStream from = null;
        FileOutputStream to = null;
        try {
        	from = new FileInputStream(fromFile);
        	to = new FileOutputStream(toFile);
        	byte[] buffer = new byte[4096];
        	int bytesRead;

        	while ((bytesRead = from.read(buffer)) != -1)
        		to.write(buffer, 0, bytesRead); // write
        }
        catch(Exception e){ 
        	e.printStackTrace();
        System.exit(0);}
        finally{
        	if (from != null){
        		try {
        			from.close();
        		} catch (IOException e) {}
        	}
        	if (to != null){
        		try {
        			to.close();
        		} catch (IOException e) {}
        	}
        }
	}
	
	public void Move(String fromFileName, String toFileName)
	{
		Copy(fromFileName, toFileName);
		File file = new File(fromFileName);
		file.delete();
	}
	
	public void XmlNewTagCopy(String fromFileName, String toFileName)
	{
		DocumentBuilderFactory dbf;
		Document toDoc = null;
		Document fromDoc = null;
		
		try
		{
			dbf= DocumentBuilderFactory.newInstance();
			dbf.setIgnoringElementContentWhitespace(false);
			DocumentBuilder dBuilder = dbf.newDocumentBuilder();
			fromDoc = dBuilder.parse(new File(fromFileName));
			toDoc = dBuilder.parse(new File(toFileName));
			//NormalizeIds();
		} catch (Exception e) {}
		
		NodeList nl = fromDoc.getDocumentElement().getChildNodes();
		for(int i=0; i< nl.getLength(); i++)
		{
			if(! CheckNodeExists(nl.item(i).getNodeName(), toDoc))
			{
				if(nl.item(i).getNodeType() != Node.TEXT_NODE) {
					Node dup = toDoc.importNode(nl.item(i), true);
					toDoc.getDocumentElement().appendChild(dup);
				}
			}
		}
		
		nl = toDoc.getDocumentElement().getChildNodes();
		for(int i=0; i< nl.getLength(); i++)
		{
			if(! CheckNodeExists(nl.item(i).getNodeName(), fromDoc)) {
				if(nl.item(i).getNodeType() != Node.TEXT_NODE)
					toDoc.getDocumentElement().removeChild(nl.item(i));
			}
		}
		
		//Copy over version as it is
		//SetValueForNamedNode("VERSION", GetValueForNamedNode("VERSION", fromDoc), toDoc);
		
		//Write the xml back
		try
		{
			// 	Prepare the DOM document for writing
			DOMSource source = new DOMSource(toDoc);
	    
			// Prepare the output file
			File file = new File(toFileName);
			StreamResult result = new StreamResult(file);
	    
			// Write the DOM document to the file
			Transformer xformer = TransformerFactory.newInstance().newTransformer();
			xformer.setOutputProperty("indent", "yes");
			xformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
		} catch (TransformerException e) {
		}
	}
	
	private static boolean CheckNodeExists(String nodeName, Document doc)
	{
		if((doc.getElementsByTagName(nodeName)).getLength() > 0)
		{
			return true;
		}
		return false;
	}
	
	/*private static String GetValueForNamedNode(String nodeName, Document doc)
		String val="";
		
		if((doc.getElementsByTagName(nodeName)).getLength() == 0)
			return "";
		
		if((doc.getElementsByTagName(nodeName)).item(0).getChildNodes().getLength() > 0){
			val = (doc.getElementsByTagName(nodeName)).item(0).getFirstChild().getNodeValue();
		}
		
		while((! val.equals("")) &&
			  (doc.getElementsByTagName(val).getLength() >0))
		{
			val = doc.getElementsByTagName(val).item(0).getFirstChild().getNodeValue();
		}
		return val;
	}
	
	private static void SetValueForNamedNode(String nodeName, String value, Document doc)
	{
		String val="";
		if((doc.getElementsByTagName(nodeName)).item(0).getChildNodes().getLength() > 0)
		{
			val = (doc.getElementsByTagName(nodeName)).item(0).getFirstChild().getNodeValue();
		}

		if((! val.equals("")) &&
		   (doc.getElementsByTagName(val).getLength() >0))
		{
			//This node referes to some other node. Do not edit it.
		}
		else
		{
			if(! val.equals(""))
			{
				(doc.getElementsByTagName(nodeName)).item(0).getFirstChild().setNodeValue(value);
			}
			else
			{
				(doc.getElementsByTagName(nodeName)).item(0).appendChild(doc.createTextNode(value));
			}
		}
	}*/
}

