/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    XML updater for prefs.xml
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/18/09 - prefs.xml should not be replaced. New tags should be 
 *                           copied over and version overritten.
 *
 ****************************************************************************************/

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilderFactory;

public class Updater {
	
	public static void main(String[] args) {
		String argname="";
		int i=0;
		String instDir="";
		String dataDir="";
		String exec="";

		while (i < args.length){
			String s = args[i++];
			if(argname.equals("inst")){
				instDir = s;
				instDir=instDir.replaceAll("-skdownloaderspace-", " ");
				argname="";
			}
			else if(argname.equals("ex")){
				exec = s;
				exec=exec.replaceAll("-skdownloaderspace-", " ");
				argname="";
			}
			else if(argname.equals("data")){
				dataDir = s;
				dataDir=dataDir.replaceAll("-skdownloaderspace-", " ");
				argname="";
			}
			else if(s.equals("-instdir")){
            	argname="inst";
            }
			else if(s.equals("-exec")){
            	argname="ex";
            }
			else if(s.equals("-datadir")){
            	argname="data";
            }
		}
		
		//JOptionPane.showMessageDialog(null, "From main - "+instDir+"\n"+updatesDir+"\n"+dataDir);
		
		if(instDir.equals("") || dataDir.equals("")){
			//JOptionPane.showMessageDialog(null, "Exiting ad dires null");
			System.exit(0);
		}
		/*JFrame frame = new JFrame("Frame in Java Swing");
		frame.setSize(300, 100);
		JLabel label = new JLabel("Updating SKDownloader...");
		frame.setLayout(new FlowLayout(FlowLayout.CENTER));
		frame.add(label);
		Dimension dim = frame.getToolkit().getScreenSize();
		Rectangle abounds = frame.getBounds();
		frame.setLocation((dim.width - abounds.width) / 2,
	    (dim.height - abounds.height) / 2);
		frame.setVisible(true);
		frame.requestFocus();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);*/
		
		//java.awt.EventQueue.invokeLater(new Runnable() {public void run() {
			DoUpdate(instDir, dataDir, exec);
		//}});
	}
	
	private static void DoUpdate(String instDir, String dataDir, String exec) {
		
		//Check if an update is available.
		FileCopy fileCopy = new FileCopy();
		String updatesDir=dataDir+File.separator+"updates";
		String startDir = updatesDir+File.separator+"skdownloader";
		//JOptionPane.showMessageDialog(null, "actually starting - "+instDir+",\n "+exec+",\n "+startDir+",\n "+dataDir);		
		int iter=0;
		//int i=0;

		try {
		//Do some sanity testing..
		if(! (new File(updatesDir)).exists()) {
			CleanAndExec(updatesDir, exec, instDir);
		}
		if(! (new File(startDir)).exists()) {
			CleanAndExec(updatesDir, exec, instDir);
		}
		
		try {
			//Thread.sleep(10000);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//Wait till downloaded has exited.
			File tStamp = new File(dataDir+File.separator+"skdownloader.running");
			while(iter <= 60) {
				if(tStamp.exists()) {
					try {
					Thread.sleep(500);
					} catch (Exception e){}
				}
				else {
					break;
				}
				iter++;
				//If downloader didn't finish after 30 secs, exit.
				if(iter==30)
					System.exit(0);
			}
			
			//JOptionPane.showMessageDialog(null, "Starting update");
			//We were creating .running file after frame creation and not at start. This means
			//.running file won't be available when updater is started. If downloader has not exited
			//by now, our update not happen. So just sleep for say two seconds.
			Thread.sleep(2000);
			
			//Update is available. Copy over files.
			List<String> result = getFileListingNoSort(new File(startDir), "");
			for(int i=0; i<result.size(); i++) {
				//System.out.println(result.get(i));
				String fromFile="";
				String toFile="";
				try{
				fromFile = startDir+File.separator+result.get(i); //.getPath(); //.getCanonicalPath();
				toFile = instDir+File.separator+result.get(i); //.getPath().substring(startDir.length()+1);
				} catch (Exception e) {
					e.printStackTrace();
				}
				//System.out.println(fromFile+", "+toFile);
				
				if(fromFile.matches(".*runtimeoptions.xml")) {
					// Copy over any additional tag introduced and version.
					// We need to keep user options intact.
					fileCopy.Copy(fromFile, toFile);
					fileCopy.XmlNewTagCopy(fromFile, dataDir+File.separator+"prefs"+File.separator+"runtimeoptions.xml");
				}
				else if(fromFile.matches(".*history.xml")) {
					// Leave this file. We don't want to clear user history.
				}
				else {
					fileCopy.Copy(fromFile, toFile);
				}
			}
			
			//Check if remove list is available
			File removeList = new File(updatesDir+File.separator+"remove.xml");
			if(removeList.exists()) {
				try {
					DocumentBuilderFactory dbf= DocumentBuilderFactory.newInstance();
					dbf.setIgnoringElementContentWhitespace(false);
					Document doc = dbf.newDocumentBuilder().parse(removeList);
					NodeList nl = doc.getElementsByTagName("file");
					String removeName;
					for(int i=0; i< nl.getLength(); i++) {
						removeName =instDir+File.separator+((Element)nl.item(i)).getAttribute("name");
						File file = new File(removeName);
						file.delete();
					}
				} catch (Exception e){}
			}
			
			//Copy over latest.xml.
			fileCopy.Copy(updatesDir+File.separator+"latest.xml", instDir+File.separator+"latest.xml");
			
			//Remove everything in updates dir, create update dir and leave.
			CleanAndExec(updatesDir, exec, instDir);
		}
		//This is a last resort hanger for us. If the updater didn't work for any chance, just cleanup and start 
		//downloader. Else, there is possibility of a hang and skdownloader never getting started.
		catch(Exception e) {
			CleanAndExec(updatesDir, exec, instDir);
		}
	}
	
	private static void CleanAndExec(String updatesDir, String exec, String instDir) {
		deleteDirectory(updatesDir);
		(new File(updatesDir)).mkdirs();
		try {
			ProcessBuilder pb = new ProcessBuilder(exec, "-instdir", instDir);
			pb.start();
		} catch (Exception e) {/*JOptionPane.showMessageDialog(null, e.toString());*/}
		System.exit(0);
	}
	
	
	static private List<String> getFileListingNoSort(File aStartingDir, String currDir) {
		//List<File> result = new ArrayList<File>();
		List<String> result = new ArrayList<String>();
		File[] filesAndDirs = aStartingDir.listFiles();
		List<File> filesDirs = Arrays.asList(filesAndDirs);
		for(File file : filesDirs) {
			if ( ! file.isFile() ) {
				//must be a directory
		        //recursive call!
				//List<File> deeperList=null;
				List<String> deeperList=null;
				try {
					if(currDir.equals(""))
						deeperList = getFileListingNoSort(file, file.getName());
					else
						deeperList = getFileListingNoSort(file, currDir+File.separator+file.getName());
				} catch (Exception e) {
					//e.printStackTrace();
				}
		        result.addAll(deeperList);
			}
			else {
				String fileName="";
				if(currDir.equals(""))
					fileName=file.getName();
				else
					fileName=currDir+File.separator+file.getName();
				result.add(fileName); //always add, even if directory
			}
		}
		return result;
	}
	
	static public boolean deleteDirectory(String dir) {
		File path = new File(dir);
		if( path.exists() ) {
			File[] files = path.listFiles();
			for(int i=0; i<files.length; i++) {
				if(files[i].isDirectory()) {
					try {
						deleteDirectory(files[i].getCanonicalPath());
					} catch (Exception e)
					{}
				}
				else {
					files[i].delete();
				}
			}
		}
		return( path.delete() );
	}
}
