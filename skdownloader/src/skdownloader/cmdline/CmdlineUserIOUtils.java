/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    File writer that writes downloaded bits to file without 
 *    loss and mixing.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.cmdline;

import java.io.BufferedReader;
import java.io.InputStreamReader;



import org.apache.log4j.Logger;

import skdownloader.core.*;

public class CmdlineUserIOUtils implements UserIOUtils
{
	private Logger logger = Logger.getLogger(CmdlineUserIOUtils.class);
	public void PrintError(String msg)
	{
		System.out.println(msg);
	}
	
	public void PrintBlockingError(String msg)
	{
		PrintError(msg);
	}
	
	public void PrintMessage(String msg)
	{
		System.out.println(msg);
	}
	
	public void PrintBlockingMessage(String msg)
	{
		PrintMessage(msg);
	}
	
	public boolean GetFromYesNoMessage(Object c, String str)
	{
		System.out.print(str+"[y/n]: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String option = "n";
		try 
		{
			option = br.readLine();
		} catch (Exception e) 
		{
			logger.error("Exception", e);
			option="n";
		}
		if((option.equals("y")) ||
		   (option.equals("yes")) ||
		   (option.equals("Y")) ||
		   (option.equals("YES")))
			return true;
		else
			return false;
	}
	
	public boolean GetFromBlockingYesNoMessage(Object c, String str)
	{
		return(GetFromYesNoMessage(c,str));
	}
	
	public boolean GetFromOkCancelMessage(Object c, String str)
	{
		return true;
	}
	
	public boolean GetFromBlockingOkCancelMessage(Object c, String str)
	{
		return (GetFromOkCancelMessage(c, str));
	}
	
	public String GetFileFromUser(Object c)
	{
		return "";
	}
}
