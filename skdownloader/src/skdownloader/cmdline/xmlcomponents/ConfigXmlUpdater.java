/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    XML updater for config.xml
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/18/09 - Support for DEBUGMODE
 *   arun         05/19/09 - Support for theme
 *
 ****************************************************************************************/

package skdownloader.cmdline.xmlcomponents;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

public class ConfigXmlUpdater
{
	//private SystemInfoHolder sysInfoHolder;
	private String instDir;
	
	DocumentBuilderFactory dbf;
	Document doc;
	int maxId;

	private static Logger logger = Logger.getLogger(ConfigXmlUpdater.class);
	
	public ConfigXmlUpdater(String instDir)
	{
		//this.sysInfoHolder = sysInfoHolder;
		this.instDir=instDir;
		maxId=0;
		try
		{
			dbf= DocumentBuilderFactory.newInstance();
			dbf.setIgnoringElementContentWhitespace(false);
			DocumentBuilder dBuilder = dbf.newDocumentBuilder();
			doc = dBuilder.parse(new File(this.instDir+File.separator+"prefs"+File.separator+"config_cmdline.xml"));
			//NormalizeIds();
		} catch (Exception e) {	
			logger.error("Exception:", e);
		}
	}
	
	public String GetValueForNamedNode(String nodeName)
	{
		String val="";
		if((doc.getElementsByTagName(nodeName)).item(0).getChildNodes().getLength() > 0)
		{
			val = (doc.getElementsByTagName(nodeName)).item(0).getFirstChild().getNodeValue();
		}
		
		while((! val.equals("")) &&
			  (doc.getElementsByTagName(val).getLength() >0))
		{
			val = doc.getElementsByTagName(val).item(0).getFirstChild().getNodeValue();
		}
		return val;
	}
	
	/*private void SetValueForNamedNode(String nodeName, String value)
	{
		String val="";
		if((doc.getElementsByTagName(nodeName)).item(0).getChildNodes().getLength() > 0)
		{
			val = (doc.getElementsByTagName(nodeName)).item(0).getFirstChild().getNodeValue();
		}

		if((! val.equals("")) &&
		   (doc.getElementsByTagName(val).getLength() >0))
		{
			//This node referes to some other node. Do not edit it.
		}
		else
		{
			if(! val.equals(""))
			{
				(doc.getElementsByTagName(nodeName)).item(0).getFirstChild().setNodeValue(value);
			}
			else
			{
				(doc.getElementsByTagName(nodeName)).item(0).appendChild(doc.createTextNode(value));
			}
		}
	}*/
	
	public void UpdateSystemVars()
	{
	}
	
	public void WriteXml()
	{
	}
}
