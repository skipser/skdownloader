/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    File writer that writes downloaded bits to file without 
 *    loss and mixing.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.cmdline;

import java.util.*;

import org.apache.log4j.Logger;

import skdownloader.core.*;
import skdownloader.core.urlcore.FileDownloader;
import skdownloader.core.urlcore.UrlDownloader;
import skdownloader.cmdline.xmlcomponents.ConfigXmlUpdater;

public class Download {
	private static Logger logger = Logger.getLogger(Download.class);
	public static void main (String[] args) {
		
		String argname="";
		String urlname="";
		String filename="";
		String dirname="";
		String instdir="";
		String version="";
		String copyright="";
		int help      =0;
		int numConnections = 4;
		DownloadStateHolder stateHolder;
		ConfigXmlUpdater configUpdater;
		
		CmdlineUserIOUtils ioutils = new CmdlineUserIOUtils();
		
		//System.out.println("Starting");
		int i=0;
		
		while (i < args.length) {
			String s = args[i++];
			if(argname.equals("url")) {
				urlname = s;
				argname="";
			}
			else if(argname.equals("file")) {
				filename = s;
				argname="";
			}
			else if(argname.equals("dir")) {
				dirname = s;
				argname="";
			}
			else if(argname.equals("instdir")) {
				instdir = s;
				argname="";
			}
			else if(argname.equals("conn")) {
				numConnections = Integer.parseInt(s);
				argname="";
			}
			else if((s.equals("-u")) ||
					(s.equals("-url"))) {
            	argname="url";
            }
			else if((s.equals("-f")) ||
					(s.equals("-file"))) {
            	argname="file";
            }
			else if((s.equals("-d")) ||
					(s.equals("-dir"))) {
            	argname="dir";
            }
			else if((s.equals("-i")) ||
					(s.equals("-instdir"))) {
            	argname="instdir";
            }
			else if((s.equals("-c")) ||
					(s.equals("-conn"))) {
            	argname="conn";
            }
			else if((s.equals("-h")) ||
					(s.equals("-help"))) {
				help=1;
            }
		}
		
		configUpdater = new ConfigXmlUpdater(instdir);
		version=configUpdater.GetValueForNamedNode("VERSION");
		copyright=configUpdater.GetValueForNamedNode("COPYRIGHT");
		
		logger.info("SKDownloader commandline version "+version);
		logger.info(copyright);
		
		if(help == 1) {
			ShowHelp(logger);
			System.exit(0);
		}
		
		if(urlname.length() != 0) {
			logger.info("Url: "+urlname);
			logger.info("Downloading to: "+dirname);
			logger.info("Simultaneous connections: "+numConnections);

			Date date1=new Date();
			UrlDownloader downloader = new UrlDownloader(urlname, dirname, numConnections, 200000, null, ioutils);
			stateHolder = downloader.GetStateHolder();
			CmdlineUIUpdater UIUpdater = new CmdlineUIUpdater();
			stateHolder.addObserver(UIUpdater);
			if(downloader.Download() == Common.OK) {
				Date date2=new Date();
				long mins=(date2.getTime()-date1.getTime())/1000;
				logger.info("Downloaded file \""+urlname+"\" successfully.");
				logger.info("Time taken: "+mins+" Seconds");
				logger.info("Avg Speed: "+stateHolder.GetDownloadSpeedString());
			}
			else {
				logger.error("Could not download file "+urlname);
			}
		}
		else {
			logger.error("Invalid url specified- "+Common.ShortenString(urlname, "middle",50 ));
		}
		
		if(filename.length() != 0) {
			FileDownloader downloader = new FileDownloader(ioutils);
			if(downloader.Download(filename, dirname) == Common.OK) {
				logger.info("Downloaded file \""+filename+"\" successfully.");
			}
			else {
				logger.error("Could not download file "+filename);
			}
		}
	}
	
	public static void ShowHelp(Logger logger) {
		logger.info("USAGE: SKDownloader <options>");
		logger.info("Options:");
		logger.info("   -url  - Url name to download");
		logger.info("   -dir  - Directory to download the file");
		logger.info("   -conn - Number of simultaneous connections(4 by default)");
		logger.info("   -help - Shows this help");
		logger.info(" ");
	}
}

