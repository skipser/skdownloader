/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    File writer that writes downloaded bits to file without 
 *    loss and mixing.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.cmdline;

import java.util.*;
import skdownloader.core.*;

public class CmdlineUIUpdater implements Observer
{
	//This will get called when an update is available from Urldownloader
	public void update(Observable obs, Object obj)
	{
		DownloadStateHolder stateHolder = (DownloadStateHolder)obs;
		String updateString = (String)obj;
		
		if(updateString.equals("DownloadedSize"))
		{
			UpdatePercentDone(stateHolder.GetFileSize(), stateHolder.GetDownloadedSize());
		}
		else if(updateString.equals("FileSize"))
		{
			System.out.println("Size: "+stateHolder.GetFileSize());
			System.out.println(" ");
		}
	}
	
	void UpdatePercentDone(long total, long done)
	{
		int percent = (int)(done*100/total);
		int numLines = (int)percent/2;
		String percentStr;
		if(percent < 100){
			percentStr=percent+"% ";
		}
		else{
			percentStr=percent+"%";
		}
		
		System.out.print("Progress : |");
		for(int i=1; i<=50; i++)
		{
			if(i<=numLines)
			{
				System.out.print("=");
			}
			else
			{
				System.out.print(" ");
			}
		}
		System.out.print("| "+percentStr);
		if(percent==100)
		{
			System.out.println();
		}
		else
		{
			System.out.print("\r");
		}
	}
	
}