package skdownloader.downloadmanager;

import skdownloader.core.Common;
import skdownloader.core.DownloadStateHolder;
import skdownloader.core.torrentcore.TorrentDownloadProgressAdminThread;
import skdownloader.core.torrentcore.TorrentDownloader;

import com.aelitis.azureus.core.AzureusCore;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import skdownloader.downloadmanager.GuiUserIOUtils;
import skdownloader.downloadmanager.MainFrame;
import skdownloader.downloadmanager.SystemInfoHolder;

public class GuiTorrentDownloaderWrapper extends TorrentDownloader implements GuiDownloaderWrapperInterface{
	private ArrayList<ImageIcon> statusAnimList = new ArrayList<ImageIcon>();
	private MainFrame mainFrame;
	SystemInfoHolder sysInfoHolder;
	private GuiUserIOUtils ioutils;
	private int uid;
	private boolean detailsDisplayed; // Denotes whether details are currently displayed or not.
	TorrentDetailsFrame torrentDetailsFrame;
	private static Logger logger = Logger.getLogger(GuiTorrentDownloaderWrapper.class);
	
	public GuiTorrentDownloaderWrapper(MainFrame mainFrame) {
		this.mainFrame=mainFrame;
	}

	//This is for new download
	public GuiTorrentDownloaderWrapper(MainFrame mainFrame, 
            String torrentname, 
            String dirname,
            int uid,
            SystemInfoHolder sysInfoHolder,
            GuiUserIOUtils ioutils,
            AzureusCore core,
            TorrentDownloadProgressAdminThread adminThread,
            boolean needDownload) {
		super(torrentname, dirname, ioutils, core, adminThread, sysInfoHolder, needDownload, true);
		this.mainFrame=mainFrame;
		this.GetStateHolder().SetParentComponent(mainFrame.GetFrame());
		this.GetStateHolder().SetManagedTorrent(needDownload);
		this.uid=uid;
		this.sysInfoHolder = sysInfoHolder;
		this.ioutils=ioutils;
		SetDetailsDisplayStatus(false);
		AddDownloadListener(mainFrame);
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/downloading.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/downloading.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif"))); //pausedforerror
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif"))); //pausedforexit
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/completed.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/cancelled.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/error.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/uploading.gif")));
	}
	

	//This is for history downloads
	public GuiTorrentDownloaderWrapper(MainFrame mainFrame, 
								   String urlname, 
								   String dirname, 
								   String filename, 
								   long size,
								   float speed,
								   long startTime,
								   long endTime,
								   long pauseStartTime,
								   long pauseEndTime,
								   int pausedTime,
								   int state,
								   String errorMessage,
								   int progress, 
								   long downloaded,
								   long uploaded,
								   String bsString,
								   String excludeListString,
								   int uid,
								   SystemInfoHolder sysInfoHolder,
								   GuiUserIOUtils ioutils, 
								   AzureusCore core,
								   TorrentDownloadProgressAdminThread adminThread,
								   boolean needDownload,
								   boolean dummy)

	{
		super(urlname, dirname, ioutils, core, adminThread, sysInfoHolder, false, false);
		this.mainFrame=mainFrame;
		this.sysInfoHolder = sysInfoHolder;
		this.ioutils=ioutils;
		this.GetStateHolder().SetFileName(filename);
		this.GetStateHolder().SetFileSize(size);
		this.GetStateHolder().SetDownloadSpeed(0);
		this.GetStateHolder().SetStartTime(startTime);
		this.GetStateHolder().SetEndTime(endTime);
		this.GetStateHolder().SetPauseStartTime(pauseStartTime);
		this.GetStateHolder().SetPauseEndTime(pauseEndTime);
		this.GetStateHolder().SetPausedTime(pausedTime);
		this.uid=uid;
		//Set status. Since this is a histoy download, it should not have an active state.
		this.GetStateHolder().SetDownloadStatus(state);
		if((GetStateHolder().GetStatus() == DownloadStateHolder.DOWNLOADING) ||
			(GetStateHolder().GetStatus() == DownloadStateHolder.STARTING)) {
			GetStateHolder().SetDownloadStatus(DownloadStateHolder.ERROR);
		}
		this.GetStateHolder().SetErrorMessage(errorMessage);
		this.GetStateHolder().setTorrentDownloadPercent(progress);
		this.GetStateHolder().SetTorrentDownloadedSize(downloaded);
		this.GetStateHolder().SetUploadedSize(uploaded);
		this.GetStateHolder().SetManagedTorrent(needDownload);
		this.GetStateHolder().SetElapsedTime();
		
		this.GetStateHolder().SetNumberOfPieces(Common.GetBitsetSize(bsString, logger));
		this.GetStateHolder().SetTorrentDownloadedBitsFromBitset(Common.String2BitSet(bsString, logger));
		this.GetStateHolder().SetExcludedList(Common.String2BitSet(excludeListString, logger), Common.GetBitsetSize(excludeListString, logger));
		SetDetailsDisplayStatus(false);
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/downloading.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/downloading.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif"))); 
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif"))); //pausedforerror
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif"))); //pausedforexit
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/completed.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/cancelled.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/error.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/uploading.gif")));
	}
	
	public void Start() {
		if(sysInfoHolder.SHOWDETAILSONADDINGDOWNLOAD)
			ShowDetails();
		start();
	}
	
	public void StartSilently() {
		start();
	}
	
	public ImageIcon GetStatusAnimImage(int index) {
		return (ImageIcon)statusAnimList.get(index);
	}
	
	public ImageIcon GetNewStatusAnimImage(int index) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("guicomponents/images/downloading.gif");
		list.add("guicomponents/images/downloading.gif");
		list.add("guicomponents/images/paused.gif");
		list.add("guicomponents/images/downloading.gif"); //pausedforerror
		list.add("guicomponents/images/downloading.gif"); //pausedforexit
		list.add("guicomponents/images/completed.png");
		list.add("guicomponents/images/cancelled.png");
		list.add("guicomponents/images/error.png");
		list.add("guicomponents/images/uploading.gif");
		
		return new ImageIcon(this.getClass().getResource(list.get(index)));
	}
	
	public boolean GetDetailsDisplayStatus() {
		return detailsDisplayed;
	}
	
	public void SetDetailsDisplayStatus(boolean status) {
		detailsDisplayed=status;
	}
	
	public void ShowDetails() {
		if(! this.GetDetailsDisplayStatus()) {
			try  {
				final DownloadStateHolder stateHolder1 = this.GetStateHolder();
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					torrentDetailsFrame = new TorrentDetailsFrame(mainFrame, stateHolder1, sysInfoHolder, ioutils);
					torrentDetailsFrame.ShowFrame();
				}});
	        }
			catch (Exception e) {
				
			}
		}
		else {
			torrentDetailsFrame.ToFront();
		}
		SetDetailsDisplayStatus(true);
	}
	
	public void HideDetails() {
		if(this.GetDetailsDisplayStatus()) {
			this.GetStateHolder().deleteObserver(torrentDetailsFrame);
			torrentDetailsFrame.DisposeFrame();
			torrentDetailsFrame=null;
		}
		SetDetailsDisplayStatus(false);
	}
	
	public int GetUid() {
		return uid;
	}

}