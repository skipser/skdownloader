/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Frame showing options.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.SystemTray;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;
//import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import javax.swing.text.Document;

import org.gudy.azureus2.core3.config.COConfigurationManager;

import skdownloader.core.UserIOUtils;
import skdownloader.downloadmanager.guicomponents.DownloaderTheme;
import skdownloader.downloadmanager.guicomponents.DownloadsTableColumnManager;
import skdownloader.downloadmanager.guicomponents.basiccomponents.FloatTextField;
import skdownloader.downloadmanager.guicomponents.basiccomponents.NumericTextField;

class OptionsFrame implements TreeSelectionListener, ActionListener, DocumentListener, TextListener
{
	private JFrame motherFrame;
	private JSplitPane optionsSplitPane;
	private JPanel treePane, optionsPane, generalPane, savetoPane, columnsPane, internetPane, proxyPane, 
	 			   connectionsPane,motherPane, downloadsPane, acceleratePane, accelerateBorderPane,
	 			   themesPane, themesBorderPane, bottomPane, updatesPane, updatesBorderPane, torrentsPane, 
	 			   torrentspeedPane, torrentmanagementPane, resetPane;
	private JLabel lbl;
	private JCheckBox chkBox, closeToTrayChkBox;
	private JRadioButton radioButt;
	private JButton applyButt;
	//private JTextField httpProxyServer, httpProxyPort, socksProxySever, socksProxyPort;
	JTextField textField;
	private JTree optionsTree;
	//private ImageIcon expandedNodeImg, collapsedNodeImg;
	private DefaultMutableTreeNode optionsNode, generalNode, savetoNode, columnsNode, themesNode, internetNode, proxyNode,
	                               connectionsNode, downloadsNode, accelerateNode, updatesNode, torrentsNode,
	                               torrentspeedNode, torrentManagementNode, resetNode;
	
	private JTabbedPane proxyTabbedPane;
	JTextField httpProxyServerTextField,  httpProxyAuthenNameTextField, defaultDownloadLocationTextField;
	TextField httpProxyPortTextField;
	NumericTextField maxTorrentDownloadSpeedField, maxTorrentUploadSpeedField, timeoutDelayTextField, retryLimitTextField, 
    accelerationMinSizeTextField, torrentListenPortField, torrentRemoveSeedingTimeField,maxTorrentUploadSpeedOnSeedingField; 
	FloatTextField torrentRemoveShareRatioField;
	JPasswordField httpProxyAuthenPasswordTextField;

	private Document httpProxyServerDocument, httpProxyNameDocument, 
	                 httpProxyPasswordDocument;
	
	private String showfilenamecolumn = "showfilenamecolumn";
	private String showsizecolumn = "showsizecolumn";
	private String showetacolumn = "showetacolumn";
	private String showprogresscolumn = "showprogresscolumn";
	private String showdownspeedcolumn = "showdownspeedcolumn";
	private String showstatuscolumn = "showstatuscolumn";
	private String showupspeedcolumn = "showupspeedcolumn";
	private String showpeerscolumn = "showpeerscolumn";
	private String showseedscolumn = "showseedscolumn";
	private String showshareratiocolumn = "showshareratiocolumn";
	private String showdownloadedcolumn = "showdownloadedcolumn";
	private String showtrackerstatuscolumn = "showtrackerstatuscolumn";
	private String showpopupwhendownloadsfinish = "showpopupwhendownloadsfinish";
	private String showdeatilsonaddingdownload = "showdeatilsonaddingdownload";
	private String trayenabled = "trayenabled";
	private String closetotray = "closetotray";
	private String savetodefaultdownloadfolder = "savetodefaultdownloadfolder";
	private String savetopreviousdownloadfolder = "savetopreviousdownloadfolder";
	private String askdownloadfoldereachtime = "askdownloadfoldereachtime";
	private String alwaysconnected = "alwaysconnected";
	private String useproxy = "useproxy";
	private String httpproxyservernottousecache = "httphttpproxyservernottousecache";
	private String httpuselogincredentials = "httpuselogincredentials";
	private String httpuseserversentdelaytime = "httpuseserversentdelaytime";
	private String startdownloadingimmdediately = "startdownloadingimmdediately";
	private String addtolistbutdonotstart = "addtolistbutdonotstart";
	private String askwhethertostartimmediately = "askwhethertostartimmediately";
	private String downloadtorrentandstarttorrent="downloadtorrentandstarttorrent";
	private String downloadtorrentasnormalfile="downloadtorrentasnormalfile";
	private String removetorrentonshareratio = "removetorrentonshareratio";
	private String removetorrentonseedingtime = "removetorrentonseedingtime";
	private String enableuploadspeedonseeding="enableuploadspeedonseeding";
	private String autoaccelerate = "autoaccelerate";
	private String autoupdate = "autoupdate";
	private String reset = "reset";
	private String shutdownoption = "shutdownoption";
	private String updatestartup = "updatestartup";
	private String updatedaily = "updatedaily";
	private String updateweekly = "updateweekly";
	private String ok="ok";
	private String cancel = "cancel";
	private String apply = "apply";
	
	public static String[] shutdownOptions= {"Do Nothing", "Shutdown SKDownloader", "Shutdown Computer"};
	
	private MainFrame parentFrame;
	private UserIOUtils ioutils;
	private DownloadsTableColumnManager colManager;
	
	SystemInfoHolder sysInfoHolder, orig_sysInfoHolder;
	
	public OptionsFrame(MainFrame parentFrame, SystemInfoHolder sysInfoHolder, DownloadsTableColumnManager colManager, UserIOUtils ioutils)
	{
		this.parentFrame=parentFrame;
		this.orig_sysInfoHolder = sysInfoHolder;
		this.colManager=colManager;
		this.sysInfoHolder = (SystemInfoHolder) sysInfoHolder.clone();
		this.ioutils=ioutils;
	}
	
	public void Show() {
		motherFrame = new JFrame("Options");
		motherFrame.setIconImage(new ImageIcon(this.getClass().getResource("guicomponents/images/skdownloader32.png")).getImage());
		motherPane = new JPanel(new BorderLayout());
		//motherPane.setBorder(BorderFactory.createLineBorder(Color.green));

		optionsSplitPane = new JSplitPane();
		optionsSplitPane.setDividerSize(2);
		optionsSplitPane.setDividerLocation(180);
		optionsSplitPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		//optionsSplitPane.setBorder(BorderFactory.createLineBorder(Color.red));
		
		treePane = new JPanel(new BorderLayout());
		
		optionsNode = new DefaultMutableTreeNode("Options");
		
		generalNode = new DefaultMutableTreeNode("General");
		optionsNode.add(generalNode);
		savetoNode = new DefaultMutableTreeNode("Save to");
		generalNode.add(savetoNode);
		themesNode = new DefaultMutableTreeNode("Themes");
		generalNode.add(themesNode);
		updatesNode = new DefaultMutableTreeNode("Updates");
		generalNode.add(updatesNode);
		columnsNode = new DefaultMutableTreeNode("Columns");
		generalNode.add(columnsNode);
		
		internetNode = new DefaultMutableTreeNode("Internet");
		optionsNode.add(internetNode);
		proxyNode = new DefaultMutableTreeNode("Proxy");
		internetNode.add(proxyNode);
		connectionsNode = new DefaultMutableTreeNode("Connections");
		internetNode.add(connectionsNode);
		//speedlimitNode = new DefaultMutableTreeNode("Speed limit");
		//internetNode.add(speedlimitNode);
		
		downloadsNode = new DefaultMutableTreeNode("Downloads");
		optionsNode.add(downloadsNode);
		accelerateNode = new DefaultMutableTreeNode("Accelerate");
		downloadsNode.add(accelerateNode);
		
		torrentsNode = new DefaultMutableTreeNode("Torrents");
		optionsNode.add(torrentsNode);
		torrentspeedNode = new DefaultMutableTreeNode("Torrent Speed");
		torrentsNode.add(torrentspeedNode);
		torrentManagementNode = new DefaultMutableTreeNode("Management");
		torrentsNode.add(torrentManagementNode);
		
		resetNode = new DefaultMutableTreeNode("Reset");
		optionsNode.add(resetNode);

		optionsTree = new JTree(optionsNode);
		optionsTree.setShowsRootHandles(true);
		optionsTree.setRootVisible(false);
		DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
	    renderer.setLeafIcon(null);
	    renderer.setOpenIcon(null);
	    renderer.setClosedIcon(null);
	    if(DownloaderTheme.GetTheme(sysInfoHolder.GetTheme()).contains("Substance")){
	    	//Dont set cell renderer
	    }
	    else {
	    	optionsTree.setCellRenderer(renderer);
	    }
	    
	    optionsTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

	    //Listen for when the selection changes.
	    optionsTree.addTreeSelectionListener(this);
	    
		JScrollPane treeView = new JScrollPane(optionsTree);
		treePane.add(treeView);

		optionsSplitPane.setLeftComponent(treePane);
		
		optionsPane = new JPanel(new CardLayout());

		//General pane on the cardlayout
		generalPane = new JPanel();
		SpringLayout layout = new SpringLayout();
		generalPane.setLayout(layout);
		JTextField heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("General");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		generalPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, generalPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, generalPane);
		layout.putConstraint(SpringLayout.EAST, generalPane, 5, SpringLayout.EAST, heading);
		JPanel generalBorderPane = new JPanel();
		generalBorderPane.setLayout(new BoxLayout(generalBorderPane, BoxLayout.PAGE_AXIS));
		generalBorderPane.setBorder(new TitledBorder("General"));
		generalBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		chkBox = new JCheckBox("Show popup when downloads finish");
		chkBox.setActionCommand(showpopupwhendownloadsfinish);
		chkBox.addActionListener(this);
		chkBox.setSelected(sysInfoHolder.SHOWPOPPUPWHENDOWNLOADFINISH);
		generalBorderPane.add(chkBox);
		generalPane.add(Box.createHorizontalGlue());
		generalBorderPane.add(chkBox);
		
		chkBox = new JCheckBox("Show details pane on adding new download.");
		chkBox.setActionCommand(showdeatilsonaddingdownload);
		chkBox.addActionListener(this);
		chkBox.setSelected(sysInfoHolder.SHOWDETAILSONADDINGDOWNLOAD);
		generalBorderPane.add(chkBox);
		generalPane.add(Box.createHorizontalGlue());
		generalBorderPane.add(chkBox);
		
		if(SystemTray.isSupported()) {
			chkBox = new JCheckBox("Enable SKDownloader in system tray [needs restart]");
			chkBox.setActionCommand(trayenabled);
			chkBox.addActionListener(this);
			chkBox.setSelected(sysInfoHolder.TRAYENABLED);
			generalBorderPane.add(chkBox);
			generalBorderPane.add(Box.createHorizontalGlue());
			closeToTrayChkBox = new JCheckBox("Close minimises to system tray.");
			closeToTrayChkBox.setActionCommand(closetotray);
			closeToTrayChkBox.addActionListener(this);
			closeToTrayChkBox.setSelected(sysInfoHolder.CLOSETOTRAY);
			closeToTrayChkBox.setEnabled(sysInfoHolder.TRAYENABLED);
			generalBorderPane.add(closeToTrayChkBox);
			generalBorderPane.add(Box.createHorizontalGlue());
		}
		
		generalPane.add(generalBorderPane);
		layout.putConstraint(SpringLayout.NORTH, generalBorderPane, 10, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, generalBorderPane, 5, SpringLayout.WEST, generalPane);
		layout.putConstraint(SpringLayout.EAST, heading, 0, SpringLayout.EAST, generalBorderPane);
		
		JPanel generalBorderPane1 = new JPanel();
		generalBorderPane1.setLayout(new BoxLayout(generalBorderPane1, BoxLayout.PAGE_AXIS));
		generalBorderPane1.setBorder(new TitledBorder("Post download options"));
		generalBorderPane1.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JComboBox<String> comboBox= new JComboBox<String>(shutdownOptions);
		comboBox.setSelectedIndex(sysInfoHolder.SHUTDOWNOPTION);
		comboBox.setActionCommand(shutdownoption);
		comboBox.addActionListener(this);
		p1.add(new JLabel("After downloading is complete: "));
		p1.add(comboBox);
		generalBorderPane1.add(p1);
		p1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JLabel tmpLbl = new JLabel("Note:This option will be always re-set when SKDownloader starts.");
		tmpLbl.setFont(new Font(tmpLbl.getFont().getName(),Font.ITALIC,tmpLbl.getFont().getSize()));
		tmpLbl.setForeground(Color.red);
		p1.add(tmpLbl);
		generalBorderPane1.add(p1);
		if(! SystemInfoHolder.IsWindows()) {
			p1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
			tmpLbl = new JLabel("Please see our forums at www.toolsbysk.com/skforums.com for linux specific requirements.");
			tmpLbl.setFont(new Font(tmpLbl.getFont().getName(),Font.ITALIC,tmpLbl.getFont().getSize()));
			tmpLbl.setForeground(Color.red);
			p1.add(tmpLbl);
			generalBorderPane1.add(p1);
		}
		generalPane.add(generalBorderPane1);
		layout.putConstraint(SpringLayout.NORTH, generalBorderPane1, 10, SpringLayout.SOUTH, generalBorderPane);
		layout.putConstraint(SpringLayout.WEST, generalBorderPane1, 5, SpringLayout.WEST, generalPane);
		layout.putConstraint(SpringLayout.EAST, generalBorderPane1, 0, SpringLayout.EAST, heading);
		//layout.putConstraint(SpringLayout.SOUTH, generalBorderPane1, 10, SpringLayout.SOUTH, p1);

		optionsPane.add(generalPane, "General");

		//Save to pane on the cardlayout
		savetoPane = new JPanel();
		layout = new SpringLayout();
		savetoPane.setLayout(layout);
		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Save to");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		savetoPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, savetoPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, savetoPane);
		layout.putConstraint(SpringLayout.EAST, savetoPane, 5, SpringLayout.EAST, heading);
		JPanel savetoBorderPane = new JPanel();
		savetoBorderPane.setLayout(new BoxLayout(savetoBorderPane, BoxLayout.PAGE_AXIS));
		//savetoBorderPane.setBorder(new ColoredTitledBorder(sysInfoHolder.GetTheme().TITLEDBORDERCOLOR, "Save to"));
		savetoBorderPane.setBorder(new TitledBorder("Save to"));
		savetoBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		JPanel tmpPane0 = new JPanel(new FlowLayout(FlowLayout.LEFT)); 
		ButtonGroup group = new ButtonGroup();
		ArrayList<JRadioButton> buttArr = new ArrayList<JRadioButton>();
		radioButt = new JRadioButton("Save to: ", true);
		radioButt.setActionCommand(savetodefaultdownloadfolder);
		radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    tmpPane0.add(radioButt);
	    savetoBorderPane.add(tmpPane0);
		savetoBorderPane.add(Box.createHorizontalGlue());
		
		tmpPane0=new JPanel(new BorderLayout());
		final JPanel p = new JPanel();
		JButton tmpButt = new JButton("Browse");
		defaultDownloadLocationTextField=new JTextField(100);
		tmpButt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser( );
	             fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	             fileChooser.setFileHidingEnabled(false);
	             if(fileChooser.showOpenDialog(p) == JFileChooser.APPROVE_OPTION)  {
	            	 try {
	            		 defaultDownloadLocationTextField.setText(fileChooser.getSelectedFile().getCanonicalPath());
	            		 sysInfoHolder.DEFAULTDOWNLOADLOCATION=fileChooser.getSelectedFile().getCanonicalPath();
	            	 } catch (Exception e1) {
	            		 defaultDownloadLocationTextField.setText(fileChooser.getSelectedFile().getAbsolutePath());
	            		 sysInfoHolder.DEFAULTDOWNLOADLOCATION=fileChooser.getSelectedFile().getAbsolutePath();
	            	 }
	             }
			}
		});
		tmpPane0.add(tmpButt, BorderLayout.WEST);
		defaultDownloadLocationTextField.setText(sysInfoHolder.DEFAULTDOWNLOADLOCATION);
		//defaultDownloadLocationDocument = defaultDownloadLocationTextField.getDocument();
		//defaultDownloadLocationDocument.addDocumentListener(this);
		tmpPane0.add(defaultDownloadLocationTextField, BorderLayout.CENTER);
		savetoBorderPane.add(tmpPane0);
		savetoBorderPane.add(Box.createHorizontalGlue());
		
		/*((FlowLayout) tmpPane1.getLayout()).setAlignment(FlowLayout.LEFT);
		tmpPane1.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		tmpPane1.setBorder(BorderFactory.createLineBorder(Color.RED));
		lbl = new JLabel("Server");
		tmpPane1.add(lbl);
		defaultDownloadLocationTextField = new JTextField(15);
		defaultDownloadLocationTextField.setText(sysInfoHolder.HTTPPROXYSERVER);
		httpProxyServerDocument = defaultDownloadLocationTextField.getDocument();
		httpProxyServerDocument.addDocumentListener(this);
		tmpPane1.add(defaultDownloadLocationTextField);
		savetoBorderPane.add(tmpPane1);*/

		
		tmpPane0 = new JPanel(new FlowLayout(FlowLayout.LEFT)); 
	    radioButt = new JRadioButton("Save to previous download location.", false);
	    radioButt.setActionCommand(savetopreviousdownloadfolder);
	    radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    tmpPane0.add(radioButt);
	    savetoBorderPane.add(tmpPane0);
		savetoBorderPane.add(Box.createHorizontalGlue());
		tmpPane0 = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    radioButt = new JRadioButton("Ask each time.", false);
	    radioButt.setActionCommand(askdownloadfoldereachtime);
	    radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    tmpPane0.add(radioButt);
	    savetoBorderPane.add(tmpPane0);
		savetoBorderPane.add(Box.createHorizontalGlue());
		group.setSelected(buttArr.get(sysInfoHolder.SAVETOOPTION).getModel(), true);
		savetoPane.add(savetoBorderPane);
		layout.putConstraint(SpringLayout.NORTH, savetoBorderPane, 10, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, savetoBorderPane, 5, SpringLayout.WEST, savetoPane);
		layout.putConstraint(SpringLayout.EAST, heading, 0, SpringLayout.EAST, savetoBorderPane);
		optionsPane.add(savetoPane, "Save to");
		//Themes pane on the cardlayout
		themesPane = new JPanel();
		layout = new SpringLayout();
		themesPane.setLayout(layout);
		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Themes");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		themesPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, themesPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, themesPane);
		layout.putConstraint(SpringLayout.EAST, themesPane, 5, SpringLayout.EAST, heading);
		themesBorderPane = new JPanel();
		themesBorderPane.setLayout(new BoxLayout(themesBorderPane, BoxLayout.PAGE_AXIS));
		themesBorderPane.setBorder(new TitledBorder("Themes"));
		themesBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		group = new ButtonGroup();
		buttArr = new ArrayList<JRadioButton>();
		
		for(int i=0; i<DownloaderTheme.GetNumberOfThemes(); i++)
		{
			radioButt = new JRadioButton(DownloaderTheme.GetThemeName(i), false);
			radioButt.setActionCommand(DownloaderTheme.GetThemeName(i));
			radioButt.addActionListener(this);
			group.add(radioButt);
			buttArr.add(radioButt);
			themesBorderPane.add(radioButt);
			themesBorderPane.add(Box.createHorizontalGlue());
		}

		group.setSelected(buttArr.get(sysInfoHolder.SELECTEDTHEME).getModel(), true);
		themesPane.add(themesBorderPane);
		layout.putConstraint(SpringLayout.NORTH, themesBorderPane, 10, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, themesBorderPane, 5, SpringLayout.WEST, savetoPane);
		layout.putConstraint(SpringLayout.EAST, heading, 0, SpringLayout.EAST, themesBorderPane);
		optionsPane.add(themesPane, "Themes");
		
		//Updates pane on the cardlayout
		updatesPane = new JPanel();
		layout = new SpringLayout();
		updatesPane.setLayout(layout);

		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Updates");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		updatesPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, updatesPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, updatesPane);
		layout.putConstraint(SpringLayout.EAST, updatesPane, 5, SpringLayout.EAST, heading);
		chkBox = new JCheckBox("Automatic updates turned on.", true);
		chkBox.setActionCommand(autoupdate);
		chkBox.addActionListener(this);
		updatesPane.add(chkBox);
		chkBox.setSelected(sysInfoHolder.AUTOUPDATE);
		layout.putConstraint(SpringLayout.NORTH, chkBox, 5, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, chkBox, 5, SpringLayout.WEST, updatesPane);
		updatesBorderPane = new JPanel();
		updatesBorderPane.setLayout(new BoxLayout(updatesBorderPane, BoxLayout.Y_AXIS));
		updatesBorderPane.setBorder(new TitledBorder("Autoupdate options"));
		updatesBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		updatesBorderPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		group = new ButtonGroup();
		buttArr = new ArrayList<JRadioButton>();
		radioButt = new JRadioButton("Every time on starting.    ", false);
		radioButt.setActionCommand(updatestartup);
		radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    updatesBorderPane.add(radioButt);
	    radioButt = new JRadioButton("Daily.    ", false);
		radioButt.setActionCommand(updatedaily);
		radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    updatesBorderPane.add(radioButt);
	    radioButt = new JRadioButton("Weekly.    ", false);
		radioButt.setActionCommand(updateweekly);
		radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    updatesBorderPane.add(radioButt);
	    group.setSelected(buttArr.get(sysInfoHolder.UPDATEOPTION).getModel(), true);
	    updatesPane.add(updatesBorderPane);
	    updatesBorderPane.setVisible(sysInfoHolder.AUTOUPDATE);
		layout.putConstraint(SpringLayout.NORTH, updatesBorderPane, 10, SpringLayout.SOUTH, chkBox);
		layout.putConstraint(SpringLayout.WEST, updatesBorderPane, 5, SpringLayout.WEST, updatesPane);
		layout.putConstraint(SpringLayout.EAST, updatesBorderPane, 0, SpringLayout.EAST, heading);
		optionsPane.add(updatesPane, "Updates");
		
		//Columns
		columnsPane = new JPanel();
		layout = new SpringLayout();
		columnsPane.setLayout(layout);

		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Table Columns");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		columnsPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, columnsPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, columnsPane);
		layout.putConstraint(SpringLayout.EAST, columnsPane, 5, SpringLayout.EAST, heading);
		
		JPanel columnsBorderPane = new JPanel();
		columnsBorderPane.setLayout(new BoxLayout(columnsBorderPane, BoxLayout.Y_AXIS));
		columnsBorderPane.setBorder(new TitledBorder("Displayed colums in table"));

		chkBox = new JCheckBox("File Name.", true);
		chkBox.setActionCommand(showfilenamecolumn);
		chkBox.addActionListener(this);
		columnsBorderPane.add(chkBox);
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_FILE_ID));
		columnsBorderPane.add(chkBox);
		chkBox = new JCheckBox("Size.", true);
		chkBox.setActionCommand(this.showsizecolumn);
		chkBox.addActionListener(this);
		columnsBorderPane.add(chkBox);
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_SIZE_ID));
		columnsBorderPane.add(chkBox);
		chkBox = new JCheckBox("Estimated Time.", true);
		chkBox.setActionCommand(this.showetacolumn);
		chkBox.addActionListener(this);
		columnsBorderPane.add(chkBox);
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_ETA_ID));
		columnsBorderPane.add(chkBox);
		chkBox = new JCheckBox("Progress.", true);
		chkBox.setActionCommand(this.showprogresscolumn);
		chkBox.addActionListener(this);
		columnsBorderPane.add(chkBox);
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_PROGRESS_ID));
		columnsBorderPane.add(chkBox);
		chkBox = new JCheckBox("Download Speed.", true);
		chkBox.setActionCommand(this.showdownspeedcolumn);
		chkBox.addActionListener(this);
		columnsBorderPane.add(chkBox);
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_DOWNSPEED_ID));
		columnsBorderPane.add(chkBox);
		chkBox = new JCheckBox("Upload Speed.", true);
		chkBox.setActionCommand(this.showupspeedcolumn);
		chkBox.addActionListener(this);
		columnsBorderPane.add(chkBox);
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_UPSPEED_ID));
		columnsBorderPane.add(chkBox);
		chkBox = new JCheckBox("Status.", true);
		chkBox.setActionCommand(this.showstatuscolumn);
		chkBox.addActionListener(this);
		columnsBorderPane.add(chkBox);
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_STATUS_ID));
		columnsBorderPane.add(chkBox);
		chkBox = new JCheckBox("Seeds.", true);
		chkBox.setActionCommand(this.showseedscolumn);
		chkBox.addActionListener(this);
		columnsBorderPane.add(chkBox);
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_SEEDS_ID));
		columnsBorderPane.add(chkBox);
		chkBox = new JCheckBox("Peers.", true);
		chkBox.setActionCommand(this.showpeerscolumn);
		chkBox.addActionListener(this);
		columnsBorderPane.add(chkBox);
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_PEERS_ID));
		columnsBorderPane.add(chkBox);
		chkBox = new JCheckBox("Share Ratio.", true);
		chkBox.setActionCommand(this.showshareratiocolumn);
		chkBox.addActionListener(this);
		columnsBorderPane.add(chkBox);
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_SHARERATIO_ID));
		columnsBorderPane.add(chkBox);
		//chkBox = new JCheckBox("Tracker Status.", true);
		//chkBox.setActionCommand(this.showtrackerstatuscolumn);
		//chkBox.addActionListener(this);
		//columnsBorderPane.add(chkBox);
		//chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_TRACKER_STATUS_ID));
		//columnsBorderPane.add(chkBox);
		chkBox = new JCheckBox("Downloaded Size.", true);
		chkBox.setActionCommand(this.showdownloadedcolumn);
		chkBox.addActionListener(this);
		columnsBorderPane.add(chkBox);
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_DOWNLOADED_ID));
		columnsBorderPane.add(chkBox);
		
		columnsPane.add(columnsBorderPane);
		columnsBorderPane.setVisible(true);
		layout.putConstraint(SpringLayout.NORTH, columnsBorderPane, 10, SpringLayout.SOUTH, chkBox);
		layout.putConstraint(SpringLayout.WEST, columnsBorderPane, 5, SpringLayout.WEST, columnsPane);
		layout.putConstraint(SpringLayout.EAST, columnsBorderPane, 0, SpringLayout.EAST, heading);
		optionsPane.add(columnsPane, "Columns");
		
		//Internet pane on the cardlayout
		internetPane = new JPanel();
		layout = new SpringLayout();
		internetPane.setLayout(layout);
		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Internet");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		internetPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, internetPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, internetPane);
		layout.putConstraint(SpringLayout.EAST, internetPane, 5, SpringLayout.EAST, heading);
		JPanel InternetBorderPane = new JPanel();
		InternetBorderPane.setLayout(new BoxLayout(InternetBorderPane, BoxLayout.PAGE_AXIS));
		InternetBorderPane.setBorder(new TitledBorder("Internet"));
		InternetBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		group = new ButtonGroup();
		buttArr = new ArrayList<JRadioButton>();
		radioButt = new JRadioButton("Always connected", true);
		radioButt.setActionCommand(alwaysconnected);
		radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    InternetBorderPane.add(radioButt);
		InternetBorderPane.add(Box.createHorizontalGlue());
		internetPane.add(InternetBorderPane);
		layout.putConstraint(SpringLayout.NORTH, InternetBorderPane, 10, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, InternetBorderPane, 5, SpringLayout.WEST, internetPane);
		layout.putConstraint(SpringLayout.EAST, InternetBorderPane, 0, SpringLayout.EAST, heading);
		optionsPane.add(internetPane, "Internet");
		//Proxy pane on the cardlayout
		proxyPane = new JPanel();
		layout = new SpringLayout();
		proxyPane.setLayout(layout);
		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Proxy");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		proxyPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, proxyPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, proxyPane);
		layout.putConstraint(SpringLayout.EAST, proxyPane, 5, SpringLayout.EAST, heading);
		JPanel proxyBorderPane = new JPanel();
		proxyBorderPane.setLayout(new BoxLayout(proxyBorderPane, BoxLayout.PAGE_AXIS));
		proxyBorderPane.setBorder(new TitledBorder("Proxy"));
		proxyBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		chkBox = new JCheckBox("Use proxy", sysInfoHolder.USEPROXY);
		chkBox.setActionCommand(useproxy);
		chkBox.addActionListener(this);
		proxyBorderPane.add(chkBox);
		proxyBorderPane.add(Box.createHorizontalGlue());
		proxyPane.add(proxyBorderPane);
		layout.putConstraint(SpringLayout.NORTH, proxyBorderPane, 10, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, proxyBorderPane, 5, SpringLayout.WEST, proxyPane);
		layout.putConstraint(SpringLayout.EAST, proxyBorderPane, 0, SpringLayout.EAST, heading);
		proxyTabbedPane = new JTabbedPane();
		proxyTabbedPane.setVisible(sysInfoHolder.USEPROXY);
		JPanel httpProxyPane = new JPanel();
		SpringLayout layout1 = new SpringLayout();
		httpProxyPane.setLayout(layout1);
		httpProxyPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		JPanel tmpPane1 = new JPanel();
		((FlowLayout) tmpPane1.getLayout()).setAlignment(FlowLayout.LEFT);
		tmpPane1.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		lbl = new JLabel("Server");
		tmpPane1.add(lbl);
		httpProxyServerTextField = new JTextField(15);
		httpProxyServerTextField.setText(sysInfoHolder.HTTPPROXYSERVER);
		httpProxyServerDocument = httpProxyServerTextField.getDocument();
		httpProxyServerDocument.addDocumentListener(this);
		tmpPane1.add(httpProxyServerTextField);
		lbl = new JLabel(": Port");
		tmpPane1.add(lbl);
		httpProxyPortTextField = new TextField(4);
		httpProxyPortTextField.setText(Integer.toString(sysInfoHolder.HTTPPROXYPORT));
		httpProxyPortTextField.addTextListener(this);
		tmpPane1.add(httpProxyPortTextField);
		httpProxyPane.add(tmpPane1);
		layout1.putConstraint(SpringLayout.NORTH, tmpPane1, 10, SpringLayout.NORTH, httpProxyPane);
		layout1.putConstraint(SpringLayout.WEST, tmpPane1, 5, SpringLayout.WEST, httpProxyPane);
		JPanel tmpPane2 = new JPanel();
		tmpPane2.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		GroupLayout layout2 = new GroupLayout(tmpPane2);
		layout2.setAutoCreateGaps(true);
		tmpPane2.setLayout(layout2);
		JCheckBox cacheChkBox= new JCheckBox("Tell proxy server not to use cache");
		cacheChkBox.setActionCommand(httpproxyservernottousecache);
		cacheChkBox.addActionListener(this);
		cacheChkBox.setSelected(sysInfoHolder.HTTPPROXYNOTUSECACHE);
		tmpPane2.add(cacheChkBox);
		JCheckBox loginChkBox= new JCheckBox("Use login credentials");
		loginChkBox.setActionCommand(httpuselogincredentials);
		loginChkBox.addActionListener(this);
		loginChkBox.setSelected(sysInfoHolder.HTTPUSEAUTHENTICATION);
		tmpPane2.add(loginChkBox);
		JLabel nameLbl = new JLabel("Name");
		tmpPane2.add(nameLbl);
		httpProxyAuthenNameTextField = new JTextField(12);
		httpProxyAuthenNameTextField.setText(sysInfoHolder.HTTPPROXYUSER);
		httpProxyAuthenNameTextField.setEnabled(sysInfoHolder.HTTPUSEAUTHENTICATION);
		httpProxyNameDocument = httpProxyAuthenNameTextField.getDocument();
		httpProxyNameDocument.addDocumentListener(this);
		httpProxyAuthenNameTextField.setMaximumSize(new Dimension((int)Short.MAX_VALUE, (int)httpProxyAuthenNameTextField.getPreferredSize().getHeight()));
		tmpPane2.add(httpProxyAuthenNameTextField);
		JLabel passwordLbl = new JLabel("  Password");
		tmpPane2.add(passwordLbl);
		httpProxyAuthenPasswordTextField = new JPasswordField(12);
		if(! sysInfoHolder.HTTPPROXYPASSWORD.equals(""))
			httpProxyAuthenPasswordTextField.setText(sysInfoHolder.HTTPPROXYPASSWORD);
		httpProxyAuthenPasswordTextField.setEnabled(sysInfoHolder.HTTPUSEAUTHENTICATION);
		httpProxyPasswordDocument = httpProxyAuthenPasswordTextField.getDocument();
		httpProxyPasswordDocument.addDocumentListener(this);
		httpProxyAuthenPasswordTextField.setMaximumSize(new Dimension((int)Short.MAX_VALUE, (int)httpProxyAuthenPasswordTextField.getPreferredSize().getHeight()));
		tmpPane2.add(httpProxyAuthenPasswordTextField);
		JLabel warningLabel = new JLabel("Note: Proxy settings would apply only to new downloads");
		warningLabel.setFont(new Font(warningLabel.getFont().getName(),Font.ITALIC,warningLabel.getFont().getSize()));
		layout2.setAutoCreateGaps(true);
		layout2.setHorizontalGroup(layout2.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addComponent(cacheChkBox)
						.addComponent(loginChkBox)
						.addGroup(layout2.createSequentialGroup()
							.addComponent(nameLbl)
							.addComponent(httpProxyAuthenNameTextField)
							.addComponent(passwordLbl)
							.addComponent(httpProxyAuthenPasswordTextField))
						.addComponent(warningLabel)
					);
		layout2.setVerticalGroup(layout2.createSequentialGroup()
					.addComponent(cacheChkBox)
					.addComponent(loginChkBox)
					.addGroup(layout2.createParallelGroup(GroupLayout.Alignment.CENTER)
						.addComponent(nameLbl)
						.addComponent(httpProxyAuthenNameTextField)
						.addComponent(passwordLbl)
						.addComponent(httpProxyAuthenPasswordTextField))
						.addComponent(warningLabel));

		httpProxyPane.add(tmpPane2);
		tmpPane2.setBorder(new TitledBorder("HTTP Options"));
		layout1.putConstraint(SpringLayout.NORTH, tmpPane2, 0, SpringLayout.SOUTH, tmpPane1);
		layout1.putConstraint(SpringLayout.WEST, tmpPane2, 0, SpringLayout.WEST, tmpPane1);
		layout1.putConstraint(SpringLayout.SOUTH, httpProxyPane, 10, SpringLayout.SOUTH, tmpPane2);
		proxyTabbedPane.addTab("HTTP", null, httpProxyPane, "HTTP Proxy settings");
		proxyPane.add(proxyTabbedPane);
		layout.putConstraint(SpringLayout.NORTH, proxyTabbedPane, 10, SpringLayout.SOUTH, proxyBorderPane);
		layout.putConstraint(SpringLayout.WEST, proxyTabbedPane, 5, SpringLayout.WEST, proxyPane);
		layout.putConstraint(SpringLayout.EAST, proxyTabbedPane, 0, SpringLayout.EAST, heading);
		optionsPane.add(proxyPane, "Proxy");
		
		//Connectinos pane on the cardlayout
		connectionsPane = new JPanel();
		layout = new SpringLayout();
		connectionsPane.setLayout(layout);
		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Connections");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		connectionsPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, connectionsPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, connectionsPane);
		layout.putConstraint(SpringLayout.EAST, connectionsPane, 5, SpringLayout.EAST, heading);
		JPanel connectionsBorderPane = new JPanel();
		connectionsBorderPane.setLayout(new BoxLayout(connectionsBorderPane, BoxLayout.Y_AXIS));
		connectionsBorderPane.setBorder(new TitledBorder("Timeout/retry"));
		connectionsBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		connectionsBorderPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		JPanel tmpPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		chkBox = new JCheckBox("If server sends a delay time, use that value.", true);
		chkBox.setActionCommand(httpuseserversentdelaytime);
		chkBox.addActionListener(this);
		chkBox.setSelected(sysInfoHolder.USEDELAYTIMEFROMSERVER);
		tmpPane.add(chkBox);
	    connectionsBorderPane.add(tmpPane);
	    connectionsBorderPane.add(Box.createHorizontalGlue());
	    tmpPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    lbl = new JLabel("Time out connections after");
	    tmpPane.add(lbl);
	    timeoutDelayTextField = new NumericTextField(3);
	    timeoutDelayTextField.setText(Integer.toString(sysInfoHolder.TIMEOUTDELAY));
	    timeoutDelayTextField.getDocument().addDocumentListener(this);
	    tmpPane.add(timeoutDelayTextField);
	    lbl = new JLabel(" seconds.");
	    tmpPane.add(lbl);
	    connectionsBorderPane.add(tmpPane);
	    tmpPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    lbl = new JLabel("Try to reconnect upto");
	    tmpPane.add(lbl);
	    retryLimitTextField = new NumericTextField(3);
	    retryLimitTextField.setText(Integer.toString(sysInfoHolder.CONNECTRETRYLIMIT));
	    retryLimitTextField.getDocument().addDocumentListener(this);
	    tmpPane.add(retryLimitTextField);
	    lbl = new JLabel(" times.");
	    tmpPane.add(lbl);
	    connectionsBorderPane.add(tmpPane);
	    connectionsBorderPane.add(Box.createHorizontalGlue());
	    connectionsPane.add(connectionsBorderPane);
		layout.putConstraint(SpringLayout.NORTH, connectionsBorderPane, 10, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, connectionsBorderPane, 5, SpringLayout.WEST, internetPane);
		layout.putConstraint(SpringLayout.EAST, connectionsBorderPane, 0, SpringLayout.EAST, heading);
		optionsPane.add(connectionsPane, "Connections");
		
		//Downloads pane
		downloadsPane = new JPanel();
		layout = new SpringLayout();
		downloadsPane.setLayout(layout);
		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Downloads");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		downloadsPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, downloadsPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, downloadsPane);
		layout.putConstraint(SpringLayout.EAST, downloadsPane, 5, SpringLayout.EAST, heading);
		JPanel downloadsBorderPane = new JPanel();
		downloadsBorderPane.setLayout(new BoxLayout(downloadsBorderPane, BoxLayout.PAGE_AXIS));
		downloadsBorderPane.setBorder(new TitledBorder("Adding downloads"));
		downloadsBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		group = new ButtonGroup();
		buttArr = new ArrayList<JRadioButton>();
		radioButt = new JRadioButton("Start downloading files immediately on adding.", true);
		radioButt.setActionCommand(startdownloadingimmdediately);
		radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    downloadsBorderPane.add(radioButt);
	    downloadsBorderPane.add(Box.createHorizontalGlue());
	    radioButt = new JRadioButton("Add to list, but do not start immediately.", false);
	    radioButt.setActionCommand(addtolistbutdonotstart);
	    radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    downloadsBorderPane.add(radioButt);
	    downloadsBorderPane.add(Box.createHorizontalGlue());
	    radioButt = new JRadioButton("Ask whether to start immediately", false);
	    radioButt.setActionCommand(askwhethertostartimmediately);
	    radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    downloadsBorderPane.add(radioButt);
	    downloadsBorderPane.add(Box.createHorizontalGlue());
	    group.setSelected(buttArr.get(sysInfoHolder.DOWNLOADSTARTOPTION).getModel(), true);
	    downloadsPane.add(downloadsBorderPane);
		layout.putConstraint(SpringLayout.NORTH, downloadsBorderPane, 10, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, downloadsBorderPane, 5, SpringLayout.WEST, downloadsPane);
		layout.putConstraint(SpringLayout.EAST, downloadsBorderPane, 0, SpringLayout.EAST, heading);
		optionsPane.add(downloadsPane, "Downloads");
		
		//Accelerate pane on the cardlayout
		acceleratePane = new JPanel();
		layout = new SpringLayout();
		acceleratePane.setLayout(layout);
		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Accelerate");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		acceleratePane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, acceleratePane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, acceleratePane);
		layout.putConstraint(SpringLayout.EAST, acceleratePane, 5, SpringLayout.EAST, heading);
		chkBox = new JCheckBox("Automatically do accelerated downloading(4 connections by default).", true);
		chkBox.setActionCommand(autoaccelerate);
		chkBox.addActionListener(this);
		acceleratePane.add(chkBox);
		chkBox.setSelected(sysInfoHolder.AUTOACCELERATE);
		layout.putConstraint(SpringLayout.NORTH, chkBox, 5, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, chkBox, 5, SpringLayout.WEST, acceleratePane);
		accelerateBorderPane = new JPanel();
		accelerateBorderPane.setLayout(new BoxLayout(accelerateBorderPane, BoxLayout.Y_AXIS));
		accelerateBorderPane.setBorder(new TitledBorder("Accelerate options"));
		accelerateBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		accelerateBorderPane.setAlignmentX(Component.LEFT_ALIGNMENT);
	    tmpPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel lb1 = new JLabel("Only accelerate if file size is greater than (kb)");
	    tmpPane.add(lb1);
	    accelerationMinSizeTextField = new NumericTextField(4);
	    accelerationMinSizeTextField.setText(Integer.toString(sysInfoHolder.MINSIZEFORACCELERATION));
	    accelerationMinSizeTextField.getDocument().addDocumentListener(this);
	    tmpPane.add(accelerationMinSizeTextField);
	    accelerateBorderPane.add(tmpPane);
	    accelerateBorderPane.add(Box.createHorizontalGlue());
	    accelerateBorderPane.setVisible(sysInfoHolder.AUTOACCELERATE);
	    acceleratePane.add(accelerateBorderPane);
    	accelerateBorderPane.setVisible(sysInfoHolder.AUTOACCELERATE);
		layout.putConstraint(SpringLayout.NORTH, accelerateBorderPane, 10, SpringLayout.SOUTH, chkBox);
		layout.putConstraint(SpringLayout.WEST, accelerateBorderPane, 5, SpringLayout.WEST, acceleratePane);
		layout.putConstraint(SpringLayout.EAST, accelerateBorderPane, 0, SpringLayout.EAST, heading);
		optionsPane.add(acceleratePane, "Accelerate");
		
		//Torrents pane
		torrentsPane = new JPanel();
		layout = new SpringLayout();
		torrentsPane.setLayout(layout);
		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Torrents");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		torrentsPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, torrentsPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, torrentsPane);
		layout.putConstraint(SpringLayout.EAST, torrentsPane, 5, SpringLayout.EAST, heading);
		JPanel torrentssBorderPane = new JPanel();
		torrentssBorderPane.setLayout(new BoxLayout(torrentssBorderPane, BoxLayout.PAGE_AXIS));
		torrentssBorderPane.setBorder(new TitledBorder("Downloading torrent using browser link"));
		torrentssBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		group = new ButtonGroup();
		buttArr = new ArrayList<JRadioButton>();
		radioButt = new JRadioButton("Download the torrent file as a regular file.", false);
		radioButt.setActionCommand(downloadtorrentasnormalfile);
		radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    torrentssBorderPane.add(radioButt);
	    torrentssBorderPane.add(Box.createHorizontalGlue());
	    radioButt = new JRadioButton("Download the torrent file and use it to start a torrent download", true);
	    radioButt.setActionCommand(downloadtorrentandstarttorrent);
	    radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    torrentssBorderPane.add(radioButt);
	    torrentssBorderPane.add(Box.createHorizontalGlue());
	    group.setSelected(buttArr.get(sysInfoHolder.TORRENTDOWNLOADSTARTOPTION).getModel(), true);
	    torrentsPane.add(torrentssBorderPane);
		layout.putConstraint(SpringLayout.NORTH, torrentssBorderPane, 10, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, torrentssBorderPane, 5, SpringLayout.WEST, torrentsPane);
		layout.putConstraint(SpringLayout.EAST, torrentssBorderPane, 0, SpringLayout.EAST, heading);
	    tmpPane2=new JPanel();
		((FlowLayout) tmpPane2.getLayout()).setAlignment(FlowLayout.LEFT);
		lbl = new JLabel("Listen Port");
		tmpPane2.add(lbl);
		torrentListenPortField = new NumericTextField(6);
		torrentListenPortField.setText(""+sysInfoHolder.TORRENT_LISTEN_PORT);
		torrentListenPortField.getDocument().addDocumentListener(this);
		tmpPane2.add(torrentListenPortField);
		tmpPane2.add(new JLabel(" [Needs Restart]"));
		torrentsPane.add(tmpPane2);
		layout.putConstraint(SpringLayout.NORTH, tmpPane2, 5, SpringLayout.SOUTH, torrentssBorderPane);
		layout.putConstraint(SpringLayout.WEST, tmpPane2, 0, SpringLayout.WEST, torrentssBorderPane);
		layout.putConstraint(SpringLayout.EAST, tmpPane2, 0, SpringLayout.EAST, torrentssBorderPane);
		optionsPane.add(torrentsPane, "Torrents");
		
		//Torrent speed pane on the cardlayout
		torrentspeedPane = new JPanel();
		layout = new SpringLayout();
		torrentspeedPane.setLayout(layout);
		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Torrent Speed");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		torrentspeedPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, torrentspeedPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, torrentspeedPane);
		layout.putConstraint(SpringLayout.EAST, torrentspeedPane, 5, SpringLayout.EAST, heading);
		JPanel torrentspeedBorderPane = new JPanel();
		torrentspeedBorderPane.setLayout(new BoxLayout(torrentspeedBorderPane, BoxLayout.PAGE_AXIS));
		torrentspeedBorderPane.setBorder(new TitledBorder("Torrent speed settings"));
		torrentspeedBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		tmpPane1 = new JPanel();
		tmpPane1.setLayout(new BoxLayout(tmpPane1,BoxLayout.Y_AXIS));
		tmpPane1.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		tmpPane2=new JPanel();
		((FlowLayout) tmpPane2.getLayout()).setAlignment(FlowLayout.LEFT);
		lbl = new JLabel("Max download speed");
		tmpPane2.add(lbl);
		//maxDownloadSpeedField = new TextField(6);
		maxTorrentDownloadSpeedField = new NumericTextField(6);
		maxTorrentDownloadSpeedField.setText(""+sysInfoHolder.MAXGLOBALTORRENTDOWNLOADSPEED);
		maxTorrentDownloadSpeedField.getDocument().addDocumentListener(this);
		tmpPane2.add(maxTorrentDownloadSpeedField);
		tmpPane2.add(new JLabel("Kb/s  [0 for unlimitted]"));
		tmpPane1.add(tmpPane2);
		tmpPane2=new JPanel();
		((FlowLayout) tmpPane2.getLayout()).setAlignment(FlowLayout.LEFT);
		lbl = new JLabel("Max upload speed");
		tmpPane2.add(lbl);
		maxTorrentUploadSpeedField = new NumericTextField(6);
		maxTorrentUploadSpeedField.setText(Integer.toString(sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEED));
		maxTorrentUploadSpeedField.getDocument().addDocumentListener(this);
		tmpPane2.add(maxTorrentUploadSpeedField);
		tmpPane2.add(new JLabel("Kb/s  [0 for unlimitted]"));
		tmpPane1.add(tmpPane2);
		
		tmpPane2=new JPanel();
		((FlowLayout) tmpPane2.getLayout()).setAlignment(FlowLayout.LEFT);
		chkBox = new JCheckBox("Max upload speed when only seeding");
		tmpPane2.add(chkBox);
		chkBox.setActionCommand(enableuploadspeedonseeding);
		chkBox.setSelected(sysInfoHolder.GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED);
		chkBox.addActionListener(this);
		maxTorrentUploadSpeedOnSeedingField = new NumericTextField(6);
		maxTorrentUploadSpeedOnSeedingField.setText(Integer.toString(sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEEDONSEEDING));
		maxTorrentUploadSpeedOnSeedingField.getDocument().addDocumentListener(this);
		maxTorrentUploadSpeedOnSeedingField.setEnabled(sysInfoHolder.GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED);
		tmpPane2.add(maxTorrentUploadSpeedOnSeedingField);
		tmpPane2.add(new JLabel("Kb/s"));
		tmpPane1.add(tmpPane2);
		
		torrentspeedBorderPane.add(tmpPane1);
		torrentspeedPane.add(torrentspeedBorderPane);
		layout.putConstraint(SpringLayout.NORTH, torrentspeedBorderPane, 5, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, torrentspeedBorderPane, 0, SpringLayout.WEST, heading);
		layout.putConstraint(SpringLayout.EAST, torrentspeedBorderPane, 0, SpringLayout.EAST, heading);
		optionsPane.add(torrentspeedPane, "Torrent Speed");
		
		//Management pane on the cardlayout
		torrentmanagementPane = new JPanel();
		layout = new SpringLayout();
		torrentmanagementPane.setLayout(layout);
		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Management");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		torrentmanagementPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, torrentmanagementPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, torrentmanagementPane);
		layout.putConstraint(SpringLayout.EAST, torrentmanagementPane, 5, SpringLayout.EAST, heading);
		
		JPanel torrentmanagementBorderPane = new JPanel();
		torrentmanagementBorderPane.setLayout(new BoxLayout(torrentmanagementBorderPane, BoxLayout.PAGE_AXIS));
		torrentmanagementBorderPane.setBorder(new TitledBorder("Automatic removal rules"));
		torrentmanagementBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		tmpPane1=new JPanel(new FlowLayout(FlowLayout.LEFT));
		chkBox = new JCheckBox("Remove if share ratio exceedes ", true);
		chkBox.setActionCommand(removetorrentonshareratio);
		chkBox.addActionListener(this);
		chkBox.setSelected(sysInfoHolder.TORRENTREMOVEONSHARERATIOOPTION);
		tmpPane1.add(chkBox);
		torrentRemoveShareRatioField = new FloatTextField(6,2);
		torrentRemoveShareRatioField.setText(""+sysInfoHolder.SHARERATIOTOREMOVETORRENT);
		torrentRemoveShareRatioField.getDocument().addDocumentListener(this);
		if(! chkBox.isSelected())
			torrentRemoveShareRatioField.setEnabled(false);
		tmpPane1.add(torrentRemoveShareRatioField);
		torrentmanagementBorderPane.add(tmpPane1);
		torrentmanagementBorderPane.add(Box.createHorizontalGlue());
		tmpPane1=new JPanel(new FlowLayout(FlowLayout.LEFT));
		chkBox = new JCheckBox("Remove if seeding time exceedes ", true);
		chkBox.setActionCommand(removetorrentonseedingtime);
		chkBox.addActionListener(this);
		chkBox.setSelected(sysInfoHolder.TORRENTREMOVEONSEEDTIMEOPTION);
		tmpPane1.add(chkBox);
		torrentRemoveSeedingTimeField = new NumericTextField(6);
		torrentRemoveSeedingTimeField.setText(""+sysInfoHolder.SEEDTIMETOREMOVETORRENT);
		torrentRemoveSeedingTimeField.getDocument().addDocumentListener(this);
		if(! chkBox.isSelected())
			torrentRemoveSeedingTimeField.setEnabled(false);
		tmpPane1.add(torrentRemoveSeedingTimeField);
		lbl = new JLabel(" hours");
		tmpPane1.add(lbl);
		torrentmanagementBorderPane.add(tmpPane1);
		torrentmanagementBorderPane.add(Box.createHorizontalGlue());
		torrentmanagementPane.add(torrentmanagementBorderPane);
		layout.putConstraint(SpringLayout.NORTH, torrentmanagementBorderPane, 15, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, torrentmanagementBorderPane, 25, SpringLayout.WEST, torrentmanagementPane);
		layout.putConstraint(SpringLayout.EAST, torrentmanagementBorderPane, 0, SpringLayout.EAST, heading);
		//layout.putConstraint(SpringLayout.SOUTH, torrentmanagementBorderPane, 5, SpringLayout.SOUTH, torrentmanagementPane);
		optionsPane.add(torrentmanagementPane, "Management");
		
		//Torrents pane
		resetPane = new JPanel();
		layout = new SpringLayout();
		resetPane.setLayout(layout);
		heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Torrents");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		resetPane.add(heading);
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, resetPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, resetPane);
		layout.putConstraint(SpringLayout.EAST, resetPane, 5, SpringLayout.EAST, heading);
		JButton butt = new JButton("Reset all settings to defaults");
		resetPane.add(butt);
		butt.setActionCommand(reset);
		butt.addActionListener(this);
		layout.putConstraint(SpringLayout.NORTH, butt, 15, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, butt, 10, SpringLayout.WEST, heading);
		//layout.putConstraint(SpringLayout.EAST, resetPane, 5, SpringLayout.EAST, heading);
		optionsPane.add(resetPane, "Reset");

		optionsSplitPane.setRightComponent(optionsPane);

		motherPane.add(optionsSplitPane, BorderLayout.CENTER);
		
		bottomPane = new JPanel(new FlowLayout(FlowLayout.TRAILING));
		butt = new JButton("OK");
		butt.setActionCommand(ok);
		butt.addActionListener(this);
		bottomPane.add(butt);
		butt = new JButton("Cancel");
		butt.setActionCommand(cancel);
		butt.addActionListener(this);
		bottomPane.add(butt);
		applyButt = new JButton("Apply");
		applyButt.setActionCommand(apply);
		applyButt.addActionListener(this);
		bottomPane.add(applyButt);
		
		motherPane.add(bottomPane, BorderLayout.SOUTH);
		motherPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		
		motherFrame.add(motherPane);
		motherFrame.setSize(sysInfoHolder.OPTIONSFRAME_WIDTH,sysInfoHolder.OPTIONSFRAME_HEIGHT);
		motherFrame.setLocation(parentFrame.GetFrame().getX()+40, parentFrame.GetFrame().getY()+60);
		motherFrame.setVisible(true);
		// Listener for main frame to close on clicking close button
		motherFrame.addWindowListener(new WindowAdapter(){
		      public void windowClosing(WindowEvent we){
		    	  orig_sysInfoHolder.OPTIONSFRAME_WIDTH=motherFrame.getWidth();
		    	  orig_sysInfoHolder.OPTIONSFRAME_HEIGHT=motherFrame.getHeight();
		    	  Close();
		      }});

		((DefaultTreeModel)optionsTree.getModel()).reload();
		for(int i=0; i<optionsTree.getRowCount(); i++)
			optionsTree.expandRow(i);
		
		optionsTree.setSelectionRow(0);
		optionsTree.setRowHeight(20);
	}
	
	/*************************************************************
	 * 
	 * Function  : valueChanged
	 * Arguments : tree selection event
	 * Return    : void
	 * Notes     : Action performer for tree events
	 * 
	 *************************************************************/
	public void valueChanged(TreeSelectionEvent e) 
    {
    	DefaultMutableTreeNode node = (DefaultMutableTreeNode)
    	optionsTree.getLastSelectedPathComponent();
    	
    	((CardLayout)optionsPane.getLayout()).show(optionsPane, node.toString());
    }

	/*************************************************************
	 * 
	 * Function  : actionPerformed
	 * Arguments : action events function
	 * Return    : void
	 * Notes     : Action performer action events
	 * 
	 *************************************************************/
	@SuppressWarnings("rawtypes")
	public void actionPerformed(ActionEvent e) 
	{
		applyButt.setEnabled(true);
		if(e.getActionCommand().equals(showpopupwhendownloadsfinish))
			sysInfoHolder.SHOWPOPPUPWHENDOWNLOADFINISH=((JCheckBox)e.getSource()).isSelected();
		else if(e.getActionCommand().equals(showdeatilsonaddingdownload))
			sysInfoHolder.SHOWDETAILSONADDINGDOWNLOAD=((JCheckBox)e.getSource()).isSelected();
		else if(e.getActionCommand().equals(trayenabled)) {
			sysInfoHolder.TRAYENABLED=((JCheckBox)e.getSource()).isSelected();
			closeToTrayChkBox.setEnabled(sysInfoHolder.TRAYENABLED);
		}
		else if(e.getActionCommand().equals(showfilenamecolumn)) {
			parentFrame.CheckAndUpdateColumns(DownloadsTableColumnManager.COL_FILE_ID, ((JCheckBox)e.getSource()).isSelected());
		}
		else if(e.getActionCommand().equals(this.showsizecolumn)) {
			parentFrame.CheckAndUpdateColumns(DownloadsTableColumnManager.COL_SIZE_ID, ((JCheckBox)e.getSource()).isSelected());
		}
		else if(e.getActionCommand().equals(this.showetacolumn)) {
			parentFrame.CheckAndUpdateColumns(DownloadsTableColumnManager.COL_ETA_ID, ((JCheckBox)e.getSource()).isSelected());
		}
		else if(e.getActionCommand().equals(this.showprogresscolumn)) {
			parentFrame.CheckAndUpdateColumns(DownloadsTableColumnManager.COL_PROGRESS_ID, ((JCheckBox)e.getSource()).isSelected());
		}
		else if(e.getActionCommand().equals(this.showdownspeedcolumn)) {
			parentFrame.CheckAndUpdateColumns(DownloadsTableColumnManager.COL_DOWNSPEED_ID, ((JCheckBox)e.getSource()).isSelected());
		}
		else if(e.getActionCommand().equals(this.showstatuscolumn)) {
			parentFrame.CheckAndUpdateColumns(DownloadsTableColumnManager.COL_STATUS_ID, ((JCheckBox)e.getSource()).isSelected());
		}
		else if(e.getActionCommand().equals(this.showupspeedcolumn)) {
			parentFrame.CheckAndUpdateColumns(DownloadsTableColumnManager.COL_UPSPEED_ID, ((JCheckBox)e.getSource()).isSelected());
		}
		else if(e.getActionCommand().equals(this.showseedscolumn)) {
			parentFrame.CheckAndUpdateColumns(DownloadsTableColumnManager.COL_SEEDS_ID, ((JCheckBox)e.getSource()).isSelected());
		}
		else if(e.getActionCommand().equals(this.showpeerscolumn)) {
			parentFrame.CheckAndUpdateColumns(DownloadsTableColumnManager.COL_PEERS_ID, ((JCheckBox)e.getSource()).isSelected());
		}
		else if(e.getActionCommand().equals(this.showshareratiocolumn)) {
			parentFrame.CheckAndUpdateColumns(DownloadsTableColumnManager.COL_SHARERATIO_ID, ((JCheckBox)e.getSource()).isSelected());
		}
		else if(e.getActionCommand().equals(this.showtrackerstatuscolumn)) {
			parentFrame.CheckAndUpdateColumns(DownloadsTableColumnManager.COL_TRACKER_STATUS_ID, ((JCheckBox)e.getSource()).isSelected());
		}
		else if(e.getActionCommand().equals(this.showdownloadedcolumn)) {
			parentFrame.CheckAndUpdateColumns(DownloadsTableColumnManager.COL_DOWNLOADED_ID, ((JCheckBox)e.getSource()).isSelected());
		}
		else if (e.getActionCommand().equals(closetotray))
			sysInfoHolder.CLOSETOTRAY=((JCheckBox)e.getSource()).isSelected();
		else if(e.getActionCommand().equals(savetodefaultdownloadfolder))
			sysInfoHolder.SAVETOOPTION=SystemInfoHolder.SAVETODEFAULTFOLDER;
		else if(e.getActionCommand().equals(savetopreviousdownloadfolder))
			sysInfoHolder.SAVETOOPTION=SystemInfoHolder.SAVETOPRVIOUSDOWNLOADLOCATION;
		else if(e.getActionCommand().equals(askdownloadfoldereachtime))
			sysInfoHolder.SAVETOOPTION=SystemInfoHolder.ASKDOWNLOADLOCATIONEACHTIME;
		else if(e.getActionCommand().equals(useproxy)) {
			sysInfoHolder.USEPROXY=((JCheckBox)e.getSource()).isSelected();
			proxyTabbedPane.setVisible(sysInfoHolder.USEPROXY);
			System.getProperties().put("proxySet", Boolean.toString(sysInfoHolder.USEPROXY));
			if(! sysInfoHolder.USEPROXY) {
				System.getProperties().put("http.proxyHost", "");
				System.getProperties().put("http.proxyPort", "");
				System.getProperties().put("http.proxyUser", "");
				System.getProperties().put("http.proxyPassword", "");
			}
			else {
				System.getProperties().put("http.proxyHost", sysInfoHolder.HTTPPROXYSERVER);
				System.getProperties().put("http.proxyPort", Integer.toString(sysInfoHolder.HTTPPROXYPORT));
				System.getProperties().put("http.proxyUser", sysInfoHolder.HTTPPROXYUSER);
				System.getProperties().put("http.proxyPassword", sysInfoHolder.HTTPPROXYPASSWORD);
			}
		}
		else if(e.getActionCommand().equals(httpproxyservernottousecache))
			sysInfoHolder.HTTPPROXYNOTUSECACHE=((JCheckBox)e.getSource()).isSelected();
		else if(e.getActionCommand().equals(httpuselogincredentials)) {
			sysInfoHolder.HTTPUSEAUTHENTICATION=((JCheckBox)e.getSource()).isSelected();
			httpProxyAuthenNameTextField.setEnabled(sysInfoHolder.HTTPUSEAUTHENTICATION);
			httpProxyAuthenPasswordTextField.setEnabled(sysInfoHolder.HTTPUSEAUTHENTICATION);
		}
		else if(e.getActionCommand().equals(httpuselogincredentials))
			sysInfoHolder.HTTPUSEAUTHENTICATION=((JCheckBox)e.getSource()).isSelected();
		else if(e.getActionCommand().equals(httpuseserversentdelaytime))
			sysInfoHolder.USEDELAYTIMEFROMSERVER=((JCheckBox)e.getSource()).isSelected();
		else if(e.getActionCommand().equals(autoaccelerate)) {
			sysInfoHolder.AUTOACCELERATE=((JCheckBox)e.getSource()).isSelected();
			accelerateBorderPane.setVisible(sysInfoHolder.AUTOACCELERATE);
		}
		else if(e.getActionCommand().equals(autoupdate)) {
			sysInfoHolder.AUTOUPDATE=((JCheckBox)e.getSource()).isSelected();
			updatesBorderPane.setVisible(sysInfoHolder.AUTOUPDATE);
		}
		else if(e.getActionCommand().equals(updatestartup))
			sysInfoHolder.UPDATEOPTION=SystemInfoHolder.UPDATEEVERYTIME;
		else if(e.getActionCommand().equals(updatedaily))
			sysInfoHolder.UPDATEOPTION=SystemInfoHolder.UPDATEDAILY;
		else if(e.getActionCommand().equals(updateweekly))
			sysInfoHolder.UPDATEOPTION=SystemInfoHolder.UPDATEWEEKLY;
		else if(e.getActionCommand().equals(startdownloadingimmdediately))
			sysInfoHolder.DOWNLOADSTARTOPTION=SystemInfoHolder.STARTDOWNLOADSIMMEDIATELY;
		else if(e.getActionCommand().equals(addtolistbutdonotstart))
			sysInfoHolder.DOWNLOADSTARTOPTION=SystemInfoHolder.DONTSTARTDOWNLOADSIMMEDIATELY;
		else if(e.getActionCommand().equals(askwhethertostartimmediately))
			sysInfoHolder.DOWNLOADSTARTOPTION=SystemInfoHolder.ASKTOSTARTDOWNLOADS;
		else if (e.getActionCommand().equals(downloadtorrentasnormalfile))
			sysInfoHolder.TORRENTDOWNLOADSTARTOPTION=SystemInfoHolder.TORRENTNORMALDOWNLOAD;
		else if (e.getActionCommand().equals(downloadtorrentandstarttorrent))
			sysInfoHolder.TORRENTDOWNLOADSTARTOPTION=SystemInfoHolder.TORRENTTORRENTDOWNLOAD;
		else if (e.getActionCommand().equals(enableuploadspeedonseeding)) {
			sysInfoHolder.GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED=((JCheckBox)e.getSource()).isSelected();
			maxTorrentUploadSpeedOnSeedingField.setEnabled(sysInfoHolder.GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED);
		}
		else if (e.getActionCommand().equals(shutdownoption)) {
			if(((JComboBox)e.getSource()).getSelectedIndex() == 0) {
				sysInfoHolder.SHUTDOWNOPTION=SystemInfoHolder.DONOTHINGWHENDOWNLOADSFINISH;
			}
			if(((JComboBox)e.getSource()).getSelectedIndex() == 1) {
				if(! parentFrame.HasDownloadingInstances()) {
					JOptionPane.showMessageDialog(motherFrame, "Shutdown options can only be set when there are active downloads.", "Attention!!!", JOptionPane.ERROR_MESSAGE);
					((JComboBox)e.getSource()).setSelectedIndex(0);
				}
				else {
					sysInfoHolder.SHUTDOWNOPTION=SystemInfoHolder.SHUTDOWNSKWHENDOWNLOADSFINISH;
				}
			}
			if(((JComboBox)e.getSource()).getSelectedIndex() == 2) {
				if(! parentFrame.HasDownloadingInstances()) {
					JOptionPane.showMessageDialog(motherFrame, "Shutdown options can only be set when there are active downloads.", "Attention!!!", JOptionPane.ERROR_MESSAGE);
					((JComboBox)e.getSource()).setSelectedIndex(0);
				}
				else if(! SystemInfoHolder.IsWindows()) {
					//Get root password to run shutdown as sudo
					JPasswordField pwd = new JPasswordField();
					Object[] message = { "Shutdown needs to be done with sudo. Please provide your account password.\n", pwd };
					int resp = JOptionPane.showConfirmDialog(null, message, "Need Password!!!", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(resp == JOptionPane.OK_OPTION) {
						sysInfoHolder.ROOTPASSWORD = new String(pwd.getPassword());
						sysInfoHolder.SHUTDOWNOPTION=SystemInfoHolder.SHUTDOWNPCWHENDOWNLOADSFINISH;
					} 
					else {
						sysInfoHolder.SHUTDOWNOPTION=SystemInfoHolder.DONOTHINGWHENDOWNLOADSFINISH;
						((JComboBox)e.getSource()).setSelectedIndex(0);
					}
				}
				else {
					sysInfoHolder.SHUTDOWNOPTION=SystemInfoHolder.SHUTDOWNPCWHENDOWNLOADSFINISH;
				}
			}
		}
		else if(e.getActionCommand().equals(apply)) {
			ApplyChanges();
			orig_sysInfoHolder.CopyFrom(sysInfoHolder);
			((JButton)e.getSource()).setEnabled(false);
		}
		else if (e.getActionCommand().equals(removetorrentonshareratio)) {
			sysInfoHolder.TORRENTREMOVEONSHARERATIOOPTION = ((JCheckBox)e.getSource()).isSelected();
			if(sysInfoHolder.TORRENTREMOVEONSHARERATIOOPTION)
				torrentRemoveShareRatioField.setEnabled(true);
			else 
				torrentRemoveShareRatioField.setEnabled(false);
		}
		else if (e.getActionCommand().equals(removetorrentonseedingtime)) {
			sysInfoHolder.TORRENTREMOVEONSEEDTIMEOPTION = ((JCheckBox)e.getSource()).isSelected();
			if(sysInfoHolder.TORRENTREMOVEONSEEDTIMEOPTION)
				torrentRemoveSeedingTimeField.setEnabled(true);
			else 
				torrentRemoveSeedingTimeField.setEnabled(false);
		}
		else if(e.getActionCommand().equals(reset)) {
			if(ioutils.GetFromOkCancelMessage(motherFrame, "This will re-set all settings, clear download history and SKDownloader will exit. Any ongoing download will be cleared. Do you want to proceed?")) {
				motherFrame.dispose();
				parentFrame.ResetToDefaults();
			}
		}
		else if(e.getActionCommand().equals(ok)) {
			ApplyChanges();
			orig_sysInfoHolder.CopyFrom(sysInfoHolder);
			Close();
		}
		else if(e.getActionCommand().equals(cancel))
			Close();
		else {
			for(int i=0;i<DownloaderTheme.GetNumberOfThemes();i++) {
				if(e.getActionCommand().equals(DownloaderTheme.GetThemeName(i))) {
					sysInfoHolder.SELECTEDTHEME=i;
					final int j=i;
					SwingUtilities.invokeLater(new Runnable() {public void run() {
						// set new skin by class name
						try	{
							if(DownloaderTheme.GetTheme(j).equals("system"))
								ioutils.PrintMessage("You need to re-start SKDownloader for system theme to reflect.");
							else
								UIManager.setLookAndFeel(DownloaderTheme.GetTheme(j));
						} catch (Exception e){}
						parentFrame.GetFrame().repaint();
						motherFrame.repaint();
						SwingUtilities.updateComponentTreeUI( parentFrame.GetFrame() );
					}});
				}
			}
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : *Update
	 * Arguments : document event
	 * Return    : void
	 * Notes     : Functions for extracting text from fields as and when typed
	 * 
	 *************************************************************/
	public void changedUpdate(DocumentEvent e)
	{
		insertUpdate(e);
	}
	public void removeUpdate(DocumentEvent e)
	{
		insertUpdate(e);
	}
	public void insertUpdate(DocumentEvent e)
	{
		if(e.getDocument() == httpProxyServerDocument){
			sysInfoHolder.HTTPPROXYSERVER=httpProxyServerTextField.getText();
			System.getProperties().put("http.proxyHost", sysInfoHolder.HTTPPROXYSERVER);
		}
		else if(e.getDocument().equals(httpProxyNameDocument)){
			sysInfoHolder.HTTPPROXYUSER=httpProxyAuthenNameTextField.getText();
			System.getProperties().put("http.proxyUser", sysInfoHolder.HTTPPROXYUSER);
		}
		else if(e.getDocument().equals(httpProxyPasswordDocument)){
			sysInfoHolder.HTTPPROXYPASSWORD=new String(httpProxyAuthenPasswordTextField.getPassword());
			System.getProperties().put("http.proxyPassword", sysInfoHolder.HTTPPROXYPASSWORD);
		}
		else if(e.getDocument().equals(retryLimitTextField.getDocument())){
			if(retryLimitTextField.getText().matches("[1-9]\\d{0,10}")){
				sysInfoHolder.CONNECTRETRYLIMIT=Integer.parseInt(retryLimitTextField.getText());
			}
			else if(retryLimitTextField.getText().equals("0") ||
					retryLimitTextField.getText().equals("")){
				sysInfoHolder.CONNECTRETRYLIMIT=0;
				if(! retryLimitTextField.getText().equals(""))
					retryLimitTextField.setText("");
			}
			
			else{
				retryLimitTextField.setText(Integer.toString(sysInfoHolder.CONNECTRETRYLIMIT));
				if(retryLimitTextField.getText().length()>=0){
					retryLimitTextField.setCaretPosition(retryLimitTextField.getText().length());
				}
			}
		}
		else if(e.getDocument().equals(timeoutDelayTextField.getDocument())){
			if(timeoutDelayTextField.getText().matches("[1-9]\\d{0,10}")){
				sysInfoHolder.TIMEOUTDELAY=Integer.parseInt(timeoutDelayTextField.getText());
			}
			else if(timeoutDelayTextField.getText().equals("0") ||
					timeoutDelayTextField.getText().equals("")){
				sysInfoHolder.TIMEOUTDELAY=0;
				if(! timeoutDelayTextField.getText().equals(""))
					timeoutDelayTextField.setText("");
			}
			else{
				timeoutDelayTextField.setText(Integer.toString(sysInfoHolder.TIMEOUTDELAY));
				if(timeoutDelayTextField.getText().length()>=0){
					timeoutDelayTextField.setCaretPosition(timeoutDelayTextField.getText().length());
				}
			}
		}
		else if(e.getDocument().equals(accelerationMinSizeTextField.getDocument())) {
			if(accelerationMinSizeTextField.getText().matches("[1-9]\\d{0,10}")) {
				sysInfoHolder.MINSIZEFORACCELERATION=Integer.parseInt(accelerationMinSizeTextField.getText());
			}
			else if(accelerationMinSizeTextField.getText().equals("0") ||
					accelerationMinSizeTextField.getText().equals("")) {
				sysInfoHolder.MINSIZEFORACCELERATION=0;
				if(! accelerationMinSizeTextField.getText().equals(""))
					accelerationMinSizeTextField.setText("");
			}
			else {
				accelerationMinSizeTextField.setText(Integer.toString(sysInfoHolder.MINSIZEFORACCELERATION));
				if(accelerationMinSizeTextField.getText().length()>=0) {
					accelerationMinSizeTextField.setCaretPosition(accelerationMinSizeTextField.getText().length());
				}
			}
		}
		else if(e.getDocument().equals(maxTorrentDownloadSpeedField.getDocument())) {
			if(maxTorrentDownloadSpeedField.getText().matches("[1-9]\\d{0,10}")) {
				sysInfoHolder.MAXGLOBALTORRENTDOWNLOADSPEED=Integer.parseInt(maxTorrentDownloadSpeedField.getText());
			}
			else if(maxTorrentDownloadSpeedField.getText().equals("0") ||
					maxTorrentDownloadSpeedField.getText().equals("")) {
				sysInfoHolder.MAXGLOBALTORRENTDOWNLOADSPEED=0;
				if(! maxTorrentDownloadSpeedField.getText().equals(""))
					maxTorrentDownloadSpeedField.setText("");
			}
			else {
				maxTorrentDownloadSpeedField.setText(Integer.toString(sysInfoHolder.MAXGLOBALTORRENTDOWNLOADSPEED));
				if(maxTorrentDownloadSpeedField.getText().length()>=0) {
					maxTorrentDownloadSpeedField.setCaretPosition(maxTorrentDownloadSpeedField.getText().length());
				}
			}
		}
		else if(e.getDocument().equals(maxTorrentUploadSpeedField.getDocument())) {
			if(maxTorrentUploadSpeedField.getText().matches("[1-9]\\d{0,10}")) {
				sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEED=Integer.parseInt(maxTorrentUploadSpeedField.getText());
			}
			else if(maxTorrentUploadSpeedField.getText().equals("0") ||
					maxTorrentUploadSpeedField.getText().equals("")) {
				sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEED=0;
				if(! maxTorrentUploadSpeedField.getText().equals(""))
					maxTorrentUploadSpeedField.setText("");
			}
			else {
				maxTorrentUploadSpeedField.setText(Integer.toString(sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEED));
				if(maxTorrentUploadSpeedField.getText().length()>=0) {
					maxTorrentUploadSpeedField.setCaretPosition(maxTorrentUploadSpeedField.getText().length());
				}
			}
		}
		else if(e.getDocument().equals(torrentListenPortField.getDocument())) {
			if(torrentListenPortField.getText().matches("[1-9]\\d{0,10}")) {
				sysInfoHolder.TORRENT_LISTEN_PORT=Integer.parseInt(torrentListenPortField.getText());
			}
			else if(torrentListenPortField.getText().equals("0") ||
					torrentListenPortField.getText().equals("")) {
				sysInfoHolder.TORRENT_LISTEN_PORT=0;
				if(! torrentListenPortField.getText().equals(""))
					torrentListenPortField.setText("");
			}
			else {
				torrentListenPortField.setText(Integer.toString(sysInfoHolder.TORRENT_LISTEN_PORT));
				if(torrentListenPortField.getText().length()>=0) {
					torrentListenPortField.setCaretPosition(torrentListenPortField.getText().length());
				}
			}
		}
		else if(e.getDocument().equals(torrentRemoveShareRatioField.getDocument())) {
			if(torrentRemoveShareRatioField.getText().matches("^\\d+|\\.\\d+|\\d+\\.|\\d+\\.\\d+")) {
				sysInfoHolder.SHARERATIOTOREMOVETORRENT=Float.parseFloat(torrentRemoveShareRatioField.getText());
			}
			else if(torrentRemoveShareRatioField.getText().equals("0") ||
					torrentRemoveShareRatioField.getText().equals("")) {
				sysInfoHolder.SHARERATIOTOREMOVETORRENT=0;
				if(! torrentRemoveShareRatioField.getText().equals(""))
					torrentRemoveShareRatioField.setText("");
			}
			else {
				torrentRemoveShareRatioField.setText(Float.toString(sysInfoHolder.SHARERATIOTOREMOVETORRENT));
				if(torrentRemoveShareRatioField.getText().length()>=0) {
					torrentRemoveShareRatioField.setCaretPosition(torrentRemoveShareRatioField.getText().length());
				}
			}
		}
		else if(e.getDocument().equals(torrentRemoveSeedingTimeField.getDocument())) {
			if(torrentRemoveSeedingTimeField.getText().matches("[1-9]\\d{0,10}")) {
				sysInfoHolder.SEEDTIMETOREMOVETORRENT=(Integer.parseInt(torrentRemoveSeedingTimeField.getText()))*60;
			}
			else if(torrentRemoveSeedingTimeField.getText().equals("0") ||
					torrentRemoveSeedingTimeField.getText().equals("")) {
				sysInfoHolder.SEEDTIMETOREMOVETORRENT=0;
				if(! torrentRemoveSeedingTimeField.getText().equals(""))
					torrentRemoveSeedingTimeField.setText("");
			}
			else {
				torrentRemoveShareRatioField.setText(Float.toString(sysInfoHolder.SHARERATIOTOREMOVETORRENT));
				if(torrentRemoveShareRatioField.getText().length()>=0) {
					torrentRemoveShareRatioField.setCaretPosition(torrentRemoveShareRatioField.getText().length());
				}
			}
		}
		else if(e.getDocument().equals(maxTorrentUploadSpeedOnSeedingField.getDocument())) {
			if(maxTorrentUploadSpeedOnSeedingField.getText().matches("[1-9]\\d{0,10}")) {
				sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEEDONSEEDING=Integer.parseInt(maxTorrentUploadSpeedOnSeedingField.getText());
			}
			else if(maxTorrentUploadSpeedOnSeedingField.getText().equals("0") ||
					maxTorrentUploadSpeedOnSeedingField.getText().equals("")) {
				sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEEDONSEEDING=0;
				if(! maxTorrentUploadSpeedOnSeedingField.getText().equals(""))
					maxTorrentUploadSpeedOnSeedingField.setText("");
			}
			else {
				maxTorrentUploadSpeedOnSeedingField.setText(Float.toString(sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEEDONSEEDING));
				if(maxTorrentUploadSpeedOnSeedingField.getText().length()>=0) {
					maxTorrentUploadSpeedOnSeedingField.setCaretPosition(maxTorrentUploadSpeedOnSeedingField.getText().length());
				}
			}
		}
		
	}
	
	/*************************************************************
	 * 
	 * Function  : textValueChanged
	 * Arguments : text event
	 * Return    : void
	 * Notes     : Function for extracting text from fields as and when typed
	 * 
	 *************************************************************/
	public void textValueChanged(TextEvent  e)
	{
        // Action Listner handling code here:
		if(e.getSource() == httpProxyPortTextField)
		{
			if(httpProxyPortTextField.getText().matches("[1-9]\\d{0,10}"))
			{
				sysInfoHolder.HTTPPROXYPORT=Integer.parseInt(httpProxyPortTextField.getText());
			}
			else if(httpProxyPortTextField.getText().equals("0"))
			{
				sysInfoHolder.HTTPPROXYPORT=0;
				httpProxyPortTextField.setText("");
			}
			else if(httpProxyPortTextField.getText().equals(""))
			{
				sysInfoHolder.HTTPPROXYPORT=0;
			}
			else
			{
				httpProxyPortTextField.setText(Integer.toString(sysInfoHolder.HTTPPROXYPORT));
				if(httpProxyPortTextField.getText().length()>=0)
				{
					httpProxyPortTextField.setCaretPosition(httpProxyPortTextField.getText().length());
				}
			}
			System.getProperties().put("http.proxyPort", Integer.toString(sysInfoHolder.HTTPPROXYPORT));
		}
		
		
		
	}

	private void ApplyChanges() {
		if(orig_sysInfoHolder.MAXGLOBALTORRENTDOWNLOADSPEED != sysInfoHolder.MAXGLOBALTORRENTDOWNLOADSPEED) {
			COConfigurationManager.setParameter("Max Download Speed KBs", sysInfoHolder.MAXGLOBALTORRENTDOWNLOADSPEED);
		}
		if(orig_sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEED != sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEED) {
			COConfigurationManager.setParameter("Max Upload Speed KBs", sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEED);
		}
		if((sysInfoHolder.GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED) &&
			(orig_sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEEDONSEEDING != sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEEDONSEEDING)) {
			COConfigurationManager.setParameter("enable.seedingonly.upload.rate", sysInfoHolder.GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED);
			COConfigurationManager.setParameter("Max Upload Speed Seeding KBs", sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEEDONSEEDING);
		}
		orig_sysInfoHolder.OPTIONSFRAME_WIDTH=motherFrame.getWidth();
		orig_sysInfoHolder.OPTIONSFRAME_HEIGHT=motherFrame.getHeight();
	}
	
	/*************************************************************
	 * 
	 * Function  : Close
	 * Arguments : void
	 * Return    : void
	 * Notes     : Closes this frame.
	 * 
	 *************************************************************/
	public void Close() {
		orig_sysInfoHolder.OPTIONSFRAME_WIDTH=motherFrame.getWidth();
		orig_sysInfoHolder.OPTIONSFRAME_HEIGHT=motherFrame.getHeight();
		motherFrame.dispose();
	}

}