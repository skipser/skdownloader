/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    This is the downloader wrapper interface. Any type of download like url, torrent etc should
 *    create a wrapper implementing this interface and all the top level methods.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         07/17/10 - Creation
 *
 ****************************************************************************************/
package skdownloader.downloadmanager;

import skdownloader.core.DownloadStateHolder;

public interface GuiDownloaderWrapperInterface {
	DownloadStateHolder GetStateHolder();
	void Pause();
	void PauseForExit();
	void Resume();
	void Cancel(String string, int cancelled);
	void StartSilently();
	void Start();
	int GetUid();
	Object GetStatusAnimImage(int starting);
	void HideDetails();
	void ShowDetails();
}