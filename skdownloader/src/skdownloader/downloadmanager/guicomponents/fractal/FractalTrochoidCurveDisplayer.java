/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Trochoid curce animation display
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents.fractal;

import java.awt.*;
import java.awt.image.BufferedImage;

import javax.swing.*;

import java.lang.Math;

public class FractalTrochoidCurveDisplayer implements Runnable 
{
    double t, a, b, tF, aF, bF,
    s=150, r=1,x,y, spin=0, roll=0, rota=100, spinF, rollF, phas;
    float hue, leng, ratio=3, damp, damping, size;
    int count, idx, i, length;
    int drawer=1, c, ox, oy,
    xcentre=200,ycentre=200;
    double f;
    double sineArray[]=new double[1025];
    Color colours[]=new Color[256];
    boolean clearnow=false;
    public int axOne=100,axTwo=64, axbOne, axbTwo, ecOne, ecTwo;
    public Scrollbar dampBar, axOneBar, axTwoBar, ecOneBar, ecTwoBar, ratioBar, rotaBar, phasBar, lengthBar;
    public TextField inputBox;
    public CheckboxGroup drawType, symmGroup;
    public Checkbox wipeBox;
    public Label ratioLabel, lengthLabel;
    String theBox,symmName;
    Image offscreenImg;
    Graphics g;
    private boolean stopThread = false;
    private boolean pauseThread = false;
    
    private JPanel panel;
    //private int tabIndex;
    Thread trochoidThread;
    
    private boolean firsttime = true;
    
    BufferedImage bi = new BufferedImage(5, 5, BufferedImage.TYPE_INT_RGB);

    Graphics2D big;
    
    public FractalTrochoidCurveDisplayer(JPanel panel)
    {
    	this.panel=panel;
    	//this.tabIndex=index;
    }
    
    public void Show() 
    {
    	length=50;
    	leng = (float)(Math.PI*2*(float)length)/10;
    	axOne=100;
    	ecOne=0;
    	axTwo=75;
    	ecTwo=0;
    	ratio=404;
    	rota=80;
    	phas=500;
    	damp=20;
	
    	scrollUpdate();
    	//  Create lookup tables for colour and trig functions
    	drawer=1;
    	
    	for (i=0; i<colours.length; i++)
    	{
    		colours[i]= Color.getHSBColor(hue,(float)1.0,(float)1.0);
    		hue+=0.0039;
    	}
    	for (i=0; i<sineArray.length;i++)
    	{
    		sineArray[i]=Math.sin(i*Math.PI/512);
    	}

    	panel.setBackground (Color.black);
    
    	Start();
    }
    
    public void Start() 
    {
    	if (trochoidThread == null) 
    	{
    		trochoidThread = new Thread(this, "trochoid");
    		trochoidThread.start();
        }
    }


    public void scrollUpdate()
    {
    	if (ecOne==0) axbOne=axOne;
    	else axbOne = (int)((double)axOne*Math.sqrt(1-((float)(ecOne*ecOne)/10000)));
    	if (ecTwo==0) axbTwo=axTwo;
    	else axbTwo = (int)((double)axTwo*Math.sqrt(1-((float)(ecTwo*ecTwo)/10000)));
    	ratio = (float)(ratio/100);
    	leng = (float)(Math.PI*2*(float)length)/10;
    	rota = rota*0.0002;
    	phas = phas*0.00005;
    	damping = 1-(damp*damp/500000);
    	clearnow = true;
    }

    @SuppressWarnings("static-access")
	public void run() 
    {
    	while (trochoidThread != null) 
    	{
    		if(stopThread == true)
    			return;
    		if(pauseThread == false)
    			myPaint(g);
    		try {
    			trochoidThread.sleep(50);
    		} catch (InterruptedException e){        
    		}
    	}
    }

// The real substance of the program is contained here, in the paint method.
    public void myPaint (Graphics g) 
    {
    	g=panel.getGraphics();
    	xcentre=(panel.getWidth()/2);
    	ycentre=(panel.getHeight()/2);

    	if(firsttime)
    	{
    		bi = (BufferedImage) panel.createImage(panel.getWidth(), panel.getHeight());
    		big = bi.createGraphics();
        	big.setColor(Color.black);
    		firsttime=false;
    	}
    	big.setColor(Color.black);
    	big.fillRect(0,0,1024,768);
    	c=0;
    	f+=phas;
    	size=1;
    	for (t=0;t<leng;t+=rota)
    	{
    		x=(int)(xcentre+size*(axbOne*cosine(t)+axTwo*cosine(t*ratio-f)));
    		y=(int)(ycentre+size*(axOne*sine(t)+axbTwo*sine(t*ratio+f)));
    		big.setColor(colours[c%255]);
    		big.fillRect ((int)x,(int)y,drawer,drawer);
    		c++;
    		size*=damping;
    	}
    	g.drawImage(bi, 0, 0, null);
    }

    public void update (Graphics g) 
    {
    	myPaint (g);
    }

    public double sine (double angle)
    {
    	//return Math.sin(angle);
    	while (angle<0) angle+=Math.PI*2;
    	idx=((int)(angle*512/Math.PI))%1024;
    	return sineArray[idx];
    }

    public double cosine (double angle)
    {
    	//return Math.cos(angle);
    	while (angle<0) angle+=Math.PI*2;
    	idx=((int)(768+angle*512/Math.PI))%1024;
    	return sineArray[idx];
    }

    public void Stop() 
    {
    	trochoidThread = null;
    	stopThread=true;
    }
    
    public void Pause()
    {
    	pauseThread=true;
    }
    
    public void Resume()
    {
    	pauseThread=false;
    }
}