/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Image observer for showing animated images in table cell.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

public class DownloaderTheme
{
	private static String[] themes = {
		"org.pushingpixels.substance.api.skin.SubstanceModerateLookAndFeel",
        "org.pushingpixels.substance.api.skin.SubstanceNebulaLookAndFeel",
        "org.pushingpixels.substance.api.skin.SubstanceNebulaBrickWallLookAndFeel",
        "org.pushingpixels.substance.api.skin.SubstanceOfficeSilver2007LookAndFeel",
        "org.pushingpixels.substance.api.skin.SubstanceBusinessBlueSteelLookAndFeel",
        "org.pushingpixels.substance.api.skin.SubstanceBusinessBlackSteelLookAndFeel",
		"org.pushingpixels.substance.api.skin.SubstanceDustLookAndFeel",
		"org.pushingpixels.substance.api.skin.SubstanceDustCoffeeLookAndFeel",
		"org.pushingpixels.substance.api.skin.SubstanceCremeCoffeeLookAndFeel",
		"org.pushingpixels.substance.api.skin.SubstanceMistSilverLookAndFeel",
		"org.pushingpixels.substance.api.skin.SubstanceMistAquaLookAndFeel",
		"org.pushingpixels.substance.api.skin.SubstanceSaharaLookAndFeel",
		"system"};

	private static String[] themeNames = {
										"Moderate",
		                                "Nebula",
		                                "NebulaBrickWall",
		                                "Silver",
		                                "BusinessBlueSteel",
		                                "BuisinessBlack",
										"Dust",
										"DustCoffee",
										"CremeCoffee",
										"MistSilver",
										"MistAqua",
										"Sahara",
										"system",};

	public static String SYSTEM_TREEBG="0xDFE8EA";
	public static String SYSTEM_DOWNLOADTABLEBG="0xFFFFFF";
	
	public static String GetTheme(int index) {
		String theme = themes[0];
		if((index>0) && (index <=themes.length)) {
			theme = themes[index];
		}
		return theme;
	}
	
	public static String GetThemeName(int index) {
		String themeName = themeNames[0];
		if((index>0) && (index <=themeNames.length)) {
			themeName = themeNames[index];
		}
		return themeName;
	}
	
	public static int GetNumberOfThemes() {
		return themes.length;
	}
}