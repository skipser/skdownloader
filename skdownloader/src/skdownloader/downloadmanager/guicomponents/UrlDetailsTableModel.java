/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Table model for details frame table.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

//import java.util.*;
import java.util.ArrayList;
import javax.swing.JProgressBar;
import javax.swing.table.*;

import skdownloader.core.DownloadStateHolder;
import skdownloader.core.Common;

// This class manages the download table's data.
public class UrlDetailsTableModel extends AbstractTableModel //implements Observer 
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// These are the names for the table's columns.
    private static final String[] columnNames = {"#", "Size", "Progress", "Speed"};
    
    // These are the classes for each column's values.
    private static final Class<?>[] columnClasses = {String.class, String.class, JProgressBar.class, String.class};
    
    // The table's list of downloads.
    private ArrayList<DownloadStateHolder> threadList = new ArrayList<DownloadStateHolder>();
    
    DownloadStateHolder stateHolder;
 
    public void InitAll()
    {

    }
    
    // Add a thread collection to the table.
    public void addThread(DownloadStateHolder stateHolder)
    {
    	this.stateHolder=stateHolder;
    	if((stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) ||
    	   (stateHolder.GetStatus() == DownloadStateHolder.STARTING) ||
    	   (stateHolder.GetStatus() == DownloadStateHolder.PAUSED))
    	{
    		for(int i=0; i<stateHolder.GetNumConnections(); i++)
    		{
    			threadList.add(stateHolder);
    			fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
    		}

    		// Register to be notified when the downloader state holder changes.
    		//stateHolder.addObserver(this);
    	}
    }
    
    /*************************************************************
	 * 
	 * Function  : reloadThread
	 * Arguments : void
	 * Return    : void
	 * Notes     : If the number of connections has changed, we need 
	 *             to re-do the table.
	 * 
	 *************************************************************/
    public void reloadThread(DownloadStateHolder stateHolder)
    {
    	//Disable observer
    	//stateHolder.deleteObserver(this);
    	
    	//Remove rows
    	int l = threadList.size();
    	for(int i=l-1; i>=0; i--)
    	{
    		this.fireTableRowsDeleted(i, i);
    	}
    	threadList.clear();

    	//Add rows once again.
    	if((stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) ||
    	   (stateHolder.GetStatus() == DownloadStateHolder.STARTING) ||
    	   (stateHolder.GetStatus() == DownloadStateHolder.PAUSED))
    	{
    		for(int i=0; i<stateHolder.GetNumConnections(); i++)
    		{
    			threadList.add(stateHolder);
    			fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
    		}
    	}
    }

    //Get table's column count.
    public int getColumnCount() 
    {
        return columnNames.length;
    }
    
    // Get a column's name.
    public String getColumnName(int col) 
    {
        return columnNames[col];
    }
    
    // Get a column's class.
    public Class<?> getColumnClass(int col) 
    {
        return columnClasses[col];
    }
    
    // Get table's row count.
    public int getRowCount() 
    {
        return threadList.size();
    }
    
    // Get value for a specific row and column combination.
    public Object getValueAt(int row, int col) {        
    	//GuiUrlDownloaderWrapper downloader = (GuiUrlDownloaderWrapper) threadList.get(row);
        switch (col) {
        case 0: //thread no.
        	return (""+(row+1));
        case 1: //Size
        	int size = stateHolder.GetThreadFileSize(row);
        	return (size < 0) ? "" : Common.SizetoString(size);
        case 2: //Progress
        	//logger.PrintLog("DEBUG", "Got progress for "+row+", "+col+"- "+stateHolder.GetThreadProgress(row));
        	if(stateHolder.GetFileSize() == -2)
            	return new Float(200);
            else
            	return new Float(stateHolder.GetThreadProgress(row));
        case 3: //Speed
        	return stateHolder.GetThreadDownloadSpeedString(row);
        }
        return "";
    }
}
