package skdownloader.downloadmanager.guicomponents;

import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class ManagerTableHeader extends JTableHeader {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int unmovableColumn = -1;

	public ManagerTableHeader(TableColumnModel model) {
		super(model);
	}

	public void setUnmovableColumn(int viewIndex) {
		unmovableColumn = viewIndex;
	}

	@Override
	public TableColumn getDraggedColumn() {
		TableColumn draggedColumn = super.getDraggedColumn();
		if (draggedColumn != null) {
			int modelIndex = draggedColumn.getModelIndex();
			int index = getTable().convertColumnIndexToView(modelIndex);
			
			/* If the user tries to move an unmovable column don�t allow it */
			if (index == unmovableColumn) {
				super.setDraggedColumn(null);
			}
			
			/* if the current dragged column is dragged towards right and the next column is unmovable one, don�t allow it */
			else if (super.draggedDistance > 0) {
				if (index + 1 == unmovableColumn) {
					super.setDraggedColumn(null);
				}
			}

			/* if the current dragged column is dragged towards left and the next column is unmovable one, don�t allow it */
			else if (super.draggedDistance <0)
			if (index - 1 == unmovableColumn) {
				super.setDraggedColumn(null);
			}
		}
		return super.getDraggedColumn();
	}
} 