/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Table model for downloads table.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import skdownloader.core.*;
import skdownloader.downloadmanager.*;

// This class manages the download table's data.
public class ManagerTableModel extends AbstractTableModel //implements Observer 
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// These are the names for the table's columns.
    private String[] columnNames;
    
    // These are the classes for each column's values.
	private Class<?>[] columnClasses;
    
    // The table's list of downloads.
    private ArrayList<GuiDownloaderWrapperInterface> downloadList = new ArrayList<GuiDownloaderWrapperInterface>();
    
    MainFrame mainFrame;
    private SystemInfoHolder sysInfoHolder;
    DownloadsTableColumnManager colManager;
 
    public void InitAll(MainFrame mainFrame, SystemInfoHolder sysInfoHolder, DownloadsTableColumnManager colManager) {
    	// index  Description
    	//     0  Downloading staus label
    	//     1  Paused status label
    	//     2  Cancelled status label
    	//     3  Finished status label
    	this.mainFrame=mainFrame;
    	this.colManager=colManager;
    	this.sysInfoHolder=sysInfoHolder;
    	columnNames = colManager.GetColNamesArr();
    	columnClasses = colManager.GetColClassArr();
    }
   
    // Get a download for the specified row.
    public synchronized GuiDownloaderWrapperInterface GetDownload(int row) {
    	return (GuiDownloaderWrapperInterface) downloadList.get(row);
    }

    // Add a new download to the table.
    public synchronized void AddDownload(GuiDownloaderWrapperInterface downloader) 
    {
        // Add download to download list
        downloadList.add(downloader);
        
        // Fire table row insertion notification to table.
        fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
    }
    
    // Remove a download from the list.
    public synchronized void ClearDownload(GuiDownloaderWrapperInterface downloader){
    	//Find row to be removed.
    	int row = GetRowForDownload(downloader.GetStateHolder());
    	
    	if(row == -1)
    		return;
    	
    	//Remove row from table
        downloadList.remove(row);
        
        // Fire table row deletion notification to table.
        fireTableRowsDeleted(row, row);
    }

    //Get table's column count.
    public int getColumnCount() {
        //return columnNames.length;
    	return sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE;
    }
    
    // Get a column's name.
    public String getColumnName(int col){
        return columnNames[col];
    }
    
    // Get a column's class.
    public Class<?> getColumnClass(int col){
        return columnClasses[col];
    }
    
    // Get table's row count.
    public int getRowCount() {
        return downloadList.size();
    }
    
    public int GetRowForDownload(DownloadStateHolder stateHolder){
    	return downloadList.indexOf(stateHolder.GetDownloaderObject());
    }
    
    // Get value for a specific row and column combination.
    public Object getValueAt(int row, int col){
    	GuiDownloaderWrapperInterface downloader = null;
    	try {
    		downloader = (GuiDownloaderWrapperInterface) downloadList.get(row);
    	}catch(Exception e) {
    		return "";
    	}
    	
    	if(downloader == null)
    		return "";
    	
    	int colType = colManager.GetColType(col);
    	
        switch (colType) {
        case DownloadsTableColumnManager.COL_ICON_ID: // Status
        	if(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.STARTING) {
        		return downloader.GetStatusAnimImage(DownloadStateHolder.STARTING);
        	}
        	if(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.DOWNLOADING) {
        		return downloader.GetStatusAnimImage(DownloadStateHolder.DOWNLOADING);
        	}
        	else if(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.PAUSED) {
        		return downloader.GetStatusAnimImage(DownloadStateHolder.PAUSED);
        	}
        	else if(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.PAUSEDFOREXIT) {
        		return downloader.GetStatusAnimImage(DownloadStateHolder.PAUSEDFOREXIT);
        	}
        	else if(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.PAUSEDFORERROR) {
        		return downloader.GetStatusAnimImage(DownloadStateHolder.PAUSEDFORERROR);
        	}
        	else if(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.CANCELLED) {
        		return downloader.GetStatusAnimImage(DownloadStateHolder.CANCELLED);
        	}
        	else if(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.COMPLETED) {
        		return downloader.GetStatusAnimImage(DownloadStateHolder.COMPLETED);
        	}
        	else if(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.ERROR) {
        		return downloader.GetStatusAnimImage(DownloadStateHolder.ERROR);
        	}
        	else if(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.SEEDING) {
        		return downloader.GetStatusAnimImage(DownloadStateHolder.SEEDING);
        	}   
        	else {
        		return downloader.GetStatusAnimImage(DownloadStateHolder.CANCELLED);
        	}
        case DownloadsTableColumnManager.COL_FILE_ID: // File name
        	return downloader.GetStateHolder().GetFileName();
        case DownloadsTableColumnManager.COL_SIZE_ID: // Size
        	long size = downloader.GetStateHolder().GetFileSize();
        	return (size < 0) ? "" : Common.SizetoString(size);
        case DownloadsTableColumnManager.COL_ETA_ID: //Time remaining
        	return downloader.GetStateHolder().getTimeRemainingString();
        case DownloadsTableColumnManager.COL_PROGRESS_ID: // Progress
        	//logger.PrintLog("DEBUG", "Thread progress - "+downloader.GetStateHolder().GetThreadProgress(1));
        	if(downloader.GetStateHolder().GetFileSize() <= 0)
        		return new Float(0);
        	else if(downloader.GetStateHolder().GetProgress() <= 0)
        		return new Float(0);
        	else
        		return new Float(downloader.GetStateHolder().GetProgress());
        		
        case DownloadsTableColumnManager.COL_DOWNSPEED_ID: // Speed
        	return downloader.GetStateHolder().GetDownloadSpeedString();
        case DownloadsTableColumnManager.COL_UPSPEED_ID: // Speed
        	return downloader.GetStateHolder().GetUploadSpeedString();
        case DownloadsTableColumnManager.COL_STATUS_ID: // Status
        	return downloader.GetStateHolder().GetStatusString();
        case DownloadsTableColumnManager.COL_DOWNLOADED_ID: //Download speed
        	return Common.SizetoString(downloader.GetStateHolder().GetDownloadedSize());
        case DownloadsTableColumnManager.COL_SEEDS_ID: //Seeders
        	if(downloader.GetStateHolder().GetDownloadType() == Common.TYPE_TORRENT)
        		return downloader.GetStateHolder().GetNumberOfSeeders();
        	else
        		return "--";
        case DownloadsTableColumnManager.COL_PEERS_ID: //leeches
        	if(downloader.GetStateHolder().GetDownloadType() == Common.TYPE_TORRENT)
        		return downloader.GetStateHolder().GetNumberOfLeechers();
        	else 
        		return "--";
        case DownloadsTableColumnManager.COL_SHARERATIO_ID: //Seeders
        	if(downloader.GetStateHolder().GetDownloadType() == Common.TYPE_TORRENT)
        		return Float.toString(downloader.GetStateHolder().GetShareRatio());
        	else
        		return "--";
        }
        return "";
    }
}
