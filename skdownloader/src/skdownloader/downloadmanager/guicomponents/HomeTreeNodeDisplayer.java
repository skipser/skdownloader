/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Addon class for view state tree.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import java.util.ArrayList;

public class HomeTreeNodeDisplayer {
	public ArrayList<?> arrLst;
	public int view;
	
	public static final String VIEWS[] = {"Home"};
    
	// These are the status codes.
	public static final int HOME = 0;
	
	public HomeTreeNodeDisplayer(int view) {
		this.view=view;
	}
	
	public String toString() {
		return (VIEWS[view]);
	}
	
	public int GetId() {
		return view;
	}
}