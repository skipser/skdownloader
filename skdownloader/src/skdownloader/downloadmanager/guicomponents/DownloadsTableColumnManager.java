package skdownloader.downloadmanager.guicomponents;

import javax.swing.ImageIcon;
import javax.swing.JProgressBar;

import skdownloader.core.Common;
import skdownloader.downloadmanager.SystemInfoHolder;



public class DownloadsTableColumnManager {
	public static final int COL_ICON_ID=0;
	public static final int COL_FILE_ID=1;
	public static final int COL_SIZE_ID=2;
	public static final int COL_ETA_ID=3;
	public static final int COL_PROGRESS_ID=4;
	public static final int COL_DOWNSPEED_ID=5;
	public static final int COL_STATUS_ID=6;
	public static final int COL_UPSPEED_ID=7;
	public static final int COL_SEEDS_ID=8;
	public static final int COL_PEERS_ID=9;
	public static final int COL_SHARERATIO_ID=10;
	public static final int COL_TRACKER_STATUS_ID=11;
	public static final int COL_DOWNLOADED_ID=12;
	//IF any thing is added here, make sure to add it to the default order and width strings below.
	
	public static int maxCols = 13;
	//private int visibleCols;
	
	private static String[] colNamesRef = {null, 
										   "File",
										   "Size",
										   "Est. Time",
										   "Progress",
										   "Speed",
										   "Status",
										   "Up Speed",
										   "Seeds",
										   "Peers",
										   "Share Ratio",
										   "Tracker Status",
										   "Downloaded"};
	
	@SuppressWarnings("rawtypes")
	private static Class[] colClassesRef = {ImageIcon.class,
		   									String.class,
		   									String.class,
		   									String.class,
		   									JProgressBar.class,
		   									String.class,
		   									String.class,
		   									String.class,
		   									String.class,
		   									String.class,
		   									String.class,
		   									String.class,
		   									String.class};
	
	private String[] colNames;
	private int[] colWidths;
	@SuppressWarnings("rawtypes")
	private Class[] colClasses;
	private int[] colOrder;
	private int[] tmpColOrder;
	private int[] colIndex; //This will show the actual column number occupied by each column.
	private SystemInfoHolder sysInfoHolder;
	
	public DownloadsTableColumnManager(SystemInfoHolder sysInfoHolder) {
		this.sysInfoHolder = sysInfoHolder;
		//visibleCols = sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE;
		if(sysInfoHolder.DOWNLOADSTABLE_WIDTH_ARR_STR.equals("")) {
			sysInfoHolder.DOWNLOADSTABLE_WIDTH_ARR_STR="22 207 58 70 143 67 70 47 42 51 70 50 65 ";
		}
		colWidths = new int[maxCols];
		Common.CopyIntArray(Common.String2IntArr(sysInfoHolder.DOWNLOADSTABLE_WIDTH_ARR_STR), colWidths);
		colNames = new String[maxCols];
		colClasses = new Class[maxCols];
		colIndex = new int[maxCols];
		colOrder = new int[maxCols];
		tmpColOrder = new int[maxCols];
		if(sysInfoHolder.DOWNLOADSTABLE_ORDER_ARR_STR.equals(""))
			sysInfoHolder.DOWNLOADSTABLE_ORDER_ARR_STR="0 1 2 3 4 5 6 8 9 12 11 10 7 ";

		Common.CopyIntArray(Common.String2IntArr(sysInfoHolder.DOWNLOADSTABLE_ORDER_ARR_STR), colOrder);
		Common.CopyIntArray(Common.String2IntArr(sysInfoHolder.DOWNLOADSTABLE_ORDER_ARR_STR), tmpColOrder);
		for(int i=0; i<maxCols; i++) {
			colIndex[colOrder[i]]=i;
		}
		for(int i=0; i<maxCols; i++) {
			colNames[i] = colNamesRef[colOrder[i]];
			colClasses[i] = colClassesRef[colOrder[i]];
		}
	}
	
	public int GetActColIndex(int index) {
		return Common.GetIndexInIntArr(tmpColOrder, colOrder[index]);
	}
	
	@SuppressWarnings("rawtypes")
	public Class[] GetColClassArr() {
		return this.colClasses;
	}
	
	public String[] GetColNamesArr() {
		return this.colNames;
	}
	
	public String GetColName(int i) {
		return colNamesRef[i];
	}
	
	public int[] GetColWidthArr() {
		return this.colWidths;
	}
	
	public int GetColWidth(int i) {
		return colWidths[i];
	}
	//column type, width.
	public void SetColWidth(int i, int val) {
		colWidths[i] = val;
	}
	
	public int GetColType(int i) {
		return this.colOrder[i];
	}
	public int GetActColType(int i) {
		return this.tmpColOrder[i];
	}
	
	public int[] GetColOrderArr1() {
		return this.tmpColOrder;
	}
	
	public void InterChangeColsOrder(int i, int j) {
		int tmp = tmpColOrder[j];
		tmpColOrder[j]=tmpColOrder[i];
		tmpColOrder[i]=tmp;
	}
	
	public int GetColIndex(int i) {
		return colIndex[i];
	}
	
	public Class<?> GetColClass(int i) {
		return colClassesRef[colOrder[i]];
	}
	
	public void ResetArr(int[] arr) {
		//1. Check for duplicate entries and remove.
		for(int i=0; i< maxCols; i++) {
			arr[i]=i;
		}
	}
	
	public boolean IsVisible(int col) {
		boolean visible=false;
		for(int i=0; i< sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE; i++) {
			if(tmpColOrder[i] == col)
				visible=true;
		}
		return visible;
	}
	
	public void MakeVisible(int col) {
		if(sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE == maxCols)
			return;
		
		sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE++;
		if(colOrder[sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE-1] != col) {
			int tmp = Common.GetIndexInIntArr(colOrder, col);
			if(tmp == -1) {
				sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE--;
				return;
			}
			colOrder[tmp]=colOrder[sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE-1];
			colOrder[sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE-1]=col;
		}
		if(tmpColOrder[sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE-1] != col) {
			int tmp = Common.GetIndexInIntArr(tmpColOrder, col);
			if(tmp == -1) {
				sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE--;
				return;
			}
			tmpColOrder[tmp]=tmpColOrder[sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE-1];
			tmpColOrder[sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE-1]=col;
		}
		
		for(int i=0; i<maxCols; i++) {
			colNames[i] = colNamesRef[colOrder[i]];
			colClasses[i] = colClassesRef[colOrder[i]];
		}
	}
	
	public void MakeInvisible(int col) {
		int[] tmp = new int[maxCols];
		int j=0;
		for(int i=0; i<sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE; i++ ) {
			if(tmpColOrder[i] != col) {
				tmp[j] = tmpColOrder[i];
				j++;
			}
		}
		tmp[sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE-1] = col;
		if(sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE < maxCols) {
			for(int i=sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE; i< maxCols; i++) {
				tmp[i]=tmpColOrder[i];
			}
		}
		tmpColOrder=tmp;
		
		tmp = new int[maxCols];
		j=0;
		for(int i=0; i<sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE; i++ ) {
			if(colOrder[i] != col) {
				tmp[j] = colOrder[i];
				j++;
			}
		}
		tmp[sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE-1] = col;
		if(sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE < maxCols) {
			for(int i=sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE; i< maxCols; i++) {
				tmp[i]=colOrder[i];
			}
		}
		colOrder=tmp;
		sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE--;
		for(int i=0; i<maxCols; i++) {
			colNames[i] = colNamesRef[colOrder[i]];
			colClasses[i] = colClassesRef[colOrder[i]];
		}
	}
}