/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Table model for downloads table.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JProgressBar;
import javax.swing.table.AbstractTableModel;

import org.gudy.azureus2.core3.disk.DiskManagerFileInfo;


import skdownloader.core.Common;
import skdownloader.core.DownloadStateHolder;
import skdownloader.downloadmanager.*;

// This class manages the download table's data.
public class TorrentFileDetailsTableModel extends AbstractTableModel //implements Observer 
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// These are the names for the table's columns.
    private static final String[] columnNames = {"", "File Name", "Size", "Progress", "Priority"};
    
    // These are the classes for each column's values.
    private static final Class<?>[] columnClasses = {JCheckBox.class, String.class, String.class, JProgressBar.class, JComboBox.class};
    //private static final Class<?>[] columnClasses = {String.class, String.class, String.class};

    private ArrayList<DiskManagerFileInfo> fileList;
    
    @SuppressWarnings("unused")
	private GuiUserIOUtils ioutils;
    private DownloadStateHolder stateHolder;
    MainFrame mainFrame;
 
    public void InitAll(GuiUserIOUtils ioutils, DownloadStateHolder stateHolder) {
    	this.ioutils = ioutils;
    	this.stateHolder=stateHolder;
    	this.fileList = new ArrayList<DiskManagerFileInfo>();
    }
   
    // Get a file for the specified row.
    public DiskManagerFileInfo GetFileInfoForRow(int row) {
    	return  fileList.get(row);
    }

    // Add a new download to the table.
    public synchronized void AddFileList(DiskManagerFileInfo[] f) {
        // Add task to download list
    	fileList.clear();
    	for(int i=0; i<f.length; i++) {
    		this.fileList.add(f[i]);
    		// Fire table row insertion notification to table.
    		fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
    	}
    }
    
    //If we don't have any entries, add them from download manager. Else, just update the table.
    public synchronized void AddOrUpdateFileList() {
    	if(fileList.size() <= 0) {
    		AddFileList(stateHolder.GetTorrentDownloadManager().getDiskManagerFileInfoSet().getFiles());
    	}
    	else {
    		for(int i=0; i<getRowCount(); i++) {
    			this.fireTableRowsUpdated(i, 0);
    		}
    	}
    }

    
    public int GetRowForFileInfo(DiskManagerFileInfo f) {
    	return fileList.indexOf(f);
    }
    
    public synchronized void UpdateMapStatus(DiskManagerFileInfo f) {
    	int row = fileList.indexOf(f);
    	this.fireTableCellUpdated(row, 3);
    }
    
    public synchronized void ClearTable() {
    	if(this.getRowCount() >0)
    		this.fireTableRowsDeleted(0, this.getRowCount()-1);
    	this.fileList.clear();
    }

    //Get table's column count.
    public int getColumnCount() {
        return columnNames.length;
    }
    
    // Get a column's name.
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    // Get a column's class.
    public Class<?> getColumnClass(int col) {
        return columnClasses[col];
    }
    
    // Get table's row count.
    public int getRowCount() {
        return fileList.size();
    }
    
    
    // Get value for a specific row and column combination.
    public synchronized Object getValueAt(int row, int col) {
    	DiskManagerFileInfo info = GetFileInfoForRow(row);
    	if(info == null)
    		return "";
        switch (col) {
        case 0: // Skipped
        	return (info.isSkipped() ? false : true);
        case 1: // Name
        	return info.getFile(true).getName();
        case 2: // Size
        	return Common.SizetoString(info.getLength());
        case 3: //percent done
        	return (float)(info.getDownloaded()*100/info.getLength());
        case 4: //priority
        	return GetPriorityString(info.getPriority());
        }
        return "";
    }
    
    public synchronized void setValueAt( Object val, int row, int col) {
    	if(col == 0) {
    		val=((Boolean)val ? false : true);
    		GetFileInfoForRow(row).setSkipped((Boolean) val);
    		stateHolder.SetInExcludedList(row, (Boolean) val);
    		if((Boolean)val == false) {
    			if(stateHolder.GetStatus()==DownloadStateHolder.SEEDING && ! stateHolder.GetTorrentDownloadManager().isDownloadComplete(false))
    				stateHolder.SetDownloadStatus(DownloadStateHolder.DOWNLOADING);
    		}
    	}
    	else if(col == 4) {
    		if(((String)val).equals("normal")) {
    			GetFileInfoForRow(row).setPriority(0);
    		}
    		else if(((String)val).equals("high")) {
    			GetFileInfoForRow(row).setPriority(1);
    		}
    	}
    }
    
    private String GetPriorityString(int p) {
    	if(p==0)
    		return "normal";
    	else if(p==1)
    		return "high";
		return "";
    }
    
    
    public boolean isCellEditable(int row, int col) {
        return true;
    }


}
