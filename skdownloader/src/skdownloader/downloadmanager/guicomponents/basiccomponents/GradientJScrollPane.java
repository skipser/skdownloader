package skdownloader.downloadmanager.guicomponents.basiccomponents;

import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JScrollPane;

public class GradientJScrollPane extends JScrollPane
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4557885414493611633L;

	public GradientJScrollPane(Component o)
	{
		super(o);
	}
	
	public GradientJScrollPane()
	{
		super();
	}
	
	protected void paintComponent( Graphics g ) 
	{
		g=this.getViewport().getGraphics();
		if ( !isOpaque( ) )
		{
			super.paintComponent( g );
		}
		
		Graphics2D g2d = (Graphics2D)g;
		int w = getWidth( );
		int h = getHeight( );
		Color color1 = new Color(0xfcfcfc); //getBackground( );
		Color color2 = new Color(0xb7b7b7); //color1.darker( );

		// Paint a gradient from top to bottom
		GradientPaint gp = new GradientPaint(
		    0, 0, color1,
		    0, h, color2 );

		g2d.setPaint( gp );
		g2d.fillRect( 0, 0, w, h );
		
		setOpaque( false );
		super.paintComponent( g );
		setOpaque( true );
	}
}