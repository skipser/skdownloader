package skdownloader.downloadmanager.guicomponents.basiccomponents;

import java.util.regex.Pattern;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

public class FloatTextField extends JTextField {

	private static final long serialVersionUID = 1L;

	@Override
    protected Document createDefaultModel() {
        return new NumericDocument();
    }
    
    public FloatTextField(int len, int round) {
    	super(len);
    	((NumericDocument)super.getDocument()).setLength(len,round);
    }

    private static class NumericDocument extends PlainDocument {
		private static final long serialVersionUID = 1L;

		private int len;
		private int round;
		public void setLength(int len, int round) {
			this.len=len;
			this.round=round;
		}
        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
    		Pattern DIGITS = Pattern.compile("^\\.|\\d*|\\d+\\.(\\d+)?$");
            Pattern DOT=Pattern.compile("^.*\\..*$");
            Pattern ROUNDING=Pattern.compile("^.*\\.\\d{"+round+",}$");
            
            //Check if length exceedes
        	if(super.getText(0, offs).length() >= len)
        		return;
        	
        	//Check if rounding exceedes
        	if(ROUNDING.matcher(super.getText(0, offs)).matches())
        		return;
        	
        	//Check if multiple dots tried
        	if(DOT.matcher(super.getText(0, offs)).matches()) {
        		if(DOT.matcher(str).matches())
        			return;
        	}
        	
            if (str != null && DIGITS.matcher(str).matches())
                super.insertString(offs, str, a);
        }
    }
    
}