package skdownloader.downloadmanager.guicomponents.basiccomponents;

import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.awt.Color;

public class ColoredTitledBorder extends TitledBorder
{
	private static final long serialVersionUID = 1L;
	
	public ColoredTitledBorder(Color color, String title) 
	{
		super(new LineBorder(color));
		this.setTitle(title);
		this.setTitleColor(Color.BLACK);
	}

	public ColoredTitledBorder(Border border) 
	{
		super(border);
	}
	
	
}