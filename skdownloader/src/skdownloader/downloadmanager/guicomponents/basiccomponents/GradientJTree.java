package skdownloader.downloadmanager.guicomponents.basiccomponents;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;


public class GradientJTree extends JTree
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4557885414493611633L;
	//SystemInfoHolder sysInfoHolder;
	private Color color1, color2;
	
	public GradientJTree(DefaultMutableTreeNode o, Color color1, Color color2)
	{
		super(o);
		this.color1=color1;
		this.color2=color2;
		//this.sysInfoHolder = sysInfoHolder;
	}
	
	public GradientJTree()
	{
	}
	
    public GradientJTree( java.util.Hashtable<?,?> value )
    {
        super( value );
    }
    public GradientJTree( Object[] value )
    {
        super( value );
    }
    public GradientJTree( javax.swing.tree.TreeModel newModel )
    {
        super( newModel );
    }
    public GradientJTree( javax.swing.tree.TreeNode root )
    {
        super( root );
    }
    public GradientJTree( javax.swing.tree.TreeNode root,
        boolean asksAllowsChildren )
    {
        super( root, asksAllowsChildren );
    }
    public GradientJTree( java.util.Vector<?> value )
    {
        super( value );
    }

	protected void paintComponent( Graphics g ) 
	{
		if ( ! isOpaque( ))
        {
            super.paintComponent( g );
            return;
        }

		Graphics2D g2d = (Graphics2D)g;
		int w = getWidth( );
		int h = getHeight( );

		// Paint a gradient from top to bottom
		GradientPaint gp = new GradientPaint(
		    0, 0, color1,
		    0, h, color2 );

		g2d.setPaint( gp );
		g2d.fillRect( 0, 0, w, h );

        // Paint component
        setOpaque( false );
        super.paintComponent( g2d );
        setOpaque( true );

	}
}