package skdownloader.downloadmanager.guicomponents.basiccomponents;

import java.awt.event.ActionListener;

import javax.swing.Timer;

import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.AlphaComposite;

public class FadingMirror extends JPanel implements ActionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private float opacity = 0;
    private Timer fadeTimer;
    private String state;

    public void Fade() {
    	opacity=0f;
    	state="fade";
        fadeTimer =  new Timer(75,this);
        fadeTimer.setInitialDelay(0);
        fadeTimer.start();
    }
    
    public void Show() {
    	opacity=1;
    	state="show";
        fadeTimer =  new Timer(75,this);
        fadeTimer.setInitialDelay(0);
        fadeTimer.start();
    }
    
    
    public void actionPerformed(ActionEvent e) {
    	if(state.equals("fade"))
    	{
    		opacity += .03;
    		if(opacity > 1) 
    		{
    			opacity = 1;
    			fadeTimer.stop();
    			fadeTimer = null;
    		}
    		repaint();
    	}
    	else
    	{
    		opacity -= .03;
    		if(opacity < .0f) 
    		{
    			opacity = .0f;
    			fadeTimer.stop();
    			fadeTimer = null;
    		}
    		repaint();
    	}
    	
    }
    
    public void paintComponent(Graphics g) {
    	//g=this.getGraphics();
        ((Graphics2D) g).setComposite(
                AlphaComposite.getInstance(AlphaComposite.SRC_OVER, opacity));
        g.setColor(getBackground());
        g.fillRect(0,0,getWidth(),getHeight());
    }
}