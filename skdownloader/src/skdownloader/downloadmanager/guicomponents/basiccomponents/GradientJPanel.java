package skdownloader.downloadmanager.guicomponents.basiccomponents;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;


public class GradientJPanel extends JPanel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4557885414493611633L;
	//SystemInfoHolder sysInfoHolder;
	private Color color1, color2;
	
	public GradientJPanel(Color color1, Color color2)
	{
		//this.sysInfoHolder = sysInfoHolder;
		this.color1=color1;
		this.color2=color2;
	}

	protected void paintComponent( Graphics g ) 
	{
		if ( !isOpaque( ) )
		{
			super.paintComponent( g );
			return;
		}
		
		
		Graphics2D g2d = (Graphics2D)g;
		super.paintComponent( g );
		
		int w = getWidth( );
		int h = getHeight( );
		
		// Paint a gradient from top to bottom
		GradientPaint gp = new GradientPaint(
		    0, 0, color1,
		    0, h, color2 );

		g2d.setPaint( gp );
		g2d.fillRect( 0, 0, w, h );
		Color tmp = g2d.getColor();
		g2d.setColor(color1.darker());
		g2d.drawLine(0, 0, w, 0);
		g2d.setColor(tmp);
		
		setOpaque( false );
		super.paintComponent( g );
		setOpaque( true );
	}
}