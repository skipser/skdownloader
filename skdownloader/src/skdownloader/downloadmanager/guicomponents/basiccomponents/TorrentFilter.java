/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Filter to select only torrent files in torrent file chooser
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         07/17/10 - Creation
 *
 ****************************************************************************************/
package skdownloader.downloadmanager.guicomponents.basiccomponents;

import java.io.File;
import javax.swing.filechooser.FileFilter;


/* ImageFilter.java is used by FileChooserDemo2.java. */
public class TorrentFilter extends FileFilter {
	static String torrentExt = "torrent";

    //Accept all directories and all gif, jpg, tiff, or png files.
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }

        String extension = getExtension(f);
        if (extension != null) {
            if (extension.equals(torrentExt)) {
                    return true;
            } else {
                return false;
            }
        }
        return false;
    }
    
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }


    //The description of this filter
    public String getDescription() {
        return "Torrent Files";
    }
}
