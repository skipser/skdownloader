package skdownloader.downloadmanager.guicomponents.basiccomponents;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JButton;

public class GradientJButton extends JButton
{
	private static final long serialVersionUID = 1L;
	
	private Color color1;
	private Color color2;
	
	public GradientJButton(String label, Color color1, Color color2)
	{
		//this.sysInfoHolder = sysInfoHolder;
		super(label);
		this.color1=color1;
		this.color2=color2;
	}
	
	protected void paintComponent(Graphics g) {
/*        super.paintComponent(g);

        Dimension originalSize = super.getPreferredSize();
        int gap = (int) (originalSize.height * 0.2);
        int x = originalSize.width + gap;
        int y = gap;
        int diameter = originalSize.height - (gap * 2);

        g.setColor(Color.black);
        g.fillOval(x, y, diameter, diameter); */
		
		if ( !isOpaque( ) )
		{
			super.paintComponent( g );
			//return;
		}
		
		
		Graphics2D g2d = (Graphics2D)g;
		super.paintComponent( g );
		
		int w = getWidth( );
		int h = getHeight( );
		
		// Paint a gradient from top to bottom
		GradientPaint gp = new GradientPaint(
		    0, 0, color1,
		    0, h, color2 );

		g2d.setPaint( gp );
		g2d.fillRect( 0, 0, w, h );
		Color tmp = g2d.getColor();
		g2d.setColor(color1.darker());
		g2d.drawLine(0, 0, w, 0);
		g2d.setColor(tmp);
		
		setOpaque( false );
		//super.paintComponent( g );
		setOpaque( true );
    }

}