/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Input dialog asking for url and directory.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/18/09 - Input dialog should show again if user presses ok with invalid
 *                           values
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.GroupLayout;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;


import org.apache.log4j.Logger;

//import javax.swing.BorderFactory;
import java.awt.BorderLayout;

import skdownloader.core.Common;

public class UrlDownloadInputDialog
{
	private JLabel urlLbl;
	private JLabel dirLbl;
	private JTextField urlTF;
	private JTextField dirTF;
	@SuppressWarnings("unused")
	private JLabel msgLbl;
	private JLabel connLbl;
	private JComboBox<String> connCom;
	private JButton browseButt;
	private JFileChooser fileChooser;
	private JFrame parentFrame;
	private static Logger logger = Logger.getLogger(UrlDownloadInputDialog.class);

	public UrlDownloadInputDialog(String url, String dir, JFrame parentFrame)
	{
		msgLbl = new JLabel("Please give url and download directory", JLabel.CENTER);
		//msgLbl.setBorder(BorderFactory.createLineBorder(Color.red));
		urlLbl = new JLabel("URL");
		dirLbl = new JLabel("DIR");
		connLbl = new JLabel("Simultaneous connections:");
		browseButt = new JButton("Browse");
		urlTF  = new JTextField(30);
		urlTF.setMaximumSize(new Dimension(Short.MAX_VALUE, urlLbl.getPreferredSize().height +6));
		dirTF  = new JTextField(30);
		dirTF.setMaximumSize(new Dimension(Short.MAX_VALUE, urlLbl.getPreferredSize().height +6));
		urlTF.setText(url);
		dirTF.setText(dir);
		this.parentFrame=parentFrame;
		connCom = new JComboBox<String>();
		connCom.addItem(""+1);
		connCom.addItem(""+2);
		connCom.addItem(""+3);
		connCom.addItem(""+4);
		connCom.addItem(""+5);
		connCom.addItem(""+6);
		connCom.addItem(""+7);
		connCom.addItem(""+8);
		connCom.addItem(""+9);
		connCom.addItem(""+10);
		connCom.setSelectedIndex(3);
	}
	
	public boolean ShowDialog()
	{

	    final JPanel p  = new JPanel(new BorderLayout());
	    JPanel p1 = new JPanel();
	    
	  //1. Header
		JPanel p3 = new JPanel();
		p3.setMinimumSize(new Dimension(0,0));
		JTextPane headLabel = new JTextPane();
		String str="Please give url and dowload folder";
		SimpleAttributeSet attributes = new SimpleAttributeSet();
		StyleConstants.setFontSize(attributes, 14);
		attributes.addAttribute(StyleConstants.Bold, true);
		//StyleConstants.setForeground(attributes, new Color(13,123,13));
		StyleConstants.setUnderline(attributes, true);
		StyleConstants.setBackground(attributes, p3.getBackground());
		StyledDocument headerDocument = headLabel.getStyledDocument();
		try{
			headerDocument.insertString(0, str, attributes);
		} catch (Exception e) {}
		headerDocument.setCharacterAttributes(0, str.length(), attributes, true);
        headLabel.setOpaque(false);
        headLabel.setBackground(p3.getBackground());
        headLabel.setEditable(false);
        //headLabel.setBorder(BorderFactory.createLineBorder(Color.red));
        p3.add(headLabel);

		browseButt.addActionListener(new ActionListener() {
	          public void actionPerformed(ActionEvent e) {
	             fileChooser = new JFileChooser( );
	             fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	             fileChooser.setFileHidingEnabled(false);
	             if(fileChooser.showOpenDialog(p) == JFileChooser.APPROVE_OPTION) 
	             {
	            	 try {
	            	 dirTF.setText(fileChooser.getSelectedFile().getCanonicalPath());
	            	 } catch (Exception e1)
	            	 {
	            		 dirTF.setText(fileChooser.getSelectedFile().getAbsolutePath());
	            	 }
	             }
	          }});
    
	    JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEADING));
	    p2.add(connLbl);
	    p2.add(connCom);
	    //p1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	    //p1.setBorder(BorderFactory.createLineBorder(Color.RED));
	    
	    GroupLayout layout = new GroupLayout(p1);
	    p1.setLayout(layout);
	    layout.setAutoCreateGaps(true);
    
	    layout.setHorizontalGroup(
	    	layout.createParallelGroup()
	    		.addComponent(p3).
	    		addGroup(
	    		layout.createSequentialGroup().
	    			addGroup(
	    				layout.createParallelGroup(GroupLayout.Alignment.LEADING).
	    				addComponent(urlLbl).
	    				addComponent(dirLbl)).
	    				
	    			addGroup(
	    				layout.createParallelGroup(GroupLayout.Alignment.LEADING).
	    				addComponent(urlTF).
	    				addGroup(
	    					layout.createSequentialGroup().
	    					addComponent(dirTF).
	    					addComponent(browseButt)).
	    				addComponent(p2))
	    		
	    	));
	    
	    layout.setVerticalGroup(
	    	layout.createSequentialGroup().
	    		addComponent(p3).
	    		addGroup(
	    			layout.createParallelGroup(GroupLayout.Alignment.CENTER).
	    			addComponent(urlLbl).
	    			addComponent(urlTF)).
	    		addGroup(
	    			layout.createParallelGroup(GroupLayout.Alignment.CENTER).
	    			addComponent(dirLbl).
	    			addComponent(dirTF).
	    			addComponent(browseButt)).
	    		addComponent(p2)
	    	);
	    //p1.setBorder(BorderFactory.createLineBorder(Color.red));
	    p.add(p1);
	    //p.setBorder(BorderFactory.createLineBorder(Color.red));
	    while(true)
	    {
	    	if(JOptionPane.showConfirmDialog(parentFrame, p, "Add download",JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
	    	{
	    		if((! dirTF.getText().equals("")) &&
	    		   (Common.CheckFileExists(dirTF.getText(), logger) == Common.OK) &&
	    		   (! urlTF.getText().equals("")))
	    		{
	    			return true;
	    		}
	    		else
	    		{
	    			if(urlTF.getText().equals(""))
	    				JOptionPane.showMessageDialog(parentFrame,
                                "Please provide a valid url",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
	    			
	    			else if((dirTF.getText().equals("")) ||
	    			   (Common.CheckFileExists(dirTF.getText(), logger) != Common.OK))
	    				JOptionPane.showMessageDialog(parentFrame,
		                                             "Download directory does not exist: "+Common.LINESEPERATOR+dirTF.getText()+
		                                             Common.LINESEPERATOR+"Please retry",
		                                             "Error",
                                                     JOptionPane.ERROR_MESSAGE);
	    			
	    		}
	    	}
	    	else
	    	{
	    		return false;
	    	}
	    }
	}
	
	public String GetInputUrl()
	{
		return urlTF.getText();
	}
	
	public String GetDownloadDir()
	{
		return dirTF.getText();
	}
	
	public int GetNumConnections()
	{
		int i = connCom.getSelectedIndex();
		return i+1;
	}
}