/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Tree cell renderer for browser tree.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents.tree;

import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.ImageIcon;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;

import javax.swing.JTree;

import skdownloader.downloadmanager.SystemInfoHolder;
import skdownloader.downloadmanager.guicomponents.DownloaderTheme;

public class BrowserTreeCellRenderer extends DefaultTreeCellRenderer 
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    private ArrayList<ImageIcon> statusAnimList = new ArrayList<ImageIcon>();
    public BrowserTreeCellRenderer(SystemInfoHolder sysInfoHolder) 
    {
    	statusAnimList.add(new ImageIcon(this.getClass().getResource("images/browser_expand.png")));
    	statusAnimList.add(new ImageIcon(this.getClass().getResource("images/browser_home.png")));
    	statusAnimList.add(new ImageIcon(this.getClass().getResource("images/browser_all.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("images/browser_active.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("images/browser_completed.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("images/browser_cancelled.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("images/browser_seeding.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("images/search.png")));
    }
    
    public Component getTreeCellRendererComponent(
                        JTree tree,
                        Object value,
                        boolean sel,
                        boolean expanded,
                        boolean leaf,
                        int row,
                        boolean hasFocus)
    {
    	super.getTreeCellRendererComponent(tree, value, sel,expanded, leaf, row, hasFocus);
    	//this.setOpaque(true);
    	DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
    	if(SystemInfoHolder.IsWindows()) {
    		this.setBackgroundNonSelectionColor(Color.decode(DownloaderTheme.SYSTEM_TREEBG));
    	}

    	this.setIcon((ImageIcon)statusAnimList.get(((ViewStateTreeNodeDisplayer)node.getUserObject()).GetId()));
    	return this;
    }


}
