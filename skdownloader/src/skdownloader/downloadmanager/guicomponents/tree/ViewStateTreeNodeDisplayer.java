/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Addon class for view state tree.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents.tree;

import java.util.ArrayList;

public class ViewStateTreeNodeDisplayer
{
	public ArrayList<?> arrLst;
	public int view;
	
	public static final String VIEWS[] = {"SKDOWNLOADER", "Home", "All", "Active", "Completed", "Cancelled", "Seeding", "Search Torrents"};
    
	// These are the status codes.
	public static final int BASE=0;
	public static final int HOME=1;
	public static final int ALL = 2;
	public static final int ACTIVE = 3;
	public static final int COMPLETED = 4;
	public static final int CANCELLED = 5;
	public static final int SEEDING = 6;
	public static final int SEARCH = 7;
	
	public ViewStateTreeNodeDisplayer(ArrayList<?> arrLst, int view)
	{
		this.arrLst=arrLst;
		this.view=view;
	}
	
	public String toString()
	{
		if((view == SEARCH) || (view == HOME) || (view == BASE))
			return (VIEWS[view]);
		else
			return (VIEWS[view]+"("+arrLst.size()+")");
	}
	
	public int GetId()
	{
		return view;
	}
}