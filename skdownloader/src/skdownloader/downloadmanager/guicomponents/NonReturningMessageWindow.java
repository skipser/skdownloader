/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    A window that returns only with user input
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import javax.swing.*;
import skdownloader.downloadmanager.guicomponents.events.*;

public class NonReturningMessageWindow extends Thread
{
	JFrame parentFrame;
	JPanel msgPanel;
	JLabel label;
	
	public NonReturningMessageWindow(JFrame motherFrame)
	{
		parentFrame = new JFrame();
		parentFrame.setBounds(motherFrame.getX()+motherFrame.getWidth()/2, motherFrame.getY()+motherFrame.getHeight()/2, 200,100);
		msgPanel = new JPanel();
		label = new JLabel();
		label.setText("Checking...");
	}
	
	// Create the listener list
    protected javax.swing.event.EventListenerList listenerList =
        new javax.swing.event.EventListenerList();
    
    // This methods allows classes to register for MyEvents
    public void addCustomEventListener(CustomEventListener listener) {
        listenerList.add(CustomEventListener.class, listener);
    }

    // This methods allows classes to unregister for MyEvents
    public void removeCustomEventListener(CustomEventListener listener) {
        listenerList.remove(CustomEventListener.class, listener);
    }

    // This private class is used to fire MyEvents
    void fireMyEvent(CustomEvent evt) {
        Object[] listeners = listenerList.getListenerList();
        // Each listener occupies two elements - the first is the listener class
        // and the second is the listener instance
        for (int i=0; i<listeners.length; i+=2) {
            if (listeners[i]==CustomEventListener.class) {
                ((CustomEventListener)listeners[i+1]).CustomEventOccurred(evt);
            }
        }
    }
	
    
	public void run()
	{
		/*this.addCustomEventListener(new CustomEventListener() {
	        public void CustomEventOccurred(CustomEvent evt) {
	            // MyEvent was fired
	        	parentFrame.dispose();
	        }
		});*/

		msgPanel.add(label);
		parentFrame.add(msgPanel);
		msgPanel.setSize(200, 100);
		parentFrame.setSize(200, 100);
		parentFrame.setVisible(false);
	}
	
	public void Show(String msg)
	{
		label.setText(msg);
		parentFrame.setVisible(true);
		label.setVisible(true);
		label.repaint();
		parentFrame.repaint();
	}
	
	public void Hide()
	{
		parentFrame.setVisible(false);
	}
	
	public void Remove()
	{
		parentFrame.dispose();
	}
}