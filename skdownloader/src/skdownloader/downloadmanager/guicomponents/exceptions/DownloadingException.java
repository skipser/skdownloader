/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Custom exception used for downloading.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents.exceptions;

public class DownloadingException extends Exception
{
private static final long serialVersionUID = 1L;
String mistake;

//----------------------------------------------
// Default constructor - initializes instance variable to unknown

  public DownloadingException()
  {
    super();             // call superclass constructor
    mistake = "unknown";
  }
  

//-----------------------------------------------
// Constructor receives some kind of message that is saved in an instance variable.

  public DownloadingException(String err)
  {
    super(err);     // call super class constructor
    mistake = err;  // save message
  }
  

//------------------------------------------------  
// public method, callable by exception catcher. It returns the error message.

  public String getError()
  {
    return mistake;
  }
}
  