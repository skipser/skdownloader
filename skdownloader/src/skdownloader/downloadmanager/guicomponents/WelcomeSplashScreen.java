/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Startup splash screen
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import java.awt.SplashScreen;
import java.awt.Graphics2D;
import java.awt.AlphaComposite;
import java.awt.Color;

public class WelcomeSplashScreen extends Thread
{
	private SplashScreen splash;
	public boolean stop=false;
	
	
    static void renderSplashFrame(Graphics2D g, int frame) 
    {
        final String[] comps = {"foo", "bar", "baz"};
        g.setComposite(AlphaComposite.Clear);
        g.fillRect(120,140,200,40);
        g.setPaintMode();
        g.setColor(Color.BLACK);
        g.drawString("Loading "+comps[(frame/5)%3]+"...", 120, 150);
    }
    
    public void run() 
    {

        splash = SplashScreen.getSplashScreen();
        if (splash == null) {
            return;
        }
        Graphics2D g = splash.createGraphics();
        if (g == null) {
            return;
        }
        
        while(! stop)
        {
            splash.update();
            try {
                Thread.sleep(200);
            }
            catch(InterruptedException e) {
            }
        }
        
        try {
        	splash.close();
        }
        catch(Exception e) {
        }
        
    }
    /*public void actionPerformed(ActionEvent ae) {
        System.exit(0);
    }*/
    
/*    @SuppressWarnings("unused")
	private static WindowListener closeWindow = new WindowAdapter(){
        public void windowClosing(WindowEvent e){
            e.getWindow().dispose();
        }
    };*/
    
    public void Close()
    {
    	stop=true;
    	//splash.close();
    }
}
