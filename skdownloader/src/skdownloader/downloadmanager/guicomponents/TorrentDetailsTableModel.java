/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Table model for downloads table.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import java.util.ArrayList;
import javax.swing.JProgressBar;
import javax.swing.table.AbstractTableModel;

import org.gudy.azureus2.core3.peer.PEPeer;

import skdownloader.downloadmanager.*;

// This class manages the download table's data.
public class TorrentDetailsTableModel extends AbstractTableModel //implements Observer 
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// These are the names for the table's columns.
    private static final String[] columnNames = {"Ip", "Completed", "Status"};
    
    // These are the classes for each column's values.
    private static final Class<?>[] columnClasses = {String.class, JProgressBar.class, String.class};
    //private static final Class<?>[] columnClasses = {String.class, String.class, String.class};
    private ArrayList<PEPeer> peerList;

    
    @SuppressWarnings("unused")
	private GuiUserIOUtils ioutils;
    MainFrame mainFrame;
 
    public void InitAll(GuiUserIOUtils ioutils) {
    	// index  Description
    	//     0  Downloading staus label
    	//     1  Paused status label
    	//     2  Cancelled status label
    	//     3  Finished status label
    	this.ioutils = ioutils;
    	peerList = new ArrayList<PEPeer>();

    }
   
    // Get a task for the specified row.
    public PEPeer GetPeer(int row) {
    	PEPeer peer=null;
    	try {
    		peer=(PEPeer)peerList.get(row);
    	}catch(Exception e) {}
    	
    	return peer;
    }
    
    // Add a new download to the table.
    public synchronized void AddPeer(PEPeer peer) {
        // Add task to download list
    	peerList.add(peer);
        
        // Fire table row insertion notification to table.
        fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
    }
    
    // Remove a download from the list.
    public synchronized void RemovePeer(PEPeer peer) {
    	//Find row to be removed.
    	int row = GetRowForPeer(peer);
    	
    	if(row == -1)
    		return;
    	
        // Fire table row deletion notification to table.
        fireTableRowsDeleted(row, row);
        
    	//Remove row from table
    	peerList.remove(peer);
    }
    
    public int GetRowForPeer(PEPeer peer) {
    	return peerList.indexOf(peer);
    }
    
    public synchronized void UpdatePeerStatus(PEPeer peer) {
    	int row = peerList.indexOf(peer);
    	if(row != -1)
    		this.fireTableCellUpdated(row, 3);
    }
    
    public synchronized void ClearTable() {
    	if(this.getRowCount() >0)
    		this.fireTableRowsDeleted(0, this.getRowCount()-1);
    	this.peerList.clear();
    }

    //Get table's column count.
    public int getColumnCount() {
        return columnNames.length;
    }
    
    // Get a column's name.
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    // Get a column's class.
    public Class<?> getColumnClass(int col) {
        return columnClasses[col];
    }
    
    // Get table's row count.
    public int getRowCount() {
        return peerList.size();
    }
    
    
    // Get value for a specific row and column combination.
    public synchronized Object getValueAt(int row, int col) {
    	PEPeer peer = GetPeer(row);
    	if(peer == null)
    		return null;
        switch (col) {
        case 0: // Ip
        	return peer.getIp();
        case 1: // Completed
        	return (float)peer.getPercentDoneInThousandNotation()/10;
        case 2: //Status
        	if(peer.getPeerState() > 0)
        		return PEPeer.StateNames[peer.getPeerState()/10];
        	else
        		return "";
        }
        return "";
    }
}
