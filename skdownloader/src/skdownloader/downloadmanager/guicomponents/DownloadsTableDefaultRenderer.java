/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Default renderer used to show progressbar in table.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import skdownloader.downloadmanager.SystemInfoHolder;

public class DownloadsTableDefaultRenderer extends DefaultTableCellRenderer
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	SystemInfoHolder sysInfoHolder;
	
	public DownloadsTableDefaultRenderer(SystemInfoHolder sysInfoHolder)
	{
		this.sysInfoHolder = sysInfoHolder;
	}

	public Component getTableCellRendererComponent(  
		 JTable table, Object value, boolean isSelected, 
		 boolean hasFocus, int row, int col)
		 {
		      Component comp = super.getTableCellRendererComponent(
		                       table,  value, isSelected, hasFocus, row, col);
		      if(! isSelected)
		      {
		    	  if(((row+2) %2 ) == 0) 
		    	  {
		    		  //comp.setBackground(sysInfoHolder.GetTheme().TABLEROW1BGCOLOR);
		    		  //comp.setForeground(sysInfoHolder.GetTheme().TABLEROW1TEXTCOLOR);
		    		  
		    	  }
		    	  else
		    	  {
		    		  //comp.setBackground(sysInfoHolder.GetTheme().TABLEROW2BGCOLOR);
		    		  //comp.setForeground(sysInfoHolder.GetTheme().TABLEROW2TEXTCOLOR);
		    	  }
		      }
		      else
		      {
		    	  if(((row+2) %2 ) == 0) 
		    	  {
		    		  //comp.setBackground(sysInfoHolder.GetTheme().TABLEROW1SELECTIONBGCOLOR);
		    		  //comp.setForeground(sysInfoHolder.GetTheme().TABLEROW1SELECTIONTEXTCOLOR);
		    	  }
		    	  else
		    	  {
		    		  //comp.setBackground(sysInfoHolder.GetTheme().TABLEROW2SELECTIONBGCOLOR);
		    		  //comp.setForeground(sysInfoHolder.GetTheme().TABLEROW2SELECTIONTEXTCOLOR);
		    	  }
		      }
		      return( comp );
		  }
}