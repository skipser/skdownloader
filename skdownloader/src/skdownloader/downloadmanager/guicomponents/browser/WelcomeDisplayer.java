package skdownloader.downloadmanager.guicomponents.browser;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import javax.swing.JPanel;

import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import skdownloader.downloadmanager.SystemInfoHolder;

public class WelcomeDisplayer {
	private Browser swtBrowser;
	private Thread swtThread;
	private JPanel panel;
	private Canvas canvas;
	private String[] excludeUrls;
	private SystemInfoHolder sysInfoHolder;
	
	private static Logger logger = Logger.getLogger(WelcomeDisplayer.class);
	
	public WelcomeDisplayer(JPanel panel, SystemInfoHolder sysInfoHolder) {
		this.panel=panel;
		this.sysInfoHolder=sysInfoHolder;
		canvas = new Canvas();
		excludeUrls = new String[] {"http://www.skipser.com", "http://www.skdownloader.com"};
		
		loadSwtJar();
	}
	
    public void connect() {
    	if (this.swtThread == null) {
    		this.swtThread = new Thread() {
    			@Override
    			public void run() {
    				try {
    					Display display = new Display();
    					Shell shell = SWT_AWT.new_Shell(display, canvas);
    					shell.setLayout(new FillLayout());
    					
    					synchronized (this) {
    						swtBrowser = new Browser(shell, SWT.NONE);
    						this.notifyAll();
    					}
    					
    					shell.open();
    					while (!isInterrupted() && !shell.isDisposed()) {
    						if (!display.readAndDispatch()) {
    							display.sleep();
    						}
    					}
    					shell.dispose();
    					display.dispose();
    				} catch (Exception e) {
    					interrupt();
    				}
    			}
    		};
    		this.swtThread.start();
    	}
     
    	// 	Wait for the Browser instance to become ready
    	synchronized (this.swtThread) {
    		while (this.swtBrowser == null) {
    			try {
    				this.swtThread.wait(100);
    			} catch (InterruptedException e) {
    				this.swtBrowser = null;
    				this.swtThread = null;
    				break;
    			}
    		}
    	}
    }
    
    /**
     * Returns the Browser instance. Will return "null"
     * before "connect()" or after "disconnect()" has
     * been called.
     */
    public Browser getBrowser() {
    	return this.swtBrowser;
    }
     
    /**
     * Stops the swt background thread.
     */
    public void disconnect() {
    	if (swtThread != null) {
    		swtBrowser = null;
    		swtThread.interrupt();
    		swtThread = null;
    	}
   	}
    
    /**
     * Ensures that the SWT background thread
     * is stopped if this canvas is removed from
     * it's parent component (e.g. because the
     * frame has been disposed).
     */
    public void removeNotify() {
    	disconnect();
    }
    
    public void showBrowser() {
    	canvas.setMinimumSize(new Dimension(50,50));
    	panel.add(canvas, BorderLayout.CENTER);
    	
    	connect();
    	
    	// Now we can open a webpage, but remember that we have
    	// to use the SWT thread for this.
    	getBrowser().getDisplay().asyncExec(new Runnable() {
    		@Override
    		public void run() {
    			getBrowser().setUrl(sysInfoHolder.ADS_SRC_LOCATION);
    			getBrowser().addLocationListener(new LinkInterceptListener(excludeUrls,sysInfoHolder.ADS_SRC_LOCATION));
    		}
    	});
    }
    
    private void loadSwtJar() {
    	String swtFileName ="";
        try {
            String osName = System.getProperty("os.name").toLowerCase();
            String osArch = System.getProperty("os.arch").toLowerCase();
            URLClassLoader classLoader = (URLClassLoader) getClass().getClassLoader();
            Method addUrlMethod = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            addUrlMethod.setAccessible(true);

            String swtFileNameOsPart = 
                osName.contains("win") ? "win32" :
                osName.contains("mac") ? "macosx" :
                osName.contains("linux") || osName.contains("nix") ? "linux_gtk" :
                ""; // throw new RuntimeException("Unknown OS name: "+osName)

            String swtFileNameArchPart = osArch.contains("64") ? "x64" : "x86";
            swtFileName = "swt_"+swtFileNameOsPart+"_"+swtFileNameArchPart+".jar";
            String swtFile=sysInfoHolder.GetInstallDir()+File.separator+"lib"+File.separator+swtFileName;
            File file = new File(swtFile);
            if (! file.exists()) {
            	logger.error("Could not  file jar file : "+ swtFile);
            	return;
            }
            
            URL swtFileUrl = file.toURI().toURL(); 
            addUrlMethod.invoke(classLoader, swtFileUrl);
        }
        catch(Exception e) {
            logger.error("Unable to add the swt jar to the class path: "+swtFileName);
            logger.error("Exception", e);
        }
    }
    
}