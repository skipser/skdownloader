package skdownloader.downloadmanager.guicomponents.browser;

import org.apache.log4j.Logger;
import org.eclipse.swt.browser.LocationEvent;
import org.eclipse.swt.browser.LocationListener;

import skdownloader.downloadmanager.GUICommon;

public class LinkInterceptListener implements LocationListener {
	private String[] excludeList;
	private String excludeUrl;
	
	private static Logger logger = Logger.getLogger(LinkInterceptListener.class);
	
	public LinkInterceptListener(String[] excludeList, String excludeUrl) {
		this.excludeList=excludeList;
		this.excludeUrl=excludeUrl;
	}
	
    // method called when the user clicks a link but before the link is opened.
    public void changing(LocationEvent event) {
    	logger.debug("Got to load - "+event.location);
    	
    	// Load exclude url as it is.
    	if (event.location.equals(excludeUrl)) {
    		logger.debug("Got exclude url- "+event.location);
    		return;
    	}

    	// Load excluded items in new browser window. 
    	if (isExcluded(event.location)) {
    		logger.debug("Got in exclude list - "+event.location);
        	GUICommon.LaunchInBrowser(event.location);
 
            // Setting event.doit to false prevents the link from opening in place
            event.doit = false;
    	}
        try {
        	logger.debug("Got not excluded load - "+event.location);
    		return ;

        } catch (Exception e) {
        	logger.error("Exception", e);
        }
    }
 
    // method called after the link has been opened in place.
    public void changed(LocationEvent event) {
        // Not used in this example
    }
    
    private boolean isExcluded(String url) {
    	for (int i=0; i< excludeList.length; i++) {
    		logger.debug("Checking "+url+", "+excludeList[i]+", "+url.indexOf(excludeList[i]));
    		if (url.indexOf(excludeList[i])>-1) {
    			return true;
    		}
    	}
    	return false;
    }
}