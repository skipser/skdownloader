package skdownloader.downloadmanager.guicomponents.search;

import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;

import skdownloader.core.Common;
import skdownloader.core.urlcore.UrlDownloader;
import skdownloader.downloadmanager.GuiUserIOUtils;
import skdownloader.downloadmanager.SystemInfoHolder;

public class FenopyTorrentsSearcher extends Thread {
	
	private SearchPanel searchFrame;
	private String searchStr;
	private GuiUserIOUtils ioutils;
	private SystemInfoHolder sysInfoHolder;
	private ArrayList<SearchResultItem> itemList = new ArrayList<SearchResultItem>();
	
	public static String ID = "fenopy";
	private long searchId;
	
	private String iconUrl = "http://www.toolsbysk.com/skdownloader/images/search/fenopy.png";
	public static Logger logger = Logger.getLogger(FenopyTorrentsSearcher.class);
	
	public FenopyTorrentsSearcher(SearchThread mother, SearchPanel searchFrame, String str, GuiUserIOUtils ioutils, SystemInfoHolder sysInfoHolder, long id) {
		this.searchFrame=searchFrame;
		searchStr = str;
		this.ioutils=ioutils;
		this.sysInfoHolder=sysInfoHolder;
		this.searchId=id;
	}
	
	public void run() {
		String searchUrl;
		String content;
		try {
			searchUrl = "http://fenopy.com/?keyword="+URLEncoder.encode(searchStr, "UTF-8");
		} catch (Exception e) {
			return;
		}
		UrlDownloader downloader = new UrlDownloader(ioutils, sysInfoHolder);
		HttpResponse response = downloader.GetDownloadResponse(searchUrl);
		try {
			InputStream is = response.getEntity().getContent();
			content = Common.InputStreamToString(is, logger);
			if(content.equals(""))
				return;
		}catch(Exception e) {
			return;
		}
		
		Pattern p = Pattern.compile("<tr.*?<td.*?<a.*?href=\"(.*?)\".*?>(.*?)</a>"+  //srcLink, Name
									".*?<td.*?<td.*?<td.*?>\\s*(.*?)\\s*</td>"+  //Seeds
									".*?<td.*?>\\s*(.*?)\\s*</td>"+ //Leeches
									".*?<td.*?>\\s*(.*?)\\s*</td>"+ //Size
									".*?<td.*?<a.*?href=\"(.*?)\".*</td>", //torrent
									Pattern.DOTALL);
		//Matcher matcher = p.matcher(content);
		Pattern p2 = Pattern.compile("(<tr.*?</tr>)",Pattern.DOTALL);
		Matcher m2 = p2.matcher(content);
		while(m2.find()) {
			//System.out.println("Got it"+m2.group(1));
			Matcher matcher = p.matcher(m2.group(1));
			if(! matcher.find())
				continue;
			String srcLink = matcher.group(1);
			String name = matcher.group(2);
			if(srcLink.matches("^/.*$"))
				srcLink="http://www.fenopy.com"+srcLink;
			int seeds = 0;
			try {
				seeds = Integer.parseInt(matcher.group(3));
			}catch(Exception e) {seeds=0;}
			int leeches = 0;
			try {
				leeches=Integer.parseInt(matcher.group(4));
			} catch (Exception e) {leeches=0;}
			String tmp = matcher.group(5);
			Pattern p1 = Pattern.compile("^([\\d\\.]+)\\s+(.*)$", Pattern.DOTALL);
			Matcher m1 = p1.matcher(tmp);
			long size=0;
			float tmp1=0;
			if(m1.matches()) {
				try {
					tmp1= Float.parseFloat(m1.group(1));
				} catch(Exception e) {
					size=0;
					logger.error("Exception", e);
				}
				if(m1.group(2).matches("(?i:KB)"))
					size = (long) (tmp1*10024);
				else if(m1.group(2).matches("(?i:MB)"))
					size = (long) (tmp1*1048576);
				else if(m1.group(2).matches("(?i:GB)"))
					size = (long) (tmp1*1073741824);
			}
			String torrent = matcher.group(6);
			if(torrent.matches("^/.*$"))
				torrent="http://www.fenopy.com"+torrent;
			
			//itemList.add(new SearchResultItem( matcher.group(1), matcher.group(2),matcher.group(3), matcher.group(4), matcher.group(5), matcher.group(6), iconUrl));
			itemList.add(new SearchResultItem(torrent, 
											  srcLink, 
											  name, 
											  "-", 
											  "-", 
											  size, 
											  seeds, 
											  leeches,
											  ID,
											  iconUrl));
		}
		if(! itemList.isEmpty())
			searchFrame.AddItems(itemList, searchId);
	}
	
}