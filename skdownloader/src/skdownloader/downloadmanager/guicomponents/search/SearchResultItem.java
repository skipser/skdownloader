package skdownloader.downloadmanager.guicomponents.search;

public class SearchResultItem {
	
	public String torrent;
	public String srcLink;
	public String name;
	public String type;
	public String age;
	public long size;
	public int seeds;
	public int leeches;
	public String iconUrl;
	public String srcId;
	
	public SearchResultItem(String torrent, 
							String srcLink, 
							String name, 
							String type, 
							String age, 
							long size, 
							int seeds, 
							int leeches,
							String srcId,
							String iconUrl) {
		this.torrent=torrent;
		this.srcLink = srcLink;
		this.name=name;
		this.type = type;
		this.age=age;
		this.size=size;
		this.seeds=seeds;
		this.leeches=leeches;
		this.srcId=srcId;
		this.iconUrl=iconUrl;
	}
	
	public int getSeeds() {
		return seeds;
	}
}