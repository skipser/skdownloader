package skdownloader.downloadmanager.guicomponents.search;

import skdownloader.downloadmanager.GuiUserIOUtils;
import skdownloader.downloadmanager.SystemInfoHolder;

public class SearchThread extends Thread {
	private SearchPanel searchPanel;
	private String searchString;
	private GuiUserIOUtils ioutils;
	private SystemInfoHolder sysInfoHolder;
	
	private boolean abandoned = false;
	private long id;
	
	public SearchThread(SearchPanel fr, String str, GuiUserIOUtils ioutils, SystemInfoHolder sysInfoHolder, long id) {
		this.searchPanel = fr;
		this.searchString=str;
		this.ioutils=ioutils;
		this.sysInfoHolder = sysInfoHolder;
		this.id=id;
	}
	
	public void run() {
		KickassTorrentsSearcher searcher = new KickassTorrentsSearcher(this, searchPanel, searchString, ioutils, sysInfoHolder, id);
		searcher.start();
		ThePirateBayTorrentsSearcher searcher1 = new ThePirateBayTorrentsSearcher(this, searchPanel, searchString, ioutils, sysInfoHolder,id);
		searcher1.start();
		//FenopyTorrentsSearcher searcher2 = new FenopyTorrentsSearcher(this, searchPanel, searchString, logger, sysInfoHolder);
		//searcher2.start();
		
		//Wait for all threads to finish.
		try {
			searcher.join();
		} catch (Exception e) {}
		try {
			searcher1.join();
		} catch (Exception e) {}
		//try {
		//	searcher2.join();
		//} catch (Exception e) {}
		
		if(! abandoned)
			searchPanel.SearchThreadDone();
	}
	
	public void Abandon() {
		this.abandoned=true;
	}
	
	public static void main(String[] args) {

		SearchThread t = new SearchThread(null, "godfather", new GuiUserIOUtils(), new SystemInfoHolder("aaa", "bbb"), 0);
		t.run();
		
	}
}