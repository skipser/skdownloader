package skdownloader.downloadmanager.guicomponents.search;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import org.apache.log4j.Logger;

import skdownloader.core.Common;
import skdownloader.downloadmanager.GuiUserIOUtils;
import skdownloader.downloadmanager.MainFrame;
import skdownloader.downloadmanager.SystemInfoHolder;

public class SearchPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<SearchResultItem> itemList;
	private MainFrame mainFrame;
	private GuiUserIOUtils ioutils;
	private SystemInfoHolder sysInfoHolder;
	SearchResultsWebPanel webPane;
	//final JTextField searchField;
	JLabel searchLabel;
	final String searchInitText = "Search Torrents";
	
	private static Logger logger = Logger.getLogger(SearchPanel.class);
	
	private boolean showingDetails=false;
	private long searchId=0; 
	
	SearchThread t = null;
	
	public SearchPanel(MainFrame mainFrame, GuiUserIOUtils ioutils, SystemInfoHolder sysInfoHolder) {
		itemList = new ArrayList<SearchResultItem>();
		this.mainFrame=mainFrame;
		this.ioutils=ioutils;
		this.sysInfoHolder = sysInfoHolder;
		//searchField = new JTextField();
	}
	
	public void InitComponents() {
		this.setLayout(new BorderLayout());
		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		searchLabel = new JLabel("Please give a search torrent name to search");
		searchLabel.setFont(new Font(searchLabel.getFont().getName(),Font.BOLD,searchLabel.getFont().getSize()+4));
		searchLabel.setForeground(new Color(0x5a1a1a));
		searchPanel.add(searchLabel);
        this.add(searchPanel, BorderLayout.NORTH);
		
		webPane = new SearchResultsWebPanel();
		webPane.init(this);
		
		webPane.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		this.add(webPane, BorderLayout.CENTER);
		
		init();
	}
	
	public void SetPage(String str) {
		webPane.SetText(str);
	}
	
	public void init() {
		String str="<body style=\"margin-top:10px;\">"+
		"<br /><br /><div style=\"width:100%; text-align:center;\">Please give a search string</div></body>";
		SetPage(str);
	}

	
	public void Search(String s) {
		if(s.equals(searchInitText) || s.equals("") || s.length() < 3) {
			JOptionPane.showMessageDialog(mainFrame.GetFrame(), "Please give a valid search text", "Attention!!!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		searchLabel.setText("Search results for \""+s+"\"");
		searchId=(new Date()).getTime();

		if(t!= null)
			t.Abandon();
		t=null;
		showingDetails=false;
		itemList.clear();
		LoadSearchLoadingScreen();
		t = new SearchThread(this, s, ioutils, sysInfoHolder, searchId);
		t.start();
	}
	
	public void SearchThreadDone() {
		if(itemList.isEmpty()) {
			//We didn't get any entries. Inform user
			String html ="<html><body>"+
			"		<br /><br /><br />"+
			"		<div style=\"width:100%; text-align:center;\"><br /><br/>Sorry, no results available ...</div>"+
			"		</body>"+
			"		</html>";
					SetPage(html);
		}
	}
	
	public void LoadSearchLoadingScreen() {
		String html ="<html><body>"+
"		<br /><br /><br />"+
"		<div style=\"width:100%; text-align:center;\"> <img src=\"http://www.skdownloader.skipser.com/img/search/loading.gif\" alt=\"\" ></img><br />Loading...</div>"+
"		</body>"+
"		</html>";
		SetPage(html);
	}
	
	public synchronized void AddItems(ArrayList<SearchResultItem> list, long id) {
		if(this.searchId != id)
			return;
		//Add to global list, sort them again and re-show the html screen
		Iterator<?> it = list.iterator();
		while(it.hasNext()) {
			itemList.add((SearchResultItem)it.next());
		}
		
		RefreshDisplay(false);
	}
	
	public void RefreshDisplay(boolean force) {
		if(! force) {
			if(showingDetails)
				return;
		}
		showingDetails=false;
		itemList = SortBySeeds(itemList);
		Iterator<?> it = itemList.iterator();
		String htmlStr="<body style=\"margin-top:10px;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px;\">"+
		"<table style='width:100%; margin:5px; border:1px solid #666666;' cellspacing='0'>"+
		"<tr bgcolor='#CCCCCC' style='background:#CCCCCC;' height='30'>"+
			"<td width='40' style=\"border-bottom:1px solid gray;\"></td>"+
			"<td style=\"border-bottom:1px solid gray;\">Name</td>"+
			"<td width='60' style=\"border-bottom:1px solid gray;\"></td>"+
			"<td width='80' style=\"border-bottom:1px solid gray;\">Size</td>"+
			"<td width='60' style=\"border-bottom:1px solid gray;\">Age</td>"+
			"<td width='60' style=\"border-bottom:1px solid gray;\">Seeds</td>"+
			"<td width='60' style=\"border-bottom:1px solid gray;\">Peers</td>"+
		"</tr>";
		String rowCol="#d9ecf6";
		int i=0;
		while(it.hasNext()) {
			SearchResultItem item = (SearchResultItem) it.next();
			htmlStr+="<tr bgcolor='"+rowCol+"' height='40'>"+System.getProperty("line.separator")+
				"<td style=\"text-align:center;margin-top:5px;margin-bottom:5px;\"><img src=\""+item.iconUrl+"\"></img></td>"+System.getProperty("line.separator")+
				"<td><span style=\"font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px;\">"+item.name+"</span></td>"+System.getProperty("line.separator")+
				"<td><a href=\""+item.torrent+"#istorrent\" style=\"border:0 none;\"><img src=\"http://www.skdownloader.skipser.com/img/search/download.png\" border=\"0\" style=\"border:0 none;width:24px;height:24px;border-style: none\" title=\"Download\"></img></a></td>"+System.getProperty("line.separator")+
				"<td>"+Common.SizetoString(item.size)+"</td>"+System.getProperty("line.separator")+
				"<td>"+item.age+"</td>"+System.getProperty("line.separator")+
				"<td>"+item.seeds+"</td>"+System.getProperty("line.separator")+
				"<td>"+item.leeches+"</td>"+System.getProperty("line.separator")+
			"</tr>";
			rowCol= (rowCol.equals("#d9ecf6")? "#eff6f9" : "#d9ecf6");
			i++;
			if(i==50)
				break;
		}
		htmlStr += "</table></body>";
		htmlStr = "<html><head><style>img { border-style: none; }</style></head>"+htmlStr+"</html>";
		logger.debug(htmlStr);
		SetPage(htmlStr);
	}
	
	public void ShowDetails(int index) {
		showingDetails=true;
		SearchResultItem item = itemList.get(index);
		String html="<body style=\"margin-top:5px; margin-bottom:20px; margin-right:10px; padding:0;font-family:Geneva, Arial, Helvetica, sans-serif;  background-color:#FFFFFF;\">"+
""+
"	<!--div style=\"width:100%; height:250px; text-align:center; background:#d3e2eb;\"-->"+ 
"		<div style=\"width:600px; height:150px;  margin:0 auto; background:#f3faff; border: 10px solid #d3e2eb\">"+
"			<div style=\"padding-left:10px; margin-top:15px; text-align: left;\">"+
"				<div style=\"margin-left:10px; padding-bottom:5px;\">"+
"					<span style=\"font-family:Verdana, Arial, Helvetica, sans-serif; font-size:18px;font-weight:bold; color:#663300;\">"+item.name+"</span><br />"+
"					<span style=\"font-family:Helvetica, Arial, sans-serif; font-size:14px; color:#666666;\"><strong>Size:</strong> "+Common.SizetoString(item.size)+" | <strong>Seeds:</strong> "+item.seeds+" | <strong>Peers:</strong> "+item.leeches+"</span>"+
"				</div>"+
"				<a href=\""+item.torrent+"#istorrent\" style=\"text-decoration:none;\">"+
"				<div style=\"width:140px; height:30px; border:5px solid #6399d6; background-color:#a7c5e8; color:#FFFFFF; text-align:center; margin-left:10px;\">"+
"				<table><tr style=\"line-height:30px;\">"+
"				<td><span style=\"font-family:Arial; font-size:24px; font-weight:bold; color:#FFFFFF;\">Download</span></td>"+
"				</tr></table>"+
"				</div>"+	
"				</a>"+
"			</div>"+
"			<div style=\"margin-top:20px; padding-left:10px; text-align:left;\">"+
"			<span style=\"font-family:Verdana, Arial, Helvetica, sans-serif; font-size:14px;\">Click on the link below to go to the source page of this torrent.</span><br/>"+
"			<a href=\""+item.srcLink+"\"><span style=\"font-family:'Courier New', Courier, monospace; font-size:14px;\">"+Common.ShortenString(item.srcLink, "",50)+"</span></a>"+
"			</div>"+
"			<div style=\"width:100%; height:19px; margin-right:10px; padding-bottom:5px;\">"+
"				<a href=\"http://www.skdownloader.com#backtoresults\" style=\"text-decoration:none;\">" +
"				<div style=\"width:120px; border:2px solid #000099; height:16px; background:#496986; color:#FFFFFF; font-family:'verdana'; font-size:12px; text-align:center; float:right;\">"+
"				&lt;&nbsp;&nbsp;Back to results"+
"				</div>"+
"				</a>"+
"			</div>"+
"		</div>"+
"	<!--/div-->"+
"</body>";
		SetPage(html);
		logger.debug(html);
	}
	
	public void AddDownload(String str) {
		mainFrame.AddNewTorrentDownload(str, true);
		showingDetails=true;
		String html="<body style=\"margin-top:50px; margin-bottom:20px; padding:0;font-family:Geneva, Arial, Helvetica, sans-serif;  background-color:#FFFFFF; color:blue;\">"+
					"<div style=\"text-align:center\">"+
					"<h3>The torrent has been added for download</h3>"+
					"<div style=\"width:120px; border:2px solid #000099; height:16px; background:#496986; color:#FFFFFF; font-family:'verdana'; font-size:12px; text-align:center; margin:0 auto;\">"+
					"<a href=\"http://www.skdownloader.com#backtoresults\" style=\"text-decoration:none;\">" +
					"&lt;&nbsp;&nbsp;Back to results"+
					"</a>"+
					"</div>"+
					"</div>"+
					"</body>";
		SetPage(html);
	}
	
	public synchronized ArrayList<SearchResultItem> SortBySeeds(ArrayList<SearchResultItem> arr) {
		ArrayList<SearchResultItem> arr1 = new ArrayList<SearchResultItem>();
		SearchResultItem item;
		SearchResultItem item1;
		Iterator<?> it = arr.iterator();
		while(it.hasNext()) {
			if(arr1.size() == 0)
				arr1.add((SearchResultItem) it.next());
			else {
				item = (SearchResultItem) it.next();
				Iterator<?> it1 = arr1.iterator();
				int i=0; int j=0;
				while(it1.hasNext()) {
					item1 = (SearchResultItem) it1.next();
					if(item.getSeeds() > item1.getSeeds()) {
						j=i;
						break;
					}
					else if(i == (arr1.size()-1)) {
						j=arr1.size();
					}
					i++;
				}
				arr1.add(j, item);
			}
		}
		return arr1;
	}
}