package skdownloader.downloadmanager.guicomponents.search;

import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;

import skdownloader.core.Common;
import skdownloader.core.urlcore.UrlDownloader;
import skdownloader.downloadmanager.GuiUserIOUtils;
import skdownloader.downloadmanager.SystemInfoHolder;

public class KickassTorrentsSearcher extends Thread {
	
	private SearchPanel searcPanel;
	private String searchStr;
	private GuiUserIOUtils ioutils;
	private SystemInfoHolder sysInfoHolder;
	private ArrayList<SearchResultItem> itemList = new ArrayList<SearchResultItem>();
	
	public static String ID = "kickasstorrents";
	private long searchId;
	
	private String iconUrl = "http://www.skdownloader.skipser.com/img/search/kickasstorrents.png";
	private static Logger logger = Logger.getLogger(KickassTorrentsSearcher.class);
	
	public KickassTorrentsSearcher(SearchThread mother, SearchPanel searcPanel, String str, GuiUserIOUtils ioutils, SystemInfoHolder sysInfoHolder, long id) {
		this.searcPanel=searcPanel;
		searchStr = str;
		this.ioutils=ioutils;
		this.sysInfoHolder=sysInfoHolder;
		this.searchId=id;
	}
	
	public void run() {
		String searchUrl;
		String content;
		try {
			searchUrl = "http://kickass.to/search/"+URLEncoder.encode(searchStr, "UTF-8");
		} catch (Exception e) {
			logger.error("Exception", e);
			return;
		}
		UrlDownloader downloader = new UrlDownloader(ioutils, sysInfoHolder);
		HttpResponse response = downloader.GetDownloadResponse(searchUrl);
		try {
			InputStream is = response.getEntity().getContent();
			content = Common.InputStreamToString(is, logger);
			if(content.equals(""))
				return;
		}catch(Exception e) {
			return;
		}
		
		//logger.debug(content);
		
		String content1 = "<tr class=\" even\" id=\"torrent_4208936\">"+
"		<td class=\"fontSize12px torrentnameCell\">"+
"	<div class=\"iaconbox\">"+
"		<div><a title=\"??????? ??? ????\" href=\"http://torcache.com/torrent/F472592A1DE7A00B146EB0F740E33B90E793D705.torrent\" onclick=\"pageTracker._trackEvent('Download', 'Download torrent file', 'Movies');\" class=\"idownload\"></a></div>"+
"		<div><a title=\"???? ???????\" class=\"idirect\" href=\"http://leechmonster.com/torrent/?hash=f472592a1de7a00b146eb0f740e33b90e793d705&sign=c3133265f54be78af2a11517082f13c8\" onclick=\"pageTracker._trackEvent('Download', 'Download torrent via http', 'Torrent Movies');\" ></a></div>"+
"		<div><a title=\"??????? ?????\" class=\"imovz\" href=\"http://kickassmovz.com/46068\" onclick=\"pageTracker._trackEvent('Download', 'Download movie', 'Torrent Movies');\" ></a></div>"+
"		<div><a class=\"iverif\" href=\"/the-godfather-2-1990-hd-x264-pluto-t4208936.html\" title=\"???????? ???\"></a></div>	<a rel=\"4208936,0\" class=\"icomment\" href=\"/the-godfather-2-1990-hd-x264-pluto-t4208936.html#comments\">"+
"		<span class=\"icommentdiv\"></span>14"+
"	</a>"+
"	</div>"+
"	<div class=\"torrentname\">"+
"		<a href=\"/the-godfather-2-1990-hd-x264-pluto-t4208936.html\" class=\"torType filmType\"><!-- img src=\"http://static.kickasstorrents.com/images/fileicons/film.gif\" alt=\"file type\" class=\"torType\"/ --></a>"+
"		<a href=\"/the-godfather-2-1990-hd-x264-pluto-t4208936.html\">The <strong class=\"red\">Godfather</strong>-2 1990 HD x264~PlutO~</a>"+
"					<span>"+
"				 ???"+ 
"					<span id=\"cat_4208936\">"+
"												<a href=\"/movies/\">Movies</a>"+
"		</span></span>	"+
"		</div>"+
"		</td>"+
"				<td class=\"nobr\">23.83 <span>GB</span></td>"+
"		<td>13</td>"+
"		<td>2&nbsp;years&nbsp;ago</td>"+
""+
"		<td class=\"green\">83</td>"+
"		<td class=\"red lasttd\">253</td>"+
"</tr>";
		if(content1.equals("")) {} // Just to avoid a not used warning for content1
		
		//Pattern p = Pattern.compile("<tr.*?<td.*?<a\\s+title=\".*?href=\"(.*?)\".*?<td.*?>(.*?)</td>.*?<td.*?<td.*?<td.*?>.*?</td>.*?<td.*?>(.*?)</td>");
		//Pattern p = Pattern.compile("<tr.*?<td.*?<a\\s+.*?href=\"(.*?)\".*?<td.*?>(.*?)</td>.*?<td.*?<td.*?<td.*?>(.*?)</td>.*?<td.*?>(.*?)</td>", Pattern.DOTALL);
		Pattern p = Pattern.compile("<tr.*?<td.*?<a\\s+.*?href=\"(http[^\"]+\\.torrent).*?\"" +
									".*?<div class=\"torrentname\">.*?<a.*?<a.*?<a\\s+href=\"(.*?)\">(.*?)</a>"+
									".*?<a.*?>(.*?)</a>"+
									".*?<td.*?>(.*?)</td>" +
									".*?<td.*?<td.*?<td.*?>\\s*(.*?)\\s*</td>" +
									".*?<td.*?>\\s*(.*?)\\s*</td>",
									Pattern.DOTALL);
		//Matcher matcher = p.matcher(content);
		Pattern p2 = Pattern.compile("(<tr.*?</tr>)",Pattern.DOTALL);
		Matcher m2 = p2.matcher(content);
		//logger.debug(content);
		while(m2.find()) {
			logger.debug("Got it"+m2.group(1));
			Matcher matcher = p.matcher(m2.group(1));
			if(! matcher.find())
				continue;
			String torrent = matcher.group(1);
			if(torrent.matches("^/.*$"))
				torrent="http://www.kickass.to"+torrent;
			String srcLink = matcher.group(2);
			if(srcLink.matches("^/.*$"))
				srcLink="http://www.kickass.to"+srcLink;
			String name = matcher.group(3);
			String type = matcher.group(4);
			String tmp = matcher.group(5);
			Pattern p1 = Pattern.compile("^\\s*(.*?)\\s*<span>\\s*(.*?)\\s*</span>\\s*.*$", Pattern.DOTALL);
			Matcher m1 = p1.matcher(tmp);
			long size=0;
			float tmp1=0;
			if(m1.matches()) {
				try {
					tmp1= Float.parseFloat(m1.group(1));
				} catch(Exception e) {
					size=0;
					logger.error("Exception", e);
				}
				if(m1.group(2).matches("(?i:KB)"))
					size = (long) (tmp1*10024);
				else if(m1.group(2).matches("(?i:MB)"))
					size = (long) (tmp1*1048576);
				else if(m1.group(2).matches("(?i:GB)"))
					size = (long) (tmp1*1073741824);
			}
			logger.debug("name: "+name+", type: "+type+", size"+size);
			int seeds = 0;
			try {
				seeds = Integer.parseInt(matcher.group(6));
			}catch(Exception e) {seeds=0;}
			
			int leeches = 0;
			try {
				leeches=Integer.parseInt(matcher.group(7));
			} catch (Exception e) {leeches=0;}
			//itemList.add(new SearchResultItem( matcher.group(1), matcher.group(2),matcher.group(3), matcher.group(4), matcher.group(5), matcher.group(6), iconUrl));
			itemList.add(new SearchResultItem(torrent, 
											  srcLink, 
											  name, 
											  type, 
											  "-", 
											  size, 
											  seeds, 
											  leeches,
											  ID,
											  iconUrl));
		}
		if(! itemList.isEmpty())
			searcPanel.AddItems(itemList, searchId);
	}
	
}