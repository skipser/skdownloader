/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Thread that displays ads in frames.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation with lobo browser component
 *   arun         09/12/09 - Log ad hits
 *   arun         09/12/09 - Send useragen compatible with mozilla 5.0
 *
 ****************************************************************************************/
package skdownloader.downloadmanager.guicomponents.search;

import java.awt.BorderLayout;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.apache.log4j.Logger;

import edu.stanford.ejalbert.BrowserLauncher;
import skdownloader.downloadmanager.SystemInfoHolder;

public class SearchResultsWebPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JEditorPane htmlPanel;
	public static BrowserLauncher launcher;
	SystemInfoHolder sysInfoHolder;
	SearchPanel searchFrame;
	private static Logger logger = Logger.getLogger(SearchResultsWebPanel.class);
	public void init(final SearchPanel searchFrame) {
		this.searchFrame=searchFrame;
		htmlPanel = new JEditorPane("text/html","");
		htmlPanel.setEditorKit(JEditorPane.createEditorKitForContentType("text/html"));
		htmlPanel.setEditable(false);
		htmlPanel.getCaret().setSelectionVisible(false);

		this.setLayout(new BorderLayout());
		// Create a JFrame and add the HtmlPanel to it.
		JScrollPane scrollView = new JScrollPane(htmlPanel);
		scrollView.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.add(scrollView);
		
		htmlPanel.addHyperlinkListener(new HyperlinkListener() {
		    public void hyperlinkUpdate(HyperlinkEvent e) {
		        if(e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
		        	logger.debug("Got for download - "+e.getURL());
		        	String url = e.getURL().toString();
		        	if (url.indexOf("#istorrent")>-1) {
		        		url=url.replace("#istorrent", "");
		        		searchFrame.AddDownload(url);
		        	}
		        	else if (url.indexOf("#backtoresults")>-1) {
		        		searchFrame.RefreshDisplay(true);
		        	}
		        }
		    }
		});
	}
	
	public void SetPage(String uri)	{

	}
	
	public void SetText(String str)	{
		htmlPanel.setText(str);
	}
}
