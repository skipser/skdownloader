package skdownloader.downloadmanager.guicomponents.search;

import java.util.ArrayList;

public class SearchSourceHandler {
	ArrayList<SearchSource> sourceList;
	
	public SearchSourceHandler() {
		SearchSource source = new SearchSource();
		source.homePage="http://www.kickasstorrents.com";
		source.iconUrl="http://static.kickasstorrents.com/images/favicon.ico";
		source.matchRegExp="";
	}
}