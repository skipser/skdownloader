package skdownloader.downloadmanager.guicomponents;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.BitSet;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import skdownloader.core.Common;


public class TorrentProgressBar extends JPanel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int width, height;
    private BitSet bitSet;
    private int dataSize = 100;
    //private int bitRectWidth=0;
    Color color1 = Color.GREEN;
    Color color2 = color1.darker( );
    
    public TorrentProgressBar(int width, int height) // square
    {
        this.width = width;
        this.height = height;
        this.setPreferredSize(new Dimension(this.width, this.height));
        this.setMinimumSize(new Dimension(this.width, this.height));
        bitSet = new BitSet();
        this.setBorder(BorderFactory.createLineBorder(color2,1));
    }
    public void paintComponent(Graphics g){
    	Graphics2D g2d = (Graphics2D)g;
    	g.setColor(Color.WHITE );
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        g.setColor(Color.blue );
        
        //Color s2 = Color.yellow;
        //Color e1 = Color.pink;
        //GradientPaint gradient1 = new GradientPaint(10,10,s2,30,30,e1,true);
        GradientPaint gradient1 = new GradientPaint(0, 0, color1,0, height, color2 );
        g2d.setPaint(gradient1);
        
        int start=0;
        int end=0;
        if(this.dataSize != 0) {
        	
        	//Create map of rectangle starts for each bit in the bitset
        	int width=this.getWidth();
        	for(int i=bitSet.nextSetBit(0); i>=0; i=bitSet.nextSetBit(i+1)) {
        		start =(int) Common.MyRound((width*i/this.dataSize), 0);
        		end   =(int) Common.MyRound((width*(i+1)/this.dataSize), 0);
        		g2d.fillRect(start, 0, end-start, this.getHeight());
        	}
        	//for(int i=bitSet.nextSetBit(0); i>=0; i=bitSet.nextSetBit(i+1)) {
        	//	end = (int)(this.getWidth()*(i+1)/100);
        	//	start = (int)(this.getWidth()*i/100);
        	//	g2d.fillRect(start, 0, end-start, this.getHeight());
        	//}
        }
    }
    
    public void Set(int i) {
    	this.bitSet.set(i);
    	this.repaint();
    }
    
    public void SetFull() {
    	this.bitSet.set(0, this.dataSize);
    	this.repaint();
    }
    
    public void SetFromBitset(BitSet bs, int size) {
    	if(this.dataSize != size) {
    		this.bitSet=null;
    		this.dataSize=size;	
    		this.bitSet = new BitSet(dataSize);
    	}
    	
    	for(int i=bs.nextSetBit(0); i>=0; i=bs.nextSetBit(i+1)) {
    		bitSet.set(i);
    	}
    	this.repaint();
    }
    
}

