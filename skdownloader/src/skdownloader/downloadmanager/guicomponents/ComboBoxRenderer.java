package skdownloader.downloadmanager.guicomponents;

import java.awt.Component;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class ComboBoxRenderer extends JComboBox<String> implements TableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ComboBoxRenderer(String[] items) {
		super(items); 
	}
	
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (isSelected) {
			setForeground(table.getSelectionForeground()); 
			super.setBackground(table.getSelectionBackground());
		} 
		else { 
			setForeground(table.getForeground());
			setBackground(table.getBackground()); 
		}
		// Select the current value 
		setSelectedItem(value);
		return this; 
	}
}