/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Image observer for showing animated images in table cell.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import java.awt.Image;
import java.awt.image.ImageObserver;
import javax.swing.JTable;

public class CellImageObserver implements ImageObserver 
{
    JTable table;
    int row;
    int col;

    public CellImageObserver(JTable table, int row, int col) {
    	this.table = table;
    	this.row = row;
    	this.col = col;
    }

    public boolean imageUpdate(Image img, int flags, int x, int y, int w, int h) {
    	if ((flags & (FRAMEBITS | ALLBITS)) != 0) {
    		table.repaint(table.getCellRect(row, col, false));
    	}
    	return (flags & (ALLBITS | ABORT)) == 0;
    }
}
