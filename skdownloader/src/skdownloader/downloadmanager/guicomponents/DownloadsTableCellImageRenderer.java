/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Default renderer used to show Images in table.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import skdownloader.downloadmanager.SystemInfoHolder;

public class DownloadsTableCellImageRenderer extends DefaultTableCellRenderer
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//private SystemInfoHolder sysInfoHolder;
	
	public DownloadsTableCellImageRenderer(SystemInfoHolder sysInfoHolder)
	{
		//this.sysInfoHolder=sysInfoHolder;
	}

	public Component getTableCellRendererComponent(  
			 JTable table, Object value, boolean isSelected, 
			 boolean hasFocus, int row, int col)
			 {
			      Component comp = super.getTableCellRendererComponent(
			                       table,  value, isSelected, hasFocus, row, col);
			      if(! isSelected)
			      {
			    	  if(((row+2) %2 ) == 0) 
			    	  {
			    		  //comp.setBackground(sysInfoHolder.GetTheme().TABLEROW1BGCOLOR);
			    		  
			    	  }
			    	  else
			    	  {
			    		  //comp.setBackground(sysInfoHolder.GetTheme().TABLEROW2BGCOLOR);
			    	  }
			      }
			      else
			      {
			    	  if(((row+2) %2 ) == 0) 
			    	  {
			    		  //comp.setBackground(sysInfoHolder.GetTheme().TABLEROW1SELECTIONBGCOLOR);
			    		  
			    	  }
			    	  else
			    	  {
			    		  //comp.setBackground(sysInfoHolder.GetTheme().TABLEROW2SELECTIONBGCOLOR);
			    	  }
			      }
			      return( comp );
			  }
		
		public void setValue(Object value)
		{
			if(value.getClass() == ImageIcon.class)
			{
				setIcon((ImageIcon)value);
			}
		}
}