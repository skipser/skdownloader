/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Table model for downloads table.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;

import skdownloader.downloadmanager.GuiUserIOUtils;
import skdownloader.downloadmanager.MainFrame;
import skdownloader.downloadmanager.guicomponents.search.SearchResultItem;

// This class manages the download table's data.
public class SearchTableModel extends AbstractTableModel //implements Observer 
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// These are the names for the table's columns.
    private static final String[] columnNames = {"#", "Name", "Size", "Seeds", "Leeches", ""};
    
    // These are the classes for each column's values.
    private static final Class<?>[] columnClasses = {String.class, String.class, String.class, String.class, String.class, JButton.class};
    
    // The table's list of downloads.
    private ArrayList<SearchResultItem> torrentList = new ArrayList<SearchResultItem>();
    
    @SuppressWarnings("unused")
	private GuiUserIOUtils ioutils;
    MainFrame mainFrame;
 
    public void InitAll(GuiUserIOUtils ioutils, MainFrame mainFrame)
    {
    	// index  Description
    	//     0  Downloading staus label
    	//     1  Paused status label
    	//     2  Cancelled status label
    	//     3  Finished status label
    	this.mainFrame=mainFrame;
    	this.ioutils = ioutils;

    }
   
    // Get a download for the specified row.
    public synchronized SearchResultItem GetDownload(int row) {
    	return (SearchResultItem) torrentList.get(row);
    }

    // Add a new download to the table.
    public synchronized void AddItem(SearchResultItem item) 
    {
        // Add download to download list
    	torrentList.add(item);
        
        // Fire table row insertion notification to table.
        fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
    }
    
    // Remove a download from the list.
    public synchronized void ClearItem(SearchResultItem item){
    	//Find row to be removed.
    	int row = GetRowForItem(item);
    	
    	if(row == -1)
    		return;
    	
    	//Remove row from table
    	torrentList.remove(row);
        
        // Fire table row deletion notification to table.
        fireTableRowsDeleted(row, row);
    }

    //Get table's column count.
    public int getColumnCount() {
        return columnNames.length;
    }
    
    // Get a column's name.
    public String getColumnName(int col){
        return columnNames[col];
    }
    
    // Get a column's class.
    public Class<?> getColumnClass(int col){
        return columnClasses[col];
    }
    
    // Get table's row count.
    public int getRowCount() {
        return torrentList.size();
    }
    
    public int GetRowForItem(SearchResultItem item){
    	return torrentList.indexOf(item);
    }
    
    // Get value for a specific row and column combination.
    public Object getValueAt(int row, int col){
    	SearchResultItem item = null;
    	try {
    		item = (SearchResultItem) torrentList.get(row);
    	}catch(Exception e) {
    		return "";
    	}
    	
    	if(item == null)
    		return "";
    	
        switch (col) {
        case 0: // index
        	return row;
        case 1: // Name
        	return item.name;
        case 2: // Size
        	return item.size;
        case 3: // seeds
        	return item.seeds;
        case 4: // Leeches
        	return item.leeches;
        }
        return "";
    }
    
    
    
    public boolean isCellEditable() {
    	return true;
    	}
}
