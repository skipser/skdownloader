package skdownloader.downloadmanager.guicomponents;

/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Sliding frame to show messges like download finish.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/17/09 - Fix sleep not working properly in Linux
 *
 ****************************************************************************************/

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.JWindow;
import javax.swing.SpringLayout;
import javax.swing.border.EtchedBorder;


import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.lang.reflect.Method;


public class SlidingFrame extends Thread implements MouseListener, ActionListener
{

	private JWindow frame;
    private JPanel motherPane, bodyPane;

    JLabel closeLbl;
    //private String frameText;
    private String frameHdr;
    private String fileName;
    private String time;
    private String size;
    
    JTextField fileNameLbl2, timeLbl1, sizeLbl1;
    
    private static int SLIDING_FRAME_WIDTH=250;
    private static int SLIDING_FRAME_HEIGHT=130;
    //private static int CURRENT_X;
    //private static int CURRENT_Y;
    
    private String copyfilepathtoclipboard = "copyfilepathtoclipboard";
    private String copyurltoclipboard = "copyurltoclipboard";
    private String close = "close";
    
    Dimension dim;
    //private float opacity = 0f;
    
    private static int MAX_DISPLAY_TIME=10000;
    
    public SlidingFrame(String hdr, String fileName, String filePath, String url, String time, String size)
    {
    	frameHdr = hdr;
    	this.fileName=fileName;
    	this.time=time;
    	this.size=size;
    	//frameText=text;
	    Toolkit toolkit =  Toolkit.getDefaultToolkit ();
        dim = toolkit.getScreenSize();
    }

	public void run()
	{
		java.awt.EventQueue.invokeLater(new Runnable() {public void run() 
		{
			ShowFrame();
		}});
	}
		
	public void ShowFrame()
		{
		frame = new JWindow();
		motherPane = new JPanel();
		SpringLayout layout = new SpringLayout();
		motherPane.setLayout(layout);
		motherPane.setBackground(new Color(0x99cccc));
		//motherPane.setBackground(new Color(0xffeecd));
		
		motherPane.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		JLabel header = new JLabel(frameHdr,
								   new ImageIcon(this.getClass().getResource("images/skdownloader32.png")),
                				   JLabel.CENTER);
		header.setVerticalTextPosition(JLabel.CENTER);
		header.setHorizontalTextPosition(JLabel.RIGHT);
		header.setFont(new Font(header.getFont().getName(),Font.BOLD,header.getFont().getSize()));
		header.setAlignmentX(Component.LEFT_ALIGNMENT);
		header.setInheritsPopupMenu(true);
		//header.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		motherPane.add(header);
		
		layout.putConstraint(SpringLayout.NORTH, header, 5, SpringLayout.NORTH, motherPane);
		layout.putConstraint(SpringLayout.WEST, header, 5, SpringLayout.WEST, motherPane);

		bodyPane = new JPanel();
		//GroupLayout layout1 = new GroupLayout(bodyPane);
		//layout1.setAutoCreateGaps(true);
		GridLayout layout1 = new GridLayout(0,1);
		bodyPane.setLayout(layout1);
		
		fileNameLbl2 = new JTextField();
		//fileNameLbl2.setFont(new Font(fileNameLbl.getFont().getName(),fileNameLbl.getFont().getStyle(),fileNameLbl.getFont().getSize()));
		fileNameLbl2.setText(" "+fileName);
		fileNameLbl2.setBorder(BorderFactory.createEmptyBorder());
		fileNameLbl2.setBackground(new Color(0xf0f5fa));
		fileNameLbl2.setCaretPosition(0);
		bodyPane.add(fileNameLbl2);
		timeLbl1     = new JTextField();
		timeLbl1.setText(" Time: "+time);
		timeLbl1.setBorder(BorderFactory.createEmptyBorder());
		timeLbl1.setBackground(new Color(0xf0f5fa));
		timeLbl1.setCaretPosition(0);
		bodyPane.add(timeLbl1);
		sizeLbl1     = new JTextField();
		sizeLbl1.setText(" Size: "+size);
		sizeLbl1.setBorder(BorderFactory.createEmptyBorder());
		sizeLbl1.setBackground(new Color(0xf0f5fa));
		sizeLbl1.setCaretPosition(0);
		bodyPane.add(sizeLbl1);
		bodyPane.setBackground(new Color(0xf0f5fa));
		//bodyPane.setBackground(new Color(0x80abc6));
		bodyPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		motherPane.add(bodyPane);
        
        layout.putConstraint(SpringLayout.NORTH, bodyPane, 5, SpringLayout.SOUTH, header);
		layout.putConstraint(SpringLayout.WEST, bodyPane, 5, SpringLayout.WEST, motherPane);
        
        closeLbl = new JLabel("Close");
        closeLbl.setForeground(Color.blue);
        closeLbl.addMouseListener(this);
        motherPane.add(closeLbl);
        layout.putConstraint(SpringLayout.NORTH, closeLbl, 5, SpringLayout.SOUTH, bodyPane);
        layout.putConstraint(SpringLayout.WEST, closeLbl, 5, SpringLayout.WEST, motherPane);
		layout.putConstraint(SpringLayout.EAST, motherPane, 5, SpringLayout.EAST, bodyPane);
		layout.putConstraint(SpringLayout.SOUTH, motherPane, 5, SpringLayout.SOUTH, closeLbl);
        
	    final JPopupMenu popupMenu = new JPopupMenu();

	     // Copy file path
	    JMenuItem menuItem = new JMenuItem("Copy file path to clipboard", new ImageIcon(this.getClass().getResource("images/copy.png")));
	    menuItem.setActionCommand(copyfilepathtoclipboard);
	    menuItem.addActionListener(this);
	    popupMenu.add(menuItem);

	    // Copy url
	    menuItem = new JMenuItem("Copy Url to clipboard", new ImageIcon(this.getClass().getResource("images/copy.png")));
	    menuItem.setActionCommand(copyurltoclipboard);
	    menuItem.addActionListener(this);
	    popupMenu.add(menuItem);
	    
	    // Separator
	    popupMenu.addSeparator();

	    // Close
	    menuItem = new JMenuItem("Close", new ImageIcon(this.getClass().getResource("images/browser_cancelled.png")));
	    menuItem.setActionCommand(close);
	    menuItem.addActionListener(this);
	    menuItem.setEnabled(true);
	    popupMenu.add(menuItem);

	    motherPane.setComponentPopupMenu(popupMenu);

	    frame.add(motherPane);

        //frame.setBounds(dim.width, (int)((dim.height-SLIDING_FRAME_HEIGHT)/2), SLIDING_FRAME_WIDTH, SLIDING_FRAME_HEIGHT);
	    frame.setBounds(dim.width-SLIDING_FRAME_WIDTH, (int)((dim.height-SLIDING_FRAME_HEIGHT)/2), SLIDING_FRAME_WIDTH, SLIDING_FRAME_HEIGHT);
	    try {
		    Class<?> awtUtilitiesClass = Class.forName("com.sun.awt.AWTUtilities");
		    Method mSetWindowOpacity = awtUtilitiesClass.getMethod("setWindowOpacity", Window.class, float.class);
		    mSetWindowOpacity.invoke(null, frame, Float.valueOf(0));
	    }catch(Exception e) {}
	    frame.setVisible(true);
	    frame.setAlwaysOnTop(true);
	    
	    //Listener for window closing event.
	    frame.addWindowListener(new WindowAdapter(){
	      public void windowClosing(WindowEvent we){
	        Close();
	      }
	    });

	    java.awt.EventQueue.invokeLater(new Runnable() {public void run() {
	    /*int CURRENT_Y=(int)((dim.height-SLIDING_FRAME_HEIGHT)/2);
	    int sleepTime = 1500*2/SLIDING_FRAME_WIDTH;
		frame.setLocationByPlatform(false);
	    for(int i=0; i<=SLIDING_FRAME_WIDTH; i+=10)
		{
			int CURRENT_X=dim.width-i;
			frame.setBounds(CURRENT_X, CURRENT_Y, i, SLIDING_FRAME_HEIGHT);

			try {
			Thread.sleep(sleepTime);
			} catch (Exception e) {}
		}*/
	    	try {
	    		Class<?> awtUtilitiesClass = Class.forName("com.sun.awt.AWTUtilities");
	    		Method mSetWindowOpacity = awtUtilitiesClass.getMethod("setWindowOpacity", Window.class, float.class);
	    		for(int i=5; i<=100; i+=5) {
	    			float j=(float)i/100;
	    			mSetWindowOpacity.invoke(null, frame, Float.valueOf(j));
	    			Thread.sleep(50);
	    		}
	    	} catch(Exception e) {}
	    }});
	
		//Start timer to automatically cloase after max seconds
		javax.swing.Timer t = new javax.swing.Timer(MAX_DISPLAY_TIME, new ActionListener() {
	          public void actionPerformed(ActionEvent e) {
	              Close();
	          }
	       });
		t.start();
	}
	
	public void Close()
	{
	    int CURRENT_Y=(int)((dim.height-SLIDING_FRAME_HEIGHT)/2);
	    int sleepTime = 1000*2/SLIDING_FRAME_WIDTH;
		for(int i=SLIDING_FRAME_WIDTH-5; i>=0; i-=10)
		{
			int CURRENT_X=dim.width-i;
			frame.setBounds(CURRENT_X, CURRENT_Y, i, SLIDING_FRAME_HEIGHT);
			try {
			Thread.sleep(sleepTime);
			} catch (Exception e) {}
		}

		frame.removeAll();
		frame.dispose();
	}
	

	public void mousePressed(MouseEvent e) {
    }
    
    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    	closeLbl.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }

    public void mouseExited(MouseEvent e) {
    	closeLbl.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
    
    public void mouseClicked(MouseEvent e) {
    	Close();
    }

	public void actionPerformed(ActionEvent e) 
	{
		if(e.getActionCommand().equals(close))
		{
			Close();
		}
	}
}