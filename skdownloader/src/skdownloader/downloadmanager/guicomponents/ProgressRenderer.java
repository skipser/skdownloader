/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Renderer to show progress bar in table cell.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.JProgressBar;
import javax.swing.table.TableCellRenderer;

import skdownloader.downloadmanager.SystemInfoHolder;

// This class renders a JProgressBar in a table cell.
public class ProgressRenderer extends JProgressBar implements TableCellRenderer 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//private SystemInfoHolder sysInfoHolder;

	// Constructor for ProgressRenderer.
	public ProgressRenderer(int min, int max, SystemInfoHolder sysInfoHolder) 
	{
		super(min, max);
		//this.sysInfoHolder=sysInfoHolder;
	}

	/* Returns this JProgressBar as the renderer
	for the given table cell. */
	public Component getTableCellRendererComponent(
    JTable table, Object value, boolean isSelected,
    boolean hasFocus, int row, int column) {
		// Set JProgressBar's percent complete value.
		if((value == null) || (value == "") || ((int) ((Float) value).floatValue() == 200))
			setString("Size unknown");
		else{
			if(getString() != null)
				setString(null);
			setValue((int) ((Float) value).floatValue());
		}

		return this;
	}
}
