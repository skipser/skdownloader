/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Version displayer
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

//import edu.stanford.ejalbert.BrowserLauncher;

import java.awt.BorderLayout;

import skdownloader.downloadmanager.GUICommon;
import skdownloader.downloadmanager.SystemInfoHolder;

public class VersionDisplayer
{
	private SystemInfoHolder sysInfoHolder;
	private JFrame parentFrame;
	
	public VersionDisplayer(JFrame parentFrame, SystemInfoHolder sysInfoHolder)
	{
		this.parentFrame=parentFrame;
		this.sysInfoHolder = sysInfoHolder;
	}
	
	public void Show()
	{
		JPanel panel = new JPanel((new BorderLayout()));
		JTextPane textPane = new JTextPane();
		textPane.setContentType("text/html");
		textPane.setEditable(false);
		textPane.setText("<html>" +
				         "  <body> <font size=\"3\" face=\"Verdana\">" +
				         "    <br/>" +
				         "    SKDownloader" +
				         "    <br/><br/>" +
				         "    Version: "+sysInfoHolder.VERSION+
				         "    <br/><br/>" +
				         sysInfoHolder.COPYRIGHT+
				         "    <br/>"+
				         "     Visit <a href=\""+sysInfoHolder.COMPANYURL+"\"><font color=\"blue\">"+sysInfoHolder.COMPANYURL+"</font></a>" +
				         "     <br/>" +
				         "  </font></body>" +
				         "</html>");
		
		textPane.addHyperlinkListener(new HyperlinkListener() {
            public void hyperlinkUpdate(HyperlinkEvent r) {
                try {
                    if(r.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    	GUICommon.LaunchInBrowser(r.getURL().toString());
                    }
                    
                }catch(Exception e){}
            }
        });

		panel.add(textPane);
		JOptionPane.showMessageDialog(parentFrame, panel, "Version",JOptionPane.OK_OPTION, new ImageIcon(this.getClass().getResource("images/skdownloader64.png")));
	}
}