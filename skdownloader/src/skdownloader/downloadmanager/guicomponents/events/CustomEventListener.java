/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Generic custom event listener
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents.events;

import java.util.EventListener;

// Declare the listener class. It must extend EventListener.
    // A class must implement this interface to get MyEvents.
public interface CustomEventListener extends EventListener 
{
	public void CustomEventOccurred(CustomEvent evt);
}
