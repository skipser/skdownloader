/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    GEneric custom event.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.guicomponents.events;

import java.util.EventObject;

// Declare the event. It must extend EventObject.
 public class CustomEvent extends EventObject 
 {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomEvent(Object source) 
	 {
		 super(source);
	 }
 }
 
