/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Main GUI displayer
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/19/09 - Add color scheme support
 *   arun         06/02/09 - History download in paused state not started causing exception on resume
 *   arun         06/07/09 - Remove entry for automatically cancelled downloads
 *   arun         07/30/09 - Add first time run function
 *   arun         06/02/10 - Disable ads in details window.
 *
 ****************************************************************************************/

package skdownloader.downloadmanager;

import javax.swing.border.EtchedBorder;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSplitPane;
import javax.swing.JScrollPane;
import javax.swing.JProgressBar;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.AbstractButton;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.GroupLayout;

import java.awt.FlowLayout;
import java.awt.BorderLayout;

import javax.swing.BorderFactory;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.Font;
import java.awt.Frame;
import java.awt.MenuItem;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Observer;
import java.util.Observable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import skdownloader.core.Common;
import skdownloader.core.DownloadListener;
import skdownloader.core.DownloadStateHolder;
import skdownloader.core.UpdateChecker;
import skdownloader.core.torrentcore.TorrentDownloadProgressAdminThread;
import skdownloader.downloadmanager.guicomponents.DownloaderTheme;
import skdownloader.downloadmanager.guicomponents.DownloadsTableColumnManager;
import skdownloader.downloadmanager.guicomponents.ManagerTableHeader;
import skdownloader.downloadmanager.guicomponents.ManagerTableModel;
import skdownloader.downloadmanager.guicomponents.TorrentDownloadInputDialog;
import skdownloader.downloadmanager.guicomponents.ProgressRenderer;
import skdownloader.downloadmanager.guicomponents.UrlDownloadInputDialog;
import skdownloader.downloadmanager.guicomponents.CellImageObserver;
import skdownloader.downloadmanager.guicomponents.SlidingFrame;
import skdownloader.downloadmanager.guicomponents.VersionDisplayer;
import skdownloader.downloadmanager.guicomponents.browser.WelcomeDisplayer;
import skdownloader.downloadmanager.guicomponents.search.SearchPanel;
import skdownloader.downloadmanager.guicomponents.tree.BrowserTreeCellRenderer;
import skdownloader.downloadmanager.guicomponents.tree.BrowserTreeSubstanceCellRenderer;
import skdownloader.downloadmanager.guicomponents.tree.ViewStateTreeNodeDisplayer;
import skdownloader.downloadmanager.xmlcomponents.ConfigXmlUpdater;
import skdownloader.downloadmanager.xmlcomponents.HistoryXmlUpdater;
import skdownloader.downloadmanager.xmlcomponents.RuntimeOptionsXmlUpdater;

import com.aelitis.azureus.core.AzureusCore;
import com.aelitis.azureus.core.AzureusCoreFactory;

import org.apache.log4j.Logger;
import org.gudy.azureus2.core3.config.COConfigurationManager;
import org.gudy.azureus2.core3.download.DownloadManager;
import org.gudy.azureus2.core3.security.SESecurityManager;

import java.awt.Desktop;

import org.w3c.dom.NodeList;


public class MainFrame implements ActionListener,  KeyListener, Observer, TreeSelectionListener, DownloadListener{
	private JFrame motherFrame;
	JTabbedPane tabbedPane;
	private JPanel managerPanel, creditsPanel, rightPanel;
	private JToolBar topButtonsPanel;
	private JSplitPane browserManagerSplitPanel; // This is a split panel for holding download list and browser
	private JPanel browserPanel, downloadTablePanel, welcomePanel;
	private SearchPanel searchPanel;
	
	JMenuBar menuBar;

	private JButton addButt, addTorrentButt, pauseButt, resumeButt, cancelButt, clearButt, scheduleButt;
	
	private JTable downloadsTable;
	private ManagerTableModel tableModel;
	private JPopupMenu tablePopupMenu;
	
	private JTree browserTree;
	private DefaultMutableTreeNode baseTreeNode, homeTreeNode, allTreeNode, activeTreeNode, cancelledTreeNode, completedTreeNode, seedingTreeNode, searchTreeNode;
	private ViewStateTreeNodeDisplayer baseTreeNodeDisplayer, homeTreeNodeDisplayer ,allTreeNodeDisplayer, activeTreeNodeDisplayer, cancelledTreeNodeDisplayer, completedTreeNodeDisplayer, seedingTreeNodeDisplayer, searchTreeNodeDisplayer;
	final JTextField searchField;

	WelcomeDisplayer welcomeDisplayer;
	UpdateChecker updateChecker;
	
	private CreditsDisplayer creditsDisplayer;
	
	// Flag for whether or not table selection is being cleared.
    private boolean clearing = false;
	
	// Currently selected download.
    private GuiDownloaderWrapperInterface selectedDownload;
    
    //Arrays for differnt types of views.
    ArrayList<GuiDownloaderWrapperInterface> baseArr, homeArr, allDownloadsArr, completedDownloadsArr, 
                                       activeDownloadsArr, cancelledDownloadsArr, seedingDownloadsArr, searchDownloadsArr;
    
    //Default number of connections for a download added.
    private int numConnections=GUICommon.NUMCONNECTIONS;
    
    private String addurldownload = "addurldownload";
    private String addtorrentdownload = "addtorrentdownload";
    private String showdetails = "showdetails";
    private String pausedownload = "pausedownload";
    private String resumedownload = "resumedownload";
    private String canceldownload = "canceldownload";
    private String cleardownload = "cleardownload";
    private String scheduledownloads = "scheduledownloads";
    private String pausealldownloads = "pausealldownloads";
    private String resumealldownloads = "resumealldownloads";
    private String cancelalldownloads = "cancelalldownloads";
    private String clearalldownloads = "clearalldownloads";
    private String cleardownloadanddeletefile = "cleardownloadanddeletefile";
    private String copyfilepathtoclipboard = "copyfilepathtoclipboard";
    private String copyurltoclipboard = "copyurltoclipboard";
    private String showfolder = "showfolder";
    private String searchfortorrent="searchfortorrent";
    public static String searchfortorrent1 = "searchfortorrent1";
    private String options = "options";
    private String close = "close";
    private String exit = "exit";
    private String searchInitText = "Search Torrents";
    
    private String DOWNLOADTABLEPANEL = "DOWNLOADTABLEPANEL";
    private String SEARCHPANEL = "SEARCHPANEL";
    private String WELCOMEPANEL = "WELCOMEPANEL";
    
    private JMenuItem pausePopupmenuItem, resumePopupmenuItem, 
                      cancelPopupmenuItem, clearPopupmenuItem, 
                      copyurlPopupmenuItem, closePopupmenuItem,
                      deletePopupmenuItem, detailsPopupmenuItem,
                      folderPopupmenuItem;
	
    GuiUserIOUtils ioutils;
    SystemInfoHolder sysInfoHolder;
    private DownloadsTableColumnManager colManager;
    
    private TorrentDownloadProgressAdminThread torrentProgressAdminThread;

    public int currentViewState;
	
	HistoryXmlUpdater historyXmlUpdater ;
	ConfigXmlUpdater configUpdater;
	RuntimeOptionsXmlUpdater runtimeOptionsUpdater;
	
	public PopupMenu trayPopup = new PopupMenu();
	final TrayIcon trayIcon = new TrayIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/skdownloader32.png")).getImage(), "SKDownloader", trayPopup);
	
	private AzureusCore core;
	private static Logger logger = Logger.getLogger(MainFrame.class);
	
	public MainFrame(SystemInfoHolder h) {
		this. sysInfoHolder=h;
		searchField = new JTextField();
	}
	
	/*************************************************************
	 * 
	 * Function  : ShowFrame
	 * Arguments : void
	 * Return    : void
	 * Notes     : Main function for creating and displaying frame.
	 * 
	 *************************************************************/
	public void ShowFrame() {
		//Update sysInfoHolder with preferences
		sysInfoHolder.TORRENT_LISTEN_PORT=19919; //Just setting to default just in case..
		configUpdater = new ConfigXmlUpdater(sysInfoHolder);
		configUpdater.UpdateSystemVars();

		runtimeOptionsUpdater = new RuntimeOptionsXmlUpdater(sysInfoHolder);
		runtimeOptionsUpdater.UpdateRuntimeOptions();
		
		//Start input listener thread
		InputListenerThread listener = new InputListenerThread(this, sysInfoHolder, sysInfoHolder.LIVEPORT);
		listener.start();

		//Create and add system tray icon
		if(sysInfoHolder.TRAYENABLED) {
			AddSystemTray();
		}
		
		//Do any first time configuration required.
		if(sysInfoHolder.FIRSTRUN == 1) {
			RunFirstTimeConfig();
		}
		
		ioutils = new GuiUserIOUtils();
		
		SetSystemProxy();
		
		currentViewState = ViewStateTreeNodeDisplayer.HOME;
		historyXmlUpdater = new HistoryXmlUpdater(sysInfoHolder);
		
		//Set theme
		JFrame.setDefaultLookAndFeelDecorated(true);
	    JDialog.setDefaultLookAndFeelDecorated(true);
		try {
			if(DownloaderTheme.GetTheme(sysInfoHolder.GetTheme()).equals("system"))
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			else 
			UIManager.setLookAndFeel(DownloaderTheme.GetTheme(sysInfoHolder.GetTheme()));
		} catch (Exception e1) {
		}
		
		allDownloadsArr = new ArrayList<GuiDownloaderWrapperInterface>();
		completedDownloadsArr = new ArrayList<GuiDownloaderWrapperInterface>();
		activeDownloadsArr = new ArrayList<GuiDownloaderWrapperInterface>();
		cancelledDownloadsArr = new ArrayList<GuiDownloaderWrapperInterface>();
		seedingDownloadsArr = new ArrayList<GuiDownloaderWrapperInterface>();
		searchDownloadsArr = new ArrayList<GuiDownloaderWrapperInterface>();
		
		motherFrame = new JFrame("SKDownloader");
		// Disable close operating on close button press. We need to do this conditionally
		motherFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		motherFrame.setIconImage(new ImageIcon(this.getClass().getResource("guicomponents/images/skdownloader32.png")).getImage());
		motherFrame.setMinimumSize(new Dimension(sysInfoHolder.APP_MIN_WIDTH, sysInfoHolder.APP_MIN_HEIGHT));
		//Set size from preferred size.
		motherFrame.setSize(new Dimension(sysInfoHolder.APP_PREFERRED_WIDTH, sysInfoHolder.APP_PREFERRED_HEIGHT));

		//Build the first menu.
		menuBar = new JMenuBar();
		JMenu menu = new JMenu("File");
		menu.setMnemonic(KeyEvent.VK_A);
		menu.getAccessibleContext().setAccessibleDescription("File items");
		menuBar.add(menu);
		
		JMenuItem menuItem = new JMenuItem("Add new url", KeyEvent.VK_T);
		menuItem.setActionCommand(addurldownload);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.ALT_MASK));
		menuItem.getAccessibleContext().setAccessibleDescription("Add a new url download");
		menuItem.setIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/add_menu_image.png")));
		menuItem.addActionListener(this);
		menu.add(menuItem);
		
		menuItem = new JMenuItem("Add new torrent", KeyEvent.VK_T);
		menuItem.setActionCommand(addtorrentdownload);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.ALT_MASK));
		menuItem.getAccessibleContext().setAccessibleDescription("Add a new torrent download");
		menuItem.setIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/add_menu_image.png")));
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menu.addSeparator();

		menuItem = new JMenuItem("Exit", KeyEvent.VK_T);
		menuItem.setActionCommand(exit);
		menuItem.getAccessibleContext().setAccessibleDescription("Exit application");
		//menuItem.setIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/browser_cancelled.png")));
		menuItem.addActionListener(this);
		menu.add(menuItem);
		
		menu = new JMenu("Tools");
		menu.setMnemonic(KeyEvent.VK_A);
		menu.getAccessibleContext().setAccessibleDescription("Tools");
		menuBar.add(menu);
		
		menuItem = new JMenuItem("Options", KeyEvent.VK_T);
		menuItem.setActionCommand(options);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.ALT_MASK));
		menuItem.getAccessibleContext().setAccessibleDescription("Configuration options for skdownloader");
		menuItem.setIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/configuration_menu_image.png")));
		menuItem.addActionListener(this);
		menu.add(menuItem);

		menuItem = new JMenuItem("Clear all inactive entries", KeyEvent.VK_T);
		menuItem.setActionCommand(clearalldownloads);
		menuItem.getAccessibleContext().setAccessibleDescription("Clear all history downloads");
		menuItem.setIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/clear_menu_image.png")));
		menuItem.addActionListener(this);
		menu.add(menuItem);
		//menu.addSeparator();
		
		//Downloads menu
		menu = new JMenu("Downloads");
		menu.setMnemonic(KeyEvent.VK_A);
		menu.getAccessibleContext().setAccessibleDescription("Downloads");
		menuBar.add(menu);

		menuItem = new JMenuItem("Pause all downloads", KeyEvent.VK_T);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.ALT_MASK));
		menuItem.setActionCommand(pausealldownloads);
		menuItem.getAccessibleContext().setAccessibleDescription("Pauses all active downloads");
		menuItem.setIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/pause_menu_image.png")));
		menuItem.addActionListener(this);
		menu.add(menuItem);
		
		menuItem = new JMenuItem("Resusme all downloads", KeyEvent.VK_T);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.ALT_MASK));
		menuItem.setActionCommand(resumealldownloads);
		menuItem.getAccessibleContext().setAccessibleDescription("Resumes any paused downloads");
		menuItem.setIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/resume_menu_image.png")));
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menu.addSeparator();
		
		menuItem = new JMenuItem("Cancel all downloads", KeyEvent.VK_T);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.ALT_MASK));
		menuItem.setActionCommand(cancelalldownloads);
		menuItem.getAccessibleContext().setAccessibleDescription("Stops all active downloads");
		menuItem.setIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/cancel_menu_image.png")));
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menu.addSeparator();
		
		menuItem = new JMenuItem("Schedule download", KeyEvent.VK_T);
		menuItem.setIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/schedule_menu_image.png")));
		menuItem.setActionCommand(scheduledownloads);
		menuItem.getAccessibleContext().setAccessibleDescription("Schedule downloads");
		menuItem.addActionListener(this);
		menu.add(menuItem);
		
		menu = new JMenu("Help");
		menu.setMnemonic(KeyEvent.VK_A);
		menu.getAccessibleContext().setAccessibleDescription("Help items");
		menuBar.add(menu);
		
		menuItem = new JMenuItem("Visit home page", KeyEvent.VK_T);
		menuItem.setActionCommand("VisitHomepage");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.ALT_MASK));
		menuItem.getAccessibleContext().setAccessibleDescription("SKDownloader home page");
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menu.addSeparator();
		
		menuItem = new JMenuItem("Check for updates", KeyEvent.VK_U);
		menuItem.setActionCommand("CheckForUpdate");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.ALT_MASK));
		menuItem.getAccessibleContext().setAccessibleDescription("Check for updates");
		menuItem.addActionListener(this);
		menu.add(menuItem);
		
		menuItem = new JMenuItem("About", KeyEvent.VK_V);
		menuItem.setActionCommand("Credits");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
		menuItem.getAccessibleContext().setAccessibleDescription("Show credits");
		menuItem.addActionListener(this);
		menu.add(menuItem);
		
		menuItem = new JMenuItem("Version", KeyEvent.VK_V);
		menuItem.setActionCommand("Version");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.ALT_MASK));
		menuItem.getAccessibleContext().setAccessibleDescription("Show version");
		menuItem.addActionListener(this);
		menu.add(menuItem);
		
		menuBar.setBorder(null);
		motherFrame.setJMenuBar(menuBar);
		
		managerPanel = new JPanel();
		creditsPanel = new JPanel();
		
		tabbedPane = new JTabbedPane();
		tabbedPane.addTab( "Manager", managerPanel );
		tabbedPane.addTab( "Credits", creditsPanel );
		
		motherFrame.add(managerPanel);

		GroupLayout layout = new GroupLayout(managerPanel);
		managerPanel.setLayout(layout);
		
		topButtonsPanel = new JToolBar();
		topButtonsPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
		addButt = new JButton("Add URL", new ImageIcon(this.getClass().getResource("guicomponents/images/add_butt_normal_image.png")));
		addButt.setVerticalTextPosition(AbstractButton.BOTTOM);
		addButt.setHorizontalTextPosition(AbstractButton.CENTER);
		addButt.addActionListener(this);
		addButt.setActionCommand(addurldownload);
		addButt.setToolTipText("Add new download");
		topButtonsPanel.add(addButt);
		
		addTorrentButt = new JButton("Add Torrent", new ImageIcon(this.getClass().getResource("guicomponents/images/add_butt_normal_image.png")));
		addTorrentButt.setVerticalTextPosition(AbstractButton.BOTTOM);
		addTorrentButt.setHorizontalTextPosition(AbstractButton.CENTER);
		addTorrentButt.addActionListener(this);
		addTorrentButt.setActionCommand(addtorrentdownload);
		addTorrentButt.setToolTipText("Add new torrent download");
		topButtonsPanel.add(addTorrentButt);
		
		pauseButt = new JButton("Pause", new ImageIcon(this.getClass().getResource("guicomponents/images/pause_butt_normal_image.png")));
		pauseButt.setDisabledIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/pause_butt_disabled_image.png")));
		pauseButt.setVerticalTextPosition(AbstractButton.BOTTOM);
		pauseButt.setHorizontalTextPosition(AbstractButton.CENTER);
		pauseButt.addActionListener(this);
		pauseButt.setActionCommand(pausedownload);
		pauseButt.setToolTipText("Pause download");
		topButtonsPanel.add(pauseButt);
		
		resumeButt = new JButton("Resume", new ImageIcon(this.getClass().getResource("guicomponents/images/resume_butt_normal_image.png")));
		resumeButt.setDisabledIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/resume_butt_disabled_image.png")));
		resumeButt.setVerticalTextPosition(AbstractButton.BOTTOM);
		resumeButt.setHorizontalTextPosition(AbstractButton.CENTER);
		resumeButt.addActionListener(this);
		resumeButt.setActionCommand(resumedownload);
		resumeButt.setToolTipText("Resume download");
		topButtonsPanel.add(resumeButt);
		
		cancelButt = new JButton("Cancel", new ImageIcon(this.getClass().getResource("guicomponents/images/cancel_butt_normal_image.png")));
		cancelButt.setDisabledIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/cancel_butt_disabled_image.png")));
		cancelButt.setVerticalTextPosition(AbstractButton.BOTTOM);
		cancelButt.setHorizontalTextPosition(AbstractButton.CENTER);
		cancelButt.addActionListener(this);
		cancelButt.setActionCommand(canceldownload);
		cancelButt.setToolTipText("Cancel download");
		topButtonsPanel.add(cancelButt);
		
		clearButt = new JButton("Delete", new ImageIcon(this.getClass().getResource("guicomponents/images/clear_butt_normal_image.png")));
		clearButt.setDisabledIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/clear_butt_disabled_image.png")));
		clearButt.setVerticalTextPosition(AbstractButton.BOTTOM);
		clearButt.setHorizontalTextPosition(AbstractButton.CENTER);
		clearButt.addActionListener(this);
		clearButt.setActionCommand(cleardownload);
		clearButt.setToolTipText("Delete download");
		topButtonsPanel.add(clearButt);
		
		if((sysInfoHolder.SCHEDULESTARTOPTION == SystemInfoHolder.SCHEDULESTARTDISABLED) &&
		   (sysInfoHolder.SCHEDULESTOPOPTION == SystemInfoHolder.SCHEDULESTOPDISABLED)) {
			scheduleButt = new JButton("Schedule", new ImageIcon(this.getClass().getResource("guicomponents/images/schedule_off.png")));
		}
		else {
			scheduleButt = new JButton("Schedule", new ImageIcon(this.getClass().getResource("guicomponents/images/schedule_on.png")));
		}
		scheduleButt.setPressedIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/schedule_off.png")));
		scheduleButt.setVerticalTextPosition(AbstractButton.BOTTOM);
		scheduleButt.setHorizontalTextPosition(AbstractButton.CENTER);
		scheduleButt.addActionListener(this);
		scheduleButt.setActionCommand(scheduledownloads);
		scheduleButt.setToolTipText("Schedule download start or stop..");
		topButtonsPanel.add(scheduleButt);
		
		pauseButt.setEnabled(false);
		resumeButt.setEnabled(false);
		cancelButt.setEnabled(false);
		clearButt.setEnabled(false);
		
		browserManagerSplitPanel = new JSplitPane();
		browserManagerSplitPanel.setDividerSize(3);
		browserManagerSplitPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		browserManagerSplitPanel.setDividerLocation(sysInfoHolder.BROWSERMANAGER_SPLILTPANE_PREFERRED_DIVIDER);
		
		browserPanel = new JPanel();
		browserPanel.setLayout(new BorderLayout());
		
		//Browser tree for views.
		baseTreeNodeDisplayer=new ViewStateTreeNodeDisplayer(baseArr, ViewStateTreeNodeDisplayer.BASE);
		baseTreeNode = new DefaultMutableTreeNode(baseTreeNodeDisplayer);
		browserTree = new JTree(baseTreeNode);
		browserTree.setFont(new Font(browserTree.getFont().getFamily(), Font.BOLD, browserTree.getFont().getSize()));
		browserTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		browserTree.putClientProperty("JTree.lineStyle", "Angled");
		homeTreeNodeDisplayer=new ViewStateTreeNodeDisplayer(homeArr, ViewStateTreeNodeDisplayer.HOME);
		homeTreeNode = new DefaultMutableTreeNode(homeTreeNodeDisplayer);
		baseTreeNode.add(homeTreeNode);
		allTreeNodeDisplayer=new ViewStateTreeNodeDisplayer(allDownloadsArr, ViewStateTreeNodeDisplayer.ALL);
		allTreeNode = new DefaultMutableTreeNode(allTreeNodeDisplayer);
		baseTreeNode.add(allTreeNode);
		activeTreeNodeDisplayer=new ViewStateTreeNodeDisplayer(activeDownloadsArr, ViewStateTreeNodeDisplayer.ACTIVE);
		activeTreeNode = new DefaultMutableTreeNode(activeTreeNodeDisplayer);
		baseTreeNode.add(activeTreeNode);
		completedTreeNodeDisplayer=new ViewStateTreeNodeDisplayer(completedDownloadsArr, ViewStateTreeNodeDisplayer.COMPLETED);
        completedTreeNode = new DefaultMutableTreeNode(completedTreeNodeDisplayer);
        baseTreeNode.add(completedTreeNode);
        cancelledTreeNodeDisplayer=new ViewStateTreeNodeDisplayer(cancelledDownloadsArr, ViewStateTreeNodeDisplayer.CANCELLED);
        cancelledTreeNode = new DefaultMutableTreeNode(cancelledTreeNodeDisplayer);
        baseTreeNode.add(cancelledTreeNode);
        seedingTreeNodeDisplayer=new ViewStateTreeNodeDisplayer(seedingDownloadsArr, ViewStateTreeNodeDisplayer.SEEDING);
        seedingTreeNode = new DefaultMutableTreeNode(seedingTreeNodeDisplayer);
        baseTreeNode.add(seedingTreeNode);
        searchTreeNodeDisplayer=new ViewStateTreeNodeDisplayer(searchDownloadsArr, ViewStateTreeNodeDisplayer.SEARCH);
        searchTreeNode = new DefaultMutableTreeNode(searchTreeNodeDisplayer);
        baseTreeNode.add(searchTreeNode);
        JScrollPane browserScrollPanel = new JScrollPane(browserTree);
        browserPanel.add(browserScrollPanel);
        browserTree.addTreeSelectionListener(this);
        browserTree.setRowHeight(35);
        browserTree.scrollPathToVisible(new TreePath(((DefaultTreeModel) browserTree.getModel()).getPathToRoot(((DefaultMutableTreeNode)(browserTree.getModel()).getRoot()).getLastLeaf())));
        
        JPanel searchBoxPanel = new JPanel();
        searchBoxPanel.setLayout(new BorderLayout());
        searchBoxPanel.add(searchField);
        searchField.setForeground(Color.gray);
        searchField.setText(searchInitText);
        searchField.addFocusListener(new FocusListener() {
        	public void focusGained(FocusEvent e) {
            	if(searchField.getText().equals(searchInitText)) {
            		searchField.setText("");
            		searchField.setForeground(Color.BLACK);
            	}
        	}
        	public void focusLost(FocusEvent e) {
        		if(searchField.getText().equals("")) {
        			searchField.setText(searchInitText);
        			searchField.setForeground(Color.gray);
        		}
        	}
        });    
        searchField.setActionCommand(searchfortorrent);
        searchField.addActionListener(this);
        JButton searchButt = new JButton((Icon) (new ImageIcon(this.getClass().getResource("guicomponents/images/search.png"))));
        searchButt.setActionCommand(searchfortorrent);
        searchButt.addActionListener(this);
        searchBoxPanel.add(searchButt, BorderLayout.EAST);
        browserPanel.add(searchBoxPanel, BorderLayout.SOUTH);
        
		browserManagerSplitPanel.setLeftComponent(browserPanel);
		
		rightPanel = new JPanel(new CardLayout());
		rightPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		downloadTablePanel = new JPanel(new BorderLayout());
		rightPanel.add(downloadTablePanel, DOWNLOADTABLEPANEL);
		searchPanel = new SearchPanel(this, ioutils, sysInfoHolder);
		searchPanel.InitComponents();
		rightPanel.add(searchPanel,  SEARCHPANEL);
		welcomePanel = new JPanel();
		welcomePanel.setLayout(new BorderLayout());
		rightPanel.add(welcomePanel,  WELCOMEPANEL);
		((CardLayout)(rightPanel.getLayout())).show(rightPanel, DOWNLOADTABLEPANEL);
		browserManagerSplitPanel.setRightComponent(rightPanel);
		
		// Set up Downloads table.
		colManager = new DownloadsTableColumnManager(sysInfoHolder);
        tableModel = new ManagerTableModel();
        tableModel.InitAll(this, sysInfoHolder, colManager);
        downloadsTable = new JTable(tableModel);
        ManagerTableHeader header = new  ManagerTableHeader(downloadsTable.getColumnModel());
        header.setUnmovableColumn(DownloadsTableColumnManager.COL_ICON_ID);
        downloadsTable.setTableHeader(header);
        downloadsTable.getColumnModel().addColumnModelListener(new TableColumnModelListener(){
			public void columnAdded(TableColumnModelEvent e) {
				// TODO Auto-generated method stub
			}
			public void columnMarginChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
			}
			public void columnMoved(TableColumnModelEvent e) {
				// TODO Auto-generated method stub
				if(e.getFromIndex() == e.getToIndex())
					return;
				//Remember the col change, so we will be storing it for next startup
				colManager.InterChangeColsOrder(e.getFromIndex(), e.getToIndex());
			}
			public void columnRemoved(TableColumnModelEvent e) {
				// TODO Auto-generated method stub
			}
			public void columnSelectionChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});
        AddTableMouseListener(downloadsTable);
        downloadsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e){
                tableSelectionChanged();
            }
        });
        downloadsTable.getTableHeader().addMouseListener(new MouseAdapter() {
        	public void mouseClicked(MouseEvent e) {
        		if(e.getButton() == MouseEvent.BUTTON3) {
        			ShowColumnSelectPopUp(e);
        		}
        	}
        });
        downloadsTable.addKeyListener(this);
        
        // Allow only one row at a time to be selected.
        downloadsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        //Disable auto resizing
        downloadsTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        downloadsTable.setCellSelectionEnabled(false);
        downloadsTable.setRowSelectionAllowed(true);
        downloadsTable.setColumnSelectionAllowed(false);
        // Set up ProgressBar as renderer for progress column.
        ProgressRenderer renderer = new ProgressRenderer(0, 100, sysInfoHolder);
        renderer.setStringPainted(true); // show progress text
        renderer.setValue(0);
        downloadsTable.setDefaultRenderer(JProgressBar.class, renderer);
        
        downloadsTable.setRowHeight((int) renderer.getPreferredSize().getHeight());
        for(int i=0; i< sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE; i++) {
        	downloadsTable.getColumnModel().getColumn(i).setPreferredWidth(colManager.GetColWidth(colManager.GetColType(i)));
        }
       
        //Popup menu for table
        tablePopupMenu = new JPopupMenu();
        //Show details
        detailsPopupmenuItem = new JMenuItem("Show details", new ImageIcon(this.getClass().getResource("guicomponents/images/details_menu_image.png")));
        detailsPopupmenuItem.setActionCommand(showdetails);
        detailsPopupmenuItem.addActionListener(this);
        tablePopupMenu.add(detailsPopupmenuItem);
        
        //Show in explorer menu only for Windows
        //if(sysInfoHolder.OS.equals(SystemInfoHolder.windows)) {
        	folderPopupmenuItem = new JMenuItem("Open folder", new ImageIcon(this.getClass().getResource("guicomponents/images/folder_menu_image.png")));
        	folderPopupmenuItem.setActionCommand(showfolder);
        	folderPopupmenuItem.addActionListener(this);
        	tablePopupMenu.add(folderPopupmenuItem);
        //}
        tablePopupMenu.addSeparator();
	     // Pause download
	    pausePopupmenuItem = new JMenuItem("Pause download", new ImageIcon(this.getClass().getResource("guicomponents/images/pause_menu_image.png")));
	    pausePopupmenuItem.setActionCommand(pausedownload);
	    pausePopupmenuItem.addActionListener(this);
	    tablePopupMenu.add(pausePopupmenuItem);
	    //Resume download
	    resumePopupmenuItem = new JMenuItem("Resume download", new ImageIcon(this.getClass().getResource("guicomponents/images/resume_menu_image.png")));
	    resumePopupmenuItem.setActionCommand(resumedownload);
	    resumePopupmenuItem.addActionListener(this);
	    tablePopupMenu.add(resumePopupmenuItem);
	    // cancel download
	    cancelPopupmenuItem = new JMenuItem("Cancel download", new ImageIcon(this.getClass().getResource("guicomponents/images/cancel_menu_image.png")));
	    cancelPopupmenuItem.setActionCommand(canceldownload);
	    cancelPopupmenuItem.addActionListener(this);
	    tablePopupMenu.add(cancelPopupmenuItem);
	    //clear download
	    clearPopupmenuItem = new JMenuItem("Delete entry", new ImageIcon(this.getClass().getResource("guicomponents/images/clear_menu_image.png")));
	    clearPopupmenuItem.setActionCommand(cleardownload);
	    clearPopupmenuItem.addActionListener(this);
	    tablePopupMenu.add(clearPopupmenuItem);
	    //clear download and delete file if present
	    deletePopupmenuItem = new JMenuItem("Delete entry and remove file", new ImageIcon(this.getClass().getResource("guicomponents/images/clear_menu_image.png")));
	    deletePopupmenuItem.setActionCommand(cleardownloadanddeletefile);
	    deletePopupmenuItem.addActionListener(this);
	    tablePopupMenu.add(deletePopupmenuItem);
	    tablePopupMenu.addSeparator();
	    //Copy file path 
	    JMenuItem copypathPopupmenuItem = new JMenuItem("Copy file path to clipboard", new ImageIcon(this.getClass().getResource("guicomponents/images/copy.png")));
	    copypathPopupmenuItem.setActionCommand(copyfilepathtoclipboard);
	    copypathPopupmenuItem.addActionListener(this);
	    tablePopupMenu.add(copypathPopupmenuItem);
	    // Copy url
	    copyurlPopupmenuItem = new JMenuItem("Copy Url to clipboard", new ImageIcon(this.getClass().getResource("guicomponents/images/copy.png")));
	    copyurlPopupmenuItem.setActionCommand(copyurltoclipboard);
	    copyurlPopupmenuItem.addActionListener(this);
	    tablePopupMenu.add(copyurlPopupmenuItem);
	    // Separator
	    tablePopupMenu.addSeparator();
	    // Close
	    closePopupmenuItem = new JMenuItem("Close", new ImageIcon(this.getClass().getResource("guicomponents/images/browser_cancelled.png")));
	    closePopupmenuItem.setActionCommand(close);
	    closePopupmenuItem.addActionListener(this);
	    tablePopupMenu.add(closePopupmenuItem);

	    JScrollPane tableScrollPane = new JScrollPane(downloadsTable);
	    downloadTablePanel.add(tableScrollPane, BorderLayout.CENTER);
		
		layout.setHorizontalGroup(
			layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addComponent(topButtonsPanel)
				.addGroup(layout.createSequentialGroup()
					.addComponent(browserManagerSplitPanel))
					);
		layout.setVerticalGroup(
			layout.createSequentialGroup()
				.addComponent(topButtonsPanel)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
					.addComponent(browserManagerSplitPanel))
			);
		
        //We need seperate renderer for substance theme. Otherwise, background of tree will be distorted.
        if(DownloaderTheme.GetTheme(sysInfoHolder.GetTheme()).contains("Substance"))
        	browserTree.setCellRenderer(new BrowserTreeSubstanceCellRenderer(sysInfoHolder));
        else {
        	browserTree.setCellRenderer(new BrowserTreeCellRenderer(sysInfoHolder));
        	if(SystemInfoHolder.IsWindows()) {
        		browserScrollPanel.getViewport().setBackground(Color.decode(DownloaderTheme.SYSTEM_TREEBG));
        		browserTree.setOpaque(true);
        		browserTree.setBackground(Color.decode(DownloaderTheme.SYSTEM_TREEBG));
        	}
        	tableScrollPane.getViewport().setBackground(Color.decode(DownloaderTheme.SYSTEM_DOWNLOADTABLEBG));
        }
		
		// Listener for main frame to close on clicking close button
		motherFrame.addWindowListener(new WindowAdapter(){
		      public void windowClosing(WindowEvent we){
		    	  if(sysInfoHolder.TRAYENABLED && sysInfoHolder.TrayAvailable() && sysInfoHolder.CLOSETOTRAY)
		    		  MinimizeToTray();
		    	  else
		    		  CleanAndExit(false);
		      }
		});
        motherFrame.setLocation(sysInfoHolder.APP_START_X, sysInfoHolder.APP_START_Y);
		motherFrame.setVisible(true);

		//Start welcome screen
		welcomeDisplayer = new WelcomeDisplayer(welcomePanel, sysInfoHolder);
		welcomeDisplayer.showBrowser();

		//Startup torrent core
		core = AzureusCoreFactory.create(); //.getSingleton(); //AzureusCoreFactory.create();
		COConfigurationManager.setParameter("Max Download Speed KBs", sysInfoHolder.MAXGLOBALTORRENTDOWNLOADSPEED);
		COConfigurationManager.setParameter("Max Upload Speed KBs", sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEED);
		COConfigurationManager.setParameter("enable.seedingonly.upload.rate", sysInfoHolder.GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED);
		COConfigurationManager.setParameter("Max Upload Speed Seeding KBs", sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEEDONSEEDING);
		COConfigurationManager.setParameter("TCP.Listen.Port", sysInfoHolder.TORRENT_LISTEN_PORT);
		COConfigurationManager.setParameter("UDP.Listen.Port", sysInfoHolder.TORRENT_LISTEN_PORT);
		COConfigurationManager.setParameter("UDP.NonData.Listen.Port", sysInfoHolder.TORRENT_LISTEN_PORT);
		core.start();

		//Start torrent progress admin thread
		torrentProgressAdminThread = new TorrentDownloadProgressAdminThread(sysInfoHolder);
		torrentProgressAdminThread.start();
		
		//Populate arrays with history downloads
		AddAllHistoryDownloads();

		//Show table for current view state.
		RedoDownloadsListsAndUpdateTable();
		
		//Remove unwanted downloadmanagers from torrent core
		CleanTorrentCore(core);
		
		//Start any history downloads if present
		StartHistoryDownloads();
		
		//Start updater thread
		CheckForUpdates(false);
		
		//This is a workaround. With out this, tree node text can have ...
		((DefaultTreeModel)browserTree.getModel()).reload();
		browserTree.setSelectionRow(ViewStateTreeNodeDisplayer.HOME);
		
		//Start scheduler thread
		ScheduleMonitorThread scheduler = new ScheduleMonitorThread(this, sysInfoHolder);
		scheduler.start();
		
		sysInfoHolder.isStarting=false;
	}
	
	public AzureusCore GetTorrentCore() {
		return core;
	}
	
	public void ToFront() {
		motherFrame.setVisible(true);
		motherFrame.setState ( Frame.NORMAL );
		motherFrame.setAlwaysOnTop(true);
		motherFrame.toFront();
		motherFrame.setAlwaysOnTop(false);
	}
	
	public void MinimizeToTray() {
		if(sysInfoHolder.TRAYENABLED && sysInfoHolder.TrayAvailable()&& sysInfoHolder.CLOSETOTRAY)
			motherFrame.setVisible(false);
		else
			motherFrame.setState(Frame.ICONIFIED);
	}

	/*************************************************************
	 * 
	 * Function  : StartHistoryDownloads
	 * Arguments : void
	 * Return    : void
	 * Notes     : Starts history downloads that were pausedforexit
	 *             or pausedforerror.
	 * 
	 *************************************************************/
	private void StartHistoryDownloads() {
		for(int i=0; i< allDownloadsArr.size(); i++) {
			if((allDownloadsArr.get(i).GetStateHolder().GetStatus() ==DownloadStateHolder.PAUSEDFORERROR) ||
			   (allDownloadsArr.get(i).GetStateHolder().GetStatus() ==DownloadStateHolder.PAUSEDFOREXIT) ||
			   (allDownloadsArr.get(i).GetStateHolder().GetStatus() ==DownloadStateHolder.SEEDING) ||
			   (allDownloadsArr.get(i).GetStateHolder().GetStatus() ==DownloadStateHolder.PAUSED)) {
				allDownloadsArr.get(i).StartSilently();
			}
			else if (allDownloadsArr.get(i).GetStateHolder().GetStatus() ==DownloadStateHolder.COMPLETED) {
				if(allDownloadsArr.get(i).GetStateHolder().GetDownloadType() == Common.TYPE_TORRENT) {
					allDownloadsArr.get(i).StartSilently();
				}
			}
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : actionPerformed
	 * Arguments : void
	 * Return    : void
	 * Notes     : Action commands.
	 * 
	 *************************************************************/
	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if(cmd.equals(exit)) {
			CleanAndExit(false);
		}
		else if(cmd.equals(addurldownload)) {
			AddNewUrlDownload();
		}
		else if(cmd.equals(addtorrentdownload)) {
			AddNewTorrentDownload();
		}
		else if(cmd.equals(showdetails)) {
			ShowDetailsForSelectedDownload();
		}
		else if(cmd.equals(showfolder)) {
			ShowFolderForSelectedDownload();
		}
		else if(cmd.equals(pausedownload)) {
			PauseSelectedDownload();
		}
		else if(cmd.equals(pausealldownloads)) {
			PauseAllDownloads();
		}
		else if(cmd.equals(resumedownload)) {
			ResumeSelectedDownload();
		}
		else if(cmd.equals(resumealldownloads)) {
			ResumeAllDownloads();
		}
		else if(cmd.equals(canceldownload)) {
			CancelSelectedDownload();
		}
		else if(cmd.equals(cancelalldownloads)) {
			CancelAllDownloadsWithCOnfirm();
		}
		else if(cmd.equals(cleardownload)) {
			RemoveSelectedDownload();
		}
		else if(cmd.equals(clearalldownloads)) {
			if(ioutils.GetFromYesNoMessage(this.motherFrame, "This will remove all entries. Are you sure?"))
				RemoveAllDownloads();
		}
		else if(cmd.equals(scheduledownloads)) {
			ScheduleDownloads();
		}
		else if(cmd.equals(cleardownloadanddeletefile)) {
			RemoveSelectedDownloadAndDeleteFile();
		}
		else if(cmd.equals(copyfilepathtoclipboard)) {
			CopyFilepathOfSelectedDownloadToClipboard();
		}
		else if(cmd.equals(copyurltoclipboard)) {
			CopyUrlOfSelectedDownloadToClipboard();
		}
		else if(cmd.equals("VisitHomepage")) {
			VisitHomepage();
		}
		else if (cmd.equals("CheckForUpdate")) {
			CheckForUpdates(true);
		}
		else if(cmd.equals("Version")) {
			ShowVersion();
		}
		else if(cmd.equals("Credits")) {
			ShowCredits();
		}
		else if(cmd.equals(options)) {
			ShowOptions();
		}
		else if(cmd.equals(searchfortorrent)) {
			this.currentViewState=ViewStateTreeNodeDisplayer.SEARCH;
			browserTree.setSelectionRow(ViewStateTreeNodeDisplayer.SEARCH);
			ShowTorrentSearch(searchField.getText());
		}
		/*else if(cmd.equals(searchfortorrent1)) {
			this.currentViewState=ViewStateTreeNodeDisplayer.SEARCH;
			browserTree.setSelectionRow(5);
			searchPanel.Search();
		}*/
	}
	
	
	/*************************************************************
	 * 
	 * Function  : AddNewDownload
	 * Arguments : void
	 * Return    : void
	 * Notes     : Adds new download. Creates dowloader, starts it
	 *             and adds it to table.
	 * 
	 *************************************************************/
	public void AddNewUrlDownload() {
		AddNewUrlDownload("");
	}

	public void CheckAndAddDownloadFromBrowser(String addurl) {
		if(sysInfoHolder.TORRENTDOWNLOADSTARTOPTION == SystemInfoHolder.TORRENTTORRENTDOWNLOAD) {
			if (addurl.matches("^.*\\.torrent(\\?.*)?$")) {
				AddNewTorrentDownload(addurl, true);
			}
			else {
				AddNewUrlDownload(addurl);
			}
		}
		else {
			AddNewUrlDownload(addurl);
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : AddNewDownload
	 * Arguments : void
	 * Return    : void
	 * Notes     : Adds new download. Creates dowloader, starts it
	 *             and adds it to table.
	 * 
	 *************************************************************/
	public void AddNewUrlDownload(String addurl) {
		if(this.currentViewState != ViewStateTreeNodeDisplayer.HOME)
			browserTree.setSelectionRow(ViewStateTreeNodeDisplayer.ALL);
		this.ToFront();
		int minAcclelerateFileSize=0;
		//Get url from user
		String opDir="";
		if(sysInfoHolder.SAVETOOPTION == SystemInfoHolder.SAVETODEFAULTFOLDER)
			opDir=sysInfoHolder.DEFAULTDOWNLOADLOCATION;
		else if(sysInfoHolder.SAVETOOPTION == SystemInfoHolder.SAVETOPRVIOUSDOWNLOADLOCATION)
			opDir=sysInfoHolder.PREVIOUSDOWNLOADLOCATION;
		else if (sysInfoHolder.SAVETOOPTION == SystemInfoHolder.ASKDOWNLOADLOCATIONEACHTIME)
			opDir=sysInfoHolder.PREVIOUSDOWNLOADLOCATION;

		UrlDownloadInputDialog d1;
		if(addurl.equals("")) {
			d1 = new UrlDownloadInputDialog(GUICommon.GetUrlFromClipboard(), opDir, motherFrame);
		}
		else {
			d1 = new UrlDownloadInputDialog(addurl, opDir, motherFrame);
		}
		if(! d1.ShowDialog())
			return;
		String urlname=d1.GetInputUrl();
		String dirname=d1.GetDownloadDir();
		numConnections = d1.GetNumConnections();

		//Check if output dir exists
		while(Common.CheckFileExists(dirname, logger)!=Common.OK) {
			JOptionPane.showMessageDialog(motherFrame,
                    "Download directory does not exist: "+dirname, "Error",
                    JOptionPane.ERROR_MESSAGE);
			d1 = new UrlDownloadInputDialog(urlname, dirname, motherFrame);
			if(! d1.ShowDialog())
				return;
			urlname=d1.GetInputUrl();
			dirname=d1.GetDownloadDir();
			numConnections = d1.GetNumConnections();
		}
		sysInfoHolder.PREVIOUSDOWNLOADLOCATION=dirname;
		  
		//Check if valid url
		if(! GUICommon.CheckIfGoodUrl(urlname)) {
			ioutils.PrintError("Invalid url specified "+GUICommon.LINESEPERATOR+Common.ShortenString(urlname, "middle",50 ));
			return;
		}
		
		if(sysInfoHolder.AUTOACCELERATE) {
			minAcclelerateFileSize=sysInfoHolder.MINSIZEFORACCELERATION*1024;
		}
		
		//Add entry in history xml
		int id = historyXmlUpdater.AddDownloadEntry(urlname, Common.TYPE_URL, "", dirname, -1, 0,0,0,0, 0, 0, 0, 0, DownloadStateHolder.STARTING, "", 0,0,0,false,"","");
		
		//Create downloader
		GuiUrlDownloaderWrapper downloader = new GuiUrlDownloaderWrapper(this, 
				                                                         urlname, 
				                                                         dirname, 
				                                                         numConnections,
				                                                         minAcclelerateFileSize,
				                                                         id,
				                                                         sysInfoHolder,
				                                                         ioutils);
		allDownloadsArr.add(0, downloader);
		  
		//Update table for new addition
		RedoDownloadsListsAndUpdateTable();
		
		//Check how downloader should start and set downloader status.
		if(sysInfoHolder.DOWNLOADSTARTOPTION == SystemInfoHolder.DONTSTARTDOWNLOADSIMMEDIATELY) {
			downloader.GetStateHolder().SetDownloadStatus(DownloadStateHolder.PAUSED);
		}
		else if(sysInfoHolder.DOWNLOADSTARTOPTION == SystemInfoHolder.ASKTOSTARTDOWNLOADS) {
			if(! ioutils.GetFromYesNoMessage(motherFrame, "Do you want to start download immediately?")) {
				downloader.GetStateHolder().SetDownloadStatus(DownloadStateHolder.PAUSED);
			}
		}
		  
		//Start download
		downloader.Start();
	}
	
	/*************************************************************
	 * 
	 * Function  : AddNewTorrentDownload
	 * Arguments : void
	 * Return    : void
	 * Notes     : Adds new torrentdownload. Creates dowloader, 
	 *             starts it and adds it to table.
	 * 
	 *************************************************************/
	public void AddNewTorrentDownload() {
		AddNewTorrentDownload("", false);
	}
	
	/*************************************************************
	 * 
	 * Function  : AddNewDownload
	 * Arguments : void
	 * Return    : void
	 * Notes     : Adds new download. Creates dowloader, starts it
	 *             and adds it to table.
	 * 
	 *************************************************************/
	public void AddNewTorrentDownload(String addtorrent, boolean download) {
		if(this.currentViewState != ViewStateTreeNodeDisplayer.SEARCH && this.currentViewState != ViewStateTreeNodeDisplayer.HOME)
			browserTree.setSelectionRow(1);
		this.ToFront();
		//Get url from user
		String opDir="";
		String ipDir="";
		String torrentname="";
		String dirname="";
		if(sysInfoHolder.SAVETOOPTION == SystemInfoHolder.SAVETODEFAULTFOLDER)
			opDir=sysInfoHolder.DEFAULTDOWNLOADLOCATION;
		else if(sysInfoHolder.SAVETOOPTION == SystemInfoHolder.SAVETOPRVIOUSDOWNLOADLOCATION)
			opDir=sysInfoHolder.PREVIOUSDOWNLOADLOCATION;
		else if (sysInfoHolder.SAVETOOPTION == SystemInfoHolder.ASKDOWNLOADLOCATIONEACHTIME)
			opDir=sysInfoHolder.PREVIOUSDOWNLOADLOCATION;
		
		ipDir=sysInfoHolder.PREVIOUSTORRENTINPUTLOCATION;

		TorrentDownloadInputDialog d1=null;
		if(addtorrent.equals("")) {
			String torrent=GUICommon.GetTorrentFromClipboard();
			d1 = new TorrentDownloadInputDialog(torrent, ipDir, opDir, motherFrame);
			if(GUICommon.CheckIfGoodUrl(torrent)) {
				download=true;
			}
		}
		else {
			if(sysInfoHolder.SAVETOOPTION == SystemInfoHolder.ASKDOWNLOADLOCATIONEACHTIME) 
				d1 = new TorrentDownloadInputDialog(addtorrent, ipDir, opDir, motherFrame);
			else {
				torrentname=addtorrent;
				dirname=opDir;
			}
		}
		
		if(d1 != null) {
			if(!d1.ShowDialog())
				return;
			torrentname=d1.GetInputTorrent();
			dirname=d1.GetDownloadDir();

			//For input torrent, find directory and set it to last input directory.
			sysInfoHolder.PREVIOUSTORRENTINPUTLOCATION=(new File(torrentname)).getParent();
		}

		//Check if torrent file exists if it is not a url download request
		if(! download && Common.CheckFileExists(torrentname, logger)!=Common.OK) {
			JOptionPane.showMessageDialog(motherFrame,
					                      "Torrent file does not exist: "+torrentname, "Error",
	                                      JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		//Check if output dir exists
		while(Common.CheckFileExists(dirname, logger)!=Common.OK) {
			JOptionPane.showMessageDialog(motherFrame,
                    "Download directory does not exist: "+dirname, "Error",
                    JOptionPane.ERROR_MESSAGE);
			d1 = new TorrentDownloadInputDialog(torrentname, ipDir, dirname, motherFrame);
			if(!d1.ShowDialog())
				return;
			torrentname=d1.GetInputTorrent();
			dirname=d1.GetDownloadDir();
		}
		sysInfoHolder.PREVIOUSDOWNLOADLOCATION=dirname;
		
		//Add entry in history xml
		int id = historyXmlUpdater.AddDownloadEntry(torrentname, Common.TYPE_TORRENT, "", dirname, -1, 0, 0, 0, 0, 0, 0, 0, 0, DownloadStateHolder.STARTING, "", 0,0,0,download,"","");

		GuiTorrentDownloaderWrapper downloader = new GuiTorrentDownloaderWrapper(this,torrentname, dirname, id, sysInfoHolder, ioutils, core, torrentProgressAdminThread, download);
		allDownloadsArr.add(0, downloader);
		  
		//Update table for new addition
		RedoDownloadsListsAndUpdateTable();
		
		//Start download
		downloader.Start();
	}
	
	/*************************************************************
	 * 
	 * Function  : AddAllHistoryDownloads
	 * Arguments : void
	 * Return    : void
	 * Notes     : Add all history downloads to addDownloadsArr
	 * 
	 *************************************************************/
	public void AddAllHistoryDownloads() {
		NodeList nodeList = historyXmlUpdater.GetHistoryList();
		if(nodeList== null)
			return;
		
		int status;
		int type;
		for(int i=0; i<nodeList.getLength(); i++) {
			status = Integer.parseInt(historyXmlUpdater.GetNodeState(nodeList.item(i)));
			type = Integer.parseInt(historyXmlUpdater.GetNodeType(nodeList.item(i)));
			//Just a double check for invalid states and correction accordingly
			if((status == DownloadStateHolder.STARTING) ||
			   (status == DownloadStateHolder.DOWNLOADING)) {
				status = DownloadStateHolder.ERROR;
			}
			if(type == Common.TYPE_URL) {
				//If state is paused, then we need to collect thread data and initialize downloader to be started later
				if((status == DownloadStateHolder.PAUSEDFOREXIT) ||
					(status == DownloadStateHolder.PAUSEDFORERROR) ||
					(status == DownloadStateHolder.PAUSED)) {
					ArrayList<Integer> downloadSizeArr = historyXmlUpdater.GetArrayForThreadAttribute(nodeList.item(i), "chunksize");
					ArrayList<Integer> chunkOffsetArr = historyXmlUpdater.GetArrayForThreadAttribute(nodeList.item(i), "chunkoffset");
					ArrayList<Integer> writeRangeStartArr = historyXmlUpdater.GetArrayForThreadAttribute(nodeList.item(i), "writerangestart");
					
					GuiUrlDownloaderWrapper downloader = new GuiUrlDownloaderWrapper(this, 
							historyXmlUpdater.GetNodeUrl(nodeList.item(i)),
							historyXmlUpdater.GetNodeDir(nodeList.item(i)),
							historyXmlUpdater.GetNodeFile(nodeList.item(i)),
							Long.parseLong(historyXmlUpdater.GetNodeSize(nodeList.item(i))),
							Float.parseFloat(historyXmlUpdater.GetNodeSpeed(nodeList.item(i))),
							Long.parseLong(historyXmlUpdater.GetNodeStartTime(nodeList.item(i))),
							Long.parseLong(historyXmlUpdater.GetNodeEndTime(nodeList.item(i))),
							Long.parseLong(historyXmlUpdater.GetNodePauseStartTime(nodeList.item(i))),
							Long.parseLong(historyXmlUpdater.GetNodePauseEndTime(nodeList.item(i))),
							Integer.parseInt(historyXmlUpdater.GetNodePausedTime(nodeList.item(i))),
							status,
							downloadSizeArr,
							chunkOffsetArr,
							writeRangeStartArr,
							historyXmlUpdater.GetNodeErrorMessage(nodeList.item(i)),
							Integer.parseInt(historyXmlUpdater.GetNodeProgress(nodeList.item(i))),
							Integer.parseInt(historyXmlUpdater.GetNodeId(nodeList.item(i))),
							sysInfoHolder,
							ioutils,
							true
					);
					allDownloadsArr.add(0,downloader);
					
					//Set proxy if required.
					if(sysInfoHolder.USEPROXY) {
						downloader.GetStateHolder().SetProxy(sysInfoHolder.HTTPPROXYSERVER, sysInfoHolder.HTTPPROXYPORT,
							                             sysInfoHolder.HTTPPROXYUSER, sysInfoHolder.HTTPPROXYPASSWORD,
							                             "", "", "", "");
					}
				}
				else {
					GuiUrlDownloaderWrapper downloader = new GuiUrlDownloaderWrapper(this, 
						                                            historyXmlUpdater.GetNodeUrl(nodeList.item(i)),
						                                            historyXmlUpdater.GetNodeDir(nodeList.item(i)),
						                                            historyXmlUpdater.GetNodeFile(nodeList.item(i)),
						                                            Long.parseLong(historyXmlUpdater.GetNodeSize(nodeList.item(i))),
						                                            Float.parseFloat(historyXmlUpdater.GetNodeSpeed(nodeList.item(i))),
						                                            Long.parseLong(historyXmlUpdater.GetNodeStartTime(nodeList.item(i))),
						                                            Long.parseLong(historyXmlUpdater.GetNodeEndTime(nodeList.item(i))),
						                                            Integer.parseInt(historyXmlUpdater.GetNodePausedTime(nodeList.item(i))),
						                                            status,
						                                            historyXmlUpdater.GetNodeErrorMessage(nodeList.item(i)),
						                                            Integer.parseInt(historyXmlUpdater.GetNodeProgress(nodeList.item(i))),
						                                            Integer.parseInt(historyXmlUpdater.GetNodeId(nodeList.item(i))),
						                                            sysInfoHolder,
						                                            ioutils,
						                                            true);
					allDownloadsArr.add(0,downloader);
				}
			}
			//For torrent downloads
			else {
				GuiTorrentDownloaderWrapper downloader = new GuiTorrentDownloaderWrapper(this, 
						historyXmlUpdater.GetNodeUrl(nodeList.item(i)),
						historyXmlUpdater.GetNodeDir(nodeList.item(i)),
						historyXmlUpdater.GetNodeFile(nodeList.item(i)),
						Long.parseLong(historyXmlUpdater.GetNodeSize(nodeList.item(i))),
						Float.parseFloat(historyXmlUpdater.GetNodeSpeed(nodeList.item(i))),
						Long.parseLong(historyXmlUpdater.GetNodeStartTime(nodeList.item(i))),
						Long.parseLong(historyXmlUpdater.GetNodeEndTime(nodeList.item(i))),
						Long.parseLong(historyXmlUpdater.GetNodePauseStartTime(nodeList.item(i))),
						Long.parseLong(historyXmlUpdater.GetNodePauseEndTime(nodeList.item(i))),
						Integer.parseInt(historyXmlUpdater.GetNodePausedTime(nodeList.item(i))),
						status,
						historyXmlUpdater.GetNodeErrorMessage(nodeList.item(i)),
						Integer.parseInt(historyXmlUpdater.GetNodeProgress(nodeList.item(i))),
						Long.parseLong(historyXmlUpdater.GetNodeDownloaded(nodeList.item(i))),
						Long.parseLong(historyXmlUpdater.GetNodeUploaded(nodeList.item(i))),
						historyXmlUpdater.GetNodepiecesetString(nodeList.item(i)),
						historyXmlUpdater.GetNodeExcludesetString(nodeList.item(i)),
						Integer.parseInt(historyXmlUpdater.GetNodeId(nodeList.item(i))),
						sysInfoHolder,
						ioutils,
						core,
						torrentProgressAdminThread,
						Boolean.parseBoolean(historyXmlUpdater.GetNodeManaged(nodeList.item(i))),
						false
				);
				allDownloadsArr.add(0,downloader);
			}
		}
	}

	/*************************************************************
	 * 
	 * Function  : PauseSelectedDownload
	 * Arguments : downloader
	 * Return    : void
	 * Notes     : Pause selected download.
	 * 
	 *************************************************************/
	public void PauseSelectedDownload() {
		int row = downloadsTable.getSelectedRow();
		if(row == -1)
			return;
		GuiDownloaderWrapperInterface downloader = tableModel.GetDownload(row);
		PauseDownload(downloader);
	}
	
	/*************************************************************
	 * 
	 * Function  : PauseDownload
	 * Arguments : downloader
	 * Return    : void
	 * Notes     : Pause download.
	 * 
	 *************************************************************/
	public void PauseDownload(GuiDownloaderWrapperInterface downloader) {
		//Check if we have a post complete shutdown option enabled.
		if(sysInfoHolder.SHUTDOWNOPTION != SystemInfoHolder.DONOTHINGWHENDOWNLOADSFINISH) {
			String msg=OptionsFrame.shutdownOptions[sysInfoHolder.SHUTDOWNOPTION];
			sysInfoHolder.SHUTDOWNOPTION=SystemInfoHolder.DONOTHINGWHENDOWNLOADSFINISH;
			ioutils.PrintMessage("The post completion operation \""+msg+"\" is disabled to prevent unexpected shutdown");
		}
		downloader.Pause();
		UpdateButtonsForSelectedDownload();
	}
	
	/*************************************************************
	 * 
	 * Function  : PauseDownloadForExit
	 * Arguments : downloader
	 * Return    : void
	 * Notes     : Pause download.
	 * 
	 *************************************************************/
	public void PauseDownloadForExit(GuiDownloaderWrapperInterface downloader) {
		downloader.PauseForExit();
		UpdateButtonsForSelectedDownload();
	}
	
	/*************************************************************
	 * 
	 * Function  : PauseAllDownloads
	 * Arguments : void
	 * Return    : void
	 * Notes     : Pause all downloads from list
	 * 
	 *************************************************************/
	public void PauseAllDownloads() {
		for(int i=0; i<activeDownloadsArr.size(); i++) {
			if((((GuiDownloaderWrapperInterface)activeDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.DOWNLOADING) ||
				(((GuiDownloaderWrapperInterface)activeDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.SEEDING) ||
				(((GuiDownloaderWrapperInterface)activeDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.STARTING)) {
				PauseDownload((GuiDownloaderWrapperInterface)activeDownloadsArr.get(i));
			}					
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : PauseAllDownloadsWithCOnfirm
	 * Arguments : 
	 * Return    : true if user's nod for pause. False otherwise
	 * Notes     : Pauses all downloads.
	 * 
	 *************************************************************/
	public boolean PauseAllDownloadsWithCOnfirm(boolean force) {
		boolean retval=true;
		ArrayList<GuiDownloaderWrapperInterface> arr = new ArrayList<GuiDownloaderWrapperInterface>();
		boolean ask=false;
		if(activeDownloadsArr.size() >0) {
			for(int i=0; i<activeDownloadsArr.size(); i++) {
				if((activeDownloadsArr.get(i).GetStateHolder().GetStatus() != DownloadStateHolder.PAUSED) &&
				   (activeDownloadsArr.get(i).GetStateHolder().GetStatus() != DownloadStateHolder.PAUSEDFORERROR) &&
				   (activeDownloadsArr.get(i).GetStateHolder().GetStatus() != DownloadStateHolder.STARTING)) {
					//For seeding torrents, no need to ask for pausing.
					if(activeDownloadsArr.get(i).GetStateHolder().GetStatus() != DownloadStateHolder.SEEDING)
						ask=true;
					arr.add(activeDownloadsArr.get(i));
				}
			}
		}
		
		if(ask && (! force)) {
			this.ToFront();
			retval = ioutils.GetFromOkCancelMessage(motherFrame, "There are active downloads. Do you want to pause downloads and exit?");
		}
		
		if(retval) {
			for(int i=0; i<arr.size(); i++) {
				PauseDownloadForExit(arr.get(i));
				BackupActiveDownloadDetailsInHistoryXml(arr.get(i).GetStateHolder());
			}
		}
		
		return retval;
	}

	/*************************************************************
	 * 
	 * Function  : ResumeSelectedDownload
	 * Arguments : void
	 * Return    : void
	 * Notes     : Resume selected download.
	 * 
	 *************************************************************/
	public void ResumeSelectedDownload() {
		int row = downloadsTable.getSelectedRow();
		if(row == -1)
			return;
		GuiDownloaderWrapperInterface downloader = tableModel.GetDownload(row);
		ResumeDownload(downloader);
	}
	
	/*************************************************************
	 * 
	 * Function  : ResumeDownload
	 * Arguments : downloader
	 * Return    : void
	 * Notes     : Resumts this download.
	 * 
	 *************************************************************/
	public void ResumeDownload(GuiDownloaderWrapperInterface downloader) {
		downloader.Resume();
		UpdateButtonsForSelectedDownload();
	}
	
	/*************************************************************
	 * 
	 * Function  : ResumeAllDownloads
	 * Arguments : void
	 * Return    : void
	 * Notes     : Resumes all paused downloads
	 * 
	 *************************************************************/
	public void ResumeAllDownloads() {
		for(int i=0; i<activeDownloadsArr.size(); i++) {
			if((((GuiDownloaderWrapperInterface)activeDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.PAUSED)) {
				ResumeDownload((GuiDownloaderWrapperInterface)activeDownloadsArr.get(i));
			}					
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : CancelSelectedDownload
	 * Arguments : void
	 * Return    : void
	 * Notes     : Cancels download for selected row.
	 * 
	 *************************************************************/
	public void CancelSelectedDownload() {
		int response = JOptionPane.showConfirmDialog(null, "Really cancel download?", "Confirm",
				JOptionPane.YES_NO_OPTION, 
				JOptionPane.QUESTION_MESSAGE);
			if (response == JOptionPane.NO_OPTION) {
			} else if (response == JOptionPane.YES_OPTION) {
				int row = downloadsTable.getSelectedRow();
				if(row == -1)
					return;
				GuiDownloaderWrapperInterface downloader = tableModel.GetDownload(row);
				CancelDownload(downloader);
			} else if (response == JOptionPane.CLOSED_OPTION) {
			}
	}
	/*************************************************************
	 * 
	 * Function  : CancelDownload
	 * Arguments : downloader.
	 * Return    : void
	 * Notes     : Cancel a download. Cancels and updates buttons.
	 * 
	 *************************************************************/
	public void CancelDownload(GuiDownloaderWrapperInterface downloader) {
		if(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.SEEDING)
			return;
		downloader.Cancel("Cancelled", DownloadStateHolder.CANCELLED);
		UpdateButtonsForSelectedDownload();
	}

	/*************************************************************
	 * 
	 * Function  : CancelAllDownloadsWithCOnfirm
	 * Arguments : 
	 * Return    : true if user's nod for cancel. False otherwise
	 * Notes     : Cancel all downloads.
	 * 
	 *************************************************************/
	public boolean CancelAllDownloadsWithCOnfirm() {
		boolean retval=true;
		ArrayList<GuiDownloaderWrapperInterface> arr = new ArrayList<GuiDownloaderWrapperInterface>();
		if(activeDownloadsArr.size() >0) {
			if(ioutils.GetFromOkCancelMessage(motherFrame, "All active downloads will be cancelled. Do you want to cancel downloads and exit?")) {
				for(int i=0; i<activeDownloadsArr.size(); i++) {
					arr.add(activeDownloadsArr.get(i));
				}
			}
			else {
				retval=false;
				return false;
			}
		}
		
		for(int i=0; i<arr.size(); i++) {
			CancelDownload(arr.get(i));
		}
		return retval;
	}
	
	/*************************************************************
	 * 
	 * Function  : RemoveSelectedDownload
	 * Arguments : void
	 * Return    : void
	 * Notes     : Remove selected download
	 * 
	 *************************************************************/
	public void RemoveSelectedDownload() {
		int row = downloadsTable.getSelectedRow();
		if(row == -1)
			return;
		int ans;
		ans = ioutils.GetFromYesNoCancelMessage(motherFrame, "Do you want to delete the files as well?");
		if(ans == Common.CANCEL)
			return;
		GuiDownloaderWrapperInterface downloader = tableModel.GetDownload(row);
		if(ans == Common.YES)
			RemoveDownload(downloader, true);
		else if (ans == Common.NO)
			RemoveDownload(downloader, false);
	}
	
	/*************************************************************
	 * 
	 * Function  : RemoveDownload
	 * Arguments : downloader
	 * Return    : void
	 * Notes     : Removes a download entry completely.
	 * 
	 *************************************************************/
	public void RemoveDownload(GuiDownloaderWrapperInterface downloader, boolean deleteFiles) {
		if((downloader.GetStateHolder().GetStatus() == DownloadStateHolder.DOWNLOADING) ||
				(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.PAUSED) ||
				(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.SEEDING) ||
				(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.STARTING)||
				(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.PAUSEDFORERROR)||
				(downloader.GetStateHolder().GetStatus() == DownloadStateHolder.PAUSEDFOREXIT)) {
			//We use exiting status here. Other wise, cancel will set the status in stateHolder which can cause cancel/remove
			//to be called again from the update method for listening for stateholder events.
			downloader.Cancel("Cancelling to remove", DownloadStateHolder.EXITING);
		}
		if(deleteFiles)
				Common.deleteFileOrDirectory(downloader.GetStateHolder().GetDirName()+File.separator+downloader.GetStateHolder().GetFileName(), logger);

		historyXmlUpdater.DeleteDownloadNodeWithId(downloader.GetUid());
		allDownloadsArr.remove(downloader);
		RedoDownloadsListsAndUpdateTable();
		
		//If it is a managed torrent download, remove the downloaded torrent as well.
		if(downloader.GetStateHolder().GetDownloadType()==Common.TYPE_TORRENT) {
			if(downloader.GetStateHolder().IsManagedTorrent())
				Common.DeleteFile(downloader.GetStateHolder().GetUrlName());
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : RemoveSelectedDownloadAndDeleteFile
	 * Arguments : void
	 * Return    : void
	 * Notes     : Removes download from list and deletes file.
	 * 
	 *************************************************************/
	public void RemoveSelectedDownloadAndDeleteFile() {
		int row = downloadsTable.getSelectedRow();
		GuiDownloaderWrapperInterface downloader = tableModel.GetDownload(row);
		RemoveDownload(downloader, true);
		//Check if file exists and remove file
		String filename = downloader.GetStateHolder().GetDirName()+File.separator+downloader.GetStateHolder().GetFileName();
		if(Common.CheckFileExists(filename, logger) == Common.OK) {
			Common.DeleteFile(filename);
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : RemoveAllDownloads
	 * Arguments : void
	 * Return    : void
	 * Notes     : Removes all downloads from list
	 * 
	 *************************************************************/
	public void RemoveAllDownloads() {
		ArrayList<GuiDownloaderWrapperInterface> arr = new ArrayList<GuiDownloaderWrapperInterface>();
		for(int i=0; i<allDownloadsArr.size(); i++) {
			if((((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.DOWNLOADING) ||
				(((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.PAUSED) ||
				(((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.SEEDING) ||
				(((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.PAUSEDFORERROR) ||
				(((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.PAUSEDFOREXIT) ||
				(((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.STARTING))
				{
					//Do not delete
				}
			else {
				arr.add((GuiDownloaderWrapperInterface)allDownloadsArr.get(i));
			}
		}
		
		for(int i=0; i<arr.size(); i++) {
			RemoveDownload(arr.get(i), false);
		}
	}
	
	private void ScheduleDownloads() {
		ScheduleFrame f = new ScheduleFrame(this, sysInfoHolder);
		f.Show();
	}
	
	/*************************************************************
	 * 
	 * Function  : CopyFilepathOfSelectedDownloadToClipboard
	 * Arguments : void
	 * Return    : void
	 * Notes     : Copies over file path of selected download to clipboard
	 * 
	 *************************************************************/
	public void CopyFilepathOfSelectedDownloadToClipboard() {
		int row = downloadsTable.getSelectedRow();
		GuiDownloaderWrapperInterface downloader = tableModel.GetDownload(row);
		CopyFilepathOfDownloadToClipboard(downloader);
	}
	
	/*************************************************************
	 * 
	 * Function  : CopyFilepathOfSelectedDownloadToClipboard
	 * Arguments : downloader
	 * Return    : void
	 * Notes     : Copies over file path of downloader passed to clipboard
	 * 
	 *************************************************************/
	public void CopyFilepathOfDownloadToClipboard(GuiDownloaderWrapperInterface downloader) {
		String path = downloader.GetStateHolder().GetDirName();
		GUICommon.setClipboardContents(path);
	}
	
	/*************************************************************
	 * 
	 * Function  : CopyUrlOfSelectedDownloadToClipboard
	 * Arguments : void
	 * Return    : void
	 * Notes     : Copies over url of selected download to clipboard
	 * 
	 *************************************************************/
	public void CopyUrlOfSelectedDownloadToClipboard() {
		int row = downloadsTable.getSelectedRow();
		GuiDownloaderWrapperInterface downloader = tableModel.GetDownload(row);
		CopyUrlOfDownloadToClipboard(downloader);
	}
	
	/*************************************************************
	 * 
	 * Function  : CopyUrlOfDownloadToClipboard
	 * Arguments : downloader
	 * Return    : void
	 * Notes     : Copies over url of downloader passed to clipboard
	 * 
	 *************************************************************/
	public void CopyUrlOfDownloadToClipboard(GuiDownloaderWrapperInterface downloader) {
		String path = downloader.GetStateHolder().GetUrlName();
		GUICommon.setClipboardContents(path);
	}
	
	/*************************************************************
	 * 
	 * Function  : ShowDownloadsInTable
	 * Arguments : table view state
	 * Return    : void
	 * Notes     : Re-initializes table rows for current view state.
	 *             Called when user switches view state.
	 * 
	 *************************************************************/
	public void ShowDownloadsForState(int state){
		if(state==ViewStateTreeNodeDisplayer.HOME) {
			this.currentViewState=state;
			((CardLayout)(rightPanel.getLayout())).show(rightPanel, WELCOMEPANEL);
		}
		if(state==ViewStateTreeNodeDisplayer.ALL) {
			this.currentViewState=state;
			((CardLayout)(rightPanel.getLayout())).show(rightPanel, DOWNLOADTABLEPANEL);
			RePutAllItemsInTableFromList(allDownloadsArr);
		}
		if(state==ViewStateTreeNodeDisplayer.ACTIVE) {
			this.currentViewState=state;
			((CardLayout)(rightPanel.getLayout())).show(rightPanel, DOWNLOADTABLEPANEL);
			RePutAllItemsInTableFromList(activeDownloadsArr);
		}
		if(state==ViewStateTreeNodeDisplayer.COMPLETED) {
			this.currentViewState=state;
			((CardLayout)(rightPanel.getLayout())).show(rightPanel, DOWNLOADTABLEPANEL);
			RePutAllItemsInTableFromList(completedDownloadsArr);
		}
		if(state==ViewStateTreeNodeDisplayer.CANCELLED) {
			this.currentViewState=state;
			((CardLayout)(rightPanel.getLayout())).show(rightPanel, DOWNLOADTABLEPANEL);
			RePutAllItemsInTableFromList(cancelledDownloadsArr);
		}
		if(state==ViewStateTreeNodeDisplayer.SEEDING) {
			this.currentViewState=state;
			((CardLayout)(rightPanel.getLayout())).show(rightPanel, DOWNLOADTABLEPANEL);
			RePutAllItemsInTableFromList(seedingDownloadsArr);
		}
		if(state==ViewStateTreeNodeDisplayer.SEARCH) {
			this.currentViewState=state;
			((CardLayout)(rightPanel.getLayout())).show(rightPanel, SEARCHPANEL);
		}
	}
	
	public ArrayList<GuiDownloaderWrapperInterface> GetDownloadsArrForState(int state) {
		if(state==ViewStateTreeNodeDisplayer.ALL) {
			return allDownloadsArr;
		}
		else if(state==ViewStateTreeNodeDisplayer.ACTIVE) {
			return activeDownloadsArr;
		}
		else if(state==ViewStateTreeNodeDisplayer.COMPLETED) {
			return completedDownloadsArr;
		}
		else if(state==ViewStateTreeNodeDisplayer.CANCELLED) {
			return cancelledDownloadsArr;
		}
		else if(state==ViewStateTreeNodeDisplayer.SEEDING) {
			return seedingDownloadsArr;
		}
		return allDownloadsArr;
	}
	
	/*************************************************************
	 * 
	 * Function  : RePutAllItemsInTableFromList
	 * Arguments : downloads list
	 * Return    : void
	 * Notes     : Remove all rows from table and re-adds downloads
	 *             from array passed .
	 * 
	 *************************************************************/
	public void RePutAllItemsInTableFromList(ArrayList<GuiDownloaderWrapperInterface> list) {
		//Remove existing entries
		ArrayList<GuiDownloaderWrapperInterface> rowsArr = new ArrayList<GuiDownloaderWrapperInterface>();
		for(int i=0; i<tableModel.getRowCount(); i++) {
			rowsArr.add(tableModel.GetDownload(i));
		}
		for(int i=0; i<rowsArr.size(); i++) {
			ClearTableEntryForDownload((GuiDownloaderWrapperInterface)rowsArr.get(i));
		}
		for(int i=0; i<list.size(); i++) {
			AddTableEntryForDownload((GuiDownloaderWrapperInterface)list.get(i));
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : ClearTableEntryForDownload
	 * Arguments : downloader
	 * Return    : void
	 * Notes     : Clear row from table for that downloader object.
	 * 
	 *************************************************************/
	public synchronized void ClearTableEntryForDownload(GuiDownloaderWrapperInterface downloader) {
		int row = tableModel.GetRowForDownload(downloader.GetStateHolder());
		
		if(row == -1)
			return;
		
		//Set Clearing flag
		clearing = true;
		
		//disable image icon observer
		((ImageIcon) downloadsTable.getModel().getValueAt(row, colManager.GetColIndex(DownloadsTableColumnManager.COL_ICON_ID))).setImageObserver(null);
		
		//Remove mainframe as observer
		//downloader.GetStateHolder().deleteObserver(this);
		
		//Clear download from table
		tableModel.ClearDownload(downloader);
		
		//Clear selected download
		selectedDownload=null;
		
		//Update buttons
		UpdateButtonsForSelectedDownload();
		
		//Reset clearing flag
		clearing = false;
	}
	
	/*************************************************************
	 * 
	 * Function  : AddTableEntryForDownload
	 * Arguments : downloader
	 * Return    : void
	 * Notes     : Add a new row for a download. Also add listener
	 *             for the download in case it is not completed.
	 * 
	 *************************************************************/
	public synchronized void AddTableEntryForDownload(GuiDownloaderWrapperInterface downloader) {
		//add it to table.
		tableModel.AddDownload(downloader);
		
		//Add listener if it is an active download
		if((downloader.GetStateHolder().GetStatus() == DownloadStateHolder.DOWNLOADING) ||
		   (downloader.GetStateHolder().GetStatus() == DownloadStateHolder.SEEDING) ||
		   (downloader.GetStateHolder().GetStatus() == DownloadStateHolder.PAUSED) ||
		   (downloader.GetStateHolder().GetStatus() == DownloadStateHolder.PAUSEDFORERROR) ||
		   (downloader.GetStateHolder().GetStatus() == DownloadStateHolder.PAUSEDFOREXIT) ||
		   (downloader.GetStateHolder().GetStatus() == DownloadStateHolder.STARTING)) {
			downloader.GetStateHolder().addObserver(this);
		}
		  
		((ImageIcon) downloadsTable.getModel().getValueAt(downloadsTable.getRowCount()-1, colManager.GetColIndex(DownloadsTableColumnManager.COL_ICON_ID))).
		setImageObserver(new CellImageObserver(downloadsTable, downloadsTable.getRowCount()-1, colManager.GetColIndex(DownloadsTableColumnManager.COL_ICON_ID)));
	}	
	
	/*************************************************************
	 * 
	 * Function  : RedoDownloadsListsAndUpdateTable
	 * Arguments : downloader object
	 * Return    : void
	 * Notes     : Re-instantiates all state arrays from allDownloads data
	 *             and updates table by adding or clearing the row.
	 * 
	 *************************************************************/
	public synchronized void RedoDownloadsListsAndUpdateTable() {
		// Clear all subset arrays
		activeDownloadsArr.clear();
		completedDownloadsArr.clear();
		cancelledDownloadsArr.clear();
		seedingDownloadsArr.clear();
		int selectedRow=downloadsTable.getSelectedRow();
		DownloadStateHolder selected=null;
		if(selectedRow > -1)
			selected = tableModel.GetDownload(downloadsTable.getSelectedRow()).GetStateHolder();
		
		//Populate arrays with downloads
		for(int i=0; i<allDownloadsArr.size(); i++) {
			//Check for active doanload
			if((((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.DOWNLOADING) ||
			   (((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.SEEDING) ||
			   (((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.STARTING) ||
			   (((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.PAUSED) ||
			   (((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.PAUSEDFOREXIT) ||
			   (((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.PAUSEDFORERROR)) {
				activeDownloadsArr.add(allDownloadsArr.get(i));
				if((((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.SEEDING)) {
					seedingDownloadsArr.add(allDownloadsArr.get(i));
				}
			}
			//Check for completed
			else if((((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.COMPLETED)) {
				completedDownloadsArr.add(allDownloadsArr.get(i));
			}
			//Check for cancelled
			else if((((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.CANCELLED) ||
					(((GuiDownloaderWrapperInterface)allDownloadsArr.get(i)).GetStateHolder().GetStatus() == DownloadStateHolder.ERROR)) {
				cancelledDownloadsArr.add(allDownloadsArr.get(i));
			}
		}
		
		((DefaultTreeModel)browserTree.getModel()).reload();

		//Re-populate rows for current state again in table
		ShowDownloadsForState(this.currentViewState);
		browserTree.setSelectionRow(this.currentViewState);
		
		//Select row after re-shuffling. Resume causes row selection to go away without this
		if(selected != null && allDownloadsArr.contains(selected.GetDownloaderObject())) {
			selectedRow=tableModel.GetRowForDownload(selected);
			if(GetDownloadsArrForState(this.currentViewState).contains(selected.GetDownloaderObject()))
				downloadsTable.changeSelection(selectedRow, 0, false, false);
		}
		else {
			if(selectedRow >= tableModel.getRowCount()-1)
				downloadsTable.changeSelection(tableModel.getRowCount()-1, 0, false, false);
			else
				downloadsTable.changeSelection(selectedRow, 0, false, false);
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : keyPressed
	 * Arguments : key event
	 * Return    : void
	 * Notes     : Check for key events.
	 * 
	 *************************************************************/
	//Handle key typed events.
	public void keyPressed(KeyEvent keyEvent) {
		if(keyEvent.getKeyCode() == KeyEvent.VK_DELETE)
		{
			keyEvent.consume();
			RemoveSelectedDownload();
		}
	}
	public void keyTyped(KeyEvent event) {
	}
	public void keyReleased(KeyEvent event) {
	}
	
	/*************************************************************
	 * 
	 * Function  : valueChanged
	 * Arguments : tree event
	 * Return    : void
	 * Notes     : to check for view state selection changes.
	 * 
	 *************************************************************/
    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                           browserTree.getLastSelectedPathComponent();

        if (node == null) return;

        int view = ((ViewStateTreeNodeDisplayer)node.getUserObject()).GetId();
        if (view == ViewStateTreeNodeDisplayer.HOME) {
            if(this.currentViewState != ViewStateTreeNodeDisplayer.HOME)
            	ShowDownloadsForState(ViewStateTreeNodeDisplayer.HOME);
        }
        if (view == ViewStateTreeNodeDisplayer.ALL) {
            if(this.currentViewState != ViewStateTreeNodeDisplayer.ALL)
            	ShowDownloadsForState(ViewStateTreeNodeDisplayer.ALL);
        }
        else if (view == ViewStateTreeNodeDisplayer.ACTIVE) {
            if(this.currentViewState != ViewStateTreeNodeDisplayer.ACTIVE)
            	ShowDownloadsForState(ViewStateTreeNodeDisplayer.ACTIVE);
        }
        else if (view == ViewStateTreeNodeDisplayer.CANCELLED) {
            if(this.currentViewState != ViewStateTreeNodeDisplayer.CANCELLED)
            	ShowDownloadsForState(ViewStateTreeNodeDisplayer.CANCELLED);
        }
        else if (view == ViewStateTreeNodeDisplayer.COMPLETED) {
            if(this.currentViewState != ViewStateTreeNodeDisplayer.COMPLETED)
            	ShowDownloadsForState(ViewStateTreeNodeDisplayer.COMPLETED);
        }
        else if (view == ViewStateTreeNodeDisplayer.SEEDING) {
            if(this.currentViewState != ViewStateTreeNodeDisplayer.SEEDING)
            	ShowDownloadsForState(ViewStateTreeNodeDisplayer.SEEDING);
        }
        else if (view == ViewStateTreeNodeDisplayer.SEARCH) {
            if(this.currentViewState != ViewStateTreeNodeDisplayer.SEARCH)
            	ShowDownloadsForState(ViewStateTreeNodeDisplayer.SEARCH);
        }
    }


	/*************************************************************
	 * 
	 * Function  : tableSelectionChanged
	 * Arguments : null
	 * Return    : void
	 * Notes     : Update buttons when row selection has changed
	 * 
	 *************************************************************/
	// Called when table row selection changes.
	private void tableSelectionChanged() {
		/* Unregister from receiving notifications
	       from the last selected download. */
		if (selectedDownload != null) {
			//selectedDownload.GetStateHolder().deleteObserver(this);
		}
	        
		/* If not in the middle of clearing a download,
	       set the selected download and register to
	       	receive notifications from it. */
		if (!clearing)  {
			//Only if there is a selected row
			if(downloadsTable.getSelectedRow()!= -1) {
				selectedDownload = tableModel.GetDownload(downloadsTable.getSelectedRow());
				//selectedDownload.GetStateHolder().addObserver(this);
				UpdateButtonsForSelectedDownload();
			}
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : UpdateButtonsForSelectedDownload
	 * Arguments : null
	 * Return    : void
	 * Notes     : Update each button's state based off of the
	 *             currently selected download's status.
	 * 
	 *************************************************************/
	private void UpdateButtonsForSelectedDownload() {
		//java.awt.EventQueue.invokeLater(new Runnable() {public void run() {
			if (selectedDownload != null) {
				int status = selectedDownload.GetStateHolder().GetStatus();
				switch (status) {
				case DownloadStateHolder.STARTING:
					pauseButt.setEnabled(true);
					if(selectedDownload.GetStateHolder().GetConnectionProtocol() == DownloadStateHolder.FTP)
						pauseButt.setEnabled(false);
					resumeButt.setEnabled(false);
					cancelButt.setEnabled(true);
					clearButt.setEnabled(true);
					break;
				case DownloadStateHolder.DOWNLOADING:
					pauseButt.setEnabled(true);
					if(selectedDownload.GetStateHolder().GetConnectionProtocol() == DownloadStateHolder.FTP)
						pauseButt.setEnabled(false);
					resumeButt.setEnabled(false);
					cancelButt.setEnabled(true);
					clearButt.setEnabled(true);
					break;
				case DownloadStateHolder.SEEDING:
					pauseButt.setEnabled(true);
					resumeButt.setEnabled(false);
					cancelButt.setEnabled(false);
					clearButt.setEnabled(true);
					break;
				case DownloadStateHolder.PAUSED:
						pauseButt.setEnabled(false);
						resumeButt.setEnabled(true);
						if(selectedDownload.GetStateHolder().GetDownloadType() == Common.TYPE_TORRENT && selectedDownload.GetStateHolder().GetProgress() >=100)
							cancelButt.setEnabled(false);
						else
							cancelButt.setEnabled(true);
						clearButt.setEnabled(true);
					break;
				case DownloadStateHolder.PAUSEDFORERROR:
					pauseButt.setEnabled(false);
					resumeButt.setEnabled(true);
					if(selectedDownload.GetStateHolder().GetDownloadType() == Common.TYPE_TORRENT && selectedDownload.GetStateHolder().GetProgress() >=100)
						cancelButt.setEnabled(false);
					else
						cancelButt.setEnabled(true);
					clearButt.setEnabled(true);
					break;
				case DownloadStateHolder.ERROR:
					pauseButt.setEnabled(false);
					resumeButt.setEnabled(false);
					cancelButt.setEnabled(false);
					clearButt.setEnabled(true);
					break;
				case DownloadStateHolder.COMPLETED:
					pauseButt.setEnabled(false);
					resumeButt.setEnabled(false);
					cancelButt.setEnabled(false);
					clearButt.setEnabled(true);
					break;
				default: // COMPLETE or CANCELLED
					pauseButt.setEnabled(false);
					resumeButt.setEnabled(false);
					cancelButt.setEnabled(false);
					clearButt.setEnabled(true);
				}
			} else {
				// No download is selected in table.
				pauseButt.setEnabled(false);
				resumeButt.setEnabled(false);
				cancelButt.setEnabled(false);
				clearButt.setEnabled(false);
			}
		//}});
	}
	
	/*************************************************************
	 * 
	 * Function  : SetScheduleButtIcon
	 * Arguments : VOID
	 * Return    : void
	 * Notes     : Sets schedule button icom appropriately. When ever
	 *             schedule is added, this needs to be caled from scheduleframe
	 *             to change icon to pressed one.
	 * 
	 *************************************************************/
	public void SetScheduleButtIcon() {
		if((sysInfoHolder.SCHEDULESTARTOPTION == SystemInfoHolder.SCHEDULESTARTDISABLED) &&
				   (sysInfoHolder.SCHEDULESTOPOPTION == SystemInfoHolder.SCHEDULESTOPDISABLED)) {
			scheduleButt.setIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/schedule_off.png")));
		}
		else {
			scheduleButt.setIcon(new ImageIcon(this.getClass().getResource("guicomponents/images/schedule_on.png")));
		}
	}
	  
	  
	/*************************************************************
	 * 
	 * Function  : update
	 * Arguments : observable and update argument
	 * Return    : void
	 * Notes     : Depending on status change, modifies status xml and 
	 *             starts sliding frame if required. Also runs updatebuttons
	 *             to update buttons for selected download for state change.
	 * 
	 *************************************************************/
	public void update(Observable o1, Object arg1) {
		final Observable o = o1;
		final Object arg = arg1;
		java.awt.EventQueue.invokeLater(new Runnable() {public void run() {
			if(o.getClass().getName().equals("skdownloader.core.DownloadStateHolder")) {
				DownloadStateHolder stateHolder = (DownloadStateHolder)o;
				String updateString = (String)arg;
				int index = tableModel.GetRowForDownload(stateHolder);
		
				if(updateString.equals("DownloadedPercent")) {
					if(index != -1)
						if(colManager.GetColIndex(DownloadsTableColumnManager.COL_PROGRESS_ID) <= tableModel.getColumnCount())
							tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_PROGRESS_ID));
				}
				else if(updateString.equals("FileSize")) {
					if(index != -1)
						if(colManager.GetColIndex(DownloadsTableColumnManager.COL_SIZE_ID) <= tableModel.getColumnCount())
							tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_SIZE_ID));
				}
				else if(updateString.equals("DownloadStatus")) {
					if(index != -1) {
						if(colManager.GetColIndex(DownloadsTableColumnManager.COL_ICON_ID) <= tableModel.getColumnCount()) {
							tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_ICON_ID));
							((ImageIcon) downloadsTable.getModel().getValueAt(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_ICON_ID))).setImageObserver(new CellImageObserver(downloadsTable, index, colManager.GetColIndex(DownloadsTableColumnManager.COL_ICON_ID)));
						}
						if(colManager.GetColIndex(DownloadsTableColumnManager.COL_STATUS_ID) <= tableModel.getColumnCount()) {
							tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_STATUS_ID));
						}

					}
					
					if(stateHolder.GetStatus() == DownloadStateHolder.STARTING){
						//Do nothing
					}
					if(stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING){
						//Do nothing
					}
					else if (stateHolder.GetStatus() == DownloadStateHolder.PAUSED){
						//Add it as an active download to history xml with details.
						//This will enable resuming on skdownloader restart.
						//Common.Sleep(500);
						RedoDownloadsListsAndUpdateTable();
						BackupActiveDownloadDetailsInHistoryXml(stateHolder);
					}
					else if (stateHolder.GetStatus() == DownloadStateHolder.PAUSEDFORERROR){
						//Add it as an active download to history xml with details.
						//This will enable resuming on skdownloader restart.
						//Common.Sleep(500);
						RedoDownloadsListsAndUpdateTable();
						BackupActiveDownloadDetailsInHistoryXml(stateHolder);
						//logger.PrintError("Encountered network error while downloading "+Common.ShortenString(stateHolder.GetFileName(), "middle",50 )+
						//		Common.LINESEPERATOR+"Download is currently paused. Please check the network and try resuming.");

					}
					else if (stateHolder.GetStatus() == DownloadStateHolder.PAUSEDFOREXIT){
						//Add it as an active download to history xml with details.
						//This will enable resuming on skdownloader restart.
						BackupActiveDownloadDetailsInHistoryXml(stateHolder);
					}
					else if (stateHolder.GetStatus() == DownloadStateHolder.SEEDING){
						//Add it as an active download to history xml with details.
						//This will enable resuming on skdownloader restart.
						//Common.Sleep(500);
						RedoDownloadsListsAndUpdateTable();
						BackupInactiveDownloadDetailsInHistoryXml(stateHolder);
					}
					else if (stateHolder.GetStatus() == DownloadStateHolder.COMPLETED){
						//Sleep for cell updates to complete. With out this, percent update display might show inconsistencies.
						//Common.Sleep(500);
						RedoDownloadsListsAndUpdateTable();
						BackupInactiveDownloadDetailsInHistoryXml(stateHolder);
					}
					else if((stateHolder.GetStatus() == DownloadStateHolder.CANCELLED) ||
							(stateHolder.GetStatus() == DownloadStateHolder.ERROR)){
						//Sleep for cell update to complete. Don't remove this or percent update does not work properly.
						//Common.Sleep(500);
						if((stateHolder.GetErrorMessage().equals("Cancelled as already downloaded.")) ||
						   (stateHolder.GetErrorMessage().equals("Cancelled as download already in progress."))) {
							RemoveDownload((GuiDownloaderWrapperInterface)stateHolder.GetDownloaderObject(),false);
						}
						else {
							BackupInactiveDownloadDetailsInHistoryXml(stateHolder);
						}
						RedoDownloadsListsAndUpdateTable();
						
					}
				}
				else if(updateString.equals("DownloadSpeed")) {
					if(index != -1) {
						if(colManager.GetColIndex(DownloadsTableColumnManager.COL_DOWNSPEED_ID) <= tableModel.getColumnCount())
							tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_DOWNSPEED_ID));
						if(colManager.GetColIndex(DownloadsTableColumnManager.COL_ETA_ID) <= tableModel.getColumnCount())
						tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_ETA_ID));
					}
				}
				else if(updateString.equals("UploadSpeed")) {
					if(index != -1 && colManager.GetColIndex(DownloadsTableColumnManager.COL_UPSPEED_ID) <= tableModel.getColumnCount())
						tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_UPSPEED_ID));
				}
				else if(updateString.equals("FileName")) {
					if(index != -1 && colManager.GetColIndex(DownloadsTableColumnManager.COL_FILE_ID) <= tableModel.getColumnCount())
						tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_FILE_ID));
				}
				else if(updateString.equals("SeedersUpdated")) {
					if(index != -1 && colManager.GetColIndex(DownloadsTableColumnManager.COL_SEEDS_ID) <= tableModel.getColumnCount())
						tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_SEEDS_ID));
				}
				else if(updateString.equals("LeechersUpdated")) {
					if(index != -1 && colManager.GetColIndex(DownloadsTableColumnManager.COL_PEERS_ID) <= tableModel.getColumnCount())
						tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_PEERS_ID));
				}
				else if(updateString.equals("DownloadedSize")) {
					if(index != -1 && colManager.GetColIndex(DownloadsTableColumnManager.COL_DOWNLOADED_ID) <= tableModel.getColumnCount())
						tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_DOWNLOADED_ID));
					if(index != -1 && colManager.GetColIndex(DownloadsTableColumnManager.COL_SHARERATIO_ID) <= tableModel.getColumnCount())
						tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_SHARERATIO_ID));
				}
				else if(updateString.equals("UploadedSize")) {
					if(index != -1 && colManager.GetColIndex(DownloadsTableColumnManager.COL_SHARERATIO_ID) <= tableModel.getColumnCount()) {
						tableModel.fireTableCellUpdated(index, colManager.GetColIndex(DownloadsTableColumnManager.COL_SHARERATIO_ID));
					}
				}
				else if(updateString.equals("DestroyMe")) {
					((GuiDownloaderWrapperInterface)stateHolder.GetDownloaderObject()).HideDetails();
					RemoveDownload((GuiDownloaderWrapperInterface)stateHolder.GetDownloaderObject(),false);
				}
			}

			// Update buttons if the selected download has changed.
			if (selectedDownload != null && selectedDownload.GetStateHolder().equals(o)) {
				if(((String)arg).equals("DownloadStatus")) {
					UpdateButtonsForSelectedDownload();
				}
			}
		}});
	}
	
	public void BackupActiveDownloadDetailsInHistoryXml(DownloadStateHolder stateHolder) {
		
		//A download entry with status DOWNLOADING or STARTING cannot got in. This is just a double check
		if((stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.STARTING)) {
			stateHolder.GetDownloaderObject().Pause();
			stateHolder.SetDownloadStatus(DownloadStateHolder.PAUSED);
		}
		ArrayList<Integer> downloadSizeArr = new ArrayList<Integer>();
		ArrayList<Integer> chunkOffsetArr = new ArrayList<Integer>();
		ArrayList<Integer> writeRangeStartArr = new ArrayList<Integer>();
		
		//Collect data into arrays for the downloader
		for(int i=0; i<stateHolder.GetNumConnections(); i++) {
			downloadSizeArr.add(stateHolder.GetThreadFileSize(i));
			chunkOffsetArr.add(stateHolder.GetThreadChunkOffset(i));
			writeRangeStartArr.add(stateHolder.GetThreadWriteRangeStart(i));
		}
		historyXmlUpdater.UpdateToActiveDownloadEntry(
				((GuiDownloaderWrapperInterface)stateHolder.GetDownloaderObject()).GetUid(),
				stateHolder.GetUrlName(),
				stateHolder.GetDownloadType(),
                stateHolder.GetFileName(),
                stateHolder.GetDirName(), 
                stateHolder.GetFileSize(),
                downloadSizeArr,
                chunkOffsetArr,
                writeRangeStartArr,
                stateHolder.GetDownloadSpeed(),
                stateHolder.GetStartTime(),
                stateHolder.GetSeedingStartTime(),
                stateHolder.GetEndTime(),
                stateHolder.GetDownloadTime(),
                stateHolder.GetPauseStartTime(),
                stateHolder.GetPauseEndTime(),
                stateHolder.GetPausedTime(),
                stateHolder.GetStatus(),
                stateHolder.GetErrorMessage(),
                stateHolder.GetProgress(),
                stateHolder.GetDownloadedSize(),
                stateHolder.GetUploadedSize(),
                stateHolder.IsManagedTorrent(),
                stateHolder.GetTorrentDownloadedBitsAsString(),
                stateHolder.GetExcludedListAsString());
	}
	
	public void BackupInactiveDownloadDetailsInHistoryXml(DownloadStateHolder stateHolder) {
		historyXmlUpdater.UpdateToInactiveDownloadEntry(
				((GuiDownloaderWrapperInterface)stateHolder.GetDownloaderObject()).GetUid(),
				stateHolder.GetUrlName(),
				stateHolder.GetDownloadType(),
                stateHolder.GetFileName(),
                stateHolder.GetDirName(), 
                stateHolder.GetFileSize(),
                stateHolder.GetDownloadSpeed(),
                stateHolder.GetStartTime(),
                stateHolder.GetSeedingStartTime(),
                stateHolder.GetEndTime(),
                stateHolder.GetDownloadTime(),
                stateHolder.GetPauseStartTime(),
                stateHolder.GetPauseEndTime(),
                stateHolder.GetPausedTime(),
                stateHolder.GetStatus(),
                stateHolder.GetErrorMessage(),
                stateHolder.GetProgress(),
                stateHolder.GetDownloadedSize(),
                stateHolder.GetUploadedSize(),
                stateHolder.IsManagedTorrent(),
                stateHolder.GetTorrentDownloadedBitsAsString(),
                stateHolder.GetExcludedListAsString());
	}
	
	/*************************************************************
	 * 
	 * Function  : AddTableMouseListener
	 * Arguments : table
	 * Return    : void
	 * Notes     : Adds listener to check double click in the table.
	 * 
	 *************************************************************/
	public void AddTableMouseListener(final JTable table) {
		table.addMouseListener(new MouseAdapter() { public void mouseClicked(MouseEvent event) { 
			//Check for right click 
			if(event.getButton()==MouseEvent.BUTTON3) {
				//Get mouse point and from it, the downloader stateholder
				int row = downloadsTable.rowAtPoint(new Point(event.getX(), event.getY()));
				downloadsTable.getSelectionModel().setSelectionInterval(row, row);

				DownloadStateHolder stateHolder = (tableModel.GetDownload(row)).GetStateHolder();
					
				//For each state, disable menus as required.
				if(stateHolder.GetStatus() == DownloadStateHolder.STARTING) {
					pausePopupmenuItem.setEnabled(true);
					resumePopupmenuItem.setEnabled(false);
					cancelPopupmenuItem.setEnabled(true);
					clearPopupmenuItem.setEnabled(false);
					deletePopupmenuItem.setEnabled(false);
					folderPopupmenuItem.setEnabled(false);
				}
				else if(stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) {
					pausePopupmenuItem.setEnabled(true);
					resumePopupmenuItem.setEnabled(false);
					cancelPopupmenuItem.setEnabled(true);
					clearPopupmenuItem.setEnabled(true);
					deletePopupmenuItem.setEnabled(true);
					folderPopupmenuItem.setEnabled(true);
				}
				else if(stateHolder.GetStatus() == DownloadStateHolder.SEEDING) {
					pausePopupmenuItem.setEnabled(true);
					resumePopupmenuItem.setEnabled(false);
					if(selectedDownload.GetStateHolder().GetDownloadType() == Common.TYPE_TORRENT && selectedDownload.GetStateHolder().GetProgress() >=100)
						cancelPopupmenuItem.setEnabled(false);
					else
						cancelPopupmenuItem.setEnabled(true);
					clearPopupmenuItem.setEnabled(true);
					deletePopupmenuItem.setEnabled(true);
					folderPopupmenuItem.setEnabled(true);
				}
				else if((stateHolder.GetStatus() == DownloadStateHolder.PAUSED) ||
						(stateHolder.GetStatus() == DownloadStateHolder.PAUSEDFORERROR)) {	
					pausePopupmenuItem.setEnabled(false);
					resumePopupmenuItem.setEnabled(true);
					if(selectedDownload.GetStateHolder().GetDownloadType() == Common.TYPE_TORRENT && selectedDownload.GetStateHolder().GetProgress() >=100)
						cancelPopupmenuItem.setEnabled(false);
					else
						cancelPopupmenuItem.setEnabled(true);
					clearPopupmenuItem.setEnabled(true);
					deletePopupmenuItem.setEnabled(true);
					folderPopupmenuItem.setEnabled(true);
				}
				else if(stateHolder.GetStatus() == DownloadStateHolder.CANCELLED) {
					pausePopupmenuItem.setEnabled(false);
					resumePopupmenuItem.setEnabled(false);
					cancelPopupmenuItem.setEnabled(false);
					clearPopupmenuItem.setEnabled(true);
					deletePopupmenuItem.setEnabled(true);
					folderPopupmenuItem.setEnabled(true);
				}
				else if(stateHolder.GetStatus() == DownloadStateHolder.COMPLETED) {
					pausePopupmenuItem.setEnabled(false);
					resumePopupmenuItem.setEnabled(false);
					cancelPopupmenuItem.setEnabled(false);
					clearPopupmenuItem.setEnabled(true);
					deletePopupmenuItem.setEnabled(true);
					folderPopupmenuItem.setEnabled(true);
				}
				else if(stateHolder.GetStatus() == DownloadStateHolder.ERROR) {
					pausePopupmenuItem.setEnabled(false);
					resumePopupmenuItem.setEnabled(false);
					cancelPopupmenuItem.setEnabled(false);
					clearPopupmenuItem.setEnabled(true);
					deletePopupmenuItem.setEnabled(true);
					folderPopupmenuItem.setEnabled(false);
				}
					
				tablePopupMenu.show(downloadsTable, event.getX(), event.getY());
			}
			else {
				// check for double click
				if (event.getClickCount() < 2)
					return;
				
				//find column of click and
				int tableRow = table.rowAtPoint(event.getPoint());
				DownloadStateHolder stateHolder = (tableModel.GetDownload(tableRow)).GetStateHolder();

				ShowDetails(stateHolder);
			}
		}});
	}
	
	private void UpdatePrefs() {
		sysInfoHolder.APP_PREFERRED_HEIGHT=motherFrame.getHeight();
		sysInfoHolder.APP_PREFERRED_WIDTH=motherFrame.getWidth();
		sysInfoHolder.APP_START_X=motherFrame.getX();
		sysInfoHolder.APP_START_Y=motherFrame.getY();
		sysInfoHolder.BROWSERMANAGER_SPLILTPANE_PREFERRED_DIVIDER=browserManagerSplitPanel.getDividerLocation();
		for(int i=0; i< sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE; i++) {
			colManager.SetColWidth(colManager.GetActColType(i), downloadsTable.getColumnModel().getColumn(i).getWidth());
		}
		sysInfoHolder.DOWNLOADSTABLE_WIDTH_ARR_STR = Common.IntArr2String(colManager.GetColWidthArr());
		sysInfoHolder.DOWNLOADSTABLE_ORDER_ARR_STR = Common.IntArr2String(colManager.GetColOrderArr1());
		
		//This one is now redundant.
		configUpdater.WriteXml();
		
    	//Update runtime options. This updates all user changeable variables.
    	runtimeOptionsUpdater.WriteXml();
	}
	  
	/*************************************************************
	 * 
	 * Function  : GetFrame
	 * Arguments : void
	 * Return    : void
	 * Notes     : Returns JFrame of this object.
	 * 
	 *************************************************************/
	public Component GetFrame() {
		return motherFrame;
	}
	
	/*************************************************************
	 * 
	 * Function  : VisitHomepage
	 * Arguments : void
	 * Return    : void
	 * Notes     : Open the home page in browser
	 * 
	 *************************************************************/
	public void VisitHomepage() {
		GUICommon.LaunchInBrowser(sysInfoHolder.COMPANYURL);
	}
	
	public void ShowVersion() {
		VersionDisplayer versionDisplayer = new VersionDisplayer(motherFrame, sysInfoHolder);
		versionDisplayer.Show();
	}
	
	
	public void CleanAndExit(boolean force) {
		CleanForExit(force);
    	SESecurityManager.exitVM(0);
    	System.exit(0);
	}
	
	public void CleanForExit(boolean force) {
		if(force) {
			PauseAllDownloadsWithCOnfirm(force);
		}
		else if(! PauseAllDownloadsWithCOnfirm(force)) {
    	    return;
    	}

    	sysInfoHolder.isShuttingDown=true;

    	//Close out any details frames
		for(int i=0; i<activeDownloadsArr.size(); i++) {
			(activeDownloadsArr.get(i)).HideDetails();
		}
		
    	//Update preferences
    	UpdatePrefs();

    	welcomeDisplayer.disconnect();
    	welcomeDisplayer.removeNotify();
	
    	motherFrame.dispose();
    	
    	try {
    		core.stop();
    	} catch (Exception e) {
    		logger.error("Exception", e);
    	}
		
		//Remove tray icon
		if(sysInfoHolder.TrayAvailable())
			SystemTray.getSystemTray().remove(trayIcon);
	}

	
	private void ShowOptions() {
		OptionsFrame options = new OptionsFrame(this, sysInfoHolder, colManager, ioutils);
		options.Show();
	}
	
	private void ShowTorrentSearch(String str) {
		((CardLayout)(rightPanel.getLayout())).show(rightPanel, SEARCHPANEL);
		//searchPanel.SetSearchText(searchField.getText());
		searchPanel.Search(searchField.getText());
		searchField.setText(searchInitText);
		searchField.setForeground(Color.gray);
		addButt.requestFocusInWindow();
	}
	
	/*************************************************************
	 * 
	 * Function  : ShowDetails
	 * Arguments : stateHolder
	 * Return    : void
	 * Notes     : Show details for the downloader
	 * 
	 *************************************************************/
	private void ShowDetails(DownloadStateHolder stateHolder) {
		((GuiDownloaderWrapperInterface)stateHolder.GetDownloaderObject()).ShowDetails();
	}
	
	/*************************************************************
	 * 
	 * Function  : ShowDetailsForSelectedDownload
	 * Arguments : void
	 * Return    : void
	 * Notes     : Show details for selected download.
	 * 
	 *************************************************************/
	public void ShowDetailsForSelectedDownload() {
		int row = downloadsTable.getSelectedRow();
		if(row == -1)
			return;
		DownloadStateHolder stateHolder = tableModel.GetDownload(row).GetStateHolder();
		ShowDetails(stateHolder);
	}
	
	/*************************************************************
	 * 
	 * Function  : ShowFolder
	 * Arguments : stateHolder
	 * Return    : void
	 * Notes     : Show folder for the downloader
	 * 
	 *************************************************************/
	private void ShowFolder(DownloadStateHolder stateHolder) {
		String folder = stateHolder.GetDirName();
		try {
			if(Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
				(Desktop.getDesktop()).open(new File(folder));
	        }
			else {
				ioutils.PrintMessage("Due to a Java technical issue, file browser can't be opened on this platform.\nThe download location is -\n"+stateHolder.GetDirName());
			}
		} catch (IOException e1) {
		}
		return;
		/*
		//For windows, explorer will not work if there is "/" as a path seperator
		if((System.getProperty("os.name").toLowerCase()).indexOf("win")>=0) {
			folder=Common.ReplaceChar('/', "\\", folder);
        }
		try {
			Runtime rt = Runtime.getRuntime();
			rt.exec("explorer.exe \""+folder+"\"");
		} catch (Exception e) {
			logger.error("Exception", e);
		}*/
	}
	
	/*************************************************************
	 * 
	 * Function  : ShowFolderForSelectedDownload
	 * Arguments : void
	 * Return    : void
	 * Notes     : Show folder for selected download.
	 * 
	 *************************************************************/
	public void ShowFolderForSelectedDownload() {
		int row = downloadsTable.getSelectedRow();
		if(row == -1)
			return;
		DownloadStateHolder stateHolder = tableModel.GetDownload(row).GetStateHolder();
		ShowFolder(stateHolder);
	}
	
	private void ShowCredits() {
		//Create credits pane contents
		creditsDisplayer = new CreditsDisplayer(this);
		creditsDisplayer.Show();
	}
	
	public void ReDraw() {
		motherFrame.setVisible(false);
		motherFrame.setVisible(true);
	}
	
	public void CheckForUpdates(boolean immediate) {
		if(sysInfoHolder.isUpdateChecking)
			return;
		
		sysInfoHolder.isUpdateChecking=true;
		
		Date d1 = new Date(sysInfoHolder.LASTUDATEDAT);
		boolean startUpdate=false;
		
		if(immediate) {
			startUpdate=true;
		}
		else {
			if(sysInfoHolder.AUTOUPDATE == false) {
				startUpdate=false;
			}
			else if(sysInfoHolder.UPDATEOPTION == SystemInfoHolder.UPDATEEVERYTIME) {
				startUpdate=true;
			}
			else if(sysInfoHolder.UPDATEOPTION == SystemInfoHolder.UPDATEDAILY) {
				Calendar c1=Calendar.getInstance();
				c1.setTime(d1);
				if(c1.get(Calendar.DAY_OF_YEAR) < Calendar.getInstance().get(Calendar.DAY_OF_YEAR)) {
					startUpdate=true;
				}
			}
			else if(sysInfoHolder.UPDATEOPTION == SystemInfoHolder.UPDATEWEEKLY) {
				Calendar c1=Calendar.getInstance();
				c1.setTime(d1);
				if(c1.get(Calendar.WEEK_OF_YEAR) < Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)) {
					startUpdate=true;
				}
			}
		}
		
		if(startUpdate) {
			//Re set last updated time.
			updateChecker = new UpdateChecker(sysInfoHolder, ioutils, immediate);
			updateChecker.start();
		}
		else {
			sysInfoHolder.isUpdateChecking=false;
		}
	}
	
	private void SetSystemProxy() {
		if(sysInfoHolder.USEPROXY) {
			System.setProperty("proxySet", Boolean.toString(sysInfoHolder.USEPROXY));
			System.setProperty("http.proxyHost", sysInfoHolder.HTTPPROXYSERVER);
			System.setProperty("http.proxyPort", Integer.toString(sysInfoHolder.HTTPPROXYPORT));
			System.setProperty("http.proxyUser", sysInfoHolder.HTTPPROXYUSER);
			System.setProperty("http.proxyPassword", sysInfoHolder.HTTPPROXYPASSWORD);
		}
	}
	
	//public void SetSystemCookieManager()
	//{
	//	java.net.CookieManager cm = new java.net.CookieManager();
	//	cm.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
	//	java.net.CookieHandler.setDefault(cm);
//
	//}
	
	private void RunFirstTimeConfig() {
		sysInfoHolder.FIRSTRUN=0;
		
		//Check and set OS. OS field was moved from config.xml to runtimeoptions.xml as config.xml is 
		//fully overwritten during updates from version 2.4.0. This is not needed for fresh instalations
		//where OS would be set the first time itself.
		if(sysInfoHolder.OS.equals("other")) {
			String os=System.getProperty("os.name").toLowerCase();
			if(os.indexOf("win")>=0) {
				sysInfoHolder.OS="windows";
			}
			else if((os.indexOf("uni")>=0) || (os.indexOf("lin")>=0)) {
				sysInfoHolder.OS="linux";
			}
		}
	}
	
	public void DownloadComplete(DownloadStateHolder stateHolder) {
		if(stateHolder.GetDownloadType() == Common.TYPE_URL && stateHolder.GetStatus() == DownloadStateHolder.COMPLETED) {
			if(sysInfoHolder.SHOWPOPPUPWHENDOWNLOADFINISH) {
				SlidingFrame sFrame = new SlidingFrame("Download Finished", stateHolder.GetFileName(), stateHolder.GetDirName(),
				                               	stateHolder.GetUrlName(), stateHolder.GetElapsedTimeString(), Common.SizetoString(stateHolder.GetFileSize()));
				sFrame.start();
			}
		}
		else {
			SlidingFrame sFrame = new SlidingFrame("Torrent download finished...", stateHolder.GetFileName(), stateHolder.GetDirName(),
                   	stateHolder.GetUrlName(), stateHolder.GetElapsedTimeString(), Common.SizetoString(stateHolder.GetFileSize()));
			sFrame.start();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void CleanTorrentCore(AzureusCore core) {
		List<DownloadManager> downloadManagers = core.getGlobalManager().getDownloadManagers();
		for(DownloadManager manager : downloadManagers){
			String torrent = Common.GetFileNameFromAbsPath(manager.getTorrentFileName());
			boolean gotit=false;
			for(int i=0; i<allDownloadsArr.size(); i++) {
				if(Common.GetFileNameFromAbsPath(allDownloadsArr.get(i).GetStateHolder().GetUrlName()).equals(torrent)) {
					if(allDownloadsArr.get(i).GetStateHolder().GetStatus() != DownloadStateHolder.CANCELLED)
						gotit=true;
				}
			}
			if(! gotit) {
				try {
					core.getGlobalManager().removeDownloadManager(manager);
				} catch (Exception e) {}
			}
		}
	}
	
	
	private void AddSystemTray() {
		//Check and add tray
		sysInfoHolder.SetTrayAvailable(false);
		if(! sysInfoHolder.TRAYENABLED)
			return;
		
		if(! SystemTray.isSupported())
			return;

		final SystemTray tray = SystemTray.getSystemTray();
		trayIcon.setImageAutoSize(true);
		
        MenuItem item = new MenuItem("Add new Torrent download");
        trayPopup.add(item);
        item.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	AddNewTorrentDownload();
            }
        });
        item = new MenuItem("Add new URL download");
        trayPopup.add(item);
        item.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	AddNewUrlDownload();
            }
        });
        item = new MenuItem("-");
        trayPopup.add(item);
        item = new MenuItem("Bring to front");
        trayPopup.add(item);
        item.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	ToFront();
            }
        });
        item = new MenuItem("-");
        trayPopup.add(item);
        item = new MenuItem("Quit");
        trayPopup.add(item);
        item.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	CleanAndExit(false);
            }
        });
        
        trayIcon.addMouseListener(new MouseAdapter() { public void mouseClicked(MouseEvent event) { 
			//Check for right click 
			if(event.getButton()==MouseEvent.BUTTON3) {
			}
			else {
				// check for double click
				if (event.getClickCount() >= 2)
					ToFront();
			}
		}});


        try {
        	tray.add(trayIcon);
        } catch (Exception e) {
        	sysInfoHolder.SetTrayAvailable(false);
        }
        sysInfoHolder.SetTrayAvailable(true);
	}

	private void ShowColumnSelectPopUp(final MouseEvent e) {
		JPopupMenu menu = new JPopupMenu();
		JTextField heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Select Columns");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		menu.add(heading);
		JCheckBox chkBox = new JCheckBox(colManager.GetColName(DownloadsTableColumnManager.COL_FILE_ID));
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_FILE_ID));
		chkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				CheckAndUpdateColumns(DownloadsTableColumnManager.COL_FILE_ID, ((JCheckBox)e1.getSource()).isSelected());
				ShowColumnSelectPopUp(e);
			}
		});
		menu.add(chkBox);
		chkBox = new JCheckBox(colManager.GetColName(DownloadsTableColumnManager.COL_SIZE_ID));
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_SIZE_ID));
		chkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				CheckAndUpdateColumns(DownloadsTableColumnManager.COL_SIZE_ID, ((JCheckBox)e1.getSource()).isSelected());
				ShowColumnSelectPopUp(e);
			}
		});
		menu.add(chkBox);
		chkBox = new JCheckBox(colManager.GetColName(DownloadsTableColumnManager.COL_ETA_ID));
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_ETA_ID));
		chkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				CheckAndUpdateColumns(DownloadsTableColumnManager.COL_ETA_ID, ((JCheckBox)e1.getSource()).isSelected());
				ShowColumnSelectPopUp(e);
			}
		});
		menu.add(chkBox);
		chkBox = new JCheckBox(colManager.GetColName(DownloadsTableColumnManager.COL_PROGRESS_ID));
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_PROGRESS_ID));
		chkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				CheckAndUpdateColumns(DownloadsTableColumnManager.COL_PROGRESS_ID, ((JCheckBox)e1.getSource()).isSelected());
				ShowColumnSelectPopUp(e);
			}
		});
		menu.add(chkBox);
		chkBox = new JCheckBox(colManager.GetColName(DownloadsTableColumnManager.COL_DOWNSPEED_ID));
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_DOWNSPEED_ID));
		chkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				CheckAndUpdateColumns(DownloadsTableColumnManager.COL_DOWNSPEED_ID, ((JCheckBox)e1.getSource()).isSelected());
				ShowColumnSelectPopUp(e);
			}
		});
		menu.add(chkBox);
		chkBox = new JCheckBox(colManager.GetColName(DownloadsTableColumnManager.COL_UPSPEED_ID));
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_UPSPEED_ID));
		chkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				CheckAndUpdateColumns(DownloadsTableColumnManager.COL_UPSPEED_ID, ((JCheckBox)e1.getSource()).isSelected());
				ShowColumnSelectPopUp(e);
			}
		});
		menu.add(chkBox);
		chkBox = new JCheckBox(colManager.GetColName(DownloadsTableColumnManager.COL_STATUS_ID));
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_STATUS_ID));
		chkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				CheckAndUpdateColumns(DownloadsTableColumnManager.COL_STATUS_ID, ((JCheckBox)e1.getSource()).isSelected());
				ShowColumnSelectPopUp(e);
			}
		});
		menu.add(chkBox);
		chkBox = new JCheckBox(colManager.GetColName(DownloadsTableColumnManager.COL_SEEDS_ID));
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_SEEDS_ID));
		chkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				CheckAndUpdateColumns(DownloadsTableColumnManager.COL_SEEDS_ID, ((JCheckBox)e1.getSource()).isSelected());
				ShowColumnSelectPopUp(e);
			}
		});
		menu.add(chkBox);
		chkBox = new JCheckBox(colManager.GetColName(DownloadsTableColumnManager.COL_PEERS_ID));
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_PEERS_ID));
		chkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				CheckAndUpdateColumns(DownloadsTableColumnManager.COL_PEERS_ID, ((JCheckBox)e1.getSource()).isSelected());
				ShowColumnSelectPopUp(e);
			}
		});
		menu.add(chkBox);
		chkBox = new JCheckBox(colManager.GetColName(DownloadsTableColumnManager.COL_SHARERATIO_ID));
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_SHARERATIO_ID));
		chkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				CheckAndUpdateColumns(DownloadsTableColumnManager.COL_SHARERATIO_ID, ((JCheckBox)e1.getSource()).isSelected());
				ShowColumnSelectPopUp(e);
			}
		});
		menu.add(chkBox);
		chkBox = new JCheckBox(colManager.GetColName(DownloadsTableColumnManager.COL_DOWNLOADED_ID));
		chkBox.setSelected(colManager.IsVisible(DownloadsTableColumnManager.COL_DOWNLOADED_ID));
		chkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				CheckAndUpdateColumns(DownloadsTableColumnManager.COL_DOWNLOADED_ID, ((JCheckBox)e1.getSource()).isSelected());
				ShowColumnSelectPopUp(e);
			}
		});
		menu.add(chkBox);
		
		menu.show(e.getComponent(), e.getX(), e.getY());
	}
	
	public void CheckAndUpdateColumns(int col, boolean selected) {
		if(selected) {
			if(! colManager.IsVisible(col)) {
				colManager.MakeVisible(col);
				tableModel.fireTableStructureChanged();
				for(int i=0; i< sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE; i++) {
		        	downloadsTable.getColumnModel().getColumn(i).setPreferredWidth(colManager.GetColWidth(colManager.GetActColType(i)));
		        }
			}
		}
		else {
			if(colManager.IsVisible(col)) {
				colManager.MakeInvisible(col);
				tableModel.fireTableStructureChanged();
				for(int i=0; i< sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE; i++) {
		        	downloadsTable.getColumnModel().getColumn(i).setPreferredWidth(colManager.GetColWidth(colManager.GetActColType(i)));
		        }
			}
		}
	}
	
	public boolean HasDownloadingInstances() {
		for(int i=0; i< activeDownloadsArr.size(); i++) {
			if(activeDownloadsArr.get(i).GetStateHolder().GetStatus() == DownloadStateHolder.DOWNLOADING) {
				return true;
			}
		}
		return false;
	}
	
	public void ResetToDefaults() {
		CleanForExit(true);
		if((new File(sysInfoHolder.getDataDir())).exists()) {
			Common.deleteDirectory(sysInfoHolder.getDataDir(), logger);
			(new File(sysInfoHolder.getDataDir())).mkdir();
		}
		SESecurityManager.exitVM(0);
    	System.exit(0);
	}
}
