/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    GUI wrapper for urldownloader to incorporate additional gui functions.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import skdownloader.core.DownloadStateHolder;
import skdownloader.core.urlcore.UrlDownloader;

public class GuiUrlDownloaderWrapper extends UrlDownloader implements GuiDownloaderWrapperInterface
{
	private ArrayList<ImageIcon> statusAnimList = new ArrayList<ImageIcon>();
	//private DownloadurlDetailsFrame detailsFrame;
	private MainFrame mainFrame;
	SystemInfoHolder sysInfoHolder;
	private GuiUserIOUtils ioutils;
	private int uid;
	//private int tableIndex; //This denotes the row index were details of this download are displayed.
	private boolean detailsDisplayed; // Denotes whether details are currently displayed or not.
	UrlDetailsFrame urlDetailsFrame;
	
	public GuiUrlDownloaderWrapper(MainFrame mainFrame)
	{
		this.mainFrame=mainFrame;
	}
	
	//This is for new download
	public GuiUrlDownloaderWrapper(MainFrame mainFrame, 
			                       String urlname, 
			                       String dirname,
			                       int numConnections,
			                       int minAcclelerateFileSize,
			                       int uid,
			                       SystemInfoHolder sysInfoHolder,
			                       GuiUserIOUtils ioutils)
	{
		super(urlname, dirname, numConnections, minAcclelerateFileSize, sysInfoHolder, ioutils);
		this.mainFrame=mainFrame;
		this.GetStateHolder().SetParentComponent(mainFrame.GetFrame());
		this.uid=uid;
		this.sysInfoHolder = sysInfoHolder;
		this.ioutils=ioutils;
		SetDetailsDisplayStatus(false);
		AddDownloadListener(mainFrame);
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/downloading.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/downloading.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif"))); //pausedforerror
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif"))); //pausedforexit
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/completed.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/cancelled.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/error.png")));
	}
	
	//This is for inactive history downloads(Completed, cancelled or error)
	public GuiUrlDownloaderWrapper(MainFrame mainFrame, 
			                       String urlname, 
			                       String dirname, 
			                       String filename, 
			                       long size,
			                       float speed,
			                       long startTime,
			                       long endTime,
			                       int pausedTime,
			                       int state, 
			                       String errorMessage,
			                       int progress, 
			                       int uid,
			                       SystemInfoHolder sysInfoHolder,
			                       GuiUserIOUtils ioutils, 
			                       boolean dummy)
	
	{
		super(urlname, dirname, 0, 0, sysInfoHolder, ioutils);
		this.mainFrame=mainFrame;
		this.sysInfoHolder = sysInfoHolder;
		this.ioutils=ioutils;
		this.GetStateHolder().SetFileSize(size);
		this.GetStateHolder().SetDownloadSpeed(speed);
		this.GetStateHolder().SetStartTime(startTime);
		this.GetStateHolder().SetEndTime(endTime);
		this.GetStateHolder().SetPausedTime(pausedTime);
		this.GetStateHolder().SetFileName(filename);
		this.uid=uid;
		//Set status. Since this is a histoy download, it should not have an active state.
		this.GetStateHolder().SetDownloadStatus(state);
		this.GetStateHolder().SetErrorMessage(errorMessage);
		if((GetStateHolder().GetStatus() == DownloadStateHolder.DOWNLOADING) ||
		   (GetStateHolder().GetStatus() == DownloadStateHolder.PAUSED))
		{
			GetStateHolder().SetDownloadStatus(DownloadStateHolder.ERROR);
		}
		this.GetStateHolder().SetProgress(progress);
		this.GetStateHolder().SetElapsedTime();
		if(GetStateHolder().GetStatus() == DownloadStateHolder.COMPLETED)
			this.GetStateHolder().SetDownloadedSize(this.GetStateHolder().GetFileSize());
		SetDetailsDisplayStatus(false);
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/downloading.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/downloading.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif"))); //pausedforerror
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif"))); //pausedforexit
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/completed.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/cancelled.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/error.png")));
	}
	
	
	//This is for paused downloads
	public GuiUrlDownloaderWrapper(MainFrame mainFrame, 
								   String urlname, 
								   String dirname, 
								   String filename, 
								   long size,
								   float speed,
								   long startTime,
								   long endTime,
								   long pauseStartTime,
								   long pauseEndTime,
								   int pausedTime,
								   int state,
								   ArrayList<Integer> downloadSizeArr,
								   ArrayList<Integer> chunkOffsetArr,
								   ArrayList<Integer> writeRangeStartArr,
								   String errorMessage,
								   int progress, 
								   int uid,
								   SystemInfoHolder sysInfoHolder,
								   GuiUserIOUtils ioutils, 
								   boolean dummy)

	{
		super(urlname, dirname, downloadSizeArr.size(), 0, sysInfoHolder, ioutils);
		this.mainFrame=mainFrame;
		this.sysInfoHolder = sysInfoHolder;
		this.ioutils=ioutils;
		this.GetStateHolder().SetFileName(filename);
		this.GetStateHolder().SetFileSize(size);
		this.GetStateHolder().SetDownloadSpeed(0);
		this.GetStateHolder().SetStartTime(startTime);
		this.GetStateHolder().SetEndTime(endTime);
		this.GetStateHolder().SetPauseStartTime(pauseStartTime);
		this.GetStateHolder().SetPauseEndTime(pauseEndTime);
		this.GetStateHolder().SetPausedTime(pausedTime);
		this.uid=uid;
		//Set status. Since this is a histoy download, it should not have an active state.
		this.GetStateHolder().SetDownloadStatus(state);
		this.GetStateHolder().SetErrorMessage(errorMessage);
		this.GetStateHolder().SetProgress(progress);
		this.GetStateHolder().SetElapsedTime();
		this.GetStateHolder().SetNumConnections(downloadSizeArr.size());
		for(int i=0; i<this.GetStateHolder().GetNumConnections(); i++)
		{
			this.GetStateHolder().SetThreadFileSize(i, downloadSizeArr.get(i));
			this.GetStateHolder().SetThreadChunkOffset(i, chunkOffsetArr.get(i));
			this.GetStateHolder().SetThreadWriteRangeStart(i, writeRangeStartArr.get(i));
		}
		SetDetailsDisplayStatus(false);
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/downloading.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/downloading.gif")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif"))); 
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif"))); //pausedforerror
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/paused.gif"))); //pausedforexit
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/completed.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/cancelled.png")));
		statusAnimList.add(new ImageIcon(this.getClass().getResource("guicomponents/images/error.png")));
	}
	
	public void Start() {
		ShowDetails();
		start();
	}
	
	public void StartSilently() {
		start();
	}
	
	public ImageIcon GetStatusAnimImage(int index) {
		return (ImageIcon)statusAnimList.get(index);
	}
	
	public ImageIcon GetNewStatusAnimImage(int index) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("guicomponents/images/downloading.gif");
		list.add("guicomponents/images/downloading.gif");
		list.add("guicomponents/images/paused.gif");
		list.add("guicomponents/images/downloading.gif"); //pausedforerror
		list.add("guicomponents/images/downloading.gif"); //pausedforexit
		list.add("guicomponents/images/completed.png");
		list.add("guicomponents/images/cancelled.png");
		list.add("guicomponents/images/error.png");
		
		return new ImageIcon(this.getClass().getResource(list.get(index)));
	}
	
	public boolean GetDetailsDisplayStatus() {
		return detailsDisplayed;
	}
	
	public void SetDetailsDisplayStatus(boolean status) {
		detailsDisplayed=status;
	}
	
	public void ShowDetails() {
		if(! this.GetDetailsDisplayStatus()) {
			try  {
				final DownloadStateHolder stateHolder1 = this.GetStateHolder();
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					urlDetailsFrame = new UrlDetailsFrame(mainFrame, stateHolder1, sysInfoHolder, ioutils);
					urlDetailsFrame.ShowFrame();
				}});
	        }
			catch (Exception e) {
				
			}
		}
		else
			urlDetailsFrame.ToFront();
		SetDetailsDisplayStatus(true);
	}
	
	public void HideDetails() {
		if(this.GetDetailsDisplayStatus()) {
			this.GetStateHolder().deleteObserver(urlDetailsFrame);
			urlDetailsFrame.DisposeFrame();
			urlDetailsFrame=null;
		}
		SetDetailsDisplayStatus(false);
	}
	
	public int GetUid() {
		return uid;
	}
}