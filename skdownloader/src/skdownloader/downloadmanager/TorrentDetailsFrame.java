package skdownloader.downloadmanager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import org.gudy.azureus2.core3.peer.PEPeer;
import org.gudy.azureus2.core3.peer.PEPeerManager;
import org.gudy.azureus2.core3.peer.PEPeerManagerListener;
import org.gudy.azureus2.core3.peer.PEPiece;

import com.aelitis.azureus.core.peermanager.peerdb.PeerItem;

import skdownloader.core.Common;
import skdownloader.core.DownloadStateHolder;
import skdownloader.downloadmanager.guicomponents.CheckBoxRenderer;
import skdownloader.downloadmanager.guicomponents.ComboBoxRenderer;
import skdownloader.downloadmanager.guicomponents.DownloadsTableDefaultRenderer;
import skdownloader.downloadmanager.guicomponents.ProgressRenderer;
import skdownloader.downloadmanager.guicomponents.TorrentDetailsTableModel;
import skdownloader.downloadmanager.guicomponents.TorrentFileDetailsTableModel;
import skdownloader.downloadmanager.guicomponents.TorrentProgressBar;

public class TorrentDetailsFrame implements ActionListener, Observer, PEPeerManagerListener{
	
	private MainFrame parentFrame;
	private DownloadStateHolder stateHolder;
	private SystemInfoHolder sysInfoHolder;
	private GuiUserIOUtils ioutils;
	
	JFrame detailsFrame;
	JPanel motherPanel, generalPanel, detailsPabel, buttonsPanel, taskTablePanel;
	JPanel detailsTextPanel;
	JTable taskTable, fileTable;
	TorrentDetailsTableModel tableModel;
	TorrentFileDetailsTableModel fileTableModel;
	JTextPane headLabel, sizeText, progressText, seedersText, shareText, downSpeedText, upSpeedText, stateText, downloadedText, uploadedText, elapsedTimeText, etaTimeText, downloadFileText, piecesText, hashText,  creatorText, privacyText, createdAtText, torrentSizeText, infoText;
	private JTextField fileText, torrentText;
	JLabel imgLabel, healthText;
	JTextArea commentText;
	
	//This is a hack to make sure that the details frame is not hidden when we resume from a completed state.
	//Normally, when state is changed to seeding, the details frame will hide.
	private boolean wasDownloading = true; 
	
	JButton pauseButt, resumeButt, cancelButt, detailsButt;
	TorrentProgressBar progressBar;
	
	JTabbedPane tabbedPane;
	
	//ArrayList<TaskRepresenter> taskList = new ArrayList<TaskRepresenter>();
	//ArrayList<TaskRepresenter> tmpList = new ArrayList<TaskRepresenter>();
	
	int row = 0;
	int column = 0;
	private boolean isPeerListener=false;
	
	private ImageIcon goodIcon, mediumIcon, badIcon;
	
	public TorrentDetailsFrame(MainFrame parentFrame, DownloadStateHolder stateHolder, SystemInfoHolder sysInfoHolder, GuiUserIOUtils ioutils) {
		this.parentFrame=parentFrame;
		this.stateHolder=stateHolder;
		this.sysInfoHolder=sysInfoHolder;
		this.ioutils=ioutils;
		this.goodIcon=new ImageIcon(this.getClass().getResource("guicomponents/images/torrent_status_good.gif"));
		this.mediumIcon=new ImageIcon(this.getClass().getResource("guicomponents/images/torrent_status_medium.gif"));
		this.badIcon=new ImageIcon(this.getClass().getResource("guicomponents/images/torrent_status_bad.gif"));
	}
	
	public void ShowFrame() {
		//Font fontDefaultNormal12 = new Font("",Font.PLAIN, 12); //For normal labels
		Font fontDefaultNormal10 = new Font("",Font.PLAIN, 10); //For labels needing smaller font.
		
		detailsFrame = new JFrame();
		detailsFrame.setIconImage(new ImageIcon(this.getClass().getResource("guicomponents/images/skdownloader32.png")).getImage());
		
		//Mother panel is the main panel for all components.
		motherPanel = new JPanel(new BorderLayout());
		
		tabbedPane = new JTabbedPane();
		//tabbedPane.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		tabbedPane.setBorder(null);
		generalPanel = new JPanel();
		generalPanel.setBorder(null);
		tabbedPane.addTab("General", generalPanel);
		JPanel torrentDetailsPanel=new JPanel();
		tabbedPane.addTab("Torrent Details", torrentDetailsPanel );
		JPanel fileDetailsPanel=new JPanel();
		tabbedPane.addTab("file Details", fileDetailsPanel );
		//1. Header
		detailsFrame.setTitle(stateHolder.GetProgress()+"% - "+stateHolder.GetFileName());
		
		//Details pabel holds everything other than table.
		detailsTextPanel = new JPanel();
        detailsTextPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        detailsTextPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        
        JPanel topTextPanel = new JPanel();
		topTextPanel.setLayout(new GridBagLayout());
		GridBagConstraints c1 = new GridBagConstraints();
        topTextPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        
        //2.1 Torrent in details text panel
		JLabel fileLbl = new JLabel(" File:");
		//fileLbl.setFont(fileLbl.getFont().deriveFont(fileLbl.getFont().getStyle() ^ Font.BOLD));
		fileLbl.setForeground(Color.BLUE);
		fileLbl.setMinimumSize(fileLbl.getPreferredSize());
		fileText = new JTextField();
		fileText.setCaretPosition(0);
		fileText.setOpaque(false);
		fileText.setEditable(false);
		//torrentText.setBorder(null);
		fileText.setMaximumSize(fileText.getPreferredSize());
		c1.fill = GridBagConstraints.HORIZONTAL;
		c1.insets = new Insets(5,0,5,2);
		c1.gridx = 0;c1.gridy = 0; c1.anchor=GridBagConstraints.CENTER;
		c1.weightx=0;
		topTextPanel.add(fileLbl, c1);
		c1.gridx = 1;c1.gridy = 0; c1.anchor=GridBagConstraints.CENTER;
		c1.weightx=1;
		c1.gridwidth=3;
		topTextPanel.add(fileText, c1);
		
		topTextPanel.setBorder(null);
		c1.gridx = 0;c1.gridy = 1; c1.anchor=GridBagConstraints.CENTER;
		c1.weightx=1;
		c1.gridwidth=4;
		topTextPanel.add(new JSeparator(SwingConstants.HORIZONTAL), c1);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;c.gridy = 0; c.anchor=GridBagConstraints.CENTER;
		c.weightx=1;
		c.gridwidth=4;
		detailsTextPanel.add(topTextPanel, c);

		JLabel doneLbl = new JLabel(" Downloaded");
		doneLbl.setHorizontalAlignment(JLabel.LEADING);
		doneLbl.setForeground(Color.BLUE);
		//doneLbl.setFont(doneLbl.getFont().deriveFont(doneLbl.getFont().getStyle() ^ Font.BOLD));
		downloadedText = new JTextPane();
		downloadedText.setEditable(false);
		downloadedText.setOpaque(false);
		downloadedText.setMaximumSize(downloadedText.getPreferredSize());
		c.insets = new Insets(5,0,0,0);
		c.gridx = 0;c.gridy = 3; c.anchor=GridBagConstraints.LINE_END;
		c.weightx=0;
		c.gridwidth=1;
		detailsTextPanel.add(doneLbl, c);
		c.gridx = 1;c.gridy = 3; c.anchor=GridBagConstraints.CENTER;
		c.weightx=1;
		detailsTextPanel.add(downloadedText, c);
		
		
		JLabel uploadedLbl = new JLabel(" Uploaded");
		uploadedLbl.setHorizontalAlignment(JLabel.LEADING);
		uploadedLbl.setForeground(Color.BLUE);
		//uploadedLbl.setFont(uploadedLbl.getFont().deriveFont(uploadedLbl.getFont().getStyle() ^ Font.BOLD));
		uploadedText = new JTextPane();
		uploadedText.setEditable(false);
		uploadedText.setOpaque(false);
		uploadedText.setMaximumSize(uploadedText.getPreferredSize());
		c.insets = new Insets(5,0,0,0);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;c.gridy = 4; c.anchor=GridBagConstraints.LINE_END;
		c.weightx=0;
		detailsTextPanel.add(uploadedLbl, c);
		c.gridx = 1;c.gridy = 4; c.anchor=GridBagConstraints.WEST;
		c.weightx=1;
		detailsTextPanel.add(uploadedText, c);
		
		//2.4 download speed
		JLabel downSpeedLbl = new JLabel(" Down Speed");
		downSpeedLbl.setHorizontalAlignment(JLabel.LEADING);
		downSpeedLbl.setForeground(Color.BLUE);
		//downSpeedLbl.setFont(downSpeedLbl.getFont().deriveFont(downSpeedLbl.getFont().getStyle() ^ Font.BOLD));
		downSpeedText = new JTextPane();
		downSpeedText.setEditable(true);
		downSpeedText.setOpaque(false);
		downSpeedText.setMaximumSize(downSpeedText.getPreferredSize());
		c.insets = new Insets(5,0,0,0);
		c.gridx = 0;c.gridy = 5; c.anchor=GridBagConstraints.LINE_END;
		c.weightx=0;
		detailsTextPanel.add(downSpeedLbl, c);
		c.gridx = 1;c.gridy = 5; c.anchor=GridBagConstraints.CENTER;
		c.weightx=1;
		detailsTextPanel.add(downSpeedText, c);
		
		//2.4 download speed
		JLabel upSpeedLbl = new JLabel(" Up Speed");
		upSpeedLbl.setHorizontalAlignment(JLabel.LEADING);
		upSpeedLbl.setForeground(Color.BLUE);
		//upSpeedLbl.setFont(upSpeedLbl.getFont().deriveFont(upSpeedLbl.getFont().getStyle() ^ Font.BOLD));
		upSpeedText = new JTextPane();
		upSpeedText.setEditable(false);
		upSpeedText.setOpaque(false);
		upSpeedText.setMaximumSize(upSpeedText.getPreferredSize());
		c.insets = new Insets(5,0,0,0);
		c.gridx = 0;c.gridy = 6; c.anchor=GridBagConstraints.LINE_END;
		c.weightx=0;
		detailsTextPanel.add(upSpeedLbl, c);
		c.gridx = 1;c.gridy = 6; c.anchor=GridBagConstraints.CENTER;
		c.weightx=1;
		detailsTextPanel.add(upSpeedText, c);
		
		//2.3 Time
		JLabel etaTimeLbl = new JLabel(" Time left");
		etaTimeLbl.setHorizontalAlignment(JLabel.LEADING);
		etaTimeLbl.setForeground(Color.BLUE);
		//etaTimeLbl.setFont(etaTimeLbl.getFont().deriveFont(etaTimeLbl.getFont().getStyle() ^ Font.BOLD));
		etaTimeText = new JTextPane();
		etaTimeText.setEditable(false);
		etaTimeText.setOpaque(false);
		etaTimeText.setMaximumSize(etaTimeText.getPreferredSize());
		c.insets = new Insets(5,0,5,0);
		c.gridx = 0;c.gridy =7; c.anchor=GridBagConstraints.LINE_END;
		c.weightx=0;
		detailsTextPanel.add(etaTimeLbl, c);
		c.gridx = 1;c.gridy = 7; c.anchor=GridBagConstraints.WEST;
		c.weightx=1;
		detailsTextPanel.add(etaTimeText, c);
		
		 //2.3 Size
		JLabel sizeLbl = new JLabel(" Size");
		sizeLbl.setHorizontalAlignment(JLabel.LEADING);
		sizeLbl.setForeground(Color.BLUE);
		//sizeLbl.setFont(sizeLbl.getFont().deriveFont(sizeLbl.getFont().getStyle() ^ Font.BOLD));
		sizeText = new JTextPane();
		sizeText.setEditable(false);
		sizeText.setOpaque(false);
		sizeText.setMaximumSize(sizeText.getPreferredSize());
		c.insets = new Insets(5,0,0,0);
		c.gridx = 2;c.gridy = 3; c.anchor=GridBagConstraints.CENTER;
		c.weightx=0;
		c.gridwidth=1;
		detailsTextPanel.add(sizeLbl, c);
		c.gridx = 3;c.gridy = 3; c.anchor=GridBagConstraints.CENTER;
		c.weightx=1;
		detailsTextPanel.add(sizeText, c);
		
		//2.3 Seders
		JLabel seedersLbl = new JLabel(" S/L");
		seedersLbl.setHorizontalAlignment(JLabel.LEADING);
		seedersLbl.setForeground(Color.BLUE);
		//seedersLbl.setFont(seedersLbl.getFont().deriveFont(seedersLbl.getFont().getStyle() ^ Font.BOLD));
		seedersText = new JTextPane();
		seedersText.setEditable(false);
		seedersText.setOpaque(false);
		seedersText.setMaximumSize(seedersText.getPreferredSize());
		c.insets = new Insets(5,0,0,0);
		c.gridx = 2;c.gridy = 4; c.anchor=GridBagConstraints.CENTER;
		c.weightx=0;
		detailsTextPanel.add(seedersLbl, c);
		c.gridx = 3;c.gridy = 4; c.anchor=GridBagConstraints.CENTER;
		c.weightx=1;
		detailsTextPanel.add(seedersText, c);
		
		//2.3 Leechers
		JLabel shareLbl = new JLabel(" Share");
		shareLbl.setHorizontalAlignment(JLabel.LEADING);
		shareLbl.setForeground(Color.BLUE);
		//shareLbl.setFont(shareLbl.getFont().deriveFont(shareLbl.getFont().getStyle() ^ Font.BOLD));
		shareText = new JTextPane();
		shareText.setEditable(false);
		shareText.setOpaque(false);
		shareText.setMaximumSize(shareText.getPreferredSize());
		c.insets = new Insets(5,0,5,0);
		c.gridx = 2;c.gridy = 5; c.anchor=GridBagConstraints.WEST;
		c.weightx=0;
		detailsTextPanel.add(shareLbl, c);
		c.gridx = 3;c.gridy = 5; c.anchor=GridBagConstraints.WEST;
		c.weightx=1;
		detailsTextPanel.add(shareText, c);

		//2.3 Time
		JLabel healthLbl = new JLabel(" Health");
		healthLbl.setHorizontalAlignment(JLabel.LEADING);
		healthLbl.setForeground(Color.BLUE);
		//healthLbl.setFont(healthLbl.getFont().deriveFont(healthLbl.getFont().getStyle() ^ Font.BOLD));
		healthText = new JLabel();
		healthText.setIcon(GetHealthImage());
		healthText.setText(":");
		healthText.setVerticalTextPosition(JLabel.CENTER);
		healthText.setHorizontalTextPosition(JLabel.LEADING);
		JPanel healthPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		healthPanel.add(healthText);
		healthText.setMaximumSize(new Dimension(17,17));
		c.insets = new Insets(5,0,0,0);
		c.gridx = 2;c.gridy = 6; c.anchor=GridBagConstraints.WEST;
		c.weightx=0;
		detailsTextPanel.add(healthLbl, c);
		c.gridx = 3;c.gridy = 6; c.anchor=GridBagConstraints.FIRST_LINE_START;
		c.weightx=0;
		detailsTextPanel.add(healthPanel, c);
		
		//2.3 Time
		JLabel elapsedTimeLbl = new JLabel(" Elapsed");
		elapsedTimeLbl.setHorizontalAlignment(JLabel.LEADING);
		elapsedTimeLbl.setForeground(Color.BLUE);
		//elapsedTimeLbl.setFont(elapsedTimeLbl.getFont().deriveFont(elapsedTimeLbl.getFont().getStyle() ^ Font.BOLD));
		elapsedTimeText = new JTextPane();
		elapsedTimeText.setEditable(false);
		elapsedTimeText.setOpaque(false);
		elapsedTimeText.setMaximumSize(elapsedTimeText.getPreferredSize());
		c.insets = new Insets(5,0,0,0);
		c.gridx = 2;c.gridy = 7; c.anchor=GridBagConstraints.WEST;
		c.weightx=0;
		detailsTextPanel.add(elapsedTimeLbl, c);
		c.gridx = 3;c.gridy = 7; c.anchor=GridBagConstraints.WEST;
		c.weightx=1;
		detailsTextPanel.add(elapsedTimeText, c);
				
		JPanel progressPanel = new JPanel(new BorderLayout());
		progressBar = new TorrentProgressBar(200,20);
		progressBar.SetFromBitset(stateHolder.GetTorrentDownloadedBits(), stateHolder.GetNumberOfPieces());
		progressPanel.add(progressBar, BorderLayout.CENTER);
		progressText = new JTextPane();
		progressText.setText(this.stateHolder.GetProgress()+"%");
		progressText.setEditable(false);
		progressText.setOpaque(false);
		progressPanel.add(progressText, BorderLayout.EAST);

		
		progressPanel.setBorder(BorderFactory.createEmptyBorder(0,5,5,5));

		//Buttons
		buttonsPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));		
		pauseButt=new JButton("Pause");
		pauseButt.setFont(fontDefaultNormal10);
		pauseButt.addActionListener(this);
		pauseButt.setActionCommand("PauseDownload");		
		buttonsPanel.add(pauseButt);
		
		resumeButt=new JButton("Resume");
		resumeButt.setFont(fontDefaultNormal10);
		resumeButt.addActionListener(this);
		resumeButt.setActionCommand("ResumeDownload");	
		buttonsPanel.add(resumeButt);
		
		cancelButt=new JButton("Cancel");
		cancelButt.setFont(fontDefaultNormal10);
		cancelButt.addActionListener(this);
		cancelButt.setActionCommand("CancelDownload");	
		buttonsPanel.add(cancelButt);

		JLabel seperatorText = new JLabel("  ");
		buttonsPanel.add(seperatorText);

		imgLabel= new JLabel(((GuiTorrentDownloaderWrapper)(stateHolder.GetDownloaderObject())).GetNewStatusAnimImage(stateHolder.GetStatus()));
		buttonsPanel.add(imgLabel);
		
		stateText = new JTextPane();
		if((stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.PAUSED) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.COMPLETED) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.STARTING))
		{
			stateText.setForeground(new Color(13,123,13));
		}
		else if((stateHolder.GetStatus() == DownloadStateHolder.CANCELLED) ||
				   (stateHolder.GetStatus() == DownloadStateHolder.ERROR))
		{
			stateText.setForeground(Color.RED);
		}
		//stateText.setFont(fileLbl.getFont().deriveFont(fileLbl.getFont().getStyle() ^ Font.BOLD));
		stateText.setText(stateHolder.GetStatusString());
		stateText.setEditable(false);
		stateText.setOpaque(false);
		stateText.setMaximumSize(stateText.getPreferredSize());
		buttonsPanel.add(stateText);

		taskTablePanel = new JPanel(new BorderLayout());
		taskTablePanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        tableModel = new TorrentDetailsTableModel();
        tableModel.InitAll(ioutils);
        taskTable = new JTable(tableModel);
        taskTable.setCellSelectionEnabled(false);
        taskTable.setRowSelectionAllowed(false);
        taskTable.setColumnSelectionAllowed(false);
        
        ProgressRenderer renderer = new ProgressRenderer(0, 100, sysInfoHolder);
        renderer.setStringPainted(true); // show progress text
        renderer.setValue(0);
        taskTable.setDefaultRenderer(JProgressBar.class, renderer);
        DownloadsTableDefaultRenderer renderer1 = new DownloadsTableDefaultRenderer(sysInfoHolder);
        taskTable.setDefaultRenderer(String.class, renderer1);
        taskTable.setRowHeight((int) renderer.getPreferredSize().getHeight());
        
        taskTable.getColumnModel().getColumn(0).setPreferredWidth(sysInfoHolder.PEERTABLE_WIDTH_COL0);
        taskTable.getColumnModel().getColumn(1).setPreferredWidth(sysInfoHolder.PEERTABLE_WIDTH_COL1);
        taskTable.getColumnModel().getColumn(2).setPreferredWidth(sysInfoHolder.PEERTABLE_WIDTH_COL2);
        
        JScrollPane taskTableScrollPane = new JScrollPane(taskTable);
        taskTableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        taskTableScrollPane.setOpaque(true);
        //threadTableScrollPane.getViewport().setBackground(sysInfoHolder.GetTheme().TABLEPANEBACKGROUNDCOLOR);
        taskTablePanel.add(taskTableScrollPane, BorderLayout.CENTER);
        
		JPanel infoPane = new JPanel(new FlowLayout(FlowLayout.LEADING));
		infoText = new JTextPane();
		infoText.setEditable(false);
		infoText.setOpaque(false);
		infoText.setText(stateHolder.GetInfoText());
		infoPane.add(infoText);

		GroupLayout layout1 = new GroupLayout(generalPanel);
		generalPanel.setLayout(layout1);
		layout1.setHorizontalGroup(
				layout1.createSequentialGroup()
				.addGroup(layout1.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(detailsTextPanel)
					.addComponent(buttonsPanel)
					.addComponent(progressPanel)
					.addComponent(taskTablePanel))
				);

		layout1.setVerticalGroup(
				layout1.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout1.createSequentialGroup()
					.addComponent(detailsTextPanel)
					.addComponent(buttonsPanel)
					.addComponent(progressPanel)
					.addComponent(taskTablePanel))
		);
		
		/* Torrent details frame */
        
        JTextField heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Torrent Details");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);

        
		JLabel torrentLbl1 = new JLabel(" Torrent : ");
		//torrentLbl1.setFont(torrentLbl1.getFont().deriveFont(torrentLbl1.getFont().getStyle() ^ Font.BOLD));
		torrentLbl1.setForeground(Color.BLUE);
		torrentText = new JTextField();
		torrentText.setCaretPosition(0);
		torrentText.setOpaque(false);
		torrentText.setEditable(false);
		torrentText.setMaximumSize(torrentText.getPreferredSize());

		
		JLabel torrentSizeLbl = new JLabel(" Size : ");
		//torrentSizeLbl.setFont(torrentSizeLbl.getFont().deriveFont(torrentSizeLbl.getFont().getStyle() ^ Font.BOLD));
		torrentSizeLbl.setForeground(Color.BLUE);
		torrentSizeText = new JTextPane();
		torrentSizeText.setCaretPosition(0);
		torrentSizeText.setOpaque(false);
		torrentSizeText.setEditable(false);
		torrentSizeText.setMaximumSize(torrentSizeText.getPreferredSize());
		
		JLabel piecesLbl = new JLabel(" Pieces : ");
		//piecesLbl.setFont(piecesLbl.getFont().deriveFont(piecesLbl.getFont().getStyle() ^ Font.BOLD));
		piecesLbl.setForeground(Color.BLUE);
		piecesText = new JTextPane();
		piecesText.setCaretPosition(0);
		piecesText.setOpaque(false);
		piecesText.setEditable(false);
		piecesText.setMaximumSize(torrentSizeText.getPreferredSize());

		
		JLabel hashLbl = new JLabel(" Hash : ");
		//hashLbl.setFont(hashLbl.getFont().deriveFont(hashLbl.getFont().getStyle() ^ Font.BOLD));
		hashLbl.setForeground(Color.BLUE);
		hashText = new JTextPane();
		hashText.setCaretPosition(0);
		hashText.setOpaque(false);
		hashText.setEditable(false);
		hashText.setMaximumSize(hashText.getPreferredSize());

		
		JLabel privacyLbl = new JLabel(" Privacy : ");
		//privacyLbl.setFont(privacyLbl.getFont().deriveFont(privacyLbl.getFont().getStyle() ^ Font.BOLD));
		privacyLbl.setForeground(Color.BLUE);
		privacyText = new JTextPane();
		privacyText.setCaretPosition(0);
		privacyText.setOpaque(false);
		privacyText.setEditable(false);
		privacyText.setMaximumSize(privacyText.getPreferredSize());

		
		JLabel creatorLbl = new JLabel(" Creator : ");
		//creatorLbl.setFont(creatorLbl.getFont().deriveFont(creatorLbl.getFont().getStyle() ^ Font.BOLD));
		creatorLbl.setForeground(Color.BLUE);
		creatorText = new JTextPane();
		creatorText.setCaretPosition(0);
		creatorText.setOpaque(false);
		creatorText.setEditable(false);
		creatorText.setMaximumSize(creatorText.getPreferredSize());

		
		JLabel createdAtLbl = new JLabel(" Created At : ");
		//createdAtLbl.setFont(createdAtLbl.getFont().deriveFont(createdAtLbl.getFont().getStyle() ^ Font.BOLD));
		createdAtLbl.setForeground(Color.BLUE);
		createdAtText = new JTextPane();
		createdAtText.setCaretPosition(0);
		createdAtText.setOpaque(false);
		createdAtText.setEditable(false);
		createdAtText.setMaximumSize(createdAtText.getPreferredSize());

		
		JLabel commentLbl = new JLabel(" Comment : ");
		//commentLbl.setFont(commentLbl.getFont().deriveFont(commentLbl.getFont().getStyle() ^ Font.BOLD));
		commentLbl.setForeground(Color.BLUE);
		commentText = new JTextArea();
		commentText.setBackground(Color.GRAY);
		commentText.setCaretPosition(0);
		JScrollPane scrollingArea = new JScrollPane(commentText);
		commentText.setEditable(false);
		commentText.setLineWrap(true);

		JPanel panel2 = new JPanel();
		torrentDetailsPanel.add(panel2);
		torrentDetailsPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		GroupLayout layout2 = new GroupLayout(torrentDetailsPanel);
		layout2.setAutoCreateGaps(true);
		layout2.setAutoCreateContainerGaps(true);
		torrentDetailsPanel.setLayout(layout2);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, 25));
		//commentText.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));
		scrollingArea.setMaximumSize(new Dimension(Short.MAX_VALUE, 60));
		layout2.setHorizontalGroup(
				layout2.createParallelGroup()
						.addComponent(heading)
						.addGroup(
				layout2.createSequentialGroup()
				.addGroup(layout2.createParallelGroup(GroupLayout.Alignment.TRAILING)
					.addComponent(torrentLbl1)
					.addComponent(torrentSizeLbl)
					.addComponent(piecesLbl)
					.addComponent(hashLbl)
					.addComponent(privacyLbl)
					.addComponent(creatorLbl)
					.addComponent(createdAtLbl)
					.addComponent(commentLbl))
				.addGroup(layout2.createParallelGroup(GroupLayout.Alignment.TRAILING)
					.addComponent(torrentText, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(torrentSizeText, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(piecesText, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(hashText, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(privacyText, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(creatorText, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(createdAtText, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(scrollingArea, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addComponent(panel2, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				);

		layout2.setVerticalGroup(
				layout2.createSequentialGroup()
				.addComponent(heading)
				.addGroup(layout2.createParallelGroup(GroupLayout.Alignment.BASELINE)
					.addComponent(torrentLbl1)
					.addComponent(torrentText))
				.addGroup(layout2.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(torrentSizeLbl)
					.addComponent(torrentSizeText))
				.addGroup(layout2.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(piecesLbl)
					.addComponent(piecesText))
				.addGroup(layout2.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(hashLbl)
					.addComponent(hashText))
				.addGroup(layout2.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(privacyLbl)
					.addComponent(privacyText))
				.addGroup(layout2.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(creatorLbl)
					.addComponent(creatorText))
				.addGroup(layout2.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(createdAtLbl)
					.addComponent(createdAtText))
				.addGroup(layout2.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(commentLbl)
					.addComponent(scrollingArea))
				.addComponent(panel2, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		);
		
		panel2 = new JPanel(new BorderLayout());
		
		JLabel lbl = new JLabel();
		lbl.setText("Note: Unchecked files won't be downloaded.");
		lbl.setForeground(Color.RED);
		panel2.add(lbl);
		
		JPanel panel3 = new JPanel(new BorderLayout());
		JLabel lbl1 = new JLabel("Location:");
		lbl1.setForeground(Color.BLUE);
		lbl1.setMinimumSize(lbl1.getPreferredSize());
		panel3.add(lbl1, BorderLayout.WEST);
		JTextField dirTxt = new JTextField();
		dirTxt.setCaretPosition(0);
		dirTxt.setOpaque(false);
		dirTxt.setEditable(false);
		dirTxt.setMaximumSize(dirTxt.getPreferredSize());
		panel3.add(dirTxt, BorderLayout.CENTER);
		
		fileTableModel = new TorrentFileDetailsTableModel();
		fileTableModel.InitAll(ioutils, stateHolder);
		fileTable = new JTable(fileTableModel);
		fileTable.setCellSelectionEnabled(true);
		fileTable.setRowSelectionAllowed(false);
		fileTable.setColumnSelectionAllowed(false);
        renderer = new ProgressRenderer(0, 100, sysInfoHolder);
        renderer.setStringPainted(true); // show progress text
        renderer.setValue(0);
        fileTable.setDefaultRenderer(JProgressBar.class, renderer);
        renderer1 = new DownloadsTableDefaultRenderer(sysInfoHolder);
        fileTable.setDefaultRenderer(String.class, renderer1);
        CheckBoxRenderer renderer2 = new CheckBoxRenderer();
        fileTable.setDefaultRenderer(JCheckBox.class, renderer2);
        ComboBoxRenderer renderer3 = new ComboBoxRenderer(new String[]{"normal","high"});
        fileTable.setDefaultRenderer(JComboBox.class, renderer3);
        fileTable.setRowHeight((int) renderer.getPreferredSize().getHeight());
        fileTable.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        fileTable.getColumnModel().getColumn(4).setCellEditor(new DefaultCellEditor(new JComboBox<String>(new String[] {"normal", "high"})));
        
        fileTable.getColumnModel().getColumn(0).setPreferredWidth(sysInfoHolder.FILETABLE_WIDTH_COL0);
        fileTable.getColumnModel().getColumn(1).setPreferredWidth(sysInfoHolder.FILETABLE_WIDTH_COL1);
        fileTable.getColumnModel().getColumn(2).setPreferredWidth(sysInfoHolder.FILETABLE_WIDTH_COL2);
        fileTable.getColumnModel().getColumn(3).setPreferredWidth(sysInfoHolder.FILETABLE_WIDTH_COL3);
        fileTable.getColumnModel().getColumn(4).setPreferredWidth(sysInfoHolder.FILETABLE_WIDTH_COL4);

        taskTableScrollPane = new JScrollPane(fileTable);
        taskTableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        taskTableScrollPane.setOpaque(true);
        panel2.add(taskTableScrollPane, BorderLayout.CENTER);
        
		fileDetailsPanel.add(panel2);
		fileDetailsPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		layout2 = new GroupLayout(fileDetailsPanel);
		layout2.setAutoCreateGaps(true);
		layout2.setAutoCreateContainerGaps(true);
		fileDetailsPanel.setLayout(layout2);
		layout2.setHorizontalGroup(
				layout2.createParallelGroup()
						.addComponent(panel3)
						.addComponent(lbl)
						.addComponent(panel2));
		layout2.setVerticalGroup(
				layout2.createSequentialGroup()
				.addComponent(panel3)
				.addComponent(lbl)
				.addComponent(panel2));
		
		detailsFrame.setLocation(parentFrame.GetFrame().getX()+20, parentFrame.GetFrame().getY()+20);
		
		motherPanel.add(tabbedPane, BorderLayout.CENTER);
		motherPanel.add(infoText, BorderLayout.SOUTH);
		
		detailsFrame.add(motherPanel);
		//JOptionPane.showMessageDialog(null, "Testing");
		detailsFrame.pack();
		detailsFrame.setSize(sysInfoHolder.TORRENT_DETAILS_PREFERRED_WIDTH, sysInfoHolder.TORRENT_DETAILS_PREFERRED_HEIGHT);
		detailsFrame.setMinimumSize(new Dimension(300, 400));
		detailsFrame.setVisible(true);
		
		stateHolder.addObserver(this);
		torrentText.setText(stateHolder.GetUrlName());
		torrentText.setCaretPosition(0);
		fileText.setText(stateHolder.GetFileName());
		fileText.setCaretPosition(0);
		dirTxt.setText(stateHolder.GetDirName());
		dirTxt.setCaretPosition(0);
		downloadedText.setText(": "+Common.SizetoString(stateHolder.GetDownloadedSize()));
		uploadedText.setText(": "+Common.SizetoString(stateHolder.GetUploadedSize()));
		elapsedTimeText.setText(": "+stateHolder.GetElapsedTimeString());
		etaTimeText.setText(": "+stateHolder.getTimeRemainingString());
		downSpeedText.setText(": "+stateHolder.GetDownloadSpeedString());
		upSpeedText.setText(": "+stateHolder.GetUploadSpeedString());
		sizeText.setText(": "+Common.SizetoString(stateHolder.GetFileSize()));
		seedersText.setText(": "+stateHolder.GetNumberOfSeeders()+"/"+stateHolder.GetNumberOfLeechers());
		shareText.setText(": "+""+stateHolder.GetShareRatio());
		piecesText.setText(""+stateHolder.GetNumberOfPieces());
		hashText.setText(""+stateHolder.GetTorrentHash());
		createdAtText.setText(""+stateHolder.GetTorrentCreatedDate());
		torrentSizeText.setText(""+stateHolder.GetTorrentSize());
		creatorText.setText(""+stateHolder.GetTorrentCreator());
		commentText.setText(""+stateHolder.GetTorrentComment());
		privacyText.setText(""+stateHolder.GetTorrentPrivacy());
		
		UpdateButtons();
		InitiateTaskTable();
		InitFileTable();
		
		if(stateHolder.GetDownloadedSize() >= stateHolder.GetFileSize()) {
			wasDownloading=false;
		}
		
		detailsFrame.addWindowListener(new WindowAdapter(){
		      public void windowClosing(WindowEvent we){
		    	  UpdatePrefs();
		        ((GuiTorrentDownloaderWrapper)stateHolder.GetDownloaderObject()).HideDetails();
		}});
	}

	private void UpdatePrefs() {
		sysInfoHolder.TORRENT_DETAILS_PREFERRED_HEIGHT=detailsFrame.getHeight();
		sysInfoHolder.TORRENT_DETAILS_PREFERRED_WIDTH=detailsFrame.getWidth();
		sysInfoHolder.PEERTABLE_WIDTH_COL0=taskTable.getColumnModel().getColumn(0).getWidth();
		sysInfoHolder.PEERTABLE_WIDTH_COL1=taskTable.getColumnModel().getColumn(1).getWidth();
		sysInfoHolder.PEERTABLE_WIDTH_COL2=taskTable.getColumnModel().getColumn(2).getWidth();
		sysInfoHolder.FILETABLE_WIDTH_COL0=fileTable.getColumnModel().getColumn(0).getWidth();
		sysInfoHolder.FILETABLE_WIDTH_COL1=fileTable.getColumnModel().getColumn(1).getWidth();
		sysInfoHolder.FILETABLE_WIDTH_COL2=fileTable.getColumnModel().getColumn(2).getWidth();
		sysInfoHolder.FILETABLE_WIDTH_COL3=fileTable.getColumnModel().getColumn(3).getWidth();
		sysInfoHolder.FILETABLE_WIDTH_COL4=fileTable.getColumnModel().getColumn(4).getWidth();
	}
	
	public synchronized void update(Observable o1, Object arg1) {
		final Observable o = o1;
		final Object arg = arg1;
		java.awt.EventQueue.invokeLater(new Runnable() {
		public void run() {
			if(o.getClass().getName().equals("skdownloader.core.DownloadStateHolder")) {
				DownloadStateHolder stateHolder = (DownloadStateHolder)o;
				String updateString = (String)arg;

				if(updateString.equals("DownloadedPiecesSetChange")) {
					progressBar.SetFromBitset(stateHolder.GetTorrentDownloadedBits(), stateHolder.GetNumberOfPieces());
				}
				else if(updateString.equals("DownloadedPercent")) {
					if(stateHolder.GetProgress() >= 0) {
						if((detailsFrame != null) && (detailsFrame.isVisible()))
							detailsFrame.setTitle(stateHolder.GetProgress()+"% - "+stateHolder.GetFileName());
						progressText.setText(stateHolder.GetProgress()+"%");
					}
					fileTableModel.AddOrUpdateFileList();
				}
				else if(updateString.equals("DownloadSpeed")) {
					downSpeedText.setText(": "+stateHolder.GetDownloadSpeedString());
					elapsedTimeText.setText(": "+stateHolder.GetElapsedTimeString());
					etaTimeText.setText(": "+stateHolder.getTimeRemainingString());
				}
				else if(updateString.equals("UploadSpeed")) {
					upSpeedText.setText(": "+stateHolder.GetUploadSpeedString());
				}
				else if(updateString.equals("DownloadedSize")) {
					//doneText.setText(Common.SizetoString(stateHolder.GetDownloadedSize()));
					downloadedText.setText(": "+Common.SizetoString(stateHolder.GetDownloadedSize()));
					shareText.setText(": "+""+stateHolder.GetShareRatio());
				}
				else if(updateString.equals("UploadedSize")) {
					uploadedText.setText(": "+Common.SizetoString(stateHolder.GetUploadedSize()));
					shareText.setText(": "+""+stateHolder.GetShareRatio());
				}
				else if(updateString.equals("FileSize")) {
					if(stateHolder.GetFileSize() != -2) {
						sizeText.setText(": "+Common.SizetoString(stateHolder.GetFileSize()));
						
						//Since file size is the last value set while init, we assume all other values would be populated by now.
						piecesText.setText(""+stateHolder.GetNumberOfPieces());
						hashText.setText(""+stateHolder.GetTorrentHash());
						createdAtText.setText(""+stateHolder.GetTorrentCreatedDate());
						torrentSizeText.setText(""+stateHolder.GetTorrentSize());
						creatorText.setText(""+stateHolder.GetTorrentCreator());
						commentText.setText(""+stateHolder.GetTorrentComment());
						privacyText.setText(""+stateHolder.GetTorrentPrivacy());
					}
				}
				else if(updateString.equals("DownloadStatus")) {
					UpdateButtons();
					if((stateHolder.GetStatus() == DownloadStateHolder.SEEDING)) {
						if(wasDownloading)
							((GuiTorrentDownloaderWrapper)stateHolder.GetDownloaderObject()).HideDetails();
					}
					else if((stateHolder.GetStatus() == DownloadStateHolder.CANCELLED) ||
							(stateHolder.GetStatus() == DownloadStateHolder.ERROR)) {
						((GuiTorrentDownloaderWrapper)stateHolder.GetDownloaderObject()).HideDetails();
					}
					imgLabel.setIcon(((GuiTorrentDownloaderWrapper)(stateHolder.GetDownloaderObject())).GetNewStatusAnimImage(stateHolder.GetStatus()));
					stateText.setText(stateHolder.GetStatusString());
				}
				else if(updateString.equals("FileName")) {
					fileText.setText(stateHolder.GetFileName());
					fileText.setCaretPosition(0);
				}
				else if(updateString.equals("SeedersUpdated")) {
					seedersText.setText(": "+stateHolder.GetNumberOfSeeders()+"/"+stateHolder.GetNumberOfLeechers());
					healthText.setText(":");
					healthText.setIcon(GetHealthImage());
					if(stateHolder.GetNumberOfSeeders() <= 0) {
						healthText.setIcon(badIcon);
					}
					else if(stateHolder.GetNumberOfSeeders() < 5) {
						healthText.setIcon(mediumIcon);
					}
					else {
						healthText.setIcon(goodIcon);
					}
					AddAsPeerListener();
				}
				else if(updateString.equals("LeechersUpdated")) {
					seedersText.setText(": "+stateHolder.GetNumberOfSeeders()+"/"+stateHolder.GetNumberOfLeechers());
				}
				else if(updateString.equals("NumberPices")) {
					piecesText.setText(""+stateHolder.GetNumberOfPieces());
				}
				else if(updateString.equals("StatusTextChanged")) {
					infoText.setText(stateHolder.GetInfoText());
				}
				else if(updateString.equals("FileInfoUpdated")) {
					fileTableModel.AddOrUpdateFileList();
				}
			}
		}});
    }
	
	public void ToFront() {
		detailsFrame.setState ( Frame.NORMAL );
		detailsFrame.setVisible(true);
		detailsFrame.setAlwaysOnTop(true);
		detailsFrame.toFront();
		detailsFrame.setAlwaysOnTop(false);
	}
	
	public ImageIcon GetHealthImage() {
		if(stateHolder.GetNumberOfSeeders() <= 0) {
			return badIcon;
		}
		else if(stateHolder.GetNumberOfSeeders() < 5) {
			return mediumIcon;
		}
		else {
			return goodIcon;
		}
	}
	
	public void AddAsPeerListener() {
		if(! isPeerListener) {
			if(stateHolder.GetTorrentDownloadManager().getPeerManager() != null) {
				stateHolder.GetTorrentDownloadManager().getPeerManager().addListener(this);
				isPeerListener=true;
			}
		}
	}
	
	public void PauseDownload() {
		parentFrame.PauseDownload((GuiDownloaderWrapperInterface)stateHolder.GetDownloaderObject());
		UpdateButtons();
	}
	
	public void ResumeDownload()
	{
		parentFrame.ResumeDownload((GuiDownloaderWrapperInterface)stateHolder.GetDownloaderObject());
		UpdateButtons();
	}
	
	public void CancelDownload()
	{
		parentFrame.CancelDownload((GuiDownloaderWrapperInterface)stateHolder.GetDownloaderObject());
		UpdateButtons();
	}

	public void InitiateTaskTable() {
		this.tableModel.ClearTable();
		if( (stateHolder != null) && 
			(stateHolder.GetTorrentDownloadManager() != null) && 
			(stateHolder.GetTorrentDownloadManager().getPeerManager() != null)) {
			List<PEPeer> arr = stateHolder.GetTorrentDownloadManager().getPeerManager().getPeers();
			Iterator<PEPeer> i = arr.iterator();
			while(i.hasNext()) {
				tableModel.AddPeer((PEPeer)i.next());
			}
		}
	}
	
	public void InitFileTable() {
		this.fileTableModel.ClearTable();
		if( (stateHolder != null) && (stateHolder.GetTorrentDownloadManager() != null)) {
			fileTableModel.AddOrUpdateFileList();
		}
	}
	
	
	public void actionPerformed(ActionEvent e) 
	{
		String cmd = e.getActionCommand();
		if(cmd == "Exit") {
			((GuiTorrentDownloaderWrapper)stateHolder.GetDownloaderObject()).HideDetails();
		}
		else if(cmd.equals("PauseDownload")) {
			PauseDownload();
			tableModel.ClearTable();
		}
		else if(cmd.equals("ResumeDownload")) {
			ResumeDownload();
		}
		else if(cmd.equals("CancelDownload")) {
			if(ioutils.GetFromYesNoMessage(detailsFrame, "Really cancel download?")) {
				CancelDownload();
				tableModel.ClearTable();
			}
		}
	}


	private void UpdateButtons() 
	{
		java.awt.EventQueue.invokeLater(new Runnable() {public void run() {
			int status = stateHolder.GetStatus();
			switch (status) {
			case DownloadStateHolder.STARTING:
				pauseButt.setEnabled(true);
				resumeButt.setEnabled(false);
				cancelButt.setEnabled(true);
				break;
			case DownloadStateHolder.DOWNLOADING:
				pauseButt.setEnabled(true);
			 	resumeButt.setEnabled(false);
			 	cancelButt.setEnabled(true);
			 	break;
		 	case DownloadStateHolder.PAUSED:
		 		pauseButt.setEnabled(false);
		 		resumeButt.setEnabled(true);
		 		cancelButt.setEnabled(true);
		 		break;
		 	case DownloadStateHolder.ERROR:
		 		pauseButt.setEnabled(false);
		 		resumeButt.setEnabled(false);
		 		cancelButt.setEnabled(false);
		 		break;
		 	case DownloadStateHolder.SEEDING:
		 		pauseButt.setEnabled(true);
		 		resumeButt.setEnabled(false);
		 		cancelButt.setEnabled(false);
		 		break;
		 	case DownloadStateHolder.COMPLETED:
		 		pauseButt.setEnabled(false);
		 		resumeButt.setEnabled(true);
		 		cancelButt.setEnabled(false);
		 		break;
		 	default: // COMPLETE or CANCELLED
		 		pauseButt.setEnabled(false);
		 		resumeButt.setEnabled(false);
		 		cancelButt.setEnabled(false);
			}
		}});
	}

	
	public void DisposeFrame()
	{
		detailsFrame.removeAll();
		detailsFrame.setVisible(false);
		detailsFrame.dispose();
		detailsFrame=null;
	}
	
	public void keyPressed(KeyEvent arg0) {}
	public void keyReleased(KeyEvent arg0) {}
	public void keyTyped(KeyEvent arg0) {}

	@Override
	public void destroyed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void peerAdded(PEPeerManager arg0, PEPeer arg1) {
		tableModel.AddPeer(arg1);
		
	}

	@Override
	public void peerDiscovered(PEPeerManager arg0, PeerItem arg1, PEPeer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void peerRemoved(PEPeerManager arg0, PEPeer arg1) {
		tableModel.RemovePeer(arg1);
		
	}

	@Override
	public void peerSentBadData(PEPeerManager arg0, PEPeer arg1, int arg2) {
		tableModel.RemovePeer(arg1);
		
	}

	@Override
	public void pieceAdded(PEPeerManager arg0, PEPiece arg1, PEPeer arg2) {
		tableModel.UpdatePeerStatus(arg2);
		
	}

	@Override
	public void pieceRemoved(PEPeerManager arg0, PEPiece arg1) {
		// TODO Auto-generated method stub
	}



	/*@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}*/
}