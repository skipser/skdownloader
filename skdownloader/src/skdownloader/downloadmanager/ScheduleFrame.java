/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Frame showing options.
 *
 *   PRIVATE CLASSES
 *    Frame to accept donload schedule options.
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         07/06/09 - Creation
 *
 ****************************************************************************************/


package skdownloader.downloadmanager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.border.TitledBorder;

public class ScheduleFrame implements ActionListener
{
	MainFrame parentFrame;
	//SystemInfoHolder orig_sysInfoHolder;
	//SystemInfoHolder sysInfoHolder;
	SystemInfoHolder sysInfoHolder;
	SystemInfoHolder orig_sysInfoHolder;
	
	JFrame motherFrame;
	JPanel motherPane;
	JPanel generalPane;
	
	JButton okButt;
	JButton cancelButt;
	JRadioButton radioButt;
	private JCheckBox chkBox;
	ArrayList<JCheckBox> daysChkBoxArr;
	ArrayList<JComboBox<String>> startTimeComboArr;
	ArrayList<JComboBox<String>> stopTimeComboArr;
	
	private String startnoschedule = "startnoschedule";
	private String starttodayonly = "starttodayonly";
	private String startatdays = "startatdays";
	private String startonsaturday = "startonsaturday";
	private String startonsunday = "startonsunday";
	private String startonmonday = "startonmonday";
	private String startontuesday = "startontuesday";
	private String startonwednesday = "startonwednesday";
	private String startonthursday = "startonthursday";
	private String startonfriday = "startonfriday";
	private String stopnoschedule = "stopnoschedule";
	private String stoptodayonly = "stoptodayonly";
	private String starttimechange = "starttimechange";
	private String stoptimechange = "stoptimechange";
	private String cancel = "cancel";
	private String ok = "ok";
	
	public ScheduleFrame(MainFrame parentFrame, SystemInfoHolder sysInfoHolder)
	{
		this.parentFrame=parentFrame;
		this.orig_sysInfoHolder = sysInfoHolder;
		this.sysInfoHolder = (SystemInfoHolder)sysInfoHolder.clone();
	}
	
	public void Show()
	{
		motherFrame = new JFrame();
		motherFrame.setIconImage(new ImageIcon(this.getClass().getResource("guicomponents/images/skdownloader32.png")).getImage());
		motherPane = new JPanel(new BorderLayout());
		
		//General pane on the cardlayout
		generalPane = new JPanel();
		
		SpringLayout layout = new SpringLayout();
		generalPane.setLayout(layout);
		JTextField heading = new JTextField();
		heading.setHorizontalAlignment(JTextField.CENTER);
		heading.setFont(new Font(heading.getFont().getName(),Font.BOLD,heading.getFont().getSize()));
		heading.setText("Start/Stop Schedules");
		heading.setEditable(false);
		heading.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		heading.setBackground(Color.gray);
		generalPane.add(heading);
		
		layout.putConstraint(SpringLayout.NORTH, heading, 5, SpringLayout.NORTH, generalPane);
		layout.putConstraint(SpringLayout.WEST, heading, 5, SpringLayout.WEST, generalPane);
		layout.putConstraint(SpringLayout.EAST, generalPane, 5, SpringLayout.EAST, heading);
		JPanel startScheduleBorderPane = new JPanel();
		startScheduleBorderPane.setLayout(new BoxLayout(startScheduleBorderPane, BoxLayout.PAGE_AXIS));
		startScheduleBorderPane.setBorder(new TitledBorder("Start downloads at"));
		startScheduleBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		
		JPanel timeSelectionPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JComboBox<String> hourBox = new JComboBox<String>(new String[]{"01","02","03","04","05","06","07","08","09","10","11","12"});
		JComboBox<String> minBox = new JComboBox<String>(new String[]{"00","05","10","15","20","25","30","35","40","45","50","55"});
		JComboBox<String> ampmBox = new JComboBox<String>(new String[]{"AM", "PM"});
		
		hourBox.setSelectedItem(sysInfoHolder.SCHEDULESTARTTIME.substring(0, 2));
		hourBox.setActionCommand(starttimechange);
		hourBox.addActionListener(this);
		minBox.setSelectedItem(sysInfoHolder.SCHEDULESTARTTIME.substring(2, 4));
		minBox.setActionCommand(starttimechange);
		minBox.addActionListener(this);
		ampmBox.setSelectedItem(sysInfoHolder.SCHEDULESTARTTIME.substring(4, 6));
		ampmBox.setActionCommand(starttimechange);
		ampmBox.addActionListener(this);
		
		startTimeComboArr = new ArrayList<JComboBox<String>>();
		startTimeComboArr.add(hourBox);
		timeSelectionPanel.add(hourBox);
		startTimeComboArr.add(minBox);
		timeSelectionPanel.add(minBox);
		startTimeComboArr.add(ampmBox);
		timeSelectionPanel.add(ampmBox);

		if(sysInfoHolder.SCHEDULESTARTOPTION == SystemInfoHolder.SCHEDULESTARTDISABLED)
		{
			hourBox.setEnabled(false);
			minBox.setEnabled(false);
			ampmBox.setEnabled(false);
		}
		
		timeSelectionPanel.add(new JSeparator(JSeparator.HORIZONTAL));
		
		startScheduleBorderPane.add(timeSelectionPanel);
		startScheduleBorderPane.add(new JSeparator(JSeparator.HORIZONTAL));
		//timeSelectionPanel.setBorder(BorderFactory.createLineBorder(Color.red));
		timeSelectionPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		//startScheduleBorderPane.setBorder(BorderFactory.createLineBorder(Color.red));
		
		ButtonGroup group = new ButtonGroup();
		ArrayList<JRadioButton> buttArr = new ArrayList<JRadioButton>();
		radioButt = new JRadioButton("Do not schedule downloads", true);
		radioButt.setActionCommand(startnoschedule);
		radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    radioButt.setAlignmentX(Component.LEFT_ALIGNMENT);
	    startScheduleBorderPane.add(radioButt);
	    
	    radioButt = new JRadioButton("Today only", true);
		radioButt.setActionCommand(starttodayonly);
		radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    startScheduleBorderPane.add(radioButt);
	    
	    radioButt = new JRadioButton("Download at above time on these days ", true);
		radioButt.setActionCommand(startatdays);
		radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    startScheduleBorderPane.add(radioButt);
	    group.setSelected(buttArr.get(sysInfoHolder.SCHEDULESTARTOPTION).getModel(), true);
	    
	    JPanel daysPanel = new JPanel(new GridLayout(2,4));
	    daysPanel.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
	    daysPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
	    
	    daysChkBoxArr = new ArrayList<JCheckBox>();
	    chkBox = new JCheckBox("Saturday");
	    daysChkBoxArr.add(chkBox);
	    chkBox.setActionCommand(startonsaturday);
	    chkBox.addActionListener(this);
	    chkBox.setSelected(sysInfoHolder.SCHEDULESTARTONSATURDAY);
	    chkBox.setAlignmentX(Component.LEFT_ALIGNMENT);
	    if(sysInfoHolder.SCHEDULESTARTOPTION != SystemInfoHolder.SCHEDULESTARTFORDAYS)
	    {
	    	chkBox.setEnabled(false);
	    }
	    daysPanel.add(chkBox);	   
	    
	    chkBox = new JCheckBox("Sunday");
	    daysChkBoxArr.add(chkBox);
	    chkBox.setActionCommand(startonsunday);
	    chkBox.addActionListener(this);
	    chkBox.setSelected(sysInfoHolder.SCHEDULESTARTONSUNDAY);
	    chkBox.setAlignmentX(Component.LEFT_ALIGNMENT);
	    if(sysInfoHolder.SCHEDULESTARTOPTION != SystemInfoHolder.SCHEDULESTARTFORDAYS)
	    {
	    	chkBox.setEnabled(false);
	    }
	    daysPanel.add(chkBox);
	    	    
	    //startScheduleBorderPane.add(daysPanel1);
	    
	    //JPanel daysPanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    //daysPanel2.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
	    //daysPanel2.setAlignmentX(Component.LEFT_ALIGNMENT);
	    chkBox = new JCheckBox("Monday");
	    daysChkBoxArr.add(chkBox);
	    chkBox.setActionCommand(startonmonday);
	    chkBox.addActionListener(this);
	    chkBox.setSelected(sysInfoHolder.SCHEDULESTARTONMONDAY);
	    if(sysInfoHolder.SCHEDULESTARTOPTION != SystemInfoHolder.SCHEDULESTARTFORDAYS)
	    {
	    	chkBox.setEnabled(false);
	    }
	    daysPanel.add(chkBox);
	    
	    chkBox = new JCheckBox("Tuesday");
	    daysChkBoxArr.add(chkBox);
	    chkBox.setActionCommand(startontuesday);
	    chkBox.addActionListener(this);
	    chkBox.setSelected(sysInfoHolder.SCHEDULESTARTONTUESDAY);
	    if(sysInfoHolder.SCHEDULESTARTOPTION != SystemInfoHolder.SCHEDULESTARTFORDAYS)
	    {
	    	chkBox.setEnabled(false);
	    }
	    daysPanel.add(chkBox);
	    
	    chkBox = new JCheckBox("Wednesday");
	    daysChkBoxArr.add(chkBox);
	    chkBox.setActionCommand(startonwednesday);
	    chkBox.addActionListener(this);
	    chkBox.setSelected(sysInfoHolder.SCHEDULESTARTONWEDNESDAY);
	    if(sysInfoHolder.SCHEDULESTARTOPTION != SystemInfoHolder.SCHEDULESTARTFORDAYS)
	    {
	    	chkBox.setEnabled(false);
	    }
	    daysPanel.add(chkBox);
	    
	    chkBox = new JCheckBox("Thursday");
	    daysChkBoxArr.add(chkBox);
	    chkBox.setActionCommand(startonthursday);
	    chkBox.addActionListener(this);
	    chkBox.setSelected(sysInfoHolder.SCHEDULESTARTONTHURSDAY);
	    if(sysInfoHolder.SCHEDULESTARTOPTION != SystemInfoHolder.SCHEDULESTARTFORDAYS)
	    {
	    	chkBox.setEnabled(false);
	    }
	    daysPanel.add(chkBox);
	    
	    chkBox = new JCheckBox("Friday");
	    daysChkBoxArr.add(chkBox);
	    chkBox.setActionCommand(startonfriday);
	    chkBox.addActionListener(this);
	    chkBox.setSelected(sysInfoHolder.SCHEDULESTARTONFRIDAY);
	    if(sysInfoHolder.SCHEDULESTARTOPTION != SystemInfoHolder.SCHEDULESTARTFORDAYS)
	    {
	    	chkBox.setEnabled(false);
	    }
	    daysPanel.add(chkBox);
	    daysChkBoxArr.add(chkBox);
	    daysPanel.setBorder(BorderFactory.createEtchedBorder());
	    startScheduleBorderPane.add(daysPanel);
	    //startScheduleBorderPane.add(daysPanel);
	    
	    JLabel lbl = new JLabel("NOTE: Any paused downloads will start as scheduled above.");
	    lbl.setFont(new Font(lbl.getFont().getName(),Font.ITALIC,lbl.getFont().getSize()-2));
	    startScheduleBorderPane.add(lbl);
	    
	    generalPane.add(startScheduleBorderPane);
	    layout.putConstraint(SpringLayout.NORTH, startScheduleBorderPane, 10, SpringLayout.SOUTH, heading);
		layout.putConstraint(SpringLayout.WEST, startScheduleBorderPane, 5, SpringLayout.WEST, generalPane);
		layout.putConstraint(SpringLayout.EAST, startScheduleBorderPane, 0, SpringLayout.EAST, heading);
		
		JPanel stopScheduleBorderPane = new JPanel();
		stopScheduleBorderPane.setLayout(new BoxLayout(stopScheduleBorderPane, BoxLayout.PAGE_AXIS));
		stopScheduleBorderPane.setBorder(new TitledBorder("Pause/Stop downloads at"));
		stopScheduleBorderPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		
		timeSelectionPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		hourBox = new JComboBox<String>(new String[]{"01","02","03","04","05","06","07","08","09","10","11","12"});
		minBox = new JComboBox<String>(new String[]{"00","05","10","15","20","25","30","35","40","45","50","55"});
		ampmBox = new JComboBox<String>(new String[]{"AM", "PM"});
		
		hourBox.setSelectedItem(sysInfoHolder.SCHEDULESTOPTIME.substring(0, 2));
		hourBox.setActionCommand(stoptimechange);
		hourBox.addActionListener(this);
		minBox.setSelectedItem(sysInfoHolder.SCHEDULESTOPTIME.substring(2, 4));
		minBox.setActionCommand(stoptimechange);
		minBox.addActionListener(this);
		ampmBox.setSelectedItem(sysInfoHolder.SCHEDULESTOPTIME.substring(4, 6));
		ampmBox.setActionCommand(stoptimechange);
		ampmBox.addActionListener(this);
		
		stopTimeComboArr = new ArrayList<JComboBox<String>>();
		stopTimeComboArr.add(hourBox);
		timeSelectionPanel.add(hourBox);
		stopTimeComboArr.add(minBox);
		timeSelectionPanel.add(minBox);
		stopTimeComboArr.add(ampmBox);
		timeSelectionPanel.add(ampmBox);
		timeSelectionPanel.add(new JSeparator(JSeparator.HORIZONTAL));
		
		if(sysInfoHolder.SCHEDULESTOPOPTION == SystemInfoHolder.SCHEDULESTOPDISABLED)
		{
			hourBox.setEnabled(false);
			minBox.setEnabled(false);
			ampmBox.setEnabled(false);
		}
		
		stopScheduleBorderPane.add(timeSelectionPanel);
		
		timeSelectionPanel.add(new JSeparator(JSeparator.HORIZONTAL));
		
		stopScheduleBorderPane.add(timeSelectionPanel);
		stopScheduleBorderPane.add(new JSeparator(JSeparator.HORIZONTAL));
		//timeSelectionPanel.setBorder(BorderFactory.createLineBorder(Color.red));
		timeSelectionPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		//startScheduleBorderPane.setBorder(BorderFactory.createLineBorder(Color.red));
		
		group = new ButtonGroup();
		buttArr = new ArrayList<JRadioButton>();
		radioButt = new JRadioButton("Never pause.", true);
		radioButt.setActionCommand(stopnoschedule);
		radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    radioButt.setAlignmentX(Component.LEFT_ALIGNMENT);
	    stopScheduleBorderPane.add(radioButt);
	    
	    radioButt = new JRadioButton("Pause at time above.", true);
		radioButt.setActionCommand(stoptodayonly);
		radioButt.addActionListener(this);
	    group.add(radioButt);
	    buttArr.add(radioButt);
	    stopScheduleBorderPane.add(radioButt);
	    group.setSelected(buttArr.get(sysInfoHolder.SCHEDULESTOPOPTION).getModel(), true);
	
		generalPane.add(stopScheduleBorderPane);
	    layout.putConstraint(SpringLayout.NORTH, stopScheduleBorderPane, 20, SpringLayout.SOUTH, startScheduleBorderPane);
		layout.putConstraint(SpringLayout.WEST, stopScheduleBorderPane, 5, SpringLayout.WEST, generalPane);
		layout.putConstraint(SpringLayout.EAST, stopScheduleBorderPane, 0, SpringLayout.EAST, startScheduleBorderPane);
		
	    okButt = new JButton("Ok");
	    okButt.setActionCommand(ok);
	    okButt.addActionListener(this);
	    cancelButt = new JButton("Cancel");
	    cancelButt.setActionCommand(cancel);
	    cancelButt.addActionListener(this);
	    JPanel buttPane  = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    buttPane.add(okButt);
	    buttPane.add(cancelButt);
	    generalPane.add(buttPane);
	    layout.putConstraint(SpringLayout.NORTH, buttPane, 10, SpringLayout.SOUTH, stopScheduleBorderPane);
		layout.putConstraint(SpringLayout.WEST, buttPane, 5, SpringLayout.WEST, generalPane);
		layout.putConstraint(SpringLayout.EAST, buttPane, 0, SpringLayout.EAST, stopScheduleBorderPane);

	    motherPane.add(generalPane);
	    motherFrame.add(motherPane);

	    motherFrame.pack();
	    motherFrame.setSize(450,450);
	    motherFrame.setLocation(parentFrame.GetFrame().getX()+40, parentFrame.GetFrame().getY()+60);
	    motherFrame.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) 
	{
		if(e.getActionCommand().equals(startnoschedule))
		{
			sysInfoHolder.SCHEDULESTARTOPTION = SystemInfoHolder.SCHEDULESTARTDISABLED;
			//Disable time combo boxes
			for(int i=0; i<startTimeComboArr.size(); i++)
			{
				startTimeComboArr.get(i).setEnabled(false);
			}

			//Disable days checkboxes
			for(int i=0; i<daysChkBoxArr.size(); i++)
			{
				daysChkBoxArr.get(i).setEnabled(false);
			}
		}
		else if(e.getActionCommand().equals(starttodayonly))
		{
			sysInfoHolder.SCHEDULESTARTOPTION = SystemInfoHolder.SCHEDULESTARTATTIME;
			//Enable time combo boxes
			for(int i=0; i<stopTimeComboArr.size(); i++)
			{
				startTimeComboArr.get(i).setEnabled(true);
			}

			//Disable days checkboxes
			for(int i=0; i<daysChkBoxArr.size(); i++)
			{
				daysChkBoxArr.get(i).setEnabled(false);
			}
		}
		else if(e.getActionCommand().equals(startatdays))
		{
			sysInfoHolder.SCHEDULESTARTOPTION = SystemInfoHolder.SCHEDULESTARTFORDAYS;
			//Enable time combo boxes
			for(int i=0; i<startTimeComboArr.size(); i++)
			{
				startTimeComboArr.get(i).setEnabled(true);
			}

			//Disable days checkboxes
			for(int i=0; i<daysChkBoxArr.size(); i++)
			{
				daysChkBoxArr.get(i).setEnabled(true);
			}
		}
		else if(e.getActionCommand().equals(startonfriday))
		{
			sysInfoHolder.SCHEDULESTARTONFRIDAY=((JCheckBox)e.getSource()).isSelected();
		}
		else if(e.getActionCommand().equals(startonsaturday))
		{
			sysInfoHolder.SCHEDULESTARTONSATURDAY=((JCheckBox)e.getSource()).isSelected();
		}
		else if(e.getActionCommand().equals(startonsunday))
		{
			sysInfoHolder.SCHEDULESTARTONSUNDAY=((JCheckBox)e.getSource()).isSelected();
		}
		else if(e.getActionCommand().equals(startonmonday))
		{
			sysInfoHolder.SCHEDULESTARTONMONDAY=((JCheckBox)e.getSource()).isSelected();
		}
		else if(e.getActionCommand().equals(startontuesday))
		{
			sysInfoHolder.SCHEDULESTARTONTUESDAY=((JCheckBox)e.getSource()).isSelected();
		}
		else if(e.getActionCommand().equals(startonwednesday))
		{
			sysInfoHolder.SCHEDULESTARTONWEDNESDAY=((JCheckBox)e.getSource()).isSelected();
		}
		else if(e.getActionCommand().equals(startonthursday))
		{
			sysInfoHolder.SCHEDULESTARTONTHURSDAY=((JCheckBox)e.getSource()).isSelected();
		}
		else if(e.getActionCommand().equals(this.stopnoschedule))
		{
			sysInfoHolder.SCHEDULESTOPOPTION = SystemInfoHolder.SCHEDULESTOPDISABLED;
			//Enable time combo boxes
			for(int i=0; i<startTimeComboArr.size(); i++)
			{
				stopTimeComboArr.get(i).setEnabled(false);
			}
		}
		else if(e.getActionCommand().equals(this.stoptodayonly))
		{
			sysInfoHolder.SCHEDULESTOPOPTION = SystemInfoHolder.SCHEDULESTOPATTIME;
			//Enable time combo boxes
			for(int i=0; i<startTimeComboArr.size(); i++)
			{
				stopTimeComboArr.get(i).setEnabled(true);
			}
		}
		else if (e.getActionCommand().equals(starttimechange))
		{
				sysInfoHolder.SCHEDULESTARTTIME = (String)startTimeComboArr.get(0).getSelectedItem()+
				                                         (String)startTimeComboArr.get(1).getSelectedItem()+
				                                         (String)startTimeComboArr.get(2).getSelectedItem();
		}
		else if (e.getActionCommand().equals(stoptimechange))
		{
				sysInfoHolder.SCHEDULESTOPTIME = (String)stopTimeComboArr.get(0).getSelectedItem()+
				                                        (String)stopTimeComboArr.get(1).getSelectedItem()+
				                                        (String)stopTimeComboArr.get(2).getSelectedItem();
		}
		else if(e.getActionCommand().equals(ok))
		{
			orig_sysInfoHolder.CopyFrom(sysInfoHolder);
			parentFrame.SetScheduleButtIcon();
			Close();
		}
		else if(e.getActionCommand().equals(cancel))
		{
			Close();
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : Close
	 * Arguments : void
	 * Return    : void
	 * Notes     : Closes this frame.
	 * 
	 *************************************************************/
	public void Close()
	{
		motherFrame.dispose();
	}
}
