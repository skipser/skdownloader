/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Gui implementaion for logger.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/18/09 - Add support to set debug mode from outside.
 *
 ****************************************************************************************/

package skdownloader.downloadmanager;

import skdownloader.core.Common;
import skdownloader.core.UserIOUtils;

import java.awt.Component;
import java.awt.Font;
import java.io.File;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.apache.log4j.Logger;

public class GuiUserIOUtils implements UserIOUtils {
	
	private static Logger logger = Logger.getLogger(GuiUserIOUtils.class);
	
	/*************************************************************
	 * 
	 * Function  : PrintError
	 * Arguments : 1. Error message to print
	 * Return    : nil
	 * Notes     : Function to print error messages
	 * 
	 *************************************************************/
	public void PrintError(String str)	{
		String[] arr = str.split(Common.LINESEPERATOR);
		str="";
		for(int i=0; i<arr.length; i++) {
			while(arr[i].length() > 80) {
				str+=arr[i].substring(0, 79)+Common.LINESEPERATOR;
				arr[i]=arr[i].substring(79, arr[i].length());
			}
			str+=arr[i]+Common.LINESEPERATOR;
		}
		final String str1=str;
        try {
        	if (SwingUtilities.isEventDispatchThread())	{
        		JOptionPane.showMessageDialog(null, "ERROR: "+str1, "Error", JOptionPane.ERROR_MESSAGE);
        	}
        	else {
        		SwingUtilities.invokeAndWait(new Runnable() {
        			public void run() {
        				JOptionPane.showMessageDialog(null, "ERROR: "+str1, "Error", JOptionPane.ERROR_MESSAGE);
        			}
        		});
        	}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception", e);
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : PrintMessage
	 * Arguments : 1. Message to print
	 * Return    : nil
	 * Notes     : Function to print error messages.
	 * 
	 *************************************************************/
	public void PrintMessage(String str) {
		final String str1=str;
        try {
        	if (SwingUtilities.isEventDispatchThread( )) {
        		JOptionPane.showMessageDialog(null, str1, "Attention", JOptionPane.INFORMATION_MESSAGE);
        	}
        	else {
        		SwingUtilities.invokeAndWait(new Runnable() { public void run() {
        			JOptionPane.showMessageDialog(null, str1, "Attention", JOptionPane.INFORMATION_MESSAGE);
        		}});
        	}
		} catch (Exception e) {
			logger.error("Exception", e);
		}
	}

	
	/*************************************************************
	 * 
	 * Function  : GetFromYesNoMessage
	 * Arguments : 1. Object denoting parent frame for GUI(null for command line)
	 *             2. Message
	 * Return    : true/false depending on yes/no
	 * Notes     : Function to get choice from user.
	 * 
	 *************************************************************/
	public boolean GetFromYesNoMessage(Object c, String str) {
		final boolean[] answer = new boolean[1];
		final Component c1 = (Component)c;
		final String str1=str;
		try  {
			if (SwingUtilities.isEventDispatchThread()) {
				if (JOptionPane.showConfirmDialog((Component)c, str1, "Attenion!!!", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					answer[0]=true;
				}
				else {
					answer[0]=false;
				}
			}
			else {
				SwingUtilities.invokeAndWait(new Runnable()	{ public void run() {
					if (JOptionPane.showConfirmDialog((Component)c1, str1, "Attenion!!!", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
						answer[0]=true;
					}
					else{
						answer[0]=false;
					}
				}});
			}
		} catch (Exception e) {
			answer[0] = false;
			logger.error("Exception", e);
		}
        return answer[0];
	}
	
	/*************************************************************
	 * 
	 * Function  : GetFromYesNoCancelMessage
	 * Arguments : 1. Object denoting parent frame for GUI(null for command line)
	 *             2. Message
	 * Return    : true/false depending on yes/no
	 * Notes     : Function to get choice from user.
	 * 
	 *************************************************************/
	public int GetFromYesNoCancelMessage(Object c, String str) {
		final int[] answer = new int[1];
		final Component c1 = (Component)c;
		final String str1=str;
		final Object[] options = {"Yes",
							"No",
                			"Cancel"};

		try  {
			if (SwingUtilities.isEventDispatchThread()) {
				int ret= JOptionPane.showOptionDialog((Component)c, (Object)str1, "Attenion!!!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
				if(ret == JOptionPane.YES_OPTION)
					answer[0]=Common.YES;
				else if (ret == JOptionPane.NO_OPTION)
					answer[0]=Common.NO;
				else
					answer[0]=Common.CANCEL;
			}
			else {
				SwingUtilities.invokeAndWait(new Runnable()	{ public void run() {
					int ret= JOptionPane.showOptionDialog((Component)c1, (Object)str1, "Attenion!!!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
					if(ret == JOptionPane.YES_OPTION)
						answer[0]=Common.YES;
					else if (ret == JOptionPane.NO_OPTION)
						answer[0]=Common.NO;
					else
						answer[0]=Common.CANCEL;
				}});
			}
		} catch (Exception e) {
			answer[0] = Common.CANCEL;
		}
        return answer[0];
	}

	
	/*************************************************************
	 * 
	 * Function  : GetFromOkCancelMessage
	 * Arguments : 1. Object denoting parent frame for GUI(null for command line)
	 *             2. Message
	 * Return    : true/false depending on Ok/cancel
	 * Notes     : Function to get choice from user.
	 * 
	 *************************************************************/
	public boolean GetFromOkCancelMessage(Object c, String str) {
		final boolean[] answer = new boolean[1];
		final Component c1 = (Component)c;
		final String str1=str;
		try {
			if (SwingUtilities.isEventDispatchThread())	{
				if (JOptionPane.showConfirmDialog((Component)c, str1, "Attenion!!!", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
					answer[0]=true;
				}
				else {
					answer[0]=false;
				}
			}
			else {
				SwingUtilities.invokeAndWait(new Runnable()	{ 
					public void run() {
						if (JOptionPane.showConfirmDialog((Component)c1, str1, "Attenion!!!", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
						answer[0]=true;
						}
						else {
							answer[0]=false;
						}
					}
				});
			}
		} catch (Exception e) {
			answer[0] = false;
			logger.error("Exception", e);
		}
		
        return answer[0];
	}
	
	/*************************************************************
	 * 
	 * Function  : GetFromCustomButtonsMessage
	 * Arguments : 1. Object denoting parent frame for GUI(null for command line)
	 *             2. Message
	 *             3. Two strings of buttons
	 * Return    : true/false depending on first/second button.
	 * Notes     : Function to get choice from user.
	 * 
	 *************************************************************/
	public boolean GetFromCustomButtonsMessage(Object c, final String str, String butt1, String butt2) {
		final boolean[] answer = new boolean[1];
		final Component c1 = (Component)c;
		final Object[] options = { butt1, butt2 };
		
	    try {
			if (SwingUtilities.isEventDispatchThread())	{
				if (showForGetFromCustomButtonsMessage(c1, options, str)==0) {
					answer[0]=true;
				} else {
					answer[0]=false;
				}
			} else {
				SwingUtilities.invokeAndWait(new Runnable()	{ 
					public void run() {
						if (showForGetFromCustomButtonsMessage(c1, options, str)==0) {
							answer[0]=true;
						} else {
							answer[0]=false;
						}
					}
				});
			}
	    } catch (Exception e) {
			answer[0] = false;
			logger.error("Exception", e);
		}
		
        return answer[0];
	}
	public int showForGetFromCustomButtonsMessage(Component c, Object[] options, String str) {
		JLabel label = new JLabel();
	    Font font = label.getFont();
	    StringBuffer style = new StringBuffer("font-family:" + font.getFamily() + ";");
	    style.append("font-weight:" + (font.isBold() ? "bold" : "normal") + ";");
	    style.append("font-size:" + font.getSize() + "pt;");
	    style.append("padding:15px 10px 15px 10px;");
	    
		final JEditorPane ep = new JEditorPane("text/html", "<html><body style=\"" + style + "\">" //
		            + str //
		            + "</body></html>");

		    // handle link events
		    ep.addHyperlinkListener(new HyperlinkListener() {
		        @Override
		        public void hyperlinkUpdate(HyperlinkEvent e) {
		            if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED))
		            	GUICommon.LaunchInBrowser(e.getURL().toString());
		        }
		    });
		    ep.setEditable(false);
		    ep.setBackground(label.getBackground());
		    return JOptionPane.showOptionDialog((Component)c, ep, "Attenion!!!", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
	}
	
	/*************************************************************
	 * 
	 * Function  : GetFileFromUser
	 * Arguments : Object denoting parent frame.
	 * Return    : User typed text. If user gives null, returns 
	 *             empty string
	 * Notes     : Function to get text from user.
	 * 
	 *************************************************************/
	public String GetFileFromUser(Object c) {
		final String[] answer = new String[1];
		final Component c1 = (Component)c;
		try {
			SwingUtilities.invokeAndWait(new Runnable() {
			  public void run() {
				  final JFileChooser fc = new JFileChooser();
				  //String fileName = "";
				  if (fc.showOpenDialog((Component)c1) == JFileChooser.APPROVE_OPTION) {
					  File file = fc.getSelectedFile();
					  answer[0]=file.getAbsolutePath();
				  } else {
					  answer[0]="";
				  }
			  }
			});
		} catch (Exception e) {
			answer[0]="";
			logger.error("Exception", e);
		}		
        return answer[0];
	}
}