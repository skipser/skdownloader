package skdownloader.downloadmanager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

public class InputListenerThread extends Thread {
	private int listenPort = 6235;
	private MainFrame mainFrame;
	
	Socket soc = null;
    PrintWriter out = null;
    BufferedReader in = null;
    SystemInfoHolder sysInfoHolder;
    private static Logger logger = Logger.getLogger(InputListenerThread.class);
	
	public InputListenerThread(MainFrame m, SystemInfoHolder sysInfoHolder, int listenPort) {
		this.mainFrame = m;
		this.sysInfoHolder=sysInfoHolder;
		this.listenPort=listenPort;
	}
	
	public void run() {
		Listen();
	}
	
	public void Listen() {
		try {
			logger.debug("Starting to listen on "+listenPort);
			@SuppressWarnings("resource")
			ServerSocket srvr = new ServerSocket(listenPort);
	        Socket skt;
			
			String line;
			while(true) {
				skt = srvr.accept();
				in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
				line = in.readLine();
				logger.debug("Got input "+line);
				Pattern p = Pattern.compile("skdownloader_addnew-(.*)");
				Matcher m = p.matcher(line);
				if(line.matches("skdownloader_alive_comeforward")) {
					BufferedWriter out=new BufferedWriter(new OutputStreamWriter(skt.getOutputStream()));
					if(sysInfoHolder.isShuttingDown)
						out.write("shutting_down");
					else if(sysInfoHolder.isStarting)
						out.write("starting");
					else {
						out.write("is_alive");
						mainFrame.ToFront();
					}
					out.close();
				}
				else if(line.matches("skdownloader_alive")) {
					BufferedWriter out=new BufferedWriter(new OutputStreamWriter(skt.getOutputStream()));
					if(sysInfoHolder.isShuttingDown)
						out.write("shutting_down");
					else if(sysInfoHolder.isStarting)
						out.write("starting");
					else {
						out.write("is_alive");
					}
					out.close();
				}
				else if(m.matches()) {
					final String url = m.group(1);
					if(sysInfoHolder.isShuttingDown) {
						JOptionPane.showMessageDialog(null,"SKDownloader is being shut down."+System.getProperty("line.separator")+"Please try adding downloads once it stops fully.");
					}
					else {
						SwingUtilities.invokeLater(new Runnable() {	public void run() {
							mainFrame.CheckAndAddDownloadFromBrowser(url);
						}});
					}
				}

				in.close();
				skt.close();
			}

		} catch (Exception e){
			logger.error("Exception", e);
		}
	}
}