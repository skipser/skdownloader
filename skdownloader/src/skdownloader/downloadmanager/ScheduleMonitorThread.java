/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Frame showing options.
 *
 *   PRIVATE CLASSES
 *    Frame to accept donload schedule options.
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         07/05/09 - Creation
 *   arun         07/06/06 - Once starttodayonly changes the download option, refresh button.
 *
 ****************************************************************************************/

package skdownloader.downloadmanager;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import skdownloader.core.Common;

public class ScheduleMonitorThread extends Thread
{
	SystemInfoHolder sysInfoHolder;
	MainFrame mainFrame;
	private static Logger logger = Logger.getLogger(ScheduleMonitorThread.class);
	
	public ScheduleMonitorThread(MainFrame mainFrame, SystemInfoHolder sysInfoHolder)
	{
		this.sysInfoHolder = sysInfoHolder;
		this.mainFrame = mainFrame;
	}
	
	public void run()
	{
		Start();
	}
	
	public void Start()
	{
		while(true)
		{
			//1. skip pause check if no schedules present
			if(sysInfoHolder.SCHEDULESTARTOPTION == SystemInfoHolder.SCHEDULESTARTDISABLED)
			{
				//	Nothing to do
			}
			else if (sysInfoHolder.SCHEDULESTARTOPTION == SystemInfoHolder.SCHEDULESTARTATTIME)
			{
				int hour=Integer.parseInt(sysInfoHolder.SCHEDULESTARTTIME.substring(0, 2));
				int min=Integer.parseInt(sysInfoHolder.SCHEDULESTARTTIME.substring(2, 4));
				String ampm=sysInfoHolder.SCHEDULESTARTTIME.substring(4, 6);
				if(ampm.equals("PM"))
				{
					hour += 12;
				}
			
				GregorianCalendar cal = new GregorianCalendar();
				
				if(hour == cal.get(Calendar.HOUR_OF_DAY))
				{
					if(min == cal.get(Calendar.MINUTE))
					{
						//Start all paused downloads
						mainFrame.ResumeAllDownloads();
						sysInfoHolder.SCHEDULESTARTOPTION=SystemInfoHolder.SCHEDULESTARTDISABLED;
						mainFrame.SetScheduleButtIcon();
					}
				}
			}
			else if (sysInfoHolder.SCHEDULESTARTOPTION == SystemInfoHolder.SCHEDULESTARTFORDAYS)
			{
				GregorianCalendar cal = new GregorianCalendar();
				
				int day = cal.get(Calendar.DAY_OF_WEEK); 
				int hour=Integer.parseInt(sysInfoHolder.SCHEDULESTARTTIME.substring(0, 2));
				int min=Integer.parseInt(sysInfoHolder.SCHEDULESTARTTIME.substring(2, 4));
				String ampm=sysInfoHolder.SCHEDULESTARTTIME.substring(2, 4);
				if(ampm.equals("PM"))
				{
					hour += 12;
				}
				
				boolean gotDay=false;
				if((sysInfoHolder.SCHEDULESTARTONSUNDAY && (day == GregorianCalendar.SUNDAY)) ||
					(sysInfoHolder.SCHEDULESTARTONMONDAY && (day == GregorianCalendar.MONDAY)) ||
					(sysInfoHolder.SCHEDULESTARTONTUESDAY && (day == GregorianCalendar.TUESDAY)) ||
					(sysInfoHolder.SCHEDULESTARTONWEDNESDAY && (day == GregorianCalendar.WEDNESDAY)) ||
					(sysInfoHolder.SCHEDULESTARTONTHURSDAY && (day == GregorianCalendar.THURSDAY)) ||
					(sysInfoHolder.SCHEDULESTARTONFRIDAY && (day == GregorianCalendar.FRIDAY)) ||
					(sysInfoHolder.SCHEDULESTARTONSATURDAY && (day == GregorianCalendar.SATURDAY)))
				{
					gotDay = true;
				}
	
				if(gotDay) {
					if(hour == cal.get(Calendar.HOUR_OF_DAY)) {
						if(min == cal.get(Calendar.MINUTE)) {
							//Start all paused downloads
							mainFrame.ResumeAllDownloads();
							//Sleep for 2 mins. This makes sure we don't have duplicate checks.
							Common.Sleep(120000, logger);
						}
					}
				}
			}
			
			if(sysInfoHolder.SCHEDULESTOPOPTION == SystemInfoHolder.SCHEDULESTOPDISABLED) {
				//Nothing to do
			}
			else if (sysInfoHolder.SCHEDULESTOPOPTION == SystemInfoHolder.SCHEDULESTOPATTIME) {
				int hour=Integer.parseInt(sysInfoHolder.SCHEDULESTOPTIME.substring(0, 2));
				int min=Integer.parseInt(sysInfoHolder.SCHEDULESTOPTIME.substring(2, 4));
				String ampm=sysInfoHolder.SCHEDULESTOPTIME.substring(4, 6);
				if(ampm.equals("PM")) {
					hour += 12;
				}
				
				GregorianCalendar cal = new GregorianCalendar();
				
				if(hour == cal.get(Calendar.HOUR_OF_DAY)) {
					if(min == cal.get(Calendar.MINUTE)) {
						//Start all paused downloads
						mainFrame.PauseAllDownloads();
						
						//Sleep for 1 mins. Otherwise, this can cause automaic pause when
						// user tries to resume manually.
						Common.Sleep(62000, logger);
					}
				}
			}
			
			if(sysInfoHolder.SHUTDOWNOPTION == SystemInfoHolder.SHUTDOWNPCWHENDOWNLOADSFINISH) {
				if(! mainFrame.HasDownloadingInstances()) {
					mainFrame.CleanForExit(true);
					GUICommon.ShutDownPC(sysInfoHolder.ROOTPASSWORD);
				}
			}
			else if(sysInfoHolder.SHUTDOWNOPTION == SystemInfoHolder.SHUTDOWNSKWHENDOWNLOADSFINISH) {
				if(! mainFrame.HasDownloadingInstances()) {
					mainFrame.CleanAndExit(true);
				}
			}
			
			Common.Sleep(4000, logger);
		}
	}
}
