/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Download thread details displayer.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/21/09 - Change colors according to theme
 *   arun         05/21/09 - Fix progress bar update for unknown file size
 *   arun         05/21/09 - Show ads at bottom of details pane.
 *
 ****************************************************************************************/

package skdownloader.downloadmanager;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import skdownloader.core.*;
import skdownloader.downloadmanager.guicomponents.DownloadsTableDefaultRenderer;
import skdownloader.downloadmanager.guicomponents.ProgressRenderer;
import skdownloader.downloadmanager.guicomponents.UrlDetailsTableModel;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.BorderFactory; 
import javax.swing.text.*;

import org.apache.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.util.*;

public class UrlDetailsFrame implements ActionListener,  KeyListener, Observer
{
	MainFrame parentFrame;
	
	JFrame detailsFrame;
	JPanel motherPanel, headerTextPanel, detailsPanel, buttonsPanel, threadTablePanel;
	JPanel detailsTextPanel;
	JTable threadTable;
	UrlDetailsTableModel tableModel;
	JTextPane speedText, stateText, doneText, timeText, downloadFileText;
	private JTextField urlText, fileText;
	StyledDocument headerDocument;
	JLabel downloadSpeed;
	JLabel timeRemaining;
	JLabel imgLabel;
	private DownloadStateHolder stateHolder;
	SimpleAttributeSet headerAttributes;
	
	private JProgressBar progressBar;
	
	JButton pauseButt, resumeButt, cancelButt;
	
	GuiUserIOUtils ioutils;
	SystemInfoHolder sysInfoHolder;
	
	//private static int LABEL_GAP=5;
	private static int APP_PREFERRED_WIDTH=365;
	private static int APP_PREFERRED_HEIGHT=460;
	
	private String fileSizeString = "0Kb";
	
	private static Logger logger = Logger.getLogger(UrlDetailsFrame.class);
	
	//private int threadIndex;

	public UrlDetailsFrame(MainFrame parentFrame, DownloadStateHolder stateHolder, SystemInfoHolder sysInfoHolder, GuiUserIOUtils ioutils)
	{
		this.parentFrame=parentFrame;
		this.stateHolder=stateHolder;
		this.sysInfoHolder = sysInfoHolder;
		this.ioutils=ioutils;
		if(stateHolder.GetFileSize()==-2)
			fileSizeString="Unknown";
		else
			fileSizeString=Common.SizetoString(stateHolder.GetFileSize());
	}
	
	public void ShowFrame()
	{
		//Font fontDefaultNormal12 = new Font("",Font.PLAIN, 12); //For normal labels
		Font fontDefaultNormal10 = new Font("",Font.PLAIN, 10); //For labels needing smaller font.
		
		detailsFrame = new JFrame();
		detailsFrame.setIconImage(new ImageIcon(this.getClass().getResource("guicomponents/images/skdownloader32.png")).getImage());
		
		//Mother panel is the main panel for all components.
		motherPanel = new JPanel();
		motherPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		
		//1. Header
		detailsFrame.setTitle(stateHolder.GetProgress()+"% - "+stateHolder.GetFileName());
		
		/*headerTextPanel = new JPanel();
		headLabel = new JTextPane();
		String str=stateHolder.GetFileName();
		headerAttributes = new SimpleAttributeSet();
		StyleConstants.setFontSize(headerAttributes, 15);
		StyleConstants.setForeground(headerAttributes, new Color(13,123,13));
		StyleConstants.setUnderline(headerAttributes, true);
		headerDocument = headLabel.getStyledDocument();
		try{
			headerDocument.insertString(0, str, headerAttributes);
		} catch (Exception e) {}
		headerDocument.setCharacterAttributes(0, str.length(), headerAttributes, true);
        headLabel.setOpaque(false);
        headLabel.setEditable(false);
        headerTextPanel.add(headLabel);
        headerTextPanel.setMaximumSize(new Dimension(800, 50));*/

        //2. Details Text panel holds all the text details.
        //detailsTextPanel = new GradientJPanel(sysInfoHolder.GetTheme().DETAILSPANEL1BGCOLORGRADIENT1, sysInfoHolder.GetTheme().DETAILSPANEL1BGCOLORGRADIENT2);
        detailsTextPanel = new JPanel();
        detailsTextPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        detailsTextPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        
        //2.1 Url in details text panel
		JLabel urlLbl = new JLabel(" URL:");
		urlLbl.setFont(urlLbl.getFont().deriveFont(urlLbl.getFont().getStyle() ^ Font.BOLD));
		urlLbl.setForeground(Color.BLUE);
		urlText = new JTextField();
		urlText.setText(stateHolder.GetUrlName());
		urlText.setCaretPosition(0);
		urlText.setOpaque(false);
		urlText.setEditable(false);
		//urlText.setBorder(null);
		urlText.setMaximumSize(urlText.getPreferredSize());
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10,0,0,0);
		c.gridx = 0;c.gridy = 0; c.anchor=GridBagConstraints.CENTER;
		c.weightx=0;
		detailsTextPanel.add(urlLbl, c);
		c.gridx = 1;c.gridy = 0; c.anchor=GridBagConstraints.CENTER;
		c.weightx=1;
		detailsTextPanel.add(urlText, c);


        //2.2 File name
		JLabel fileLbl = new JLabel(" File:");
		fileLbl.setForeground(Color.BLUE);
		fileLbl.setFont(fileLbl.getFont().deriveFont(fileLbl.getFont().getStyle() ^ Font.BOLD));
		fileText = new JTextField();
		fileText.setText(stateHolder.GetDirName()+File.separator+stateHolder.GetFileName());
		fileText.setCaretPosition(0);
		fileText.setEditable(false);
		fileText.setOpaque(false);
		fileText.setMaximumSize(urlText.getPreferredSize());
		c.gridx = 0;c.gridy = 1; c.anchor=GridBagConstraints.CENTER;
		c.weightx=0;
		detailsTextPanel.add(fileLbl, c);
		c.gridx = 1;c.gridy = 1; c.anchor=GridBagConstraints.CENTER;
		c.weightx=1;
		detailsTextPanel.add(fileText, c);

        //2.3 file size
		JLabel doneLbl = new JLabel(" Done:");
		doneLbl.setForeground(Color.BLUE);
		doneLbl.setFont(doneLbl.getFont().deriveFont(doneLbl.getFont().getStyle() ^ Font.BOLD));
		doneText = new JTextPane();
		doneText.setText(Common.SizetoString(stateHolder.GetDownloadedSize())+"/"+Common.SizetoString(stateHolder.GetFileSize()));
		//doneText.setText(Common.SizetoString(stateHolder.GetFileSize()));
		doneText.setEditable(false);
		doneText.setOpaque(false);
		doneText.setMaximumSize(doneText.getPreferredSize());
		c.insets = new Insets(5,0,0,0);
		c.gridx = 0;c.gridy = 2; c.anchor=GridBagConstraints.CENTER;
		c.weightx=0;
		detailsTextPanel.add(doneLbl, c);
		c.gridx = 1;c.gridy = 2; c.anchor=GridBagConstraints.CENTER;
		c.weightx=1;
		detailsTextPanel.add(doneText, c);

        //2.3 Time elapsed
		JLabel timeLbl = new JLabel(" Time:");
		timeLbl.setForeground(Color.BLUE);
		timeLbl.setFont(timeLbl.getFont().deriveFont(timeLbl.getFont().getStyle() ^ Font.BOLD));
		timeText = new JTextPane();
		timeText.setText("Elapsed - "+stateHolder.GetElapsedTimeString());
		timeText.setEditable(false);
		timeText.setOpaque(false);
		timeText.setMaximumSize(timeText.getPreferredSize());
		c.insets = new Insets(5,0,0,0);
		c.gridx = 0;c.gridy = 3; c.anchor=GridBagConstraints.CENTER;
		c.weightx=0;
		detailsTextPanel.add(timeLbl, c);
		c.gridx = 1;c.gridy = 3; c.anchor=GridBagConstraints.CENTER;
		c.weightx=1;
		detailsTextPanel.add(timeText, c);
		
        //2.4 download speed
		JLabel speedLbl = new JLabel(" Speed:");
		speedLbl.setForeground(Color.BLUE);
		speedLbl.setFont(speedLbl.getFont().deriveFont(speedLbl.getFont().getStyle() ^ Font.BOLD));
		speedText = new JTextPane();
		speedText.setText(stateHolder.GetDownloadSpeedString());
		speedText.setEditable(false);
		speedText.setOpaque(false);
		speedText.setMaximumSize(speedText.getPreferredSize());
		c.gridx = 0;c.gridy = 4; c.anchor=GridBagConstraints.CENTER;
		c.weightx=0;
		detailsTextPanel.add(speedLbl, c);
		c.gridx = 1;c.gridy = 4; c.anchor=GridBagConstraints.CENTER;
		c.weightx=1;
		detailsTextPanel.add(speedText, c);
		
		JPanel progressPanel = new JPanel(new BorderLayout());
		progressBar = new JProgressBar(0,100);
		progressBar.setStringPainted(true);
		if(stateHolder.GetFileSize() == -2)
		{
			progressBar.setValue(100);
			progressBar.setString("Size unknown");
		}
		else
			progressBar.setValue(stateHolder.GetProgress());
		progressPanel.add(progressBar);
		progressPanel.setBorder(BorderFactory.createEmptyBorder(0,5,5,5));

		//Buttons
		buttonsPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));		
		pauseButt=new JButton("Pause");
		pauseButt.setFont(fontDefaultNormal10);
		pauseButt.addActionListener(this);
		pauseButt.setActionCommand("PauseDownload");		
		buttonsPanel.add(pauseButt);
		
		resumeButt=new JButton("Resume");
		resumeButt.setFont(fontDefaultNormal10);
		resumeButt.addActionListener(this);
		resumeButt.setActionCommand("ResumeDownload");	
		buttonsPanel.add(resumeButt);
		
		cancelButt=new JButton("Cancel");
		cancelButt.setFont(fontDefaultNormal10);
		cancelButt.addActionListener(this);
		cancelButt.setActionCommand("CancelDownload");	
		buttonsPanel.add(cancelButt);
		
		JLabel seperatorText = new JLabel("  ");
		buttonsPanel.add(seperatorText);

		imgLabel= new JLabel(((GuiUrlDownloaderWrapper)(stateHolder.GetDownloaderObject())).GetNewStatusAnimImage(stateHolder.GetStatus()));
		buttonsPanel.add(imgLabel);
		
		stateText = new JTextPane();
		if((stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.PAUSED) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.COMPLETED) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.STARTING))
		{
			stateText.setForeground(new Color(13,123,13));
		}
		else if((stateHolder.GetStatus() == DownloadStateHolder.CANCELLED) ||
				   (stateHolder.GetStatus() == DownloadStateHolder.ERROR))
		{
			stateText.setForeground(Color.RED);
		}
		stateText.setFont(urlLbl.getFont().deriveFont(urlLbl.getFont().getStyle() ^ Font.BOLD));
		stateText.setText(stateHolder.GetStatusString());
		stateText.setEditable(false);
		stateText.setOpaque(false);
		stateText.setMaximumSize(stateText.getPreferredSize());
		buttonsPanel.add(stateText);

		threadTablePanel = new JPanel(new BorderLayout());
		threadTablePanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        tableModel = new UrlDetailsTableModel();
        tableModel.InitAll();
        threadTable = new JTable(tableModel);
        threadTable.setCellSelectionEnabled(false);
        threadTable.setRowSelectionAllowed(false);
        threadTable.setColumnSelectionAllowed(false);
        
        ProgressRenderer renderer = new ProgressRenderer(0, 100, sysInfoHolder);
        renderer.setStringPainted(true); // show progress text
        threadTable.setDefaultRenderer(JProgressBar.class, renderer);
        DownloadsTableDefaultRenderer renderer1 = new DownloadsTableDefaultRenderer(sysInfoHolder);
        threadTable.setDefaultRenderer(String.class, renderer1);
        threadTable.setRowHeight((int) renderer.getPreferredSize().getHeight());
        JScrollPane threadTableScrollPane = new JScrollPane(threadTable);
        threadTableScrollPane.setOpaque(true);
        //threadTableScrollPane.getViewport().setBackground(sysInfoHolder.GetTheme().TABLEPANEBACKGROUNDCOLOR);
        threadTablePanel.add(threadTableScrollPane, BorderLayout.CENTER);
        
		GroupLayout layout1 = new GroupLayout(motherPanel);
		motherPanel.setLayout(layout1);
		layout1.setHorizontalGroup(
				layout1.createSequentialGroup()
				.addGroup(layout1.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(detailsTextPanel)
					.addComponent(buttonsPanel)
					.addComponent(progressPanel)					
					.addComponent(threadTablePanel))
				);

		layout1.setVerticalGroup(
				layout1.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout1.createSequentialGroup()
					.addComponent(detailsTextPanel)
					.addComponent(buttonsPanel)
					.addComponent(progressPanel)
					.addComponent(threadTablePanel))
		);
		
		detailsFrame.setLocation(parentFrame.GetFrame().getX(), parentFrame.GetFrame().getY());
		
		detailsFrame.add(motherPanel);
		detailsFrame.setVisible(true);
		detailsFrame.setSize(APP_PREFERRED_WIDTH, APP_PREFERRED_HEIGHT);
		
        threadTable.getColumnModel().getColumn(0).setPreferredWidth((int)((APP_PREFERRED_WIDTH)*.1));
        threadTable.getColumnModel().getColumn(1).setPreferredWidth((int)((APP_PREFERRED_WIDTH)*.15));
        threadTable.getColumnModel().getColumn(2).setPreferredWidth((int)((APP_PREFERRED_WIDTH)*.65));
        threadTable.getColumnModel().getColumn(3).setPreferredWidth((int)((APP_PREFERRED_WIDTH)*.2));
        
		tableModel.addThread(stateHolder);
		
		stateHolder.addObserver(this);
		detailsFrame.addWindowListener(new WindowAdapter(){
		      public void windowClosing(WindowEvent we){
		        //detailsFrame.dispose();
		        ((GuiUrlDownloaderWrapper)stateHolder.GetDownloaderObject()).HideDetails();
		        //((GuiUrlDownloaderWrapper)stateHolder.GetDownloaderObject()).SetDetailsDisplayStatus(false);
		      }});
		UpdateButtons();
	}
	
	public void ToFront() {
		detailsFrame.setState ( Frame.NORMAL );
		detailsFrame.setVisible(true);
		detailsFrame.setAlwaysOnTop(true);
		detailsFrame.toFront();
		detailsFrame.setAlwaysOnTop(false);
	}
	
	public synchronized void update(Observable o1, Object arg1)
    {
		//if(true)
		//	return;
		//System.exit(0);
		final Observable o = o1;
		final Object arg = arg1;
		java.awt.EventQueue.invokeLater(new Runnable() {public void run()
		{
			if(o.getClass().getName().equals("skdownloader.core.DownloadStateHolder"))
			{
				DownloadStateHolder stateHolder = (DownloadStateHolder)o;
				String updateString = (String)arg;
				
				if(updateString.equals("DownloadSpeed"))
				{
					speedText.setText(stateHolder.GetDownloadSpeedString());
					timeText.setText("Elapsed: "+stateHolder.GetElapsedTimeString());
					for(int i=0; i <stateHolder.GetNumConnections(); i++)
	    			{
	    				tableModel.fireTableCellUpdated(i, 3);
	    			}
				}
				else if(updateString.equals("DownloadedPercent"))
				{
					if(stateHolder.GetFileSize() == -2)
					{
						if((detailsFrame != null) && (detailsFrame.isVisible()))
							detailsFrame.setTitle("Downloading "+stateHolder.GetFileName());
						progressBar.setString("Size unknown");
					}
					else if(stateHolder.GetProgress() >= 0)
					{
						if((detailsFrame != null) && (detailsFrame.isVisible()))
							detailsFrame.setTitle(stateHolder.GetProgress()+1+"% - "+stateHolder.GetFileName());
						if(progressBar.getString() != null)
							progressBar.setString(null);
						progressBar.setValue(stateHolder.GetProgress());
					}
				}
				else if(updateString.equals("DownloadedSize"))
				{
					//if(stateHolder.GetProgress() != 0)
					//{
						//doneText.setText(Common.SizetoString(stateHolder.GetDownloadedSize())+"/"+Common.SizetoString(stateHolder.GetFileSize()));
						doneText.setText(Common.SizetoString(stateHolder.GetDownloadedSize())+"/"+fileSizeString);
					//}
				}
				else if(updateString.equals("FileSize"))
				{
					if(stateHolder.GetFileSize() == -2)
					{
						fileSizeString="Unknown";
						progressBar.setString("Size unknown");
						logger.debug("Setting progressbar to unknown");
					}
					else
					{
						fileSizeString = Common.SizetoString(stateHolder.GetFileSize());
						doneText.setText(Common.SizetoString(stateHolder.GetDownloadedSize())+"/"+fileSizeString);
						//This is to remove the initial "Size unknown" status
						progressBar.setString(null);
						progressBar.setValue(stateHolder.GetProgress());
					}
				}
				else if(updateString.equals("DownloadStatus"))
				{
					UpdateButtons();
					if(stateHolder.GetStatus() == DownloadStateHolder.COMPLETED)
					{
						((GuiUrlDownloaderWrapper)stateHolder.GetDownloaderObject()).HideDetails();
					}
					else if((stateHolder.GetStatus() == DownloadStateHolder.CANCELLED) ||
							(stateHolder.GetStatus() == DownloadStateHolder.ERROR))
					{
						((GuiUrlDownloaderWrapper)stateHolder.GetDownloaderObject()).HideDetails();
					}
					imgLabel.setIcon(((GuiUrlDownloaderWrapper)(stateHolder.GetDownloaderObject())).GetNewStatusAnimImage(stateHolder.GetStatus()));
					stateText.setText(stateHolder.GetStatusString());
				}
				else if(updateString.equals("FileName"))
				{
					fileText.setText(stateHolder.GetDirName()+File.separator+stateHolder.GetFileName());
					fileText.setCaretPosition(0);
					/*headLabel.setText("");
					headerDocument = headLabel.getStyledDocument();
					try{
						headerDocument.insertString(0, stateHolder.GetFileName(), headerAttributes);
					} catch (Exception e) {}
					headerDocument.setCharacterAttributes(0, stateHolder.GetFileName().length(), headerAttributes, true);
					*/
				}
				if(updateString.equals("ThreadDownloadedSize"))
	    		{
	    			for(int i=0; i <stateHolder.GetNumConnections(); i++)
	    			{
	    				tableModel.fireTableCellUpdated(i, 2);
	    			}
	    		}
				else if(updateString.equals("NumConnections"))
	    		{
	    			tableModel.reloadThread(stateHolder);
	    		}
				else if(updateString.equals("ThreadFileSize"))
	    		{
	    			for(int i=0; i <stateHolder.GetNumConnections(); i++)
	    			{
	    				//logger.PrintLog("DEBUG", "Updating "+i+"1");
	    				tableModel.fireTableCellUpdated(i, 1);
	    			}
	    		}
			}
		}});
    }
	
	public void PauseDownload()
	{
		parentFrame.PauseDownload((GuiUrlDownloaderWrapper)stateHolder.GetDownloaderObject());
		UpdateButtons();
	}
	
	public void ResumeDownload()
	{
		parentFrame.ResumeDownload((GuiUrlDownloaderWrapper)stateHolder.GetDownloaderObject());
		UpdateButtons();
	}
	
	public void CancelDownload()
	{
		parentFrame.CancelDownload((GuiUrlDownloaderWrapper)stateHolder.GetDownloaderObject());
		UpdateButtons();
	}
	
	public void DisposeFrame() {
		detailsFrame.removeAll();
		detailsFrame.setVisible(false);
		detailsFrame.dispose();
		detailsFrame=null;
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		String cmd = e.getActionCommand();
		if(cmd == "Exit")
		{
			((GuiUrlDownloaderWrapper)stateHolder.GetDownloaderObject()).HideDetails();
		}
		else if(cmd.equals("PauseDownload"))
		{
			PauseDownload();
		}
		else if(cmd.equals("ResumeDownload"))
		{
			ResumeDownload();
		}
		else if(cmd.equals("CancelDownload"))
		{
			if(ioutils.GetFromYesNoMessage(detailsFrame, "Really cancel download?"))
			{
				CancelDownload();
			}
		}
	}
	
	  /* Update each button's state based off of the
    currently selected download's status. */
 private void UpdateButtons() 
 {
	 java.awt.EventQueue.invokeLater(new Runnable() {public void run() 
	 {
		 int status = stateHolder.GetStatus();
		 switch (status) 
		 {
		 case DownloadStateHolder.STARTING:
			 pauseButt.setEnabled(true);
			 if(stateHolder.GetConnectionProtocol() == DownloadStateHolder.FTP)
					pauseButt.setEnabled(false);
			 resumeButt.setEnabled(false);
			 cancelButt.setEnabled(true);
			 break;
		 case DownloadStateHolder.DOWNLOADING:
			 pauseButt.setEnabled(true);
			 if(stateHolder.GetConnectionProtocol() == DownloadStateHolder.FTP)
					pauseButt.setEnabled(false);
			 resumeButt.setEnabled(false);
			 cancelButt.setEnabled(true);
			 break;
		 case DownloadStateHolder.PAUSED:
			 pauseButt.setEnabled(false);
			 resumeButt.setEnabled(true);
			 cancelButt.setEnabled(true);
			 break;
		 case DownloadStateHolder.ERROR:
			 pauseButt.setEnabled(false);
			 resumeButt.setEnabled(false);
			 cancelButt.setEnabled(false);
			 break;
		 default: // COMPLETE or CANCELLED
			 pauseButt.setEnabled(false);
	  	  	resumeButt.setEnabled(false);
	  	  	cancelButt.setEnabled(false);
		 }
	 }});
 }
 
	//Handle key typed events.
	public void keyPressed(KeyEvent keyEvent) {
	}
	public void keyTyped(KeyEvent event) {
	}
	public void keyReleased(KeyEvent event) 
	{
	}
	  
}