/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Holder system variables/states
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/18/09 - Add DEBUGMODE
 *
 ****************************************************************************************/

package skdownloader.downloadmanager;

import java.io.File;

public class SystemInfoHolder implements Cloneable {
	private String installDir;
	private String dataDir;
	private String updatesDir;
	
	//Default values for variables.
	public int FIRSTRUN;
	public String OS;
	public static String windows = "windows";
	public static String linux = "linux";
	public int APP_MIN_WIDTH=800;
	public int APP_MIN_HEIGHT=450;
	public int APP_PREFERRED_WIDTH = 800;
	public int APP_PREFERRED_HEIGHT = 500;
	public int TORRENT_DETAILS_PREFERRED_WIDTH=365;
	public int TORRENT_DETAILS_PREFERRED_HEIGHT=460;
	public int APP_START_X = 100;
	public int APP_START_Y = 100;
	public boolean TRAYENABLED=true;
	public boolean CLOSETOTRAY=true;
	public int VERY_LARGE_NUMBER=1000;
	public int TOPBUTTONSPANEL_MIN_HEIGHT=50;
	public int TOPBUTTONSPANEL_MAX_HEIGHT=TOPBUTTONSPANEL_MIN_HEIGHT;
	public int TOPBUTTONSPANEL_MIN_WIDTH=500;
	public int TOPBUTTONSPANEL_MAX_WIDTH=VERY_LARGE_NUMBER;
	//public int BROWSERMANAGER_SPLILTPANE_MIN_HEIGHT=250;
	//private int BROWSERMANAGER_SPLILTPANE_MAX_HEIGHT=VERY_LARGE_NUMBER;
	//public int BROWSERMANAGER_SPLILTPANE_MIN_WIDTH=500;
	public int BROWSERMANAGER_SPLILTPANE_PREFERRED_DIVIDER=150;
	//private int BROWSERMANAGER_SPLILTPANE_MAX_WIDTH=VERY_LARGE_NUMBER;
	//private int DOWLOADDETAILSPANEL_MIN_HEIGHT=BROWSERMANAGER_SPLILTPANE_MIN_HEIGHT;
	//private int DOWLOADDETAILSPANEL_MAX_HEIGHT=BROWSERMANAGER_SPLILTPANE_MAX_HEIGHT;
	//private int DOWLOADDETAILSPANEL_MIN_WIDTH=250;
	//private int DOWLOADDETAILSPANEL_MAX_WIDTH=250;
	public int ADSPANEL_MIN_HEIGHT=120;
	public int ADSPANEL_MIN_WIDTH=TOPBUTTONSPANEL_MIN_WIDTH;
	
	public String DOWNLOADSTABLE_WIDTH_ARR_STR="";
	public String DOWNLOADSTABLE_ORDER_ARR_STR="";
	public int DOWNLOADSTABLE_COLS_VISIBLE=10;
	
	public int DEBUGMODE = 1;
	public static int DEBUGMODE_NODEBUG = 0;
	public static int DEBUGMODE_DEBUG = 1;
	public String VERSION;
	public String COMPANYURL;
	public String COPYRIGHT;
	public String ADS_SRC_LOCATION;
	public String UPDATESURL;
	
	public boolean SHOWPOPPUPWHENDOWNLOADFINISH=false;
	public boolean SHOWDETAILSONADDINGDOWNLOAD=true;
	public int SAVETOOPTION = 0;
	public static int SAVETODEFAULTFOLDER = 0;
	public static int SAVETOPRVIOUSDOWNLOADLOCATION = 1;
	public static int ASKDOWNLOADLOCATIONEACHTIME=2;
	public String PREVIOUSDOWNLOADLOCATION;
	public String DEFAULTDOWNLOADLOCATION;
	public String PREVIOUSTORRENTINPUTLOCATION;
	
	public boolean AUTOUPDATE=true;
	public int UPDATEOPTION = 0;
	public static int UPDATEEVERYTIME=0;
	public static int UPDATEDAILY=1;
	public static int UPDATEWEEKLY=2;
	public long LASTUDATEDAT=0; 
	
	public int INTERNETCONNECTIONOPTION = 0;
	public int ALWAYSCONNECTED = 0;
	
	public boolean USEPROXY = false;
	public String HTTPPROXYSERVER;
	public int HTTPPROXYPORT;
	public boolean HTTPUSEAUTHENTICATION;
	public String HTTPPROXYUSER;
	public String HTTPPROXYPASSWORD;
	public boolean HTTPPROXYNOTUSECACHE;
	
	public String SOCKSPROXYSERVER;
	public int SOCKSPROXYPORT;
	public boolean SOCKSUSEAUTHENTICATION;
	public String SOCKSPROXYUSER;
	public String SOCKSPROXYPASSWORD;
	public boolean SOCKSPROXYNOTUSECACHE;
	
	public String PROXYEXCEPTIONS;
	
	public boolean USEDELAYTIMEFROMSERVER=true;
	public int TIMEOUTDELAY=60;
	public int CONNECTRETRYLIMIT=10;
	
	public int DOWNLOADSTARTOPTION = 0;
	public static int STARTDOWNLOADSIMMEDIATELY=0;
	public static int DONTSTARTDOWNLOADSIMMEDIATELY=1;
	public static int ASKTOSTARTDOWNLOADS = 2;
	
	public boolean AUTOACCELERATE = true;
	public int MINSIZEFORACCELERATION = 500;
	
	public int TORRENT_LISTEN_PORT; 
	public int TORRENTDOWNLOADSTARTOPTION = 0;
	public static int TORRENTNORMALDOWNLOAD = 0;
	public static int TORRENTTORRENTDOWNLOAD = 1;
	
	public int MAXGLOBALTORRENTDOWNLOADSPEED=0;
	public int MAXGLOBALTORRENTUPLOADSPEED=0;
	public boolean GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED=false;
	public int MAXGLOBALTORRENTUPLOADSPEEDONSEEDING=0;
	
	public boolean TORRENTREMOVEONSHARERATIOOPTION=false;
	public boolean TORRENTREMOVEONSEEDTIMEOPTION=false;
	public float SHARERATIOTOREMOVETORRENT = 0;
	public int SEEDTIMETOREMOVETORRENT = 0;
	
	public int PEERTABLE_WIDTH_COL0=94;
	public int PEERTABLE_WIDTH_COL1=156;
	public int PEERTABLE_WIDTH_COL2=77;

	public int FILETABLE_WIDTH_COL0=22;
	public int FILETABLE_WIDTH_COL1=123;
	public int FILETABLE_WIDTH_COL2=54;
	public int FILETABLE_WIDTH_COL3=53;
	public int FILETABLE_WIDTH_COL4=63;
	
	public int SEARCHFRAME_WIDTH=800;
	public int SEARCHFRAME_HEIGHT=435;
	
	public int OPTIONSFRAME_WIDTH = 650;
	public int OPTIONSFRAME_HEIGHT=450;
	
	public int SELECTEDTHEME = 0;
	
	public int SCHEDULESTARTOPTION;
	public static int SCHEDULESTARTDISABLED=0;
	public static int SCHEDULESTARTATTIME=1; 
	public static int SCHEDULESTARTFORDAYS = 2;
	public boolean SCHEDULESTARTONSATURDAY;
	public boolean SCHEDULESTARTONSUNDAY;
	public boolean SCHEDULESTARTONMONDAY;
	public boolean SCHEDULESTARTONTUESDAY;
	public boolean SCHEDULESTARTONWEDNESDAY;
	public boolean SCHEDULESTARTONTHURSDAY;
	public boolean SCHEDULESTARTONFRIDAY;
	public String SCHEDULESTARTTIME;
	public String SCHEDULESTARTDAYSTOCHECK;
	public int SCHEDULESTOPOPTION;
	public static int SCHEDULESTOPDISABLED=0;
	public static int SCHEDULESTOPATTIME=1;
	public String SCHEDULESTOPTIME;
	
	public static int DONOTHINGWHENDOWNLOADSFINISH=0;
	public static int SHUTDOWNSKWHENDOWNLOADSFINISH=1;
	public static int SHUTDOWNPCWHENDOWNLOADSFINISH=2;
	public int SHUTDOWNOPTION=DONOTHINGWHENDOWNLOADSFINISH;
	
	public String ROOTPASSWORD="";
	
	public String HIDEUPDATEREMINDERFORVERSION = "100";
	
	public int LIVEPORT=6235;
	
	public boolean hasTray=false; //Represents if we have a system tray currently displayed
	
	public boolean isShuttingDown=false;
	public boolean isStarting=true;
	
	public boolean isUpdateChecking = false;
	
	public SystemInfoHolder(String installDir, String dataDir)
	{
		this.installDir=installDir;
		this.dataDir=dataDir;
		this.updatesDir=dataDir+File.separator+"updates";
	}
	
	/*************************************************************
	 * 
	 * Function  : GetInstallDirFromEnv
	 * Arguments : void
	 * Return    : Installation dir path
	 * Notes     : returns installation dir path string if set as 
	 *             env variable.
	 * 
	 *************************************************************/
	public static String GetInstallDirFromEnv()
	{
		String INSTALLDIR=System.getenv("INSTDIR");
		if(INSTALLDIR==null)
			INSTALLDIR="";
		File file = new File(INSTALLDIR+File.separator+"skdownloader.jar");
		if(! file.exists())
			INSTALLDIR="";
		return INSTALLDIR;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetInstallDir
	 * Arguments : void
	 * Return    : Installation dir path
	 * Notes     : returns installation dir path string.
	 * 
	 *************************************************************/
	public String GetInstallDir()
	{
		return installDir;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetImagesDir
	 * Arguments : void
	 * Return    : Images dir path
	 * Notes     : returns images dir path string.
	 * 
	 *************************************************************/
	public String GetImagesDir()
	{
		return GetInstallDir()+File.separator+"images";
	}
	
	public static boolean IsWindows() {
		if(System.getProperty("os.name").toLowerCase().indexOf("win")>=0) {
			return true;
		}
		return false;
	}
	
	public static String getDefaultDataDir() {
		//Fix a tmp dir for windows
		String dir="";
		if(IsWindows()) {
			//First check appdata variable. This should be consistent in most modern windows versions
			
			//If we don't find app data dir, check manually
			if((new File(System.getenv("appdata"))).exists()) {
				dir=System.getenv("appdata")+File.separator+"skdownloader";
			}
			else if((new File(System.getProperty("user.home")+File.separator+"Application Data")).exists()) {
				dir=System.getProperty("user.home")+File.separator+"Application Data"+File.separator+"skdownloader";
			}
		}
		else {
			if((new File(System.getProperty("user.home"))).exists()) {
				dir=System.getProperty("user.home")+File.separator+".skdownloader";
			}
		}
		return dir;
	}
	
	public String getDataDir() {
		return this.dataDir;
	}
	
	public String getUpdatesDir() {
		return this.updatesDir;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetHistoryXml
	 * Arguments : void
	 * Return    : History xml path string
	 * Notes     : returns history.xml path string.
	 * 
	 *************************************************************/
	public String GetHistoryXml()
	{
		return getDataDir()+File.separator+"prefs"+File.separator+"history.xml";
	}
	
	/*************************************************************
	 * 
	 * Function  : GetConfigXml
	 * Arguments : void
	 * Return    : Config xml path string
	 * Notes     : returns config.xml path string.
	 * 
	 *************************************************************/
	public String GetConfigXml()
	{
		return GetInstallDir()+File.separator+"prefs"+File.separator+"config_gui.xml";
	}
	
	/*************************************************************
	 * 
	 * Function  : GetRuntimeOptionsXml
	 * Arguments : void
	 * Return    : RuntimeOptions xml path string
	 * Notes     : returns RuntimeOptions.xml path string.
	 * 
	 *************************************************************/
	public String GetRuntimeOptionsXml()
	{
		return getDataDir()+File.separator+"prefs"+File.separator+"runtimeoptions.xml";
	}
	
	/*************************************************************
	 * 
	 * Function  : GetDefaultDownloadDir
	 * Arguments : void
	 * Return    : Directory string
	 * Notes     : Returns default downloads dir.
	 * 
	 *************************************************************/
	public String GetDefaultDownloadDir()
	{
		return System.getProperty("user.home")+File.separator+"Downloads";
	}
	
	public String GetTmpDownloadDir()
	{
		return getDataDir()+File.separator+"downloads";
	}
	
	/*************************************************************
	 * 
	 * Function  : SetTheme
	 * Arguments : theme index
	 * Return    : void
	 * Notes     : Sets theme specified by theme index
	 * 
	 *************************************************************/
	public void SetTheme(int index)
	{
		SELECTEDTHEME=index;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetTheme
	 * Arguments : void
	 * Return    : current theme object
	 * Notes     : Returns theme specified by theme index
	 * 
	 *************************************************************/
	public int GetTheme()
	{
		return SELECTEDTHEME;
	}
	
	/*************************************************************
	 * 
	 * Function  : clone
	 * Arguments : void
	 * Return    : cloned object
	 * Notes     : Creates a copy of this object
	 * 
	 *************************************************************/
	public Object clone() {
        try {
            return super.clone();
        }
        catch( CloneNotSupportedException e ) {
            return null;
        }
    }
	
	public void SetTrayAvailable(boolean have) {
		this.hasTray=have;
	}
	
	public boolean TrayAvailable() {
		return this.hasTray;
	}
	
	/*************************************************************
	 * 
	 * Function  : CopyFrom
	 * Arguments : SystemInfoHolder object
	 * Return    : void
	 * Notes     : Copies variable values from argument object to this object.
	 * 
	 *************************************************************/
	public void CopyFrom(SystemInfoHolder sysInfoHolder)
	{
		APP_MIN_HEIGHT=sysInfoHolder.APP_MIN_HEIGHT;
		APP_PREFERRED_WIDTH=sysInfoHolder.APP_PREFERRED_WIDTH;
		APP_PREFERRED_HEIGHT=sysInfoHolder.APP_PREFERRED_HEIGHT;
		TRAYENABLED=sysInfoHolder.TRAYENABLED;
		CLOSETOTRAY=sysInfoHolder.CLOSETOTRAY;
		TORRENT_DETAILS_PREFERRED_WIDTH=sysInfoHolder.TORRENT_DETAILS_PREFERRED_WIDTH;
		TORRENT_DETAILS_PREFERRED_HEIGHT=sysInfoHolder.TORRENT_DETAILS_PREFERRED_HEIGHT;
		VERY_LARGE_NUMBER=sysInfoHolder.VERY_LARGE_NUMBER;
		TOPBUTTONSPANEL_MIN_HEIGHT=sysInfoHolder.TOPBUTTONSPANEL_MIN_HEIGHT;
		TOPBUTTONSPANEL_MAX_HEIGHT=sysInfoHolder.TOPBUTTONSPANEL_MAX_HEIGHT;
		TOPBUTTONSPANEL_MIN_WIDTH=sysInfoHolder.TOPBUTTONSPANEL_MIN_WIDTH;
		TOPBUTTONSPANEL_MAX_WIDTH=sysInfoHolder.TOPBUTTONSPANEL_MAX_WIDTH;
		BROWSERMANAGER_SPLILTPANE_PREFERRED_DIVIDER=sysInfoHolder.BROWSERMANAGER_SPLILTPANE_PREFERRED_DIVIDER;
		ADSPANEL_MIN_HEIGHT=sysInfoHolder.ADSPANEL_MIN_HEIGHT;
		ADSPANEL_MIN_WIDTH=sysInfoHolder.ADSPANEL_MIN_WIDTH;
		VERSION=sysInfoHolder.VERSION;
		COMPANYURL=sysInfoHolder.COMPANYURL;
		COPYRIGHT=sysInfoHolder.COPYRIGHT;
		OS=sysInfoHolder.OS;
		ADS_SRC_LOCATION=sysInfoHolder.ADS_SRC_LOCATION;
		UPDATESURL=sysInfoHolder.UPDATESURL;
		SHOWPOPPUPWHENDOWNLOADFINISH=sysInfoHolder.SHOWPOPPUPWHENDOWNLOADFINISH;
		SHOWDETAILSONADDINGDOWNLOAD=sysInfoHolder.SHOWDETAILSONADDINGDOWNLOAD;
		DEBUGMODE=sysInfoHolder.DEBUGMODE;
		SELECTEDTHEME=sysInfoHolder.SELECTEDTHEME;
		SAVETOOPTION=sysInfoHolder.SAVETOOPTION;
		PREVIOUSDOWNLOADLOCATION=sysInfoHolder.PREVIOUSDOWNLOADLOCATION;
		PREVIOUSTORRENTINPUTLOCATION=sysInfoHolder.PREVIOUSTORRENTINPUTLOCATION;
		DEFAULTDOWNLOADLOCATION=sysInfoHolder.DEFAULTDOWNLOADLOCATION;
		INTERNETCONNECTIONOPTION=sysInfoHolder.INTERNETCONNECTIONOPTION;
		ALWAYSCONNECTED=sysInfoHolder.ALWAYSCONNECTED;
		USEPROXY=sysInfoHolder.USEPROXY;
		HTTPPROXYSERVER=sysInfoHolder.HTTPPROXYSERVER;
		HTTPPROXYPORT=sysInfoHolder.HTTPPROXYPORT;
		HTTPUSEAUTHENTICATION=sysInfoHolder.HTTPUSEAUTHENTICATION;
		HTTPPROXYUSER=sysInfoHolder.HTTPPROXYUSER;
		HTTPPROXYPASSWORD=sysInfoHolder.HTTPPROXYPASSWORD;
		HTTPPROXYNOTUSECACHE=sysInfoHolder.HTTPPROXYNOTUSECACHE;
		SOCKSPROXYSERVER=sysInfoHolder.SOCKSPROXYSERVER;
		SOCKSPROXYPORT=sysInfoHolder.SOCKSPROXYPORT;
		SOCKSUSEAUTHENTICATION=sysInfoHolder.SOCKSUSEAUTHENTICATION;
		SOCKSPROXYUSER=sysInfoHolder.SOCKSPROXYUSER;
		SOCKSPROXYPASSWORD=sysInfoHolder.SOCKSPROXYPASSWORD;
		SOCKSPROXYNOTUSECACHE=sysInfoHolder.SOCKSPROXYNOTUSECACHE;
		PROXYEXCEPTIONS=sysInfoHolder.PROXYEXCEPTIONS;
		USEDELAYTIMEFROMSERVER=sysInfoHolder.USEDELAYTIMEFROMSERVER;
		TIMEOUTDELAY=sysInfoHolder.TIMEOUTDELAY;
		CONNECTRETRYLIMIT=sysInfoHolder.CONNECTRETRYLIMIT;
		DOWNLOADSTARTOPTION=sysInfoHolder.DOWNLOADSTARTOPTION;
		AUTOACCELERATE=sysInfoHolder.AUTOACCELERATE;
		AUTOUPDATE=sysInfoHolder.AUTOUPDATE;
		MINSIZEFORACCELERATION=sysInfoHolder.MINSIZEFORACCELERATION;
		SELECTEDTHEME=sysInfoHolder.SELECTEDTHEME;
		UPDATEOPTION=sysInfoHolder.UPDATEOPTION;
		SCHEDULESTARTOPTION=sysInfoHolder.SCHEDULESTARTOPTION;
		SCHEDULESTARTONSATURDAY=sysInfoHolder.SCHEDULESTARTONSATURDAY;
		SCHEDULESTARTONSUNDAY=sysInfoHolder.SCHEDULESTARTONSUNDAY;
		SCHEDULESTARTONMONDAY=sysInfoHolder.SCHEDULESTARTONMONDAY;
		SCHEDULESTARTONTUESDAY=sysInfoHolder.SCHEDULESTARTONTUESDAY;
		SCHEDULESTARTONWEDNESDAY=sysInfoHolder.SCHEDULESTARTONWEDNESDAY;
		SCHEDULESTARTONTHURSDAY=sysInfoHolder.SCHEDULESTARTONTHURSDAY;
		SCHEDULESTARTONFRIDAY=sysInfoHolder.SCHEDULESTARTONFRIDAY;
		SCHEDULESTARTTIME=sysInfoHolder.SCHEDULESTARTTIME;
		SCHEDULESTARTDAYSTOCHECK=sysInfoHolder.SCHEDULESTARTDAYSTOCHECK;
		SCHEDULESTOPOPTION=sysInfoHolder.SCHEDULESTOPOPTION;
		SCHEDULESTOPTIME=sysInfoHolder.SCHEDULESTOPTIME;
		TORRENT_LISTEN_PORT=sysInfoHolder.TORRENT_LISTEN_PORT;
		TORRENTDOWNLOADSTARTOPTION=sysInfoHolder.TORRENTDOWNLOADSTARTOPTION;
		MAXGLOBALTORRENTUPLOADSPEED=sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEED;
		MAXGLOBALTORRENTDOWNLOADSPEED=sysInfoHolder.MAXGLOBALTORRENTDOWNLOADSPEED;
		TORRENTREMOVEONSHARERATIOOPTION=sysInfoHolder.TORRENTREMOVEONSHARERATIOOPTION;
		TORRENTREMOVEONSEEDTIMEOPTION=sysInfoHolder.TORRENTREMOVEONSEEDTIMEOPTION;
		SHARERATIOTOREMOVETORRENT=sysInfoHolder.SHARERATIOTOREMOVETORRENT;
		SEEDTIMETOREMOVETORRENT=sysInfoHolder.SEEDTIMETOREMOVETORRENT;
		MAXGLOBALTORRENTUPLOADSPEEDONSEEDING=sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEEDONSEEDING;
		GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED=sysInfoHolder.GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED;
		PEERTABLE_WIDTH_COL0=sysInfoHolder.PEERTABLE_WIDTH_COL0;
		PEERTABLE_WIDTH_COL1=sysInfoHolder.PEERTABLE_WIDTH_COL1;
		PEERTABLE_WIDTH_COL2=sysInfoHolder.PEERTABLE_WIDTH_COL2;
		FILETABLE_WIDTH_COL0=sysInfoHolder.FILETABLE_WIDTH_COL0;
		FILETABLE_WIDTH_COL1=sysInfoHolder.FILETABLE_WIDTH_COL1;
		FILETABLE_WIDTH_COL2=sysInfoHolder.FILETABLE_WIDTH_COL2;
		FILETABLE_WIDTH_COL3=sysInfoHolder.FILETABLE_WIDTH_COL3;
		FILETABLE_WIDTH_COL4=sysInfoHolder.FILETABLE_WIDTH_COL4;
		SEARCHFRAME_WIDTH=sysInfoHolder.SEARCHFRAME_WIDTH;
		SEARCHFRAME_HEIGHT=sysInfoHolder.SEARCHFRAME_HEIGHT;
		OPTIONSFRAME_WIDTH=sysInfoHolder.OPTIONSFRAME_WIDTH;
		OPTIONSFRAME_HEIGHT = sysInfoHolder.OPTIONSFRAME_HEIGHT;
		SHUTDOWNOPTION=sysInfoHolder.SHUTDOWNOPTION;
		ROOTPASSWORD=sysInfoHolder.ROOTPASSWORD;
		HIDEUPDATEREMINDERFORVERSION=sysInfoHolder.HIDEUPDATEREMINDERFORVERSION;
	}
}