/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Common functions for GUI interface.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.zip.Checksum;

import org.apache.log4j.Logger;

import edu.stanford.ejalbert.BrowserLauncher;

public class GUICommon
{
	public static int NUMCONNECTIONS = 5;
	
	public static final String LINESEPERATOR = System.getProperty("line.separator");
	private static Logger logger = Logger.getLogger(GUICommon.class);
	
	/**
	 * Get the String residing on the clipboard.
	 *
	 * @return any text found on the Clipboard; if none found, return an
	 * empty String.
	 */
	public static String getClipboardContents() {
		String result = "";
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		//odd: the Object param of getContents is not currently used
		Transferable contents = clipboard.getContents(null);
		boolean hasTransferableText =
			(contents != null) &&
			contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		if ( hasTransferableText ) {
			try {
				result = (String)contents.getTransferData(DataFlavor.stringFlavor);
			}
			catch (UnsupportedFlavorException ex) {
				//highly unlikely since we are using a standard DataFlavor
				logger.error("Exception", ex);
				return "";
			}
			catch (IOException ex) {
				logger.error("Exception", ex);
				return "";
			}
		}
		return result;
	}
	
	public static void setClipboardContents(String str) {
		StringSelection stringSelection = new StringSelection( str );
	    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	    clipboard.setContents( stringSelection, null );
	}
	
	public static boolean CheckIfGoodUrl(String s) {
		Pattern p = Pattern.compile("^(http|https).*");
		Matcher m = p.matcher(s);
		if(m.matches()) {
			return true;
		}
		return false;
	}
	
	public static String GetUrlFromClipboard() {
		String str=getClipboardContents();
		if(CheckIfGoodUrl(str)==true) {
			return str;
		}
		return("");
	}
	
	public static boolean CheckIfGoodTorrent(String s) {
		Pattern p = Pattern.compile("^.*\\.torrent");
		Matcher m = p.matcher(s);
		if(m.matches()) {
			return true;
		}
		return false;
	}
	
	public static String GetTorrentFromClipboard() {
		String str=getClipboardContents();
		if(CheckIfGoodTorrent(str)==true) {
			return str;
		}
		return("");
	}
	
	public static void LaunchInBrowser(String url) {
		BrowserLauncher launcher;
		try {
			launcher = new BrowserLauncher();
		} catch (Exception e1) {launcher = null;}
		
		if(launcher != null) {
			try {
				launcher.openURLinBrowser(url);
			} catch(Exception e) {}
		}
		else {
			try {
				Desktop.getDesktop().browse(new URI(url));
			} catch (Exception e) {}
		}
	}
	
	public static long getChecksumValue(Checksum checksum, String fname) {
		try {
			BufferedInputStream is = new BufferedInputStream(new FileInputStream(fname));
			byte[] bytes = new byte[1024];
			int len = 0;

			while ((len = is.read(bytes)) >= 0) {
				checksum.update(bytes, 0, len);
			}
			is.close();
		}
		catch (IOException e) {}
		return checksum.getValue();
	}
	
	public static void ShutDownPC(String rootPsswd) {
		//String[] shutdownCommand;
		if(SystemInfoHolder.IsWindows()) {
			String shutdownCommand = "shutdown.exe -s -t 0";
			try {
				Runtime.getRuntime().exec(shutdownCommand);
			} catch (Exception e) {}
		}
		else {
			String cmd="shutdown";
			if((new File("/sbin/shutdown")).exists()) {
				cmd="/sbin/shutdown";
			}
			else if((new File("/bin/shutdown")).exists()) {
				cmd="/bin/shutdown";
			}
			else if((new File("/usr/bin/shutdown")).exists()) {
				cmd="/usr/bin/shutdown";
			}
			String[] shutdownCommand = {"/bin/sh", "-c", "echo "+rootPsswd+" | sudo -S "+cmd+" -h now"};
			try {
				Runtime.getRuntime().exec(shutdownCommand);
			} catch (Exception e) {}
		}
		
		
	}
}