/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Starting class for downloader with main function.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
//import javax.swing.UnsupportedLookAndFeelException;


import org.apache.log4j.Logger;

import skdownloader.core.Common;

public class DownloadManager {
	private static String argname = "";
	private static String instDir="";
	private static String dataDir="";
	private static int livePort=6235;
	private static Logger logger; 

	public static void main(String[] args) {
		CheckRunningAndExit(livePort);
		int i=0;
		while (i < args.length)	{
			String s = args[i++];
			if(argname.equals("inst")) {
				instDir = s;
				argname="";
			}
			if(argname.equals("data")) {
				dataDir = s;
				argname="";
			}
			else if(s.equals("-instdir")) {
            	argname="inst";
            }
			else if(s.equals("-datadir")) {
            	argname="data";
            }
		}
		// Check if we are invoking from proper env. Otherwise exit gracefully.
		if(instDir.equals("")) {
			//install directory option not given. Check for env variable
			if(SystemInfoHolder.GetInstallDirFromEnv().equals("")) {
				//Last resort, use current directory.
				instDir=System.getProperty("user.dir");
			}
			else {
				instDir=SystemInfoHolder.GetInstallDirFromEnv();
			}
		}

		if(Common.CheckFileExists(instDir+File.separator+"prefs/history.xml", logger) != Common.OK)	{
			JOptionPane.showMessageDialog(null, "Can't find installation dir. Your installation might be corrupt. Please contact admin@toolsbysk.com");
			System.exit(1);
		}
		else {
			//Take absolute canonical path of install dir. We don't want relative paths.
			try	{
				instDir = (new File(instDir)).getCanonicalPath();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Can't find installation dir. Your installation might be corrupt. Please contact admin@toolsbysk.com");
				System.exit(1);
			}
		}
		
		if(dataDir.equals("")) {
			if(SystemInfoHolder.getDefaultDataDir().equals("")) {
				JOptionPane.showMessageDialog(null, "Could not find a data dir for your system. Pleae gve it using -dataDir");
				System.exit(1);
			}
			else if(! (new File(SystemInfoHolder.getDefaultDataDir())).exists()) {
				if(! (new File(SystemInfoHolder.getDefaultDataDir())).mkdir()) {
					JOptionPane.showMessageDialog(null, "Could find or create data dir "+SystemInfoHolder.getDefaultDataDir());
					System.exit(1);
				}
				else {
					dataDir=SystemInfoHolder.getDefaultDataDir();
				}
			}
			else {
				dataDir=SystemInfoHolder.getDefaultDataDir();
			}
		}
		
		if(! (new File(dataDir)).exists()) {
			JOptionPane.showMessageDialog(null, "Couldn't get a data dir..");
			System.exit(1);
		}
		else {
			//Check if we have all files in data dir and copy defaults if not...
			if(! (new File(dataDir+File.separator+"prefs")).exists()) {
				if(! (new File(dataDir+File.separator+"prefs")).mkdir()) {
					JOptionPane.showMessageDialog(null, "Could not create dir "+dataDir+File.separator+"prefs");
				}
			}
			if(! (new File(dataDir+File.separator+"logs")).exists()) {
				if(! (new File(dataDir+File.separator+"logs")).mkdir()) {
					JOptionPane.showMessageDialog(null, "Could not create dir "+dataDir+File.separator+"logs");
				}
			}
			if(! (new File(dataDir+File.separator+"prefs/history.xml")).exists()) {
				Common.CopyFile(instDir+File.separator+"/prefs/history.xml", dataDir+File.separator+"prefs/history.xml", logger);
			}
			if(! (new File(dataDir+File.separator+"prefs/runtimeoptions.xml")).exists()) {
				Common.CopyFile(instDir+File.separator+"/prefs/runtimeoptions.xml", dataDir+File.separator+"prefs/runtimeoptions.xml", logger);
			}
			
			if(! (new File(dataDir+File.separator+"updates")).exists()) {
				if(! (new File(dataDir+File.separator+"updates")).mkdir()) {
					JOptionPane.showMessageDialog(null, "Could not create dir "+dataDir+File.separator+"updates");
				}
			}
			if(! (new File(dataDir+File.separator+"downloads")).exists()) {
				if(! (new File(dataDir+File.separator+"downloads")).mkdir()) {
					JOptionPane.showMessageDialog(null, "Could not create dir "+dataDir+File.separator+"downloads");
				}
			}
			if(! (new File(dataDir+File.separator+"torrent_base_tmp")).exists()) {
				if(! (new File(dataDir+File.separator+"torrent_base_tmp")).mkdir()) {
					JOptionPane.showMessageDialog(null, "Could not create dir "+dataDir+File.separator+"torrent_base_tmp");
				}
			}
			
		}
		// Initialize logger path.
		String logpath = dataDir+File.separator+"logs"+File.separator+"log.txt";
		logpath.replaceAll("\\\\", "/");
		System.setProperty("logpath", logpath);
		if (System.getProperty("loglevel") != null) {
			System.setProperty("loglevel", System.getProperty("loglevel"));
		} else {
			System.setProperty("loglevel", "INFO");
		}
		
		logger = Logger.getLogger(DownloadManager.class); 
		logger.info("Starting SKDownloader");
		JFrame.setDefaultLookAndFeelDecorated(true);
		java.awt.EventQueue.invokeLater(new Runnable() {public void run() {
			try {
				System.setProperty("swing.aatext", "true");
				System.setProperty("awt.useSystemAAFontSettings", "lcd");
				// Set torrent base work dir path. This is were the tmp files along with stored torrents will be kept.
				System.setProperty("torrentbase.config.path", dataDir+File.separator+"torrent_base_tmp");
				SystemInfoHolder sysInfoHolder = new SystemInfoHolder(instDir,dataDir);
				sysInfoHolder.LIVEPORT=livePort;
				//Create timestamp file to indicate running process
				File runningFile = Common.CreateFile(sysInfoHolder.getDataDir()+File.separator+"skdownloader.running", logger);
				if(runningFile.exists())
					runningFile.deleteOnExit();
				MainFrame mainFrame = new MainFrame(sysInfoHolder);
				mainFrame.ShowFrame();
			} catch (Exception e) {
				logger.error("Exception", e);
			}
		}});
	}
	
	private static void CheckRunningAndExit(int port) {
		int alive=0; //0-not running, 1-starting, 2-alive, 3-shutting down
		try {
			Socket soc = new Socket("localhost", port);
			PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
			out.println("skdownloader_alive_comeforward");
			BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
			String line = in.readLine();
			if(line.equals("starting")) {
				alive=1;
			}
			if(line.equals("is_alive")) {
				alive=2;
			}
			else if(line.equals("shutting_down")) {
				alive=3;
			}
			out.close();
			in.close();
			soc.close();
		}
		catch(Exception e){}
		if(alive==1) {
			JOptionPane.showMessageDialog(null,"Another instance of SKDownloader is already running."+System.getProperty("line.separator")+"Please wait for some time and try again");
			System.exit(0);
		}
		else if(alive==2) {
			System.exit(0);
		}
		if(alive==3) {
			JOptionPane.showMessageDialog(null,"SKDownloader is being shut down and has not exited."+System.getProperty("line.separator")+"Please wait for some time and try again");
			System.exit(0);
		}
	}		
}


