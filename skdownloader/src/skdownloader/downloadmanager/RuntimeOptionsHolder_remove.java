

package skdownloader.downloadmanager;

public class RuntimeOptionsHolder_remove implements Cloneable
{	
	public int SCHEDULESTARTOPTION;
	public static int SCHEDULESTARTDISABLED=0;
	public static int SCHEDULESTARTATTIME=1; 
	public static int SCHEDULESTARTFORDAYS = 2;
	public boolean SCHEDULESTARTONSATURDAY;
	public boolean SCHEDULESTARTONSUNDAY;
	public boolean SCHEDULESTARTONMONDAY;
	public boolean SCHEDULESTARTONTUESDAY;
	public boolean SCHEDULESTARTONWEDNESDAY;
	public boolean SCHEDULESTARTONTHURSDAY;
	public boolean SCHEDULESTARTONFRIDAY;
	public String SCHEDULESTARTTIME;
	public String SCHEDULESTARTDAYSTOCHECK;
	public int SCHEDULESTOPOPTION;
	public static int SCHEDULESTOPDISABLED=0;
	public static int SCHEDULESTOPATTIME=1;
	public String SCHEDULESTOPTIME;
	
	/*************************************************************
	 * 
	 * Function  : CopyFrom
	 * Arguments : RuntimeOptionsHolder object
	 * Return    : void
	 * Notes     : Copies variable values from argument object to this object.
	 * 
	 *************************************************************/
	public void CopyFrom(RuntimeOptionsHolder_remove runtimeOptionsHolder)
	{
		SCHEDULESTARTOPTION=runtimeOptionsHolder.SCHEDULESTARTOPTION;
		SCHEDULESTARTONSATURDAY=runtimeOptionsHolder.SCHEDULESTARTONSATURDAY;
		SCHEDULESTARTONSUNDAY=runtimeOptionsHolder.SCHEDULESTARTONSUNDAY;
		SCHEDULESTARTONMONDAY=runtimeOptionsHolder.SCHEDULESTARTONMONDAY;
		SCHEDULESTARTONTUESDAY=runtimeOptionsHolder.SCHEDULESTARTONTUESDAY;
		SCHEDULESTARTONWEDNESDAY=runtimeOptionsHolder.SCHEDULESTARTONWEDNESDAY;
		SCHEDULESTARTONTHURSDAY=runtimeOptionsHolder.SCHEDULESTARTONTHURSDAY;
		SCHEDULESTARTONFRIDAY=runtimeOptionsHolder.SCHEDULESTARTONFRIDAY;
		SCHEDULESTARTTIME=runtimeOptionsHolder.SCHEDULESTARTTIME;
		SCHEDULESTARTDAYSTOCHECK=runtimeOptionsHolder.SCHEDULESTARTDAYSTOCHECK;
		SCHEDULESTOPOPTION=runtimeOptionsHolder.SCHEDULESTOPOPTION;
		SCHEDULESTOPTIME=runtimeOptionsHolder.SCHEDULESTOPTIME;
	}
	
	/*************************************************************
	 * 
	 * Function  : clone
	 * Arguments : void
	 * Return    : cloned object
	 * Notes     : Creates a copy of this object
	 * 
	 *************************************************************/
	public Object clone()
    {
        try
        {
            return super.clone();
        }
        catch( CloneNotSupportedException e )
    	{
            return null;
        }
    }
}
