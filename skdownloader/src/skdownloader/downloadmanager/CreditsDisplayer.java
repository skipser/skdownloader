/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Credit displayer
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/17/09 - Remove from tab and runs as seperate frame
 *   arun         06/04/09 - Fix fractal thread not exiting with close button
 *
 ****************************************************************************************/

package skdownloader.downloadmanager;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;

import org.pushingpixels.substance.api.SubstanceLookAndFeel;

import skdownloader.downloadmanager.guicomponents.fractal.FractalTrochoidCurveDisplayer;

public class CreditsDisplayer
{
	MainFrame parentFrame;
	JPanel motherPanel;
	JPanel creditsPanel;
	JPanel fractalPanel;
	JFrame motherFrame;
	FractalTrochoidCurveDisplayer fractalObj;
	int tabIndex;
	
	public CreditsDisplayer(MainFrame parentFrame)
	{
		this.parentFrame=parentFrame;

	}
	
	public void Show()
	{
		motherFrame = new JFrame("Credits");
		motherFrame.setIconImage(new ImageIcon(parentFrame.getClass().getResource("guicomponents/images/skdownloader32.png")).getImage());
		motherFrame.setMaximumSize(new Dimension(600,400));
		motherFrame.setPreferredSize(new Dimension(600,400));
		motherFrame.setMinimumSize(new Dimension(600, 400));
		motherFrame.setResizable(false);
		motherPanel = new JPanel(new BorderLayout());
		creditsPanel = new JPanel(new BorderLayout());
		Color bgcol = Color.black;
		creditsPanel.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, new Double(1));
		creditsPanel.repaint();
		creditsPanel.setBackground(bgcol);
		JTextPane textPane = new JTextPane();
		textPane.setBackground(bgcol);
		Style style = textPane.addStyle("StyleName", null);
        StyleConstants.setBackground(style, Color.blue);
		append(textPane, Color.white, Font.getFont("SansSerif"), 18, false, "Created by:\n");
		append(textPane, Color.red, Font.getFont("SansSerif"), 13, true, "skdownloader.com\n");
		append(textPane, Color.red, Font.getFont("SansSerif"), 5, true, "\n");
		append(textPane, Color.white, Font.getFont("SansSerif"), 18, false, "Contact us:\n");
		appendLink(textPane, Color.red, Font.getFont("SansSerif"), 13, true, "arun@skipser.com\n", "arun@skipser.com");
		textPane.setEditable(false);
		//textPane.setBounds(10, 10, 500, 200);
		creditsPanel.add(textPane);
		motherPanel.add(creditsPanel, BorderLayout.WEST);
		
		fractalPanel = new JPanel(new BorderLayout());
		fractalPanel.setPreferredSize(new Dimension(400, 400));
		fractalPanel.setMinimumSize(new Dimension(400, 400));
		fractalPanel.setMaximumSize(new Dimension(400, 400));
		motherPanel.add(fractalPanel, BorderLayout.CENTER);
		motherFrame.add(motherPanel);
		motherFrame.setLocation(parentFrame.GetFrame().getX()+40, parentFrame.GetFrame().getY()+60);
		motherFrame.setVisible(true);
		
		fractalObj = new FractalTrochoidCurveDisplayer(fractalPanel);
		fractalObj.Show();
		
		motherFrame.addWindowListener(new WindowAdapter(){
		      public void windowClosing(WindowEvent we){
		    	  Stop();
		      }});
	}
	
	public void append(JTextPane textPane, Color c, Font f, int sz, boolean isbold, String s)
    {
		SimpleAttributeSet attributes = new SimpleAttributeSet();
		StyleConstants.setForeground(attributes, c);
		StyleConstants.setFontSize(attributes,sz);
		StyleConstants.setBold(attributes, isbold);
		StyleConstants.setBackground(attributes, Color.black);
		Document d = textPane.getDocument();
		try
		{
			d.insertString(d.getLength(), s, attributes);
		} catch (Exception e) {
		}
		attributes=null;
    }
	
    public void appendLink(JTextPane textPane, Color c, Font f, int sz, boolean isbold, String s, String url)
    {
    	SimpleAttributeSet attributes = new SimpleAttributeSet();
    	StyleConstants.setForeground(attributes, c);
    	StyleConstants.setFontSize(attributes,sz);
    	StyleConstants.setBold(attributes, isbold);
    	//StyleConstants.setUnderline(attributes, true);
    	Document d = textPane.getDocument();
    	attributes.addAttribute(HTML.Attribute.HREF, url);
    	try
    	{
    		d.insertString(d.getLength(), s, attributes);
    	} catch (Exception ble) {
    	}
    	attributes=null;
    }

    public void Stop()
    {
    	fractalObj.Stop();
    	fractalObj=null;
    	motherFrame.dispose();
    }
    
	//public void stateChanged(ChangeEvent e)
	//{
	//	if(tabbedPane.getSelectedIndex() == tabIndex)
	//	{
	///		fractalObj.Resume();
	//	}
	///	else
	//	{
	//		fractalObj.Pause();
	//	}
	//}
}