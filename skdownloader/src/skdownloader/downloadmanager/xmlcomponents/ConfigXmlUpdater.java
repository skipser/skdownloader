/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    XML updater for config.xml
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/18/09 - Support for DEBUGMODE
 *   arun         05/19/09 - Support for theme
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.xmlcomponents;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

import skdownloader.downloadmanager.SystemInfoHolder;
//import skdownloader.downloadmanager.utils.EncryptDecrypt;

public class ConfigXmlUpdater
{
	private SystemInfoHolder sysInfoHolder;
	
	DocumentBuilderFactory dbf;
	Document doc;
	int maxId;

	
	public ConfigXmlUpdater(SystemInfoHolder sysInfoHolder)
	{
		this.sysInfoHolder = sysInfoHolder;
		maxId=0;
		try
		{
			dbf= DocumentBuilderFactory.newInstance();
			dbf.setIgnoringElementContentWhitespace(false);
			DocumentBuilder dBuilder = dbf.newDocumentBuilder();
			doc = dBuilder.parse(new File(sysInfoHolder.GetConfigXml()));
			//NormalizeIds();
		} catch (Exception e) {	
		}
	}
	
	private String GetValueForNamedNode(String nodeName)
	{
		String val="";
		if((doc.getElementsByTagName(nodeName)).item(0).getChildNodes().getLength() > 0)
		{
			val = (doc.getElementsByTagName(nodeName)).item(0).getFirstChild().getNodeValue();
		}
		
		while((! val.equals("")) &&
			  (doc.getElementsByTagName(val).getLength() >0))
		{
			val = doc.getElementsByTagName(val).item(0).getFirstChild().getNodeValue();
		}
		return val;
	}
	
	/*private void SetValueForNamedNode(String nodeName, String value)
	{
		String val="";
		if((doc.getElementsByTagName(nodeName)).item(0).getChildNodes().getLength() > 0)
		{
			val = (doc.getElementsByTagName(nodeName)).item(0).getFirstChild().getNodeValue();
		}

		if((! val.equals("")) &&
		   (doc.getElementsByTagName(val).getLength() >0))
		{
			//This node referes to some other node. Do not edit it.
		}
		else
		{
			if(! val.equals(""))
			{
				(doc.getElementsByTagName(nodeName)).item(0).getFirstChild().setNodeValue(value);
			}
			else
			{
				(doc.getElementsByTagName(nodeName)).item(0).appendChild(doc.createTextNode(value));
			}
		}
	}*/
	
	public void UpdateSystemVars()
	{
		sysInfoHolder.VERSION=GetValueForNamedNode("VERSION");
		sysInfoHolder.COMPANYURL=GetValueForNamedNode("COMPANYURL");
		sysInfoHolder.UPDATESURL=GetValueForNamedNode("UPDATESURL");
		sysInfoHolder.COPYRIGHT=GetValueForNamedNode("COPYRIGHT");
		sysInfoHolder.ADS_SRC_LOCATION=GetValueForNamedNode("ADS_SRC_LOCATION");
		sysInfoHolder.APP_MIN_HEIGHT=Integer.parseInt(GetValueForNamedNode("APP_MIN_HEIGHT"));
		sysInfoHolder.APP_MIN_WIDTH=Integer.parseInt(GetValueForNamedNode("APP_MIN_WIDTH"));
		sysInfoHolder.VERY_LARGE_NUMBER=Integer.parseInt(GetValueForNamedNode("VERY_LARGE_NUMBER"));
		sysInfoHolder.TOPBUTTONSPANEL_MIN_HEIGHT=Integer.parseInt(GetValueForNamedNode("TOPBUTTONSPANEL_MIN_HEIGHT"));
		sysInfoHolder.TOPBUTTONSPANEL_MAX_HEIGHT=Integer.parseInt(GetValueForNamedNode("TOPBUTTONSPANEL_MAX_HEIGHT"));
		sysInfoHolder.TOPBUTTONSPANEL_MIN_WIDTH=Integer.parseInt(GetValueForNamedNode("TOPBUTTONSPANEL_MIN_WIDTH"));
		sysInfoHolder.TOPBUTTONSPANEL_MAX_WIDTH=Integer.parseInt(GetValueForNamedNode("TOPBUTTONSPANEL_MAX_WIDTH"));
		sysInfoHolder.ADSPANEL_MIN_HEIGHT=Integer.parseInt(GetValueForNamedNode("ADSPANEL_MIN_HEIGHT"));
		sysInfoHolder.ADSPANEL_MIN_WIDTH=Integer.parseInt(GetValueForNamedNode("ADSPANEL_MIN_WIDTH"));
	}
	
	public void WriteXml()
	{
		// We should never write to config_gui.xml as this should contain only static data and updates will just overwrite this
		// file. Any non static data should go into runtimeoptions.xml
/*		SetValueForNamedNode("APP_MIN_HEIGHT", Integer.toString(sysInfoHolder.APP_MIN_HEIGHT));
		SetValueForNamedNode("APP_MIN_WIDTH", Integer.toString(sysInfoHolder.APP_MIN_WIDTH));
		SetValueForNamedNode("VERY_LARGE_NUMBER", Integer.toString(sysInfoHolder.VERY_LARGE_NUMBER));
		SetValueForNamedNode("TOPBUTTONSPANEL_MIN_HEIGHT", Integer.toString(sysInfoHolder.TOPBUTTONSPANEL_MIN_HEIGHT));
		SetValueForNamedNode("TOPBUTTONSPANEL_MAX_HEIGHT", Integer.toString(sysInfoHolder.TOPBUTTONSPANEL_MAX_HEIGHT));
		SetValueForNamedNode("TOPBUTTONSPANEL_MIN_WIDTH", Integer.toString(sysInfoHolder.TOPBUTTONSPANEL_MIN_WIDTH));
		SetValueForNamedNode("TOPBUTTONSPANEL_MAX_WIDTH", Integer.toString(sysInfoHolder.TOPBUTTONSPANEL_MAX_WIDTH));
		SetValueForNamedNode("ADSPANEL_MIN_HEIGHT", Integer.toString(sysInfoHolder.ADSPANEL_MIN_HEIGHT));
		SetValueForNamedNode("ADSPANEL_MIN_WIDTH", Integer.toString(sysInfoHolder.ADSPANEL_MIN_WIDTH));
		
		try
		{
			// 	Prepare the DOM document for writing
			DOMSource source = new DOMSource(doc);
	    
			// Prepare the output file
			File file = new File(sysInfoHolder.GetConfigXml());
			StreamResult result = new StreamResult(file);
	    
			// Write the DOM document to the file
			Transformer xformer = TransformerFactory.newInstance().newTransformer();
			xformer.setOutputProperty("indent", "yes");
			xformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
		} catch (TransformerException e) {
		}*/
	}
}
