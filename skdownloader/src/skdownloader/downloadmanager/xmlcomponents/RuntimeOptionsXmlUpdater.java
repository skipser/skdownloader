/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    XML updater for config.xml
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/18/09 - Support for DEBUGMODE
 *   arun         05/19/09 - Support for theme
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.xmlcomponents;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import skdownloader.downloadmanager.SystemInfoHolder;
import skdownloader.downloadmanager.utils.EncryptDecrypt;

public class RuntimeOptionsXmlUpdater
{
	private SystemInfoHolder sysInfoHolder;
	
	DocumentBuilderFactory dbf;
	Document doc;
	private Logger logger = Logger.getLogger(RuntimeOptionsXmlUpdater.class);
	
	public RuntimeOptionsXmlUpdater(SystemInfoHolder sysInfoHolder) {
		this.sysInfoHolder = sysInfoHolder;
		try
		{
			dbf= DocumentBuilderFactory.newInstance();
			dbf.setIgnoringElementContentWhitespace(false);
			DocumentBuilder dBuilder = dbf.newDocumentBuilder();
			doc = dBuilder.parse(new File(sysInfoHolder.GetRuntimeOptionsXml()));
			//NormalizeIds();
		} catch (Exception e) {
			logger.error("Exception", e);
		}
	}
	
	private String GetValueForNamedNode(String nodeName, String def)
	{
		String val="";
		try {
			if((doc.getElementsByTagName(nodeName)).item(0).getChildNodes().getLength() > 0){
				val = (doc.getElementsByTagName(nodeName)).item(0).getFirstChild().getNodeValue();
			}
			
			while((! val.equals("")) &&	(doc.getElementsByTagName(val).getLength() >0)) {
				val = doc.getElementsByTagName(val).item(0).getFirstChild().getNodeValue();
			}
		}catch(Exception e) {return def;}
		return val;
	}
	
	private void SetValueForNamedNode(String nodeName, String value) {
		String val="";
		//If node does not exist, add it.
		if((doc.getElementsByTagName(nodeName)).getLength() == 0) {
			Node name = doc.createElement(nodeName);
			name.appendChild(doc.createTextNode(value));
			doc.getDocumentElement().appendChild(name);
		}
		if((doc.getElementsByTagName(nodeName)).item(0).getChildNodes().getLength() > 0) {
			val = (doc.getElementsByTagName(nodeName)).item(0).getFirstChild().getNodeValue();
		}

		if((! val.equals("")) && (doc.getElementsByTagName(val).getLength() >0)) {
			//This node referes to some other node. Do not edit it.
		}
		else {
			if(! val.equals("")) {
				(doc.getElementsByTagName(nodeName)).item(0).getFirstChild().setNodeValue(value);
			}
			else {
				(doc.getElementsByTagName(nodeName)).item(0).appendChild(doc.createTextNode(value));
			}
		}
	}
	
	public void UpdateRuntimeOptions() {
		sysInfoHolder.OS=GetValueForNamedNode("OS", "win32");
		sysInfoHolder.FIRSTRUN=Integer.parseInt(GetValueForNamedNode("FIRSTRUN", "1"));
		sysInfoHolder.CLOSETOTRAY=Boolean.parseBoolean(GetValueForNamedNode("CLOSETOTRAY",  "true"));
		sysInfoHolder.TRAYENABLED=Boolean.parseBoolean(GetValueForNamedNode("TRAYENABLED",  "true"));
		sysInfoHolder.APP_PREFERRED_HEIGHT=Integer.parseInt(GetValueForNamedNode("APP_PREFERRED_HEIGHT", "500"));
		sysInfoHolder.APP_PREFERRED_WIDTH=Integer.parseInt(GetValueForNamedNode("APP_PREFERRED_WIDTH","800"));
		sysInfoHolder.APP_START_X=Integer.parseInt(GetValueForNamedNode("APP_START_X","0"));
		sysInfoHolder.APP_START_Y=Integer.parseInt(GetValueForNamedNode("APP_START_Y","0"));
		sysInfoHolder.TORRENT_DETAILS_PREFERRED_HEIGHT=Integer.parseInt(GetValueForNamedNode("TORRENT_DETAILS_PREFERRED_HEIGHT","460"));
		sysInfoHolder.TORRENT_DETAILS_PREFERRED_WIDTH=Integer.parseInt(GetValueForNamedNode("TORRENT_DETAILS_PREFERRED_WIDTH","365"));
		sysInfoHolder.BROWSERMANAGER_SPLILTPANE_PREFERRED_DIVIDER=Integer.parseInt(GetValueForNamedNode("BROWSERMANAGER_SPLILTPANE_PREFERRED_DIVIDER","150"));
		sysInfoHolder.DOWNLOADSTABLE_WIDTH_ARR_STR=GetValueForNamedNode("DOWNLOADSTABLE_WIDTH_ARR_STR", "");
		sysInfoHolder.DOWNLOADSTABLE_ORDER_ARR_STR=GetValueForNamedNode("DOWNLOADSTABLE_ORDER_ARR_STR", "");
		sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE=Integer.parseInt(GetValueForNamedNode("DOWNLOADSTABLE_COLS_VISIBLE", "10"));
		sysInfoHolder.DEBUGMODE=Integer.parseInt(GetValueForNamedNode("DEBUGMODE","0"));
		sysInfoHolder.SetTheme(Integer.parseInt(GetValueForNamedNode("SELECTEDTHEME","0")));
		sysInfoHolder.SHOWPOPPUPWHENDOWNLOADFINISH=Boolean.parseBoolean(GetValueForNamedNode("SHOWPOPPUPWHENDOWNLOADFINISH","true"));
		sysInfoHolder.SHOWDETAILSONADDINGDOWNLOAD=Boolean.parseBoolean(GetValueForNamedNode("SHOWDETAILSONADDINGDOWNLOAD","true"));
		sysInfoHolder.SAVETOOPTION=Integer.parseInt(GetValueForNamedNode("SAVETOOPTION","1"));
		sysInfoHolder.PREVIOUSDOWNLOADLOCATION=GetValueForNamedNode("PREVIOUSDOWNLOADLOCATION","");
		sysInfoHolder.PREVIOUSTORRENTINPUTLOCATION=GetValueForNamedNode("PREVIOUSTORRENTINPUTLOCATION","");
		sysInfoHolder.DEFAULTDOWNLOADLOCATION=GetValueForNamedNode("DEFAULTDOWNLOADLOCATION","");
		sysInfoHolder.INTERNETCONNECTIONOPTION=Integer.parseInt(GetValueForNamedNode("INTERNETCONNECTIONOPTION","0"));
		sysInfoHolder.USEPROXY=Boolean.parseBoolean(GetValueForNamedNode("USEPROXY","false"));
		sysInfoHolder.SOCKSPROXYSERVER=GetValueForNamedNode("SOCKSPROXYSERVER","");
		sysInfoHolder.SOCKSPROXYPORT=Integer.parseInt(GetValueForNamedNode("SOCKSPROXYPORT","80"));
		sysInfoHolder.SOCKSUSEAUTHENTICATION=Boolean.parseBoolean(GetValueForNamedNode("SOCKSUSEAUTHENTICATION","false"));
		sysInfoHolder.SOCKSPROXYUSER=GetValueForNamedNode("SOCKSPROXYUSER","");
		sysInfoHolder.SOCKSPROXYPASSWORD=GetValueForNamedNode("SOCKSPROXYPASSWORD","");
		sysInfoHolder.SOCKSPROXYNOTUSECACHE=Boolean.parseBoolean(GetValueForNamedNode("SOCKSPROXYNOTUSECACHE","false"));
		sysInfoHolder.HTTPPROXYSERVER=GetValueForNamedNode("HTTPPROXYSERVER","");
		sysInfoHolder.HTTPPROXYPORT=Integer.parseInt(GetValueForNamedNode("HTTPPROXYPORT","80"));
		sysInfoHolder.HTTPUSEAUTHENTICATION=Boolean.parseBoolean(GetValueForNamedNode("HTTPUSEAUTHENTICATION","false"));
		sysInfoHolder.HTTPPROXYUSER=GetValueForNamedNode("HTTPPROXYUSER","");
		sysInfoHolder.HTTPPROXYPASSWORD=EncryptDecrypt.GetDecryptedString(GetValueForNamedNode("HTTPPROXYPASSWORD",""));
		sysInfoHolder.HTTPPROXYNOTUSECACHE=Boolean.parseBoolean(GetValueForNamedNode("HTTPPROXYNOTUSECACHE","false"));
		sysInfoHolder.PROXYEXCEPTIONS=GetValueForNamedNode("PROXYEXCEPTIONS","");
		sysInfoHolder.USEDELAYTIMEFROMSERVER=Boolean.parseBoolean(GetValueForNamedNode("USEDELAYTIMEFROMSERVER","true"));
		sysInfoHolder.TIMEOUTDELAY=Integer.parseInt(GetValueForNamedNode("TIMEOUTDELAY","60"));
		sysInfoHolder.CONNECTRETRYLIMIT=Integer.parseInt(GetValueForNamedNode("CONNECTRETRYLIMIT","10"));
		sysInfoHolder.DOWNLOADSTARTOPTION=Integer.parseInt(GetValueForNamedNode("DOWNLOADSTARTOPTION","0"));
		sysInfoHolder.AUTOACCELERATE=Boolean.parseBoolean(GetValueForNamedNode("AUTOACCELERATE","true"));
		sysInfoHolder.AUTOUPDATE=Boolean.parseBoolean(GetValueForNamedNode("AUTOUPDATE","true"));
		sysInfoHolder.MINSIZEFORACCELERATION=Integer.parseInt(GetValueForNamedNode("MINSIZEFORACCELERATION","500"));
		sysInfoHolder.LASTUDATEDAT=Long.parseLong(GetValueForNamedNode("LASTUDATEDAT","0"));
		sysInfoHolder.UPDATEOPTION=Integer.parseInt(GetValueForNamedNode("UPDATEOPTION","2"));
		
		sysInfoHolder.SCHEDULESTARTOPTION=Integer.parseInt(GetValueForNamedNode("SCHEDULESTARTOPTION","0"));
		sysInfoHolder.SCHEDULESTARTTIME=GetValueForNamedNode("SCHEDULESTARTTIME","0200AM");
		sysInfoHolder.SCHEDULESTARTONSATURDAY=Boolean.parseBoolean(GetValueForNamedNode("SCHEDULESTARTONSATURDAY","false"));
		sysInfoHolder.SCHEDULESTARTONSUNDAY=Boolean.parseBoolean(GetValueForNamedNode("SCHEDULESTARTONSUNDAY","false"));
		sysInfoHolder.SCHEDULESTARTONMONDAY=Boolean.parseBoolean(GetValueForNamedNode("SCHEDULESTARTONMONDAY","false"));
		sysInfoHolder.SCHEDULESTARTONTUESDAY=Boolean.parseBoolean(GetValueForNamedNode("SCHEDULESTARTONTUESDAY","false"));
		sysInfoHolder.SCHEDULESTARTONWEDNESDAY=Boolean.parseBoolean(GetValueForNamedNode("SCHEDULESTARTONWEDNESDAY","false"));
		sysInfoHolder.SCHEDULESTARTONTHURSDAY=Boolean.parseBoolean(GetValueForNamedNode("SCHEDULESTARTONTHURSDAY","false"));
		sysInfoHolder.SCHEDULESTARTONFRIDAY=Boolean.parseBoolean(GetValueForNamedNode("SCHEDULESTARTONFRIDAY","false"));
		sysInfoHolder.SCHEDULESTOPOPTION=Integer.parseInt(GetValueForNamedNode("SCHEDULESTOPOPTION","0"));
		sysInfoHolder.SCHEDULESTOPTIME=GetValueForNamedNode("SCHEDULESTOPTIME","0100PPM");
		sysInfoHolder.TORRENTDOWNLOADSTARTOPTION=Integer.parseInt(GetValueForNamedNode("TORRENTDOWNLOADSTARTOPTION","1"));
		sysInfoHolder.MAXGLOBALTORRENTDOWNLOADSPEED=Integer.parseInt(GetValueForNamedNode("MAXGLOBALTORRENTDOWNLOADSPEED","0"));
		sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEED=Integer.parseInt(GetValueForNamedNode("MAXGLOBALTORRENTUPLOADSPEED","0"));
		sysInfoHolder.GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED=Boolean.parseBoolean(GetValueForNamedNode("GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED", "false"));
		sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEEDONSEEDING=Integer.parseInt(GetValueForNamedNode("MAXGLOBALTORRENTUPLOADSPEEDONSEEDING","0"));
		sysInfoHolder.TORRENT_LISTEN_PORT=Integer.parseInt(GetValueForNamedNode("TORRENT_LISTEN_PORT","19993"));
		sysInfoHolder.TORRENTREMOVEONSHARERATIOOPTION=Boolean.parseBoolean(GetValueForNamedNode("TORRENTREMOVEONSHARERATIOOPTION", "false"));
		sysInfoHolder.SHARERATIOTOREMOVETORRENT=Float.parseFloat(GetValueForNamedNode("SHARERATIOTOREMOVETORRENT","0"));
		sysInfoHolder.TORRENTREMOVEONSEEDTIMEOPTION=Boolean.parseBoolean(GetValueForNamedNode("TORRENTREMOVEONSEEDTIMEOPTION", "false"));
		sysInfoHolder.SEEDTIMETOREMOVETORRENT=Integer.parseInt(GetValueForNamedNode("SEEDTIMETOREMOVETORRENT","0"));
		
		sysInfoHolder.PEERTABLE_WIDTH_COL0=Integer.parseInt(GetValueForNamedNode("PEERTABLE_WIDTH_COL0","94"));
		sysInfoHolder.PEERTABLE_WIDTH_COL1=Integer.parseInt(GetValueForNamedNode("PEERTABLE_WIDTH_COL1","156"));
		sysInfoHolder.PEERTABLE_WIDTH_COL2=Integer.parseInt(GetValueForNamedNode("PEERTABLE_WIDTH_COL2","77"));
		sysInfoHolder.FILETABLE_WIDTH_COL0=Integer.parseInt(GetValueForNamedNode("FILETABLE_WIDTH_COL0","22"));
		sysInfoHolder.FILETABLE_WIDTH_COL1=Integer.parseInt(GetValueForNamedNode("FILETABLE_WIDTH_COL1","123"));
		sysInfoHolder.FILETABLE_WIDTH_COL2=Integer.parseInt(GetValueForNamedNode("FILETABLE_WIDTH_COL2","54"));
		sysInfoHolder.FILETABLE_WIDTH_COL3=Integer.parseInt(GetValueForNamedNode("FILETABLE_WIDTH_COL3","53"));
		sysInfoHolder.FILETABLE_WIDTH_COL4=Integer.parseInt(GetValueForNamedNode("FILETABLE_WIDTH_COL4","63"));
		
		sysInfoHolder.SEARCHFRAME_WIDTH=Integer.parseInt(GetValueForNamedNode("SEARCHFRAME_WIDTH","935"));
		sysInfoHolder.SEARCHFRAME_HEIGHT=Integer.parseInt(GetValueForNamedNode("SEARCHFRAME_HEIGHT","430"));
		
		sysInfoHolder.OPTIONSFRAME_WIDTH=Integer.parseInt(GetValueForNamedNode("OPTIONSFRAME_WIDTH","650"));
		sysInfoHolder.OPTIONSFRAME_HEIGHT=Integer.parseInt(GetValueForNamedNode("OPTIONSFRAME_HEIGHT","450"));
		sysInfoHolder.HIDEUPDATEREMINDERFORVERSION=GetValueForNamedNode("HIDEUPDATEREMINDERFORVERSION","100");
		
	}
	
	public synchronized void WriteXml()
	{
		SetValueForNamedNode("OS", sysInfoHolder.OS);
		SetValueForNamedNode("FIRSTRUN", Integer.toString(sysInfoHolder.FIRSTRUN));
		SetValueForNamedNode("CLOSETOTRAY", Boolean.toString(sysInfoHolder.CLOSETOTRAY));
		SetValueForNamedNode("TRAYENABLED", Boolean.toString(sysInfoHolder.TRAYENABLED));
		SetValueForNamedNode("APP_PREFERRED_HEIGHT", Integer.toString(sysInfoHolder.APP_PREFERRED_HEIGHT));
		SetValueForNamedNode("APP_PREFERRED_WIDTH", Integer.toString(sysInfoHolder.APP_PREFERRED_WIDTH));
		SetValueForNamedNode("TORRENT_DETAILS_PREFERRED_HEIGHT", Integer.toString(sysInfoHolder.TORRENT_DETAILS_PREFERRED_HEIGHT));
		SetValueForNamedNode("TORRENT_DETAILS_PREFERRED_WIDTH", Integer.toString(sysInfoHolder.TORRENT_DETAILS_PREFERRED_WIDTH));
		SetValueForNamedNode("APP_START_X", Integer.toString(sysInfoHolder.APP_START_X));
		SetValueForNamedNode("APP_START_Y", Integer.toString(sysInfoHolder.APP_START_Y));
		SetValueForNamedNode("BROWSERMANAGER_SPLILTPANE_PREFERRED_DIVIDER", Integer.toString(sysInfoHolder.BROWSERMANAGER_SPLILTPANE_PREFERRED_DIVIDER));
		SetValueForNamedNode("DOWNLOADSTABLE_WIDTH_ARR_STR", sysInfoHolder.DOWNLOADSTABLE_WIDTH_ARR_STR);
		SetValueForNamedNode("DOWNLOADSTABLE_ORDER_ARR_STR", sysInfoHolder.DOWNLOADSTABLE_ORDER_ARR_STR);
		SetValueForNamedNode("DOWNLOADSTABLE_COLS_VISIBLE", Integer.toString(sysInfoHolder.DOWNLOADSTABLE_COLS_VISIBLE));
		SetValueForNamedNode("DEBUGMODE", Integer.toString(sysInfoHolder.DEBUGMODE));
		SetValueForNamedNode("SELECTEDTHEME", Integer.toString(sysInfoHolder.SELECTEDTHEME));
		SetValueForNamedNode("SHOWPOPPUPWHENDOWNLOADFINISH", Boolean.toString(sysInfoHolder.SHOWPOPPUPWHENDOWNLOADFINISH));
		SetValueForNamedNode("SHOWDETAILSONADDINGDOWNLOAD", Boolean.toString(sysInfoHolder.SHOWDETAILSONADDINGDOWNLOAD));
		SetValueForNamedNode("SAVETOOPTION", Integer.toString(sysInfoHolder.SAVETOOPTION));
		SetValueForNamedNode("PREVIOUSDOWNLOADLOCATION", sysInfoHolder.PREVIOUSDOWNLOADLOCATION);
		SetValueForNamedNode("PREVIOUSTORRENTINPUTLOCATION", sysInfoHolder.PREVIOUSTORRENTINPUTLOCATION);
		SetValueForNamedNode("DEFAULTDOWNLOADLOCATION", sysInfoHolder.DEFAULTDOWNLOADLOCATION);
		SetValueForNamedNode("INTERNETCONNECTIONOPTION", Integer.toString(sysInfoHolder.INTERNETCONNECTIONOPTION));
		SetValueForNamedNode("USEPROXY", Boolean.toString(sysInfoHolder.USEPROXY));
		SetValueForNamedNode("SOCKSPROXYSERVER", sysInfoHolder.SOCKSPROXYSERVER);
		SetValueForNamedNode("SOCKSPROXYPORT", Integer.toString(sysInfoHolder.SOCKSPROXYPORT));
		SetValueForNamedNode("SOCKSUSEAUTHENTICATION", Boolean.toString(sysInfoHolder.SOCKSUSEAUTHENTICATION));
		SetValueForNamedNode("SOCKSPROXYUSER", sysInfoHolder.SOCKSPROXYUSER);
		SetValueForNamedNode("SOCKSPROXYPASSWORD", sysInfoHolder.SOCKSPROXYPASSWORD);
		SetValueForNamedNode("SOCKSPROXYNOTUSECACHE", Boolean.toString(sysInfoHolder.SOCKSPROXYNOTUSECACHE));
		SetValueForNamedNode("HTTPPROXYSERVER", sysInfoHolder.HTTPPROXYSERVER);
		SetValueForNamedNode("HTTPPROXYPORT", Integer.toString(sysInfoHolder.HTTPPROXYPORT));
		SetValueForNamedNode("HTTPUSEAUTHENTICATION", Boolean.toString(sysInfoHolder.HTTPUSEAUTHENTICATION));
		SetValueForNamedNode("HTTPPROXYUSER", sysInfoHolder.HTTPPROXYUSER);
		SetValueForNamedNode("HTTPPROXYPASSWORD", EncryptDecrypt.GetEncryptedString(sysInfoHolder.HTTPPROXYPASSWORD));
		SetValueForNamedNode("HTTPPROXYNOTUSECACHE", Boolean.toString(sysInfoHolder.HTTPPROXYNOTUSECACHE));
		SetValueForNamedNode("PROXYEXCEPTIONS", sysInfoHolder.PROXYEXCEPTIONS);
		SetValueForNamedNode("USEDELAYTIMEFROMSERVER", Boolean.toString(sysInfoHolder.USEDELAYTIMEFROMSERVER));
		SetValueForNamedNode("TIMEOUTDELAY", Integer.toString(sysInfoHolder.TIMEOUTDELAY));
		SetValueForNamedNode("CONNECTRETRYLIMIT", Integer.toString(sysInfoHolder.CONNECTRETRYLIMIT));
		SetValueForNamedNode("DOWNLOADSTARTOPTION", Integer.toString(sysInfoHolder.DOWNLOADSTARTOPTION));
		SetValueForNamedNode("AUTOACCELERATE", Boolean.toString(sysInfoHolder.AUTOACCELERATE));
		SetValueForNamedNode("AUTOUPDATE", Boolean.toString(sysInfoHolder.AUTOUPDATE));
		SetValueForNamedNode("MINSIZEFORACCELERATION", Integer.toString(sysInfoHolder.MINSIZEFORACCELERATION));
		SetValueForNamedNode("LASTUDATEDAT", Long.toString(sysInfoHolder.LASTUDATEDAT));
		SetValueForNamedNode("UPDATEOPTION", Integer.toString(sysInfoHolder.UPDATEOPTION));
		
		SetValueForNamedNode("SCHEDULESTARTOPTION", Integer.toString(sysInfoHolder.SCHEDULESTARTOPTION));
		SetValueForNamedNode("SCHEDULESTARTTIME", sysInfoHolder.SCHEDULESTARTTIME);
		//SetValueForNamedNode("SCHEDULESTARTDAYSTOCHECK", sysInfoHolder.SCHEDULESTARTDAYSTOCHECK);
		SetValueForNamedNode("SCHEDULESTOPOPTION", Integer.toString(sysInfoHolder.SCHEDULESTOPOPTION));
		SetValueForNamedNode("SCHEDULESTARTONSATURDAY", Boolean.toString(sysInfoHolder.SCHEDULESTARTONSATURDAY));
		SetValueForNamedNode("SCHEDULESTARTONSUNDAY", Boolean.toString(sysInfoHolder.SCHEDULESTARTONSUNDAY));
		SetValueForNamedNode("SCHEDULESTARTONMONDAY", Boolean.toString(sysInfoHolder.SCHEDULESTARTONMONDAY));
		SetValueForNamedNode("SCHEDULESTARTONTUESDAY", Boolean.toString(sysInfoHolder.SCHEDULESTARTONTUESDAY));
		SetValueForNamedNode("SCHEDULESTARTONWEDNESDAY", Boolean.toString(sysInfoHolder.SCHEDULESTARTONWEDNESDAY));
		SetValueForNamedNode("SCHEDULESTARTONTHURSDAY", Boolean.toString(sysInfoHolder.SCHEDULESTARTONTHURSDAY));
		SetValueForNamedNode("SCHEDULESTARTONFRIDAY", Boolean.toString(sysInfoHolder.SCHEDULESTARTONFRIDAY));
		SetValueForNamedNode("SCHEDULESTOPTIME", sysInfoHolder.SCHEDULESTOPTIME);
		SetValueForNamedNode("TORRENTDOWNLOADSTARTOPTION", Integer.toString(sysInfoHolder.TORRENTDOWNLOADSTARTOPTION));
		SetValueForNamedNode("MAXGLOBALTORRENTDOWNLOADSPEED", Integer.toString(sysInfoHolder.MAXGLOBALTORRENTDOWNLOADSPEED));
		SetValueForNamedNode("MAXGLOBALTORRENTUPLOADSPEED", Integer.toString(sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEED));
		SetValueForNamedNode("GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED", Boolean.toString(sysInfoHolder.GLOBALTORRENTSEEDINGUPLOADSPEEDENABLED));
		SetValueForNamedNode("MAXGLOBALTORRENTUPLOADSPEEDONSEEDING", Integer.toString(sysInfoHolder.MAXGLOBALTORRENTUPLOADSPEEDONSEEDING));
		SetValueForNamedNode("TORRENT_LISTEN_PORT", Integer.toString(sysInfoHolder.TORRENT_LISTEN_PORT));
		SetValueForNamedNode("TORRENTREMOVEONSHARERATIOOPTION", Boolean.toString(sysInfoHolder.TORRENTREMOVEONSHARERATIOOPTION));
		SetValueForNamedNode("SHARERATIOTOREMOVETORRENT", Float.toString(sysInfoHolder.SHARERATIOTOREMOVETORRENT));
		SetValueForNamedNode("TORRENTREMOVEONSEEDTIMEOPTION", Boolean.toString(sysInfoHolder.TORRENTREMOVEONSEEDTIMEOPTION));
		SetValueForNamedNode("SEEDTIMETOREMOVETORRENT", Integer.toString(sysInfoHolder.SEEDTIMETOREMOVETORRENT));
		
		SetValueForNamedNode("PEERTABLE_WIDTH_COL0", Integer.toString(sysInfoHolder.PEERTABLE_WIDTH_COL0));
		SetValueForNamedNode("PEERTABLE_WIDTH_COL1", Integer.toString(sysInfoHolder.PEERTABLE_WIDTH_COL1));
		SetValueForNamedNode("PEERTABLE_WIDTH_COL2", Integer.toString(sysInfoHolder.PEERTABLE_WIDTH_COL2));
		SetValueForNamedNode("FILETABLE_WIDTH_COL0", Integer.toString(sysInfoHolder.FILETABLE_WIDTH_COL0));
		SetValueForNamedNode("FILETABLE_WIDTH_COL1", Integer.toString(sysInfoHolder.FILETABLE_WIDTH_COL1));
		SetValueForNamedNode("FILETABLE_WIDTH_COL2", Integer.toString(sysInfoHolder.FILETABLE_WIDTH_COL2));
		SetValueForNamedNode("FILETABLE_WIDTH_COL3", Integer.toString(sysInfoHolder.FILETABLE_WIDTH_COL3));
		SetValueForNamedNode("FILETABLE_WIDTH_COL4", Integer.toString(sysInfoHolder.FILETABLE_WIDTH_COL4));
		
		SetValueForNamedNode("SEARCHFRAME_WIDTH", Integer.toString(sysInfoHolder.SEARCHFRAME_WIDTH));
		SetValueForNamedNode("SEARCHFRAME_HEIGHT", Integer.toString(sysInfoHolder.SEARCHFRAME_HEIGHT));
		
		SetValueForNamedNode("OPTIONSFRAME_WIDTH", Integer.toString(sysInfoHolder.OPTIONSFRAME_WIDTH));
		SetValueForNamedNode("OPTIONSFRAME_HEIGHT", Integer.toString(sysInfoHolder.OPTIONSFRAME_HEIGHT));
		SetValueForNamedNode("HIDEUPDATEREMINDERFORVERSION", sysInfoHolder.HIDEUPDATEREMINDERFORVERSION);
		
		try {
			// 	Prepare the DOM document for writing
			DOMSource source = new DOMSource(doc);
	    
			// Prepare the output file
			File file = new File(sysInfoHolder.GetRuntimeOptionsXml());
			StreamResult result = new StreamResult(file);
	    
			// Write the DOM document to the file
			Transformer xformer = TransformerFactory.newInstance().newTransformer();
			xformer.setOutputProperty("indent", "yes");
			xformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
		} catch (TransformerException e) {
		}
	}	
}
