/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    XML updater for history.xml
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         05/17/09 - Add default values in case of empty nodes to avoid exceptions
 *   arun         05/19/09 - Support for pausedTime calculation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager.xmlcomponents;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.dom.DOMSource; 
import javax.xml.transform.stream.StreamResult; 

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element; 

import skdownloader.downloadmanager.SystemInfoHolder;
import skdownloader.core.DownloadStateHolder;


public class HistoryXmlUpdater
{
	DocumentBuilderFactory dbf;
	Document doc;
	SystemInfoHolder sysInfoHolder;
	int maxId;
	
	public HistoryXmlUpdater(SystemInfoHolder sysInfoHolder) {
		this.sysInfoHolder=sysInfoHolder;
		maxId=0;
		try {
			dbf= DocumentBuilderFactory.newInstance();
			//dbf.setValidating(true);
			dbf.setIgnoringElementContentWhitespace(false);

			DocumentBuilder dBuilder = dbf.newDocumentBuilder();
			doc = dBuilder.parse(new File(sysInfoHolder.GetHistoryXml()));
			NormalizeIds();
		} catch (Exception e) {
		}
	}
	
	public NodeList GetHistoryList() {
		NodeList a=null;
		a=doc.getElementsByTagName("download");
		return a;
	}
	
	public String GetNodeType(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("type");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "Unknown";
	}
	
	public String GetNodeFile(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("filename");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "Unknown";
	}

	public String GetNodeUrl(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("url");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "";
	}

	public String GetNodeDir(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("dir");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "";
	}
	
	public String GetNodeSize(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("size");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "-1";
	}
	
	public String GetNodeSpeed(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("speed");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "0";
	}
	
	public String GetNodeStartTime(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("downloadstarttime");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "0";
	}
	
	public String GetNodeSeedingStartTime(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("seedingstarttime");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "0";
	}
	
	public String GetNodeEndTime(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("downloadendtime");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "0";
	}
	
	public String GetNodeDownloadTime(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("downloadtime");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "0";
	}
	
	public String GetNodePauseStartTime(Node n){
		NodeList nl = ((Element)n).getElementsByTagName("pausestarttime");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "0";
	}
	
	public String GetNodePauseEndTime(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("pauseendtime");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "0";
	}
	
	public String GetNodePausedTime(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("pausedtime");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "0";
	}
	
	public String GetNodeState(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("downloadstate");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return (Integer.toString(DownloadStateHolder.ERROR));
	}
	
	public String GetNodeErrorMessage(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("errormessage");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "";
	}

	public String GetNodeProgress(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("progress");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "0";
	}
	
	public String GetNodeId(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("id");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "0";
	}
	
	public String GetNodeDownloaded(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("downloaded");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "0";
	}
	
	public String GetNodeUploaded(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("uploaded");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "0";
	}
	
	public String GetNodeManaged(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("managed");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "false";
	}
	
	public String GetNodepiecesetString(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("pieceset");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "";
	}
	
	public String GetNodeExcludesetString(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("excludeset");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return (String)(nl.item(0).getFirstChild().getNodeValue());
		else
			return "";
	}
	
	public int GetNodeNumberOfPieces(Node n) {
		NodeList nl = ((Element)n).getElementsByTagName("pieceset");
		if(nl.getLength() > 0 && nl.item(0).getFirstChild() != null)
			return ((String)(nl.item(0).getFirstChild().getNodeValue())).length();
		else
			return 0;
	}
	
	public void DeleteDownloadNodeWithId(int id) {
		NodeList nodeList = doc.getElementsByTagName("download");
		for(int i=0; i<nodeList.getLength(); i++) {
			if(GetNodeId(nodeList.item(i)).equals(Integer.toString(id))) {
				nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
			}
		}
		WriteXml();
	}
	
	public Node GetDownloadNodeWithId(int id) {
		Node node = null;
		NodeList nodeList = doc.getElementsByTagName("download");
		for(int i=0; i<nodeList.getLength(); i++) {
			if(GetNodeId(nodeList.item(i)).equals(Integer.toString(id))) {
				node = nodeList.item(i);
			}
		}
		return node;
	}
	
	public ArrayList<Integer> GetArrayForThreadAttribute(Node n, String str) {
		//Node threads = ((Element)n).getElementsByTagName("threads").item(0);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		NodeList threadList = ((Element)n).getElementsByTagName("thread");
		
		for(int i=0; i<threadList.getLength(); i++) {
			for(int j=0; j<threadList.getLength(); j++) {
				if((((Element)threadList.item(j)).getElementsByTagName("index").item(0)).getFirstChild().getNodeValue().equals(Integer.toString(i))) {
					arr.add(Integer.parseInt((((Element)threadList.item(j)).getElementsByTagName(str).item(0)).getFirstChild().getNodeValue()));
				}
			}
		}
		return arr;
	}
	
	public int GetMaxId(){
		return maxId;
	}
	
	
	public synchronized int AddDownloadEntry(String url,
											  int type,
											  String filename,
											  String dir,
											  long size,
											  float speed,
											  long startTime,
											  long seedingStartTime,
											  long endTime,
											  long downloadTime,
											  long pauseStartTime,
											  long pauseEndTime,
											  long pausedTime,
											  int state,
											  String errorMessage,
											  int progress,
											  long downloaded,
											  long uploaded,
											  boolean managed,
											  String pieceSet,
											  String excludeSet)
	{
		//Check and set default for options. If any option is null, it can lead to an incorrect/empty xml file
		if(url == null) {url="";}
		if(filename == null) {filename="";}
		if(dir == null) {dir="";}
		if(errorMessage == null) {errorMessage="";}
		if(pieceSet==null) {pieceSet="";};
		
		Node name, root, downloadNode;
		
		root=doc.getDocumentElement();
		downloadNode = doc.createElement("download");
		root.appendChild(downloadNode);
		name =  doc.createElement("id");
		name.appendChild(doc.createTextNode(Integer.toString(maxId)));
		downloadNode.appendChild( name);
		name =  doc.createElement("url");
		name.appendChild(doc.createTextNode(url));
		downloadNode.appendChild( name);
		name =  doc.createElement("type");
		name.appendChild(doc.createTextNode(Integer.toString(type)));
		downloadNode.appendChild(name);
		name = doc.createElement("filename");
		name.appendChild(doc.createTextNode(filename));
		downloadNode.appendChild(name);
		name = doc.createElement("dir");
		name.appendChild(doc.createTextNode(dir));
		downloadNode.appendChild(name);
		name = doc.createElement("size");
		name.appendChild(doc.createTextNode(Long.toString(size)));
		downloadNode.appendChild(name);
		name = doc.createElement("speed");
		name.appendChild(doc.createTextNode(Float.toString(speed)));
		downloadNode.appendChild(name);
		name = doc.createElement("downloadstarttime");
		name.appendChild(doc.createTextNode(Long.toString(startTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("seedingstarttime");
		name.appendChild(doc.createTextNode(Long.toString(seedingStartTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("downloadendtime");
		name.appendChild(doc.createTextNode(Long.toString(endTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("downloadtime");
		name.appendChild(doc.createTextNode(Long.toString(downloadTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("pausestarttime");
		name.appendChild(doc.createTextNode(Long.toString(pauseStartTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("pauseendtime");
		name.appendChild(doc.createTextNode(Long.toString(pauseEndTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("pausedtime");
		name.appendChild(doc.createTextNode(Long.toString(pausedTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("downloadstate");
		name.appendChild(doc.createTextNode(Long.toString(state)));
		downloadNode.appendChild(name);
		name = doc.createElement("errormessage");
		name.appendChild(doc.createTextNode(errorMessage));
		downloadNode.appendChild(name);
		name = doc.createElement("progress");
		name.appendChild(doc.createTextNode(Integer.toString(progress)));
		downloadNode.appendChild(name);
		name = doc.createElement("downloaded");
		name.appendChild(doc.createTextNode(Long.toString(downloaded)));
		downloadNode.appendChild(name);
		name = doc.createElement("uploaded");
		name.appendChild(doc.createTextNode(Long.toString(uploaded)));
		downloadNode.appendChild(name);
		name = doc.createElement("managed");
		name.appendChild(doc.createTextNode(Boolean.toString(managed)));
		downloadNode.appendChild(name);
		name = doc.createElement("pieceset");
		name.appendChild(doc.createTextNode(pieceSet));
		downloadNode.appendChild(name);
		name = doc.createElement("excludeset");
		name.appendChild(doc.createTextNode(excludeSet));
		downloadNode.appendChild(name);
		
		//	Increment max id
		int old_maxid=maxId;
		maxId++;

		WriteXml();
		
		return (old_maxid);
	}
	
	public synchronized void UpdateToInactiveDownloadEntry(int id,
											  String url,
											  int type,
											  String filename,
											  String dir,
											  long l,
											  float speed,
											  long startTime,
											  long seedingStartTime,
											  long endTime,
											  long downloadTime,
											  long pauseStartTime,
											  long pauseEndTime,
											  long pausedTime,
											  int state,
											  String errorMessage,
											  int progress,
											  long downloaded,
											  long uploaded,
											  boolean managed,
											  String pieceSet,
											  String excludeSet)
	{
		//Check and set default for options. If any option is null, it can lead to an incorrect/empty xml file
		if(url == null) {url="";}
		if(filename == null) {filename="";}
		if(dir == null) {dir="";}
		if(errorMessage == null) {errorMessage="";}
		if(pieceSet==null) {pieceSet="";};
		
		Node name, downloadNode;
		
		downloadNode = GetDownloadNodeWithId(id);

		//Remove all child nodes
		while(downloadNode.hasChildNodes()) {
			downloadNode.removeChild(downloadNode.getFirstChild());
		}
		
		name =  doc.createElement("id");
		name.appendChild(doc.createTextNode(Integer.toString(id)));
		downloadNode.appendChild( name);
		name =  doc.createElement("url");
		name.appendChild(doc.createTextNode(url));
		downloadNode.appendChild( name);
		name =  doc.createElement("type");
		name.appendChild(doc.createTextNode(Integer.toString(type)));
		downloadNode.appendChild( name);
		name = doc.createElement("filename");
		name.appendChild(doc.createTextNode(filename));
		downloadNode.appendChild(name);
		name = doc.createElement("dir");
		name.appendChild(doc.createTextNode(dir));
		downloadNode.appendChild(name);
		name = doc.createElement("size");
		name.appendChild(doc.createTextNode(Long.toString(l)));
		downloadNode.appendChild(name);
		name = doc.createElement("speed");
		name.appendChild(doc.createTextNode(Float.toString(speed)));
		downloadNode.appendChild(name);
		name = doc.createElement("downloadstarttime");
		name.appendChild(doc.createTextNode(Long.toString(startTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("seedingstarttime");
		name.appendChild(doc.createTextNode(Long.toString(seedingStartTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("downloadendtime");
		name.appendChild(doc.createTextNode(Long.toString(endTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("downloadtime");
		name.appendChild(doc.createTextNode(Long.toString(endTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("pausestarttime");
		name.appendChild(doc.createTextNode(Long.toString(pauseStartTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("pauseendtime");
		name.appendChild(doc.createTextNode(Long.toString(pauseEndTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("pausedtime");
		name.appendChild(doc.createTextNode(Long.toString(pausedTime)));
		downloadNode.appendChild(name);
		name = doc.createElement("downloadstate");
		name.appendChild(doc.createTextNode(Long.toString(state)));
		downloadNode.appendChild(name);
		name = doc.createElement("errormessage");
		name.appendChild(doc.createTextNode(errorMessage));
		downloadNode.appendChild(name);
		name = doc.createElement("progress");
		name.appendChild(doc.createTextNode(Integer.toString(progress)));
		downloadNode.appendChild(name);
		name = doc.createElement("downloaded");
		name.appendChild(doc.createTextNode(Long.toString(downloaded)));
		downloadNode.appendChild(name);
		name = doc.createElement("uploaded");
		name.appendChild(doc.createTextNode(Long.toString(uploaded)));
		downloadNode.appendChild(name);
		name = doc.createElement("managed");
		name.appendChild(doc.createTextNode(Boolean.toString(managed)));
		downloadNode.appendChild(name);
		name = doc.createElement("pieceset");
		name.appendChild(doc.createTextNode(pieceSet));
		downloadNode.appendChild(name);
		name = doc.createElement("excludeset");
		name.appendChild(doc.createTextNode(excludeSet));
		downloadNode.appendChild(name);
		
		WriteXml();
	}
	/*	<download>
		<url>"http://www.toolsbysk.com"</url>
		<filename>sksplitter.exe</filename>
		<dir>/home/arun</dir>
		<downloadstarttime>123000444</downloadstarttime>
		<downloadendtime>124333232</downloadendtime>
		<downloadstate>1</downloadstate>
		<threads>
			<thread>
				<index>0</index>
				<chunksize>20000</chunksize>
				<chunkoffset>3000</chunkoffset>
				<writerangestart>5000</writerangestart>
			<thread>
			<thread>
				<index>1</index>
				<chunksize>20000</chunksize>
				<chunkoffset>3000</chunkoffset>
				<writerangestart>5000</writerangestart>
			<thread>
			<thread>
				<index>2</index>
				<chunksize>20000</chunksize>
				<chunkoffset>3000</chunkoffset>
				<writerangestart>5000</writerangestart>
			<thread>
		</threads>
	</download>
	*/
	
	public synchronized void UpdateToActiveDownloadEntry(int id,
									   String url,
									   int type,
									   String filename,
									   String dir,
									   long l,
									   ArrayList<Integer> chunkSizeArr,
									   ArrayList<Integer> chunkOffsetArr,
									   ArrayList<Integer> writeRangeStartArr,
									   float speed,
									   long startTime,
									   long seedingStartTime,
									   long endTime,
									   long downloadTime,
									   long pauseStartTime,
									   long pauseEndTime,
									   long pausedTime,
									   int state,
									   String errorMessage,
									   int progress,
									   Long downloaded,
									   Long uploaded,
									   boolean managed,
									   String pieceSet,
									   String excludeSet)
	{
		//Check and set default for options. If any option is null, it can lead to an incorrect/empty xml file
		if(url == null) {url="";}
		if(filename == null) {filename="";}
		if(dir == null) {dir="";}
		if(errorMessage == null) {errorMessage="";}
		if(pieceSet==null) {pieceSet="";};
		if(chunkSizeArr == null) {chunkSizeArr = new ArrayList<Integer>();}
		if(chunkOffsetArr == null) {chunkOffsetArr = new ArrayList<Integer>();}
		if(writeRangeStartArr == null) {writeRangeStartArr = new ArrayList<Integer>();}
		
		//Add entry for this download
		//First add an inacvive entry. active entries are supersets of inactive entry
		UpdateToInactiveDownloadEntry(id, url,type, filename,dir,l,speed,startTime,seedingStartTime,endTime,downloadTime,pauseStartTime,pauseEndTime,pausedTime,state,errorMessage,progress, downloaded, uploaded, managed, pieceSet, excludeSet);
		
		Node name, downloadNode, threadsNode, threadNode;
		
		downloadNode = GetDownloadNodeWithId(id);
		
		threadsNode = doc.createElement("threads");
		for(int i=0; i<chunkSizeArr.size(); i++) {
			threadNode = doc.createElement("thread");
			threadsNode.appendChild(threadNode);
			
			name = doc.createElement("index");
			name.appendChild(doc.createTextNode(Integer.toString(i)));
			threadNode.appendChild(name);
			name = doc.createElement("chunksize");
			name.appendChild(doc.createTextNode(Integer.toString(chunkSizeArr.get(i))));
			threadNode.appendChild(name);
			name = doc.createElement("chunkoffset");
			name.appendChild(doc.createTextNode(Integer.toString(chunkOffsetArr.get(i))));
			threadNode.appendChild(name);
			name = doc.createElement("writerangestart");
			name.appendChild(doc.createTextNode(Integer.toString(writeRangeStartArr.get(i))));
			threadNode.appendChild(name);
		}
		downloadNode.appendChild(threadsNode);
		WriteXml();
	}
	
	public void NormalizeIds() {
		NodeList nodeList = doc.getElementsByTagName("download");
		for(int i=0; i<nodeList.getLength(); i++) {
			NodeList idNl = ((Element)nodeList.item(i)).getElementsByTagName("id");
			Node node = idNl.item(0).getFirstChild();
			node.setNodeValue(""+i);
			maxId++;
		}
		//Remove text nodes in downloads. They just increase the xml size and makes reading difficult
		nodeList = doc.getElementsByTagName("downloadlist");
		NodeList children = (nodeList.item(0)).getChildNodes();
		for(int i=0; i<children.getLength(); i++) {
			if(children.item(i).getNodeType() == Node.TEXT_NODE) 
				nodeList.item(0).removeChild(children.item(i));
		}
	}
	
	public void WriteXml() {
		try {
			// 	Prepare the DOM document for writing
			DOMSource source = new DOMSource(doc);
	    
			// Prepare the output file
			File file = new File(sysInfoHolder.GetHistoryXml());
			StreamResult result = new StreamResult(file);
	    
			// Write the DOM document to the file
			Transformer xformer = TransformerFactory.newInstance().newTransformer();
			xformer.setOutputProperty("indent", "yes");
			xformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
		} catch (TransformerException e) {
		}
	}
}
