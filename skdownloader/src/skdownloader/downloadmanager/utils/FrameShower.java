package skdownloader.downloadmanager.utils;

import javax.swing.JFrame;

public class FrameShower implements Runnable 
{
	final JFrame frame;
	public FrameShower(JFrame frame) 
	{
       this.frame = frame;
	}
	public void run() 
	{
       frame.setVisible(true);
     }
}
