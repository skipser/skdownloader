package skdownloader.downloadmanager.utils;

import java.util.BitSet;

public class EncryptDecrypt
{
	public static String GetEncryptedString(String str)
	{
		//For null string, return null
		if(str.equals(""))
			return "";
		
		byte[] arr = str.getBytes();
		byte[] arr1 = new byte[arr.length];
		
		for(int i=0; i<arr.length; i++)
		{
			arr1[i] = (byte)(arr[i] ^ 0xF);
		}
		
		return (BytesToBitString(arr1));
	}
	
	public static String GetDecryptedString(String str)
	{
		//For null string, return null
		if(str.equals(""))
			return "";
		
		byte[] arr = BitStringToBytes(str);
		byte[] arr1 = new byte[arr.length];
		
		for(int i=0; i<arr.length; i++)
		{
			arr1[i] = (byte)(arr[i] ^ 0xF);
		}
		
		return (new String(arr1));
	}

	// Returns a bitset containing the values in bytes.
    // The byte-ordering of bytes must be big-endian which means the most significant bit is in element 0.
    private static String BytesToBitString(byte[] bytes) 
    {
        BitSet bits = new BitSet();
        String str = "";
        
        for (int i=0; i<bytes.length*8; i++) 
        {
            if ((bytes[bytes.length-i/8-1]&(1<<(i%8))) > 0) 
            {
                bits.set(i);
            }
        }
        
    	for(int i=0; i<bits.length(); i++)
    	{
    		if(bits.get(i)==true)
    			str += "1";
    		else
    			str += "0";
    	}
    	return str;
    }
    
    // Returns a byte array of at least length 1.
    // The most significant bit in the result is guaranteed not to be a 1
    // (since BitSet does not support sign extension).
    // The byte-ordering of the result is big-endian which means the most significant bit is in element 0.
    // The bit at index 0 of the bit set is assumed to be the least significant bit.
    private static byte[] BitStringToBytes(String str)
    {
    	char[] charArr = str.toCharArray();
    	BitSet bits = new BitSet();
    	for(int i=0; i<charArr.length; i++)
    	{
    		if(charArr[i] == '1')
    			bits.set(i, true);
    		else
    			bits.set(i, false);
    	}
    	
        byte[] bytes = new byte[bits.length()/8+1];
        for (int i=0; i<bits.length(); i++) {
            if (bits.get(i)) {
                bytes[bytes.length-i/8-1] |= 1<<(i%8);
            }
        }
        return bytes;
    }

}