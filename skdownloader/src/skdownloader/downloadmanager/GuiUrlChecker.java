/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    URL checker
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.downloadmanager;

import skdownloader.core.urlcore.UrlChecker;

import java.net.URL;
import java.net.HttpURLConnection;

public class GuiUrlChecker implements UrlChecker
{
	private String fileName="";
	private int fileSize=-1;
	public final static String FILE_NAME_SUBSTR = "filename=";
	
	public boolean CheckUrl(String urlString)
	{
		int response;
		//byte buf[] = new byte[1024];
		try {
			URL url = new URL(urlString);
			HttpURLConnection c = (HttpURLConnection) url.openConnection();
			c.setDoOutput(true);
		    c.setDoInput(true); 
		    c.setInstanceFollowRedirects(false);
			response = c.getResponseCode();
			if(c.getResponseCode() == 302)
			{
				if((c.getHeaderField("Location") != "") &&
					(c.getHeaderField("Location") != urlString))
				{
					return(CheckUrl(c.getHeaderField("Location")));
				}
			}
			
			if (response != HttpURLConnection.HTTP_OK) {
				return false;
				//throw new RuntimeException(" Download failed. Reason: "+ c.getResponseMessage());
			}
			
			//read Content-Disposition header to detect filename
			String disposition = c.getHeaderField("Content-Disposition");
			if(disposition!=null&&disposition.lastIndexOf(FILE_NAME_SUBSTR)>-1){
				String fname=disposition.substring(
						disposition.lastIndexOf(FILE_NAME_SUBSTR)
						+FILE_NAME_SUBSTR.length());
						if(fname.endsWith(";")){
							fname=fname.substring(0,fname.length()-1);
						}
						int sep = fname.lastIndexOf("/");
						fileName = fname.substring(sep + 1);
			}
			else
			{
				fileName=url.getFile();
				int sep = fileName.lastIndexOf("/");
				fileName = fileName.substring(sep + 1);
			}
			fileSize=c.getContentLength();

		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public String GetName()
	{
		
		return fileName;
	}
	
	public int GetSize()
	{
		return fileSize;
	}
}