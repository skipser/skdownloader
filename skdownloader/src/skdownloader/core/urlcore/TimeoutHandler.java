/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Timeout handler
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/
package skdownloader.core.urlcore;

import skdownloader.core.UserIOUtils;

public class TimeoutHandler
{
	//Interval for http connction timeout
	//public static int HTTP_SOCKET_TIMEOUT=20000;
	//public static int HTTP_CONNECTION_TIMEOUT=20000;
	private  static int[] socket_timeout_arr     = {50, 200, 500, 1000, 2000, 4000, 8000, 15000, 20000};
	//private  static int[] connection_timeout_arr = {2000, 4000, 8000, 15000, 20000};
	
	private int avgResponseTime;
	private int timeoutIndex; //Denotes the index from where timout retrieval should start.
	private boolean timeoutLocked=true; //Used to denote successive timeouts occurring.
	
	UserIOUtils ioutils;
	
	public TimeoutHandler(int respTime, UserIOUtils ioutils)
	{
		avgResponseTime=0;
		timeoutIndex=0;
		this.ioutils=ioutils;
		if(respTime > 0)
		{
			avgResponseTime = respTime;
			UpdateStatsWithNewResponseTime(respTime);
		}
	}
	
	public int GetAvgResponseTime()
	{
		return avgResponseTime;
	}

	/*************************************************************
	 * 
	 * Function  : GetSocketTimeout
	 * Arguments : void
	 * Return    : returns time out value to be used for connection
	 * Notes     : If there is a timeoutlock(earlier connection attempt also timedout),
	 *               - returns the next big timeout value.
	 *               - makes averageresponse time to be 0. This makes next good response time
	 *                 to be the averageresponse time
	 *             Else, 
	 *               - it returns current timeout value from timeout array.
	 * 
	 *************************************************************/
	public int GetSocketTimeout()
	{
		int retVal;
		retVal=socket_timeout_arr[timeoutIndex];
		if(timeoutLocked)
			avgResponseTime=0;
			if(timeoutIndex < socket_timeout_arr.length-1)
				timeoutIndex++;
		timeoutLocked=true;
		return retVal;
	}
	
	
	/*************************************************************
	 * 
	 * Function  : UpdateStatsWithNewResponseTime
	 * Arguments : response time for last request.
	 * Return    : void
	 * Notes     : This function updates the averageresponsetime. If
	 *             there was a timeout lock, new response time is taken
	 *             as the average. Otherwise, average is computed using
	 *             earlier average and current response time.
	 * 
	 *************************************************************/
	public void UpdateStatsWithNewResponseTime(int time)
	{
		//If there was a time lock, take current response time to be the new average.
		if(timeoutLocked)
			avgResponseTime=time;
		else
			avgResponseTime=(avgResponseTime+time)/2;
		
		timeoutLocked=false;

		timeoutIndex=0;
		while(avgResponseTime > (int)(socket_timeout_arr[timeoutIndex]*3/4))
		{
			if(timeoutIndex < socket_timeout_arr.length-1)
				timeoutIndex++;
			else
				break;
		}
	}
}