/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Holds state of a download thread
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         06/08/09 - Add comments to functions
 *
 ****************************************************************************************/

package skdownloader.core.urlcore;

import skdownloader.core.DownloadStateHolder;

public class UrlDownloadThreadStateHolder {
	private int index;
	private int size;
	private int downloaded;
	private int chunkOffset;
	private int writeRangeStart;
	private float speed;
	private int percentDone;
	private long threadStartTime;
	private long threadEndTime;
	private int state;
	
	/*************************************************************
	 * 
	 * Function  : DownloadThreadStateHolder
	 * Arguments : void
	 * Return    : void
	 * Notes     : Initializer
	 * 
	 *************************************************************/
	public UrlDownloadThreadStateHolder()
	{
		size=0;
		downloaded=0;
		speed=0;
		percentDone=0;
		state=DownloadStateHolder.DOWNLOADING;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetIndex
	 * Arguments : index
	 * Return    : void
	 * Notes     : Set thread index
	 * 
	 *************************************************************/
	public void SetIndex(int index)
	{
		this.index=index;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetIndex
	 * Arguments : void
	 * Return    : index
	 * Notes     : Get thread index
	 * 
	 *************************************************************/
	public int GetIndex()
	{
		return index;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetSize
	 * Arguments : size
	 * Return    : void
	 * Notes     : Set thread download size
	 * 
	 *************************************************************/
	public void SetSize(int size)
	{
		this.size=size;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetSize
	 * Arguments : void
	 * Return    : size
	 * Notes     : Get thread download size
	 * 
	 *************************************************************/
	public int GetSize()
	{
		return size;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetDownloaded
	 * Arguments : downloaded size
	 * Return    : void
	 * Notes     : Set thread downloaded size
	 * 
	 *************************************************************/
	public void SetDownloaded(int d)
	{
		this.downloaded=d;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetDownloaded
	 * Arguments : void
	 * Return    : downloaded size
	 * Notes     : Get thread downloaded size
	 * 
	 *************************************************************/
	public int GetDownloaded()
	{
		return downloaded;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetSpeed
	 * Arguments : speed
	 * Return    : void
	 * Notes     : Set thread download speed
	 * 
	 *************************************************************/
	public void SetSpeed(float s)
	{
		this.speed=s;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetSpeed
	 * Arguments : void
	 * Return    : speed
	 * Notes     : Get thread download speed
	 * 
	 *************************************************************/
	public float Getspeed()
	{
		return speed;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetPercentDone
	 * Arguments : percent done
	 * Return    : void
	 * Notes     : Set thread percent done
	 * 
	 *************************************************************/
	public void SetPercentDone(int d)
	{
		this.percentDone=d;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetPercentDone
	 * Arguments : void
	 * Return    : percent done
	 * Notes     : Get thread percent done
	 * 
	 *************************************************************/
	public int GetPercentDone()
	{
		return percentDone;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetStartTime
	 * Arguments : time
	 * Return    : void
	 * Notes     : Set thread start time
	 * 
	 *************************************************************/
	public void SetStartTime(long time)
	{
		threadStartTime=time;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetStartTime
	 * Arguments : void
	 * Return    : time
	 * Notes     : Get thread start time
	 * 
	 *************************************************************/
	public long GetStartTime()
	{
		return threadStartTime;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetEndTime
	 * Arguments : tie
	 * Return    : void
	 * Notes     : Set thread end time
	 * 
	 *************************************************************/
	public void SetEndTime(long time)
	{
		threadEndTime=time;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetEndTime
	 * Arguments : void
	 * Return    : time
	 * Notes     : Get thread end time
	 * 
	 *************************************************************/
	public long GetEndTime()
	{
		return threadEndTime;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetState
	 * Arguments : void
	 * Return    : status
	 * Notes     : Get thread download status
	 * 
	 *************************************************************/
	public int GetState()
	{
		return state;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetState
	 * Arguments : status
	 * Return    : void
	 * Notes     : Set thread download status
	 * 
	 *************************************************************/
	public void SetState(int state)
	{
		this.state = state;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetChunkOffset
	 * Arguments : void
	 * Return    : thread chunk offset
	 * Notes     : Get thread chunk offset
	 * 
	 *************************************************************/
	public int GetChunkOffset()
	{
		return chunkOffset;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetChunkOffset
	 * Arguments : offset
	 * Return    : void
	 * Notes     : Set thread chunk offset
	 * 
	 *************************************************************/
	public void SetChunkOffset(int offset)
	{
		this.chunkOffset = offset;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetWriteRangeStart
	 * Arguments : void
	 * Return    : thread write range start
	 * Notes     : Get thread write range start
	 * 
	 *************************************************************/
	public int GetWriteRangeStart()
	{
		return writeRangeStart;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetWriteRangeStart
	 * Arguments : write range start
	 * Return    : void
	 * Notes     : Set thread write range start
	 * 
	 *************************************************************/
	public void SetWriteRangeStart(int range)
	{
		this.writeRangeStart = range;
	}
}