package skdownloader.core.urlcore;

import java.io.BufferedInputStream;
//import java.io.BufferedOutputStream;
//import java.io.FileOutputStream;
import java.net.URLConnection;
import java.net.URL;

import org.apache.log4j.Logger;

import skdownloader.core.Common;
import skdownloader.core.DownloadStateHolder;
import skdownloader.core.DownloaderThread;
import skdownloader.core.UserIOUtils;
import skdownloader.core.MyFileWriter;

public class FtpUrlDownloaderThread extends Thread implements DownloaderThread
{
	private DownloadStateHolder stateHolder;
	private UserIOUtils ioutils;
	private MyFileWriter writer;
	URLConnection urlc=null;
	URL url=null;
	private int status;
	int totalRead;
	int amountRead;
	int readBufferSize=0; //Size of read buffer
	private byte[]              readBuffer = new byte[Common.MIN_WRITE_CHUNK*2];
	String outFile = "";
	String urlString  = "";
	private static Logger logger = Logger.getLogger(FtpUrlDownloaderThread.class);
	
	public FtpUrlDownloaderThread(DownloadStateHolder stateHolder, MyFileWriter writer, UserIOUtils ioutils, String urlString, String outFile)
	{
		this.stateHolder=stateHolder;
		this.writer=writer;
		this.ioutils=ioutils;
		this.urlString=urlString;
		this.outFile=outFile;
		totalRead=0;
		status=DownloadStateHolder.DOWNLOADING;
	}
	
	public void run()
	{
		/*
		 * type ==> a=ASCII mode, i=image (binary) mode, d= file directory
		 * listing
		 */
		urlString += ";type=i";
		BufferedInputStream bis = null;
		try
		{
			url = new URL(urlString);
		}
		catch(Exception e) {
			logger.error("Exception", e);
			ioutils.PrintError("Invalid url- "+Common.ShortenString(urlString, "middle",50 ));
			status=DownloadStateHolder.ERROR;
		}
		
		try
		{
            urlc = url.openConnection();
		} catch(Exception e) {
			logger.error("Exception", e);
			ioutils.PrintError("Error downloading file.");
			status=DownloadStateHolder.ERROR;
		}

		try
		{
            bis = new BufferedInputStream( urlc.getInputStream() );
		} catch (Exception e) {
			logger.error("Exception", e);
			ioutils.PrintError("Error accessing url- "+Common.ShortenString(urlString, "middle",50 ));
			status=DownloadStateHolder.ERROR;
		}
		
		try
		{
            while((amountRead = bis.read(readBuffer, 0, readBuffer.length)) != -1)
            {
            	writer.WriteToFile(readBuffer, amountRead);
               totalRead+=amountRead;
            }
            bis.close();
            status=DownloadStateHolder.COMPLETED;
		} catch(Exception e) {
			logger.error("Exception", e);
			ioutils.PrintError("Error writing file to disk- "+ Common.ShortenString(outFile, "middle",50 ));
			status=DownloadStateHolder.ERROR;
		}
	}

	public void Pause()
	{
		ioutils.PrintError("Cannot pause ftp download");
	}
	
	public void PauseForError()
	{
		ioutils.PrintError("Error downloading url- "+Common.ShortenString(stateHolder.GetUrlName(), "middle",50 )
							+Common.LINESEPERATOR+ "Download cancelled");
		Cancel();
	}
	
	public void PauseForExit()
	{
		Cancel();
	}
	
	public void Resume()
	{
		
	}
	
	public void Cancel()
	{
		status=DownloadStateHolder.CANCELLED;
		stateHolder.SetThreadStatus(0, status);
	}
	
	public int GetStatus()
	{
		return status;
	}
	
	public int GetDownloadSize()
	{
		return status;
	}
	
	public int GetWriteRangeStart()
	{
		return totalRead;
	}
	
	public long GetTotalRead()
	{
		return totalRead;
	}
	
	public void SingleDownload()
	{

	}
	
	public int GetChunkOffset()
	{
		return 1;
	}
}
