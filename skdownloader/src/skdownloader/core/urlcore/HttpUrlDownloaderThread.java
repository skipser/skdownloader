/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Downloader thread for each connection
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/
package skdownloader.core.urlcore;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import skdownloader.core.Common;
import skdownloader.core.DownloadStateHolder;
import skdownloader.core.DownloaderThread;
import skdownloader.core.UserIOUtils;
import skdownloader.core.MyFileWriter;
import skdownloader.downloadmanager.guicomponents.exceptions.DownloadingException;
import skdownloader.downloadmanager.guicomponents.exceptions.DownloadingFatalException;

class HttpUrlDownloaderThread extends Thread implements DownloaderThread
{
	private BufferedInputStream inStream;  //Stream for http connection
	private String              urlName=""; //url name
	private MyFileWriter        writer;     //Writer instance 
	private DownloadStateHolder stateHolder;
	private int                 downloadSize; //Total download size
	private int                 chunkOffset;  //Offset in file from which this thread will start writing
	private int                 splitSize=0;  //Size for http request in each iteration
	private int                 amountRead=0; //Bytes read in a single stream read request.
	private int                 amountReadInLoop=0; //Total bytes read in from input stream created for http request.
	private int                  totalRead=0; // Total bytes read in.
	@SuppressWarnings("unused")
	private int                  totalWrote=0; //Total bytes wrote.
	private int                 readRangeStart=0; //Start offset for each http request
	private int                 readRangeEnd=0; //End offset for each http request
	private int                 writeRangeStart=0; //Start offset for each write request
	private int                 readBufferSize=0; //Size of read buffer
	private byte[]              readBuffer = new byte[Common.MIN_WRITE_CHUNK*2];
	private                     HttpGet request = null;
	private                     int index;
	private                     DefaultHttpClient client; //Http client creates connections and reads in data
	private static Logger logger = Logger.getLogger(HttpUrlDownloaderThread.class);
	UserIOUtils                 ioutils; //Logger to log messages
	private int                 status;
	private int                 retryIndex;
	private int                 initRespTime;
	private int                 threadInitRespTime;
	private int                 previousStatus;

	public HttpUrlDownloaderThread(String urlName, DownloadStateHolder stateHolder, int status,
			                   MyFileWriter writer, long downloadSize, long offset, long writeRangeStart,
			                   int index, int respTime, UserIOUtils l)
	{
		this.urlName=urlName;
		this.stateHolder=stateHolder;
		this.writer=writer;
		this.downloadSize=(int)downloadSize;
		this.chunkOffset=(int)offset;
		//this.readRangeStart=(int)offset;
		//this.readRangeEnd=(int)offset;
		this.writeRangeStart=(int)writeRangeStart;
		this.initRespTime=respTime;
		this.index=index;
		this.totalRead=this.writeRangeStart-this.chunkOffset;
		this.totalWrote=totalRead;
		this.amountReadInLoop=0;
		this.ioutils=l;
		retryIndex=1;
		this.status=status;
		previousStatus=DownloadStateHolder.DOWNLOADING;
	}
	
	/*
	 * Strategy: Create connection with max range.
	 *           Starting reading in chunks. Once we have read in writechunk
	 *           max, write it to file. 
	 *           If exception comes, close the connection, re-set timeout and recreate connection
	 *           
	 * The following needs to be saved to restart on pause
	 * downloadSize - total size for this chunk
	 * chunkOffset - Offset in file for this chunk
	 * writeRangeStart - End point where last wrote
	 * 
	 * The following are calculated from above values.
	 * totalRead - Toal bytes read.
	 *             (Calculate it as writeRangeStart-chunkOffset. Since we have write buffering,
	 *             This should not be taken from thread instance.)
	 * readBufferSize=0;
	*/
	public void run() {
		if(downloadSize == -1) {
			SingleDownload();
			return;
		}
		
		TimeoutHandler timeoutHandler = new TimeoutHandler(initRespTime, ioutils);
		long t1;
		long t2;
		while(((status == DownloadStateHolder.DOWNLOADING) ||
			   (status == DownloadStateHolder.PAUSED) ||
			   (status == DownloadStateHolder.PAUSEDFORERROR) ||
			   (status == DownloadStateHolder.PAUSEDFOREXIT))&&
			   (retryIndex <stateHolder.GetRetryLimit())) {
			logger.debug("ThreadIndex: "+index+"Try "+retryIndex+" from urldownloaderthread:"+index);
			logger.debug("ThreadIndex: "+index+"Downloading "+downloadSize+" in thread:"+index);
			try {
				//If we are in a pausedfor error state, pause here
				while(status == DownloadStateHolder.PAUSEDFORERROR){
					//Check for sleep in 500ms intervals
					Common.Sleep(500, logger);
				}
				
				client = new DefaultHttpClient();
				if(stateHolder.UseProxy()) {
					logger.debug("Setting proxy");
					client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, (new HttpHost(stateHolder.GetHttpProxyServer(), stateHolder.GetHttpProxyPort())));
					//Set authentication if required
					if(stateHolder.GetHttpAuthenUser() !="") {
						client.getCredentialsProvider().setCredentials(new AuthScope("localhost", 8080), new UsernamePasswordCredentials(stateHolder.GetHttpAuthenUser(), stateHolder.GetHttpAuthenPassword()));
					}
				}
				
				int timeout=timeoutHandler.GetSocketTimeout();
				client.setHttpRequestRetryHandler(new DownloadRetryHandler());
				client.getParams().setParameter("http.socket.timeout", new Integer(timeout));
				client.getParams().setParameter("http.connection.timeout", new Integer(timeout));
			    client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.RFC_2965);
			    logger.debug("ThreadIndex: "+index+"Thread "+index+" Avg resp time - "+timeoutHandler.GetAvgResponseTime());
			    logger.debug("ThreadIndex: "+index+"Thread "+index+" setting timeout- "+timeout);

				request = new HttpGet(urlName);

				readRangeStart=chunkOffset+totalRead;
				readRangeEnd=chunkOffset+downloadSize-1;
				
				//Check if download has completed by any chance. If so, return
				if(readRangeStart > readRangeEnd)
					return;
				
				logger.debug("ThreadIndex: "+index+"Trying to download from -"+readRangeStart+" to "+readRangeEnd);
					
				// Create http request with header.
				t1=(new Date()).getTime();
				request.removeHeaders("Range");
				request.addHeader("User-Agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows 2000)");
			    
				if(stateHolder.GetNumConnections() > 1) {
					request.addHeader("Range", "bytes=" + readRangeStart + "-"+ readRangeEnd);
				}
				
				//Add caching disabling if needed
				if(stateHolder.UseProxy() && (!stateHolder.UseCache()))
					request.addHeader("Cache-Control", "no-cache");
				
				// Execute the request
				HttpResponse response;
				try	{
					response=client.execute(request);
				} catch (Exception e) {
					logger.error("Exception", e);
					throw new DownloadingFatalException("Http request error");
				}
				
				if ((stateHolder.GetNumConnections() > 1) && (response.getStatusLine().getStatusCode() != HttpStatus.SC_PARTIAL_CONTENT)) {
					logger.debug("ThreadIndex: "+index+"Didn't gett partial response. Status- "+response);
					Header[] headers= response.getAllHeaders();
					for(int i=0; i<headers.length; i++)	{
						logger.debug("ThreadIndex: "+index+"thread "+index+" - Header: "+headers[i].getName()+"value: "+headers[i].getValue());
					}
					throw(new DownloadingFatalException("Timeout exception for partial response."));
					//return false;
				}
				else {
					Header[] headers= response.getAllHeaders();
					for(int i=0; i<headers.length; i++)	{
						//logger.PrintThreadLog("DEBUG", index, "thread "+index+" - Header: "+headers[i].getName()+"value: "+headers[i].getValue());
					}
				}
				t2=(new Date()).getTime();
				threadInitRespTime=(int)(t2-t1);
				// Update timeoutHandler with current response time.
				timeoutHandler.UpdateStatsWithNewResponseTime((int)(t2-t1)*10);
					
				// Send http request.
				Header contentLengthHeader = response.getFirstHeader("content-length");
				if (contentLengthHeader == null) {
					//return false;
				}
				
				//Get data from http request as input stream
				try {
					inStream = new BufferedInputStream(response.getEntity().getContent());
				} catch (IOException e) {
					logger.error("Exception", e);
					throw(new DownloadingException("Exception creating input stream."));
				}

				logger.debug("ThreadIndex: "+index+"before reading, totalRead      :"+totalRead );
				logger.debug("ThreadIndex: "+index+"before reading, chunkOffset    :"+chunkOffset );
				logger.debug("ThreadIndex: "+index+"before reading, downloadSize   :"+downloadSize );
				logger.debug("ThreadIndex: "+index+"before reading, writeRangeStart:"+writeRangeStart );
				
				//Read bytes from input stream to buffer
				while(totalRead < downloadSize){
					if(status == DownloadStateHolder.PAUSEDFOREXIT){
						request.abort();
						//request.releaseConnection();
						return;
					}
					//Check for status. Pause if paused and exit if cancelled.
					if((status == DownloadStateHolder.PAUSED)) {
						//Nullify last data in write buffer. If application is closed, that would get lost
						//and would not be downloaded on re-start.
						totalRead=writeRangeStart-chunkOffset;
						readBufferSize=0;
						amountReadInLoop=0;
						request.abort();
						//request.releaseConnection();
						previousStatus = DownloadStateHolder.PAUSED;
					}
					while(status == DownloadStateHolder.PAUSED) {
						//Check for sleep in 500ms intervals
						Common.Sleep(500, logger);
					}
					//If there was a pause, raise an exception, so that a new connection will be created.
					if(previousStatus == DownloadStateHolder.PAUSED) {
						previousStatus = DownloadStateHolder.DOWNLOADING;
						timeoutHandler.UpdateStatsWithNewResponseTime(threadInitRespTime);
						throw(new DownloadingException("Timeout exception for pausing."));
					}
					//Exit thread and return if canceled or there is error.
					if((status == DownloadStateHolder.CANCELLED) ||
					   (status == DownloadStateHolder.ERROR)) {
						request.abort();
						//request.releaseConnection();
						return;
					}
						
					splitSize = ((downloadSize-totalRead)<Common.MIN_READ_CHUNK? (downloadSize - totalRead): Common.MIN_READ_CHUNK);
					try {
						while((amountRead = inStream.read(readBuffer, readBufferSize, splitSize-amountReadInLoop)) != -1) {
							//logger.PrintThreadLog("DEBUG", index, "In thread "+index+", Read "+amountRead+" bytes starting at offset "+readBufferSize);
							totalRead += amountRead;
							amountReadInLoop += amountRead;
							readBufferSize+=amountRead;
							//logger.PrintThreadLog("DEBUG", index, "readBufferSize - "+readBufferSize);
							//logger.PrintThreadLog("DEBUG", index, "amountReadInLoop - "+amountReadInLoop);
							if((totalRead == downloadSize) || (amountRead == 0)) {
								break;
							}
						}
					}
					catch(IOException e) {
						logger.error("Exception:", e);
						throw(new DownloadingException("Exception while reading http stream"));
					}

					//logger.PrintThreadLog("DEBUG", index, index+ "- Downloaded bytes in loop-"+amountReadInLoop);
					//logger.PrintThreadLog("DEBUG", index, index+ "- Total read -"+totalRead);
					
					amountReadInLoop=0;
					
					//Write buffer contents to file.
					if((readBufferSize >= Common.MIN_WRITE_CHUNK) || (totalRead == downloadSize)) {
						writer.WriteToFileAtOffset(readBuffer, writeRangeStart, readBufferSize);
						totalWrote=totalRead;
						writeRangeStart=chunkOffset+totalRead;
						readBufferSize=0;
						//logger.PrintThreadLog("DEBUG", index, "Thread"+index+": Wrote "+totalWrote+"bytes");
					}
					retryIndex=1; // Time out error should occur only after max number of successive request failures.
					logger.debug("ThreadIndex: "+index+"after reading, totalRead      :"+totalRead );
					logger.debug("ThreadIndex: "+index+"after reading, writeRangeStart    :"+writeRangeStart );
				}
		
				status=DownloadStateHolder.COMPLETED;
				inStream.close();
				// Release connection
				//request.releaseConnection();
			} catch (DownloadingException e1) {
				//This means a download error. Can be retried
				totalRead=writeRangeStart-chunkOffset;
				readBufferSize=0;
				//request.releaseConnection();
				try{
					inStream.close();
				}catch(Exception e2) {
					logger.error("Exception: ", e2);
				}
				logger.error("ThreadIndex: "+index+"Exception from thread- ", e1);
				logger.error("ThreadIndex: "+index+"state- "+status);
				logger.error("ThreadIndex: "+index+"retry- "+retryIndex);
				
				retryIndex=1;
			} catch (DownloadingFatalException e1) {
				//This means a fatal connection error which should be retried without resetting retryindex.
				totalRead=writeRangeStart-chunkOffset;
				readBufferSize=0;
				//request.releaseConnection();
				try {
					inStream.close();
				} catch(Exception e2) {
					logger.error("Exception: ", e2);
				}
				logger.error("ThreadIndex: "+index+"Exception from thread- ", e1);
				logger.error("ThreadIndex: "+index+"state- "+status);
				logger.error("ThreadIndex: "+index+"retry- "+retryIndex);

				retryIndex++;
				//If we reach maximum retry limit, this indicates a connection problem. Change to pausedforerror state
				if((retryIndex == stateHolder.GetRetryLimit()) && (totalRead > 0)) {
					PauseForError();
					retryIndex=0;
				}
			}catch (IOException e3) {
				//This means some fatal error, exit
				//request.releaseConnection();
				logger.error("ThreadIndex: "+index+"Exception from thread- ", e3);
				try{
					inStream.close();
				}catch(Exception e2) {
					logger.error("Exception: ", e2);
				}
				if((retryIndex == stateHolder.GetRetryLimit()) && (totalRead > 0)) {
					retryIndex=0;
					PauseForError();
					//Common.Sleep(Common.PROGRESS_POLL_INTERVAL * 4);
				}
				else{
					status = DownloadStateHolder.ERROR;
				}
			}
		}
		
		// This should never happen
		if(status == DownloadStateHolder.DOWNLOADING) {
			status = DownloadStateHolder.ERROR;
			//logger.PrintThreadLog("DEBUG", index, "Thread "+index+" has timedout");
		}
	}
	
	public void SingleDownload() {
		//create an instance of HttpClient
		DefaultHttpClient httpClient = new DefaultHttpClient();

		if(stateHolder.UseProxy()) {
			logger.debug("Setting proxy");
			client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, (new HttpHost(stateHolder.GetHttpProxyServer(), stateHolder.GetHttpProxyPort())));
			//Set authentication if required
			if(stateHolder.GetHttpAuthenUser() !="") {
				client.getCredentialsProvider().setCredentials(new AuthScope("localhost", 8080), new UsernamePasswordCredentials(stateHolder.GetHttpAuthenUser(), stateHolder.GetHttpAuthenPassword()));
			}
		}
		
		//create a method instance
		HttpGet getMethod = new HttpGet(urlName);
		client.setHttpRequestRetryHandler(new DownloadRetryHandler());
		
		try {
			//execute the method
			HttpResponse response = httpClient.execute(getMethod);

			if(response.getStatusLine().getStatusCode() != 200) {
				status=DownloadStateHolder.ERROR;
				return;
			}
			//get the resonse as an InputStream
			InputStream in = response.getEntity().getContent();

			byte[] b = new byte[1024];
			int len;

			while ((len = in.read(b)) != -1) {
				writer.WriteToFile(b, len);
			}

			in.close();
			//getMethod.releaseConnection();
		}catch(Exception e){
			logger.error("Exception", e);
			//do something
			status = DownloadStateHolder.ERROR;
		}
	}
	
	public void Pause() {
		status=DownloadStateHolder.PAUSED;
		stateHolder.SetThreadStatus(index, status);
	}
	
	public void PauseForExit() {
		status=DownloadStateHolder.PAUSEDFOREXIT;
		stateHolder.SetThreadStatus(index, status);
	}
	
	public void PauseForError() {
		status=DownloadStateHolder.PAUSEDFORERROR;
		stateHolder.SetThreadStatus(index, status);
	}

	public void Resume() {
		status=DownloadStateHolder.DOWNLOADING;
		stateHolder.SetThreadStatus(index, status);
	}
	
	public void Cancel() {
		status=DownloadStateHolder.CANCELLED;
		stateHolder.SetThreadStatus(index, status);
	}
	
	public int GetStatus() {
		return status;
	}
	
	public long GetTotalRead() {
		return totalRead;
	}
	
	public int GetDownloadSize() {
		return downloadSize;
	}
	
	public int GetWriteRangeStart() {
		return this.writeRangeStart;
	}
}


