/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Url downloader class that creates downloader threads and reaps them
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         06/02/10 - Check for accept range and use single thread if "none" is returned by server.
 *   arun         06/07/09 - Move cancel validation here to make history xml updating synchronized
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.core.urlcore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.event.EventListenerList;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import skdownloader.core.Common;
import skdownloader.core.DownloadListener;
import skdownloader.core.DownloadStateHolder;
import skdownloader.core.DownloaderInterface;
import skdownloader.core.DownloaderThread;
import skdownloader.core.UserIOUtils;
import skdownloader.core.MyFileWriter;
import skdownloader.downloadmanager.SystemInfoHolder;

public class UrlDownloader extends Thread implements DownloaderInterface {
	UserIOUtils ioutils;

	MyFileWriter writer;
	
	//Progress updater thread updates state holder in regular intervals
	UrlDownloadProgressAdminThread progressAdminThread;
	
	//Downloader thread for each doenload chunk
	DownloaderThread[] urlDownloaderThread;
	
	//Stateholder to hold download status/info
	DownloadStateHolder stateHolder;
	SystemInfoHolder sysInfoHolder;
	
	private boolean silent=false;
	private boolean singleTonDownload=false; //This is for a single download without any extra headers sent. 
	
	private final EventListenerList listeners = new EventListenerList();
	
	private static Logger logger = Logger.getLogger(UrlDownloader.class);
	
	public UrlDownloader() {
		progressAdminThread=null;
	}
	
	public UrlDownloader(UserIOUtils ioutils, SystemInfoHolder sysInfoHolder) {
		this.ioutils=ioutils;
		stateHolder = new DownloadStateHolder(this,ioutils);
		this.sysInfoHolder = sysInfoHolder; 
		//Set proxy if required.
		if(sysInfoHolder.USEPROXY)
			stateHolder.SetProxy(sysInfoHolder.HTTPPROXYSERVER, sysInfoHolder.HTTPPROXYPORT,sysInfoHolder.HTTPPROXYUSER, sysInfoHolder.HTTPPROXYPASSWORD,"", "", "", "");
		//Set cache control
		if(sysInfoHolder.HTTPPROXYNOTUSECACHE)
			stateHolder.SetUseCache(false);
	}
	
	public UrlDownloader(String urlname, String dirname, int numConnections, int minAcclelerateFileSize, SystemInfoHolder sysInfoHolder, UserIOUtils ioutils)	{
		this.ioutils=ioutils;
		this.sysInfoHolder=sysInfoHolder;

		stateHolder = new DownloadStateHolder(this, ioutils);
		stateHolder.SetDirName(dirname);
		stateHolder.SetUrlName(urlname);
		stateHolder.SetFileName("Unknown");
		stateHolder.SetFileSize(-1);
		stateHolder.SetNumConnections(numConnections);
		stateHolder.SetMinAccelerateFileSize(minAcclelerateFileSize);
		stateHolder.SetRetryLimit(sysInfoHolder.CONNECTRETRYLIMIT);
		progressAdminThread=null;
		
		String tmpUrl=urlname.toLowerCase();
		//if(tmpUrl.matches("^http://"))
		if(tmpUrl.regionMatches(true, 0, "http://", 0, 7)) {
			stateHolder.SetConnectionProtocol(DownloadStateHolder.HTTP);
		}
		//else if(tmpUrl.matches("^https://"))
		else if(tmpUrl.regionMatches(true, 0, "https://", 0, 8)) {
			stateHolder.SetConnectionProtocol(DownloadStateHolder.HTTPS);
		}
		else if(tmpUrl.regionMatches(true, 0, "ftp://", 0, 6)) {
			stateHolder.SetConnectionProtocol(DownloadStateHolder.FTP);
		}
		
		//Set proxy if required.
		if(sysInfoHolder.USEPROXY)
			stateHolder.SetProxy(sysInfoHolder.HTTPPROXYSERVER, sysInfoHolder.HTTPPROXYPORT,sysInfoHolder.HTTPPROXYUSER, sysInfoHolder.HTTPPROXYPASSWORD,"", "", "", "");
		//Set cache control
		if(sysInfoHolder.HTTPPROXYNOTUSECACHE)
			stateHolder.SetUseCache(false);
	}
	
	public DownloadStateHolder GetStateHolder() {
		return stateHolder;
	}
	
	public void run() {
		Download();
	}

	public int SilentDownload() {
		silent=true;
		if(singleTonDownload)
			return SingleTonDownload();
		return Common.OK;
	}
	
	public int Download() {
		long splitFileSize = 0;
		long totalAllocated = 0;
		int respTime=0;
		
		String outFileName = ""; //stateHolder.GetDirName()+File.separator+stateHolder.GetFileName();
		ArrayList<String> arrLst = new ArrayList<String>();

		//Check if ftp download
		if(stateHolder.GetConnectionProtocol() == DownloadStateHolder.FTP) {
			String filename=null;
			stateHolder.SetNumConnections(1);
			
			//Name of file
			try	{
				int sep = stateHolder.GetUrlName().lastIndexOf("/");
				filename = stateHolder.GetUrlName().substring(sep + 1);
				stateHolder.SetFileName(filename);
			} catch (Exception e) {
				logger.error("Exception", e);
				ioutils.PrintError("Invalid url: "+Common.ShortenString(stateHolder.GetUrlName(), "middle",50 ));
				if(writer != null)
					writer.Close();
				return Common.ERROR;
			}
			
			//Set name of file including path
			outFileName=stateHolder.GetDirName()+File.separator+filename;
			
			//Check if file to download already exists. If so, warn the user.
			if( (new File(outFileName)).exists()&& ! silent) {
				if(! ioutils.GetFromYesNoMessage(stateHolder.GetParentComponent(), "Output file \""+outFileName+"\" already exists."+System.getProperty("line.separator")+"Do you want to delete existing file and continue with download"))	{
					this.Cancel("Cancelled as already downloaded.", DownloadStateHolder.CANCELLED);
					return Common.ERROR;
				}
			}
			if( (new File(outFileName+"_skdownloader")).exists() && ! silent) {
				ioutils.PrintMessage("Tmp file \""+outFileName+"_skdownloader"+"\" already exists."+System.getProperty("line.separator")+"You may be in the process of downloading this file. If not, please remove this file and try again.");
				this.Cancel("Cancelled as download already in progress.", DownloadStateHolder.CANCELLED);
				return Common.ERROR;
			}
			
			//Create writer.
			try {
				writer = new MyFileWriter(outFileName, ioutils);
			} catch(Exception e) {
				logger.error("Exception", e);
				ioutils.PrintError("Error while writing to \""+Common.ShortenString(outFileName, "middle",50 )+"\".");
				Cancel("Cancelled due to disk write error.", DownloadStateHolder.ERROR);
				return Common.ERROR;
			}
			
			// Set starting time
			if(stateHolder.GetStartTime() == 0)
				stateHolder.SetStartTime(Common.GetCurrentTime());
			stateHolder.SetFileSize(-2);
			
			urlDownloaderThread = new FtpUrlDownloaderThread[1];
			urlDownloaderThread[0] = new FtpUrlDownloaderThread(stateHolder, writer, ioutils, stateHolder.GetUrlName(), outFileName);
			((FtpUrlDownloaderThread)urlDownloaderThread[0]).start();
			
			progressAdminThread = new UrlDownloadProgressAdminThread(1, writer, urlDownloaderThread, ioutils, stateHolder);
			progressAdminThread.start();
			
			stateHolder.SetDownloadStatus(DownloadStateHolder.DOWNLOADING);
			//Finish/join all downloader threads
			for (int i=1; i <=stateHolder.GetNumConnections(); ++i)	{
				try	{
					((FtpUrlDownloaderThread)urlDownloaderThread[i-1]).join();
					//Check for exited thread status. If it is error, set it.
					if(urlDownloaderThread[i-1].GetStatus() == DownloadStateHolder.ERROR)
						stateHolder.SetDownloadStatus(DownloadStateHolder.ERROR);
				}
				catch (InterruptedException e) {
					logger.error("Exception", e);
				}
			}
			//Set end time
			stateHolder.SetEndTime(Common.GetCurrentTime());
            stateHolder.SetFileSize((int) urlDownloaderThread[0].GetTotalRead());
            stateHolder.SetDownloadedSize((int) urlDownloaderThread[0].GetTotalRead());
			
			//If download completed without issues, set status to completed
			if(stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) {
				stateHolder.SetDownloadStatus(DownloadStateHolder.COMPLETED);
			}
			
			//Finish/join admin thread
			try	{
				progressAdminThread.join();
			}
			catch (InterruptedException e) {
				logger.error("Exception", e);
				ioutils.PrintError("Encountered an internal error with admin thread...");
				writer.Close();
				return Common.ERROR;
			}
			writer.Close();
			return Common.OK;
		}

		// This is for http downloads
		//Check if this is a resume of a paused download
		if((stateHolder.GetStatus() == DownloadStateHolder.PAUSED) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.PAUSEDFORERROR) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.PAUSEDFOREXIT)) {
			//No need to do any error checking.
			//Set output file name
			outFileName = stateHolder.GetDirName()+File.separator+stateHolder.GetFileName();
			
			//Set initial average response time
			respTime=10000;

		}
		else {
			arrLst = GetFileNameAndSize(stateHolder.GetUrlName());
			
			//If we get an error here, there is something wrong with url. Try a simple download
			if(arrLst.get(4).equals("ERROR")) {
				if(SingleBlindDownload(stateHolder.GetUrlName())==Common.ERROR) {
					Cancel((String)arrLst.get(5), DownloadStateHolder.ERROR);
					ioutils.PrintError("Error downloding url- "+Common.ShortenString(stateHolder.GetUrlName(), "middle",50 )+Common.LINESEPERATOR+
				          	stateHolder.GetErrorMessage()+Common.LINESEPERATOR+
				          	"Please check the url and try again");
					return Common.ERROR;
				}
				return Common.OK;
			}
			
			/*if(arrLst.get(4).equals("ERROR")) {
				if(! silent)
					logger.PrintError("Error downloding url- "+Common.ShortenString(stateHolder.GetUrlName(), "middle",50 )+Common.LINESEPERATOR+
					          	arrLst.get(5)+Common.LINESEPERATOR+
					          	"Please check the url and try again");
				Cancel((String)arrLst.get(5), DownloadStateHolder.ERROR);
				return Common.ERROR;
			}*/
		
			//Check and set file name
			if (arrLst.get(0).equals("")) {
				if(ioutils.GetFromYesNoMessage(stateHolder.GetParentComponent(),
	    			                   	"URL- "+stateHolder.GetUrlName()+Common.LINESEPERATOR+
	    			                   	"Could not get valid file name for download."+Common.LINESEPERATOR+
	    			                   	"Press 'yes' to enter a file name or 'no' to Cancel download!!"))
				{
					outFileName = ioutils.GetFileFromUser(stateHolder.GetParentComponent());
					stateHolder.SetFileName(Common.GetFileNameFromAbsPath(outFileName));
					stateHolder.SetDirName(Common.GetPathNameFromAbsPath(outFileName));
					if(stateHolder.GetFileName().equals("")) {
						//Cancel download
						Cancel("Cancelled by user.", DownloadStateHolder.CANCELLED);
						return Common.ERROR;
					}
				}
				else {
					//This case should never come.
					if(! silent)
						ioutils.PrintError("An internal error has occurred accessing this url " + Common.LINESEPERATOR+
	    				          	"Please mail this error including the url, downloader version" +
	    				          	" and your operating system to admin@toolsbysk.com");
					Cancel("Internal download Error.", DownloadStateHolder.ERROR);
					return Common.ERROR;
				}
			}
			else {
				stateHolder.SetFileName((String)arrLst.get(0));
			}
			
			//Check if output dir exists
			if(Common.CheckFileExists(stateHolder.GetDirName(), logger)!=Common.OK)	{
				//Dir does not exist
				if(! silent)
					ioutils.PrintError("Output directory \""+Common.ShortenString(stateHolder.GetDirName(), "middle",50 )+"\"does not exist");
				Cancel("Invalid download directory.", DownloadStateHolder.ERROR);
				return Common.ERROR;
			}
			
			//Set output file name
			outFileName = stateHolder.GetDirName()+File.separator+stateHolder.GetFileName();
			
			//Set download url
			stateHolder.SetUrlName((String)arrLst.get(1));
			//	logger.PrintLog("DEBUG", "Using url - "+stateHolder.GetUrlName());
			if(arrLst.get(6).equals("0")) {
				stateHolder.SetNumConnections(1);
			}
			
			//Set file size
			stateHolder.SetFileSize(Integer.parseInt((String)arrLst.get(2)));
			if(stateHolder.GetFileSize() != -1)	{
				//If file size is less than min. size, do not accelerate.
				if(stateHolder.GetFileSize() < stateHolder.GetMinAccelerateFileSize()) {
					stateHolder.SetNumConnections(1);
				}
				//Check if file to download already exists. If so, warn the user.
				if( (new File(outFileName)).exists() && ! silent) {
					if(! ioutils.GetFromYesNoMessage(stateHolder.GetParentComponent(), "Output file \""+outFileName+"\" already exists."+System.getProperty("line.separator")+"Do you want to delete existing file and continue with download"))	{
						this.Cancel("Cancelled as already downloaded.", DownloadStateHolder.CANCELLED);
						return Common.ERROR;
					}
				}
				if( (new File(outFileName+"_skdownloader")).exists()) {
					if(! silent)
						ioutils.PrintMessage("Tmp file \""+outFileName+"_skdownloader"+"\" already exists."+System.getProperty("line.separator")+"You may be in the process of downloading this file. If not, please remove this file and try again.");
					this.Cancel("Cancelled as download already in progress.", DownloadStateHolder.CANCELLED);
					return Common.ERROR;
				}
				
				//	Create file with size
				if(Common.CreateFileWithSize(outFileName, stateHolder.GetFileSize(), logger) != Common.OK) {
					if(! silent)
						ioutils.PrintError("Could not create file "+Common.ShortenString(outFileName, "middle",50 ));
					Cancel("File creation error.", DownloadStateHolder.ERROR);
					return Common.ERROR;
				}
			}
			else {
				//Force the number of threads as 1
				stateHolder.SetNumConnections(1);
			}
			
			//Set initial average response time
			respTime=Integer.parseInt((String)arrLst.get(3));
		}
		
		//Create writer object
		try	{
			writer = new MyFileWriter(outFileName, ioutils);
		} catch(Exception e) {
			logger.error("Exception", e);
			if(! silent)
				ioutils.PrintError("Error while writing to \""+Common.ShortenString(outFileName, "middle",50 )+"\".");
			Cancel("Cancelled due to disk write error.", DownloadStateHolder.ERROR);
			return Common.ERROR;
		}
		
		//Check if download was started with paused status and pause if required
		//while(stateHolder.GetStatus() == DownloadStateHolder.PAUSED)
		//{
			//Check for sleep in 500ms intervals
		//	Common.Sleep(500);
		//}
		
		//Check if download is cancelled
		if(stateHolder.GetStatus() == DownloadStateHolder.CANCELLED) {
			//Remove file and return
			writer.CANCELWRITE=1;
			writer.Close();
			return Common.OK;
		}
		
		//logger.PrintLog("DEBUG", "Starting download");
	    //Create thread array and start download
		urlDownloaderThread = new HttpUrlDownloaderThread[stateHolder.GetNumConnections()];
		try	{
			for(int i=1; i<=stateHolder.GetNumConnections(); i++) {
				//If the downloader is resuming from a paused state, we need not do calculations
				if((stateHolder.GetStatus() == DownloadStateHolder.PAUSEDFORERROR) ||
				   (stateHolder.GetStatus() == DownloadStateHolder.PAUSEDFOREXIT)) {
					//Reset pause start time
					stateHolder.SetPausedTime((int)(Common.GetCurrentTime() - stateHolder.GetPauseStartTime()));
					stateHolder.SetPauseStartTime(0);
					
					urlDownloaderThread[i-1] = new HttpUrlDownloaderThread(stateHolder.GetUrlName(), 
							                                           stateHolder,
							                                           DownloadStateHolder.DOWNLOADING, 
							                                           writer, 
							                                           stateHolder.GetThreadFileSize(i-1), 
							                                           stateHolder.GetThreadChunkOffset(i-1), 
							                                           stateHolder.GetThreadWriteRangeStart(i-1), 
							                                           i-1, 
							                                           respTime, 
							                                           ioutils);
					//logger.PrintLog("DEBUG", "Creating thread index "+i+" downloadsize "+splitFileSize+" Offset "+totalAllocated);
					((HttpUrlDownloaderThread)urlDownloaderThread[i-1]).start();
				}
				else if(stateHolder.GetStatus() == DownloadStateHolder.PAUSED)
				{
					urlDownloaderThread[i-1] = new HttpUrlDownloaderThread(stateHolder.GetUrlName(), 
							                                           stateHolder,
							                                           DownloadStateHolder.PAUSED, 
							                                           writer, 
							                                           stateHolder.GetThreadFileSize(i-1), 
							                                           stateHolder.GetThreadChunkOffset(i-1), 
							                                           stateHolder.GetThreadWriteRangeStart(i-1), 
							                                           i-1, 
							                                           respTime, 
							                                           ioutils);
					//logger.PrintLog("DEBUG", "Creating thread index "+i+" downloadsize "+splitFileSize+" Offset "+totalAllocated);
					((HttpUrlDownloaderThread)urlDownloaderThread[i-1]).start();
				}
				else {
					//If there is only one connection, allocate full size to that thread
					if(stateHolder.GetNumConnections() == -1) {
						splitFileSize=stateHolder.GetFileSize();
					}
					else {
						splitFileSize = (i<stateHolder.GetNumConnections()? (long)stateHolder.GetFileSize()/stateHolder.GetNumConnections(): stateHolder.GetFileSize()-totalAllocated);
					}
					
					urlDownloaderThread[i-1] = new HttpUrlDownloaderThread(stateHolder.GetUrlName(), 
							                                           stateHolder, 
							                                           DownloadStateHolder.DOWNLOADING,
							                                           writer,
							                                           splitFileSize, 
							                                           totalAllocated,
							                                           totalAllocated,
							                                           i-1, 
							                                           respTime, 
							                                           ioutils);
					//logger.PrintLog("DEBUG", "Creating thread index "+i+" downloadsize "+splitFileSize+" Offset "+totalAllocated);
					((HttpUrlDownloaderThread)urlDownloaderThread[i-1]).start();
					stateHolder.SetThreadFileSize(i-1, (int)splitFileSize);
					stateHolder.SetThreadChunkOffset(i-1, (int)totalAllocated);
					//urlDownloaderThread[i-1].join();
					totalAllocated += splitFileSize;
				}
			}
			// Set starting time
			if(stateHolder.GetStartTime() == 0)
				stateHolder.SetStartTime(Common.GetCurrentTime());
			
			//Set download status
			if(stateHolder.GetStatus() != DownloadStateHolder.PAUSED)
				stateHolder.SetDownloadStatus(DownloadStateHolder.DOWNLOADING);
			
			//Start download progress logger thread
			progressAdminThread = new UrlDownloadProgressAdminThread(stateHolder.GetNumConnections(), writer, urlDownloaderThread, ioutils, stateHolder);
			progressAdminThread.start();
			
			//Finish/join all downloader threads
			for (int i=1; i <=stateHolder.GetNumConnections(); ++i) {
				try	{
					((HttpUrlDownloaderThread)urlDownloaderThread[i-1]).join();
					//Check for exited thread status. If it is error, set it.
					if(urlDownloaderThread[i-1].GetStatus() == DownloadStateHolder.ERROR)
						stateHolder.SetDownloadStatus(DownloadStateHolder.ERROR);
				}
				catch (InterruptedException e) {
					logger.error("Exception", e);
					//logger.PrintLog("LOGALL", Common.GetStackTrace(e));
				}
			}
			//Set end time
			stateHolder.SetEndTime(Common.GetCurrentTime());
			
			//If download completed without issues, set status to completed
			if(stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) {
				stateHolder.SetDownloadStatus(DownloadStateHolder.COMPLETED);
				fireDownloadComplete();
				stateHolder.SetDownloadSpeed(0);
			}
			
			//Finish/join admin thread
			try	{
				progressAdminThread.join();
			}
			catch (InterruptedException e) {
				logger.error("Exception", e);
				if(! silent)
					ioutils.PrintError("Encountered an internal error with admin thread...");
			}
		}catch (Exception e){
			logger.error("Exception", e);
			if(! silent)
				ioutils.PrintError("Caught internal error while downloading");
			Cancel("Internal download Error.", DownloadStateHolder.ERROR);
		}
		
		writer.Close();
		return Common.OK;
	}
	
	public void Pause()	{
		//Set global pause
		stateHolder.SetDownloadStatus(DownloadStateHolder.PAUSED);
		
		//Set pause start time
		stateHolder.SetPauseStartTime(Common.GetCurrentTime());
		
		//Pause individual threads
		for(int i=0; i<stateHolder.GetNumConnections(); i++) {
			//Pause the thread
			urlDownloaderThread[i].Pause();
			
			//Update stateholder writerange start. This is just to make sure we store the latest
			stateHolder.SetThreadWriteRangeStart(i, ((HttpUrlDownloaderThread) urlDownloaderThread[i]).GetWriteRangeStart());
			
		}
	}
	
	public void PauseForExit() {
		//Set global pause
		stateHolder.SetDownloadStatus(DownloadStateHolder.PAUSEDFOREXIT);
		
		//Set pause start time
		stateHolder.SetPauseStartTime(Common.GetCurrentTime());
		
		//Pause individual threads
		for(int i=0; i<stateHolder.GetNumConnections(); i++) {
			//Pause the thread
			urlDownloaderThread[i].PauseForExit();
			
			//Update stateholder writerange start. This is just to make sure we store the latest
			stateHolder.SetThreadWriteRangeStart(i, ((HttpUrlDownloaderThread) urlDownloaderThread[i]).GetWriteRangeStart());
		}
	}
	
	public void PauseForError()	{
		//Set global pause
		stateHolder.SetDownloadStatus(DownloadStateHolder.PAUSEDFORERROR);
		
		//Set pause start time
		stateHolder.SetPauseStartTime(Common.GetCurrentTime());
		
		//Pause individual threads
		for(int i=0; i<stateHolder.GetNumConnections(); i++) {
			//Pause the thread
			urlDownloaderThread[i].PauseForError();
			
			//Update stateholder writerange start. This is just to make sure we store the latest
			stateHolder.SetThreadWriteRangeStart(i, ((HttpUrlDownloaderThread) urlDownloaderThread[i]).GetWriteRangeStart());
		}
	}
	
	public void Resume() {
		//Set global resume
		stateHolder.SetDownloadStatus(DownloadStateHolder.DOWNLOADING);
		
		//Calculate pause end time
		//stateHolder.SetPauseEndTime(Common.GetCurrentTime());
		stateHolder.SetPausedTime((int)(Common.GetCurrentTime() - stateHolder.GetPauseStartTime()));
		stateHolder.SetPauseStartTime(0);
		
		//Resume individual threads
		for(int i=0; i<stateHolder.GetNumConnections(); i++) {
			urlDownloaderThread[i].Resume();
		}
	}
	
	public void Cancel(String msg, int state) {
		stateHolder.SetErrorMessage(msg);
	
		//Set pause start time
		stateHolder.SetPauseStartTime(0);
		stateHolder.SetPausedTime(0);
		
		//For a started download, cancel all threads
		if((stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.PAUSED))	{
			//Cancel individual threads
			for(int i=0; i<stateHolder.GetNumConnections(); i++) {
				urlDownloaderThread[i].Cancel();
			}
		}

		//Set global cancel
		if(state != DownloadStateHolder.EXITING)
			stateHolder.SetDownloadStatus(state);
		
		//Close writer if initialized
		if(writer != null) {
			writer.Cancel();
		}
		
		//ivalidate its state, so it will get reflected in history xml
		stateHolder.ValidateDownload();
	}
	
	public int SingleTonDownload() {
		ArrayList<String> arrLst = GetFileNameAndSize(stateHolder.GetUrlName());
		
		if(arrLst.get(4).equals("ERROR")) {
				stateHolder.SetErrorMessage("Error downloding url- "+Common.ShortenString(stateHolder.GetUrlName(), "middle",50 )+Common.LINESEPERATOR+
				          	arrLst.get(5)+Common.LINESEPERATOR+
				          	"Please check the url and try again");
			return Common.ERROR;
		}
	
		//Check and set file name
		if (arrLst.get(0).equals("")) {
			stateHolder.SetErrorMessage("Could not get valid file name for download.");
			return Common.ERROR;
		}
		else {
			stateHolder.SetFileName((String)arrLst.get(0));
		}
		
		//Check if output dir exists
		if(Common.CheckFileExists(stateHolder.GetDirName(), logger)!=Common.OK)	{
			stateHolder.SetErrorMessage("Output directory \""+Common.ShortenString(stateHolder.GetDirName(), "middle",50 )+"\"does not exist");
			return Common.ERROR;
		}
		
		//Set output file name
		String outFileName = stateHolder.GetDirName()+File.separator+stateHolder.GetFileName();
		
		//Set download url
		stateHolder.SetUrlName((String)arrLst.get(1));
		
		//	logger.PrintLog("DEBUG", "Using url - "+stateHolder.GetUrlName());
		stateHolder.SetNumConnections(1);
		
		//Set file size
		stateHolder.SetFileSize(Integer.parseInt((String)arrLst.get(2)));
		
		return SingleDownload(stateHolder.GetUrlName(), outFileName);
	}
	
	public int SingleBlindDownload(String url) {
		HttpResponse response = GetDownloadResponse(url);
		if(response == null)
			return Common.ERROR;
		
		else {
			String disposition = GetHeaderValue(response.getAllHeaders(), "Content-Disposition");
			if(disposition == null) {
				stateHolder.SetErrorMessage("Could not get file name!!!");
				return Common.ERROR;
			}
				
			stateHolder.SetFileName(disposition);
			String outfile = stateHolder.GetDirName()+File.separator+stateHolder.GetFileName();
			
			try {
				InputStream in = response.getEntity().getContent();
		    	FileOutputStream fop=new FileOutputStream(new File(outfile));
		    	
		    	byte[] b = new byte[1024];
				int len;
				while ((len = in.read(b)) != -1) {
					fop.write(b, 0, len);
				}
				fop.close();
		    	
		    } catch(Exception e) {
		    	logger.error("Exception", e);
		    	stateHolder.SetErrorMessage("Error while downloading url");
	    		return Common.ERROR;
		    }
		}
		return Common.OK;
	}
	
	public int SingleDownload(String url, String outfile) {
		HttpResponse response = GetDownloadResponse(url);
		if(response == null)
			return Common.ERROR;
		
		try {
			InputStream in = response.getEntity().getContent();
	    	FileOutputStream fop=new FileOutputStream(new File(outfile));
	    	
	    	byte[] b = new byte[1024];
			int len;
			while ((len = in.read(b)) != -1) {
				fop.write(b, 0, len);
			}
			fop.close();
	    	
	    } catch(Exception e) {
	    	logger.error("Exception", e);
	    	stateHolder.SetErrorMessage("Error while downloading url");
    		return Common.ERROR;
	    }
	    
		return Common.OK;
	}
	
	public HttpResponse GetDownloadResponse(String url) {
		DefaultHttpClient client = new DefaultHttpClient();
		if(stateHolder.UseProxy()) {
			//logger.PrintLog("DEBUG", "Setting proxy");
			client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, (new HttpHost(stateHolder.GetHttpProxyServer(), stateHolder.GetHttpProxyPort())));
			//Set authentication if required
			if(stateHolder.GetHttpAuthenUser() !="") {
				client.getCredentialsProvider().setCredentials(new AuthScope("localhost", 8080), new UsernamePasswordCredentials(stateHolder.GetHttpAuthenUser(), stateHolder.GetHttpAuthenPassword()));
			}
		}
		client.getParams().setParameter("http.socket.timeout", new Integer(60000));
		client.getParams().setParameter("http.connection.timeout", new Integer(60000));

	    // Create a method instance.
		url=Common.forHTML(url);
		HttpGet method = new HttpGet(url);
		method.addHeader("Cache-Control", "no-cache");
		method.getParams().setBooleanParameter("http.protocol.handle-redirects", true);
		method.addHeader("User-Agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows 2000)");
	    client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.RFC_2965);

	    // Provide custom retry handler is necessary
	    client.setHttpRequestRetryHandler(new DownloadRetryHandler());
		client.setRedirectHandler(new CustomRedirectHandler());
	    client.addResponseInterceptor((HttpResponseInterceptor) new SKHttpResponseInterceptor());

	    try {
	    	// Execute the method.
	    	HttpResponse response = client.execute(method);

	    	if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK || response.getEntity() == null) {
	    		stateHolder.SetErrorMessage("Method failed: " + response.getStatusLine());
	    		return null;
	    	}
	    	
	    	//InputStream in = response.getEntity().getContent();
	    	return response;
	    }catch(Exception e){
	    	logger.error("Exception", e);
	    	return null;
	    }
	}
	
	/*************************************************************
	 * 
	 * Function  : GetFileNameAndSize
	 * Arguments : url as a string
	 * Return    : Array having filename, final url, file size, response time, connection status, any error messages.
	 * Notes     : From a url, checks if it exists and extracts details. Details are returned as an array list.
	 * 
	 *************************************************************/
	public ArrayList<String> GetFileNameAndSize(String urlString) {		
		ArrayList<String> arrLst = new ArrayList<String>();
		arrLst.add("");        //0 - filename
		arrLst.add(urlString); //1 - Final url
		arrLst.add("-1");      //2 - file size
		arrLst.add("0");       //3 - response time
		arrLst.add("OK");      //4 - status
		arrLst.add("");        //5 - Error message if any
		arrLst.add("1");       //6 - If range request is bytes.
		arrLst.add("0");       //7 - If compression is used
		
		long t1, t2;
		
		final String FILE_NAME_SUBSTR = "filename=";
		HttpResponse response=null;
		try	{
			t1 = (new Date()).getTime();
			DefaultHttpClient client = new DefaultHttpClient();
			client.getParams().setBooleanParameter("http.protocol.handle-redirects", false);
		    client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.RFC_2965);
			if(stateHolder.UseProxy()) {
				//logger.PrintLog("DEBUG", "Setting proxy");
				client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, (new HttpHost(stateHolder.GetHttpProxyServer(), stateHolder.GetHttpProxyPort())));
				//Set authentication if required
				if(stateHolder.GetHttpAuthenUser() !="") {
					client.getCredentialsProvider().setCredentials(new AuthScope("localhost", 8080), new UsernamePasswordCredentials(stateHolder.GetHttpAuthenUser(), stateHolder.GetHttpAuthenPassword()));
				}
			}
			
			HttpHead head = new HttpHead(urlString);
			//We want to capture the redirect link and extract final url later.
			head.addHeader("Cache-Control", "no-cache");
			head.addHeader("User-Agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows 2000)");
			try {
				response=client.execute(head);
			} catch (Exception e) {
				logger.error("Exception", e);
			}
			t2 = (new Date()).getTime();
			arrLst.set(3, Integer.toString((int)(t2-t1)));
			
			//Header[] headers= response.getAllHeaders();
			//for(int i=0; i<headers.length; i++)	{
			//	logger.PrintLog("DEBUG", "Header: "+headers[i].getName()+": "+headers[i].getValue());
			//}
			//logger.PrintLog("DEBUG", "response code- "+response);
			
			//If there is a redirection url.
			if((response.getStatusLine().getStatusCode() == 301) ||
			   (response.getStatusLine().getStatusCode() == 302) ||
			   (response.getStatusLine().getStatusCode() == 303) ||
			   (response.getStatusLine().getStatusCode() == 307)) {
				if((GetHeaderValue(response.getAllHeaders(), "Location") != "") &&
						(GetHeaderValue(response.getAllHeaders(), "Location") != urlString)) {
					arrLst=GetFileNameAndSize(GetHeaderValue(response.getAllHeaders(), "Location"));
					return arrLst;
				}
			}

			if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
				
				arrLst.set(4, "ERROR");
				arrLst.set(5, "Failed with- \""+response+":"+EntityUtils.toString(response.getEntity())+"\"");
				return arrLst;
			}
			
			if((GetHeaderValue(response.getAllHeaders(), "Accept-Ranges")).equals("none")) {
				arrLst.set(6, "0");
			}
			
			//read Content-Disposition header to detect filename if present
			String disposition = GetHeaderValue(response.getAllHeaders(), "Content-Disposition");
			if(disposition!=null&&disposition.lastIndexOf(FILE_NAME_SUBSTR)>-1)	{
				String fname=disposition.substring(
						disposition.lastIndexOf(FILE_NAME_SUBSTR)
						+FILE_NAME_SUBSTR.length());
				if(fname.endsWith(";")){
					fname=fname.substring(0,fname.length()-1);
				}
				else if (fname.matches("\".*\"")) {
					fname=fname.substring(1,fname.length()-1);
				}
				//logger.PrintLog("DEBUG", "File name got is-"+fname);
				//File name could contain unwanted html GET/POST variable data starting with "?". 
				//Remove it.
				int sep = fname.indexOf("?");
				if(sep !=-1) {
					fname=fname.substring(0, sep);
				}
				//Take file name from ending separator
				sep = fname.lastIndexOf("/");
				fname = fname.substring(sep + 1);
				fname=Common.ReplaceString("%20", " ", fname);
				arrLst.set(0, fname);
			}
			else {
				URL url = new URL(urlString);
				String fileName=url.getFile();
				//File name could contain unwanted html GET/POST variable data starting with "?". 
				//Remove it.
				int sep = fileName.indexOf("?");
				if(sep !=-1) {
					fileName=fileName.substring(0, sep);
				}
				//Take file name from ending separator
				sep = fileName.lastIndexOf("/");
				fileName = fileName.substring(sep + 1);
				fileName=Common.ReplaceString("%20", " ", fileName);
				arrLst.set(0, fileName);
			}
			arrLst.set(2, GetHeaderValue(response.getAllHeaders(), "Content-Length"));
			return arrLst;

		} catch (Exception e) {
			logger.error("Exception", e);
			arrLst.set(0, "");
			arrLst.set(2, "-1");
			arrLst.set(3, "0");
			arrLst.set(4, "ERROR");
			arrLst.set(5, "Network error. Could not establish connection");
			return arrLst;
		}
	}

	public String GetHeaderValue(Header[] headers, String headerName) {
		String ret = "";
		for(int i=0; i<headers.length; i++)	{
			logger.debug("Header: "+headers[i].getName()+" : "+headers[i].getValue());
			if(headers[i].getName().matches("(?i)"+headerName))
			{
				//logger.debug("Header: "+headers[i].getName()+" : "+headers[i].getValue());
				ret=headers[i].getValue();
			}
		}
		return ret;
	}
	
    public void AddDownloadListener(DownloadListener listener) {
        listeners.add(DownloadListener.class, listener);
    }

    public void RemoveDownloadListener(DownloadListener listener) {
        listeners.remove(DownloadListener.class, listener);
    }

    public DownloadListener[] GetDownloadListener() {
        return listeners.getListeners(DownloadListener.class);
    }

    protected void fireDownloadComplete() {
        for (DownloadListener listener : GetDownloadListener()) {
            listener.DownloadComplete(this.stateHolder);
        }
    }
}
