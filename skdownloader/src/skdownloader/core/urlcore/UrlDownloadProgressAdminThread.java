/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Admin thread for monitoring thread status and updating download states.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.core.urlcore;

import javax.swing.Timer;

import org.apache.log4j.Logger;

import skdownloader.core.Common;
import skdownloader.core.DownloadStateHolder;
import skdownloader.core.DownloaderThread;
import skdownloader.core.UserIOUtils;
import skdownloader.core.MyFileWriter;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;;

class UrlDownloadProgressAdminThread  extends Thread {
	int numThreads;
	HttpUrlDownloaderThread[] threadArr;
	long totalSize;
	UserIOUtils ioutils;
	DownloadStateHolder stateHolder;
	long lastDownloaded;
	int threadLastDownloaded[];
	float downloadSpeed;
	//MyFileWriter writer;
	long speedUpdateStartTime;
	long speedUpdateEndTime;
	Timer timer;
	private static Logger logger = Logger.getLogger(UrlDownloadProgressAdminThread.class);
	
	public UrlDownloadProgressAdminThread(int numThreads, MyFileWriter writer, DownloaderThread[] threadArr, UserIOUtils ioutils, DownloadStateHolder stateHolder) {
		this.numThreads=numThreads;
		threadLastDownloaded = new int[numThreads];
		this.threadArr=(HttpUrlDownloaderThread[]) threadArr;
		this.stateHolder = stateHolder;
		this.ioutils=ioutils;
		this.totalSize=stateHolder.GetFileSize();

		lastDownloaded=0;
		for(int i=0; i<numThreads; i++) {
			threadLastDownloaded[i]=0;
		}
	}

	ActionListener taskPerformer = new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			//Set download speed for all statuses.
			UpdateDownloadSpeed();
		}
	};
	
	public void UpdateThreadDownloadSpeed(int index) {
		int threadDownloaded = (int) threadArr[index].GetTotalRead();
		if(stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) {
			if(stateHolder.GetThreadStatus(index) == DownloadStateHolder.DOWNLOADING) {
				float speed = Common.GetDownloadSpeed(threadDownloaded-threadLastDownloaded[index], (int)(speedUpdateEndTime-speedUpdateStartTime));
				if(speed > 0)
					stateHolder.SetThreadDownloadSpeed(index, speed);
				threadLastDownloaded[index]=threadDownloaded;
				//logger.PrintLog("DEBUG", "downloadspeed1-"+stateHolder.GetDownloadSpeed());
			}
		}
		else if((stateHolder.GetStatus() == DownloadStateHolder.PAUSED) ||
				(stateHolder.GetStatus() == DownloadStateHolder.CANCELLED)) {
			if(stateHolder.GetThreadDownloadSpeed(index) != 0)
				stateHolder.SetThreadDownloadSpeed(index, 0);
			//logger.PrintLog("DEBUG", "downloadspeed2-"+stateHolder.GetDownloadSpeed());
		}
		else if(stateHolder.GetThreadStatus(index) == DownloadStateHolder.COMPLETED) {
			//stateHolder.SetThreadEndTime(index,(new Date()).getTime());
			//int time = (int)(stateHolder.GetThreadEndTime(index)-stateHolder.GetThreadStartTime(index));
			//if(time > 0) {
				stateHolder.SetThreadDownloadSpeed(index,0);
			//}
			//logger.PrintLog("DEBUG", "downloadspeed3-"+totalDownloaded+", "+time+", "+stateHolder.GetThreadDownloadSpeed(index));
		}
	}

	public void UpdateDownloadSpeed() {
		speedUpdateStartTime = speedUpdateEndTime;
		speedUpdateEndTime = Common.GetCurrentTime();
		//Find Total downloaded
		
		int totalDownloaded=0;
		for(int i=0; i<numThreads; i++) {
			totalDownloaded+=threadArr[i].GetTotalRead();
			UpdateThreadDownloadSpeed(i);
		}
		//logger.PrintLog("DEBUG", "downloadspeed-"+stateHolder.GetDownloadSpeed());
		if(stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) {
			float speed = Common.GetDownloadSpeed(totalDownloaded-lastDownloaded, (int)(speedUpdateEndTime-speedUpdateStartTime));
			if(speed > 0) {
				stateHolder.SetDownloadSpeed(speed);
				//Calculate time remaining also here
				stateHolder.setTimeRemaining((int) ((stateHolder.GetFileSize() - totalDownloaded)/speed));
				
			}
			lastDownloaded=totalDownloaded;
			//logger.PrintLog("DEBUG", "downloadspeed1-"+stateHolder.GetDownloadSpeed());
		}
		else if((stateHolder.GetStatus() == DownloadStateHolder.PAUSED) ||
				(stateHolder.GetStatus() == DownloadStateHolder.CANCELLED)) {
			if(stateHolder.GetDownloadSpeed() != 0)
				stateHolder.SetDownloadSpeed(0);
			//logger.PrintLog("DEBUG", "downloadspeed2-"+stateHolder.GetDownloadSpeed());
		}
		else if(stateHolder.GetStatus() == DownloadStateHolder.COMPLETED) {
			//int time = (int)(stateHolder.GetEndTime() - stateHolder.GetStartTime() - stateHolder.GetPausedTime());
			//if(time > 0) {
			//	stateHolder.SetDownloadSpeed(Common.GetDownloadSpeed(stateHolder.GetFileSize(), time));
			//}
			stateHolder.SetDownloadSpeed(0);
			stateHolder.SetDownloadedSize(0);
			stateHolder.setTimeRemaining(0);
			timer.stop();
		}
	}
	
	public void run() {
		//Sit in loop for PROGRESS_POLL_INTERVAL and update progress.
		//If a completion is detected, update stateholder status.

		lastDownloaded=0;
		speedUpdateEndTime=stateHolder.GetStartTime();
		
		//Start a timer to check download speed.
		timer = new Timer(Common.SPEED_POLL_INTERVAL, taskPerformer);
		timer.start();
		  
		while(true) {
			//Sleep for PROGRESS_POLL_INTERVAL
			Common.Sleep(Common.PROGRESS_POLL_INTERVAL, logger);

			//Find Total downloaded
			int totalDownloaded=0;
			if(stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) {
				for(int i=0; i<numThreads; i++) {
					totalDownloaded+=threadArr[i].GetTotalRead();
					stateHolder.setThreadDownloadedSize(i, (int) threadArr[i].GetTotalRead());
					stateHolder.SetThreadWriteRangeStart(i, threadArr[i].GetWriteRangeStart());
				}
				stateHolder.SetDownloadedSize(totalDownloaded);
			}
			
			//logger.PrintLog("DEBUG", "Downloaded- "+totalDownloaded);
			stateHolder.SetElapsedTime();
			
			//If there is any pause on error in any thread, initiate a pause, so download is not destroyed
			if(stateHolder.GetStatus() != DownloadStateHolder.PAUSEDFORERROR) {
				for(int i=0; i<numThreads; i++) {
					if(stateHolder.GetThreadStatus(i) == DownloadStateHolder.PAUSEDFORERROR) {
						//Pause all threads
						stateHolder.GetDownloaderObject().PauseForError();
						break;
					}
				}
			}
			
			//Exit after download
			if((stateHolder.GetStatus() == DownloadStateHolder.COMPLETED) ||
			   (stateHolder.GetStatus() == DownloadStateHolder.CANCELLED) ||
			   (stateHolder.GetStatus() == DownloadStateHolder.ERROR)) {
				//Total downloaded size could be <100 at this point.
				if(stateHolder.GetStatus() == DownloadStateHolder.COMPLETED)
					stateHolder.SetDownloadedSize(stateHolder.GetFileSize());
				UpdateDownloadSpeed();
				stateHolder.SetElapsedTime();
				
				//logger.PrintLog("DEBUG", "Exiting from admin thread with status- "+stateHolder.GetStatusString());
				
				break;
			}
		}
	}
}

