/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Retry handler for httpclient
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.core.urlcore;

import java.io.IOException;
import javax.net.ssl.SSLHandshakeException;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpContext;

public class DownloadRetryHandler implements HttpRequestRetryHandler {
	public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
		if (executionCount >= 2) {
			// Do not retry if over max retry count
			return false;
		}
		if (exception instanceof NoHttpResponseException) {
		// Retry if the server dropped connection on us
			return true;
		}
		if (exception instanceof SSLHandshakeException) {
			// Do not retry on SSL handshake exception
			return false;
		}
		HttpRequest request = (HttpRequest) context.getAttribute(
		ExecutionContext.HTTP_REQUEST);
		boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
		if (idempotent) {
			// Retry if the request is considered idempotent
			return true;
		}
		return false;
	}
}