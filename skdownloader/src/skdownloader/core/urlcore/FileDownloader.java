/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Downloads file from disk
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.core.urlcore;

import java.io.File;
import java.io.FileInputStream;

import org.apache.log4j.Logger;

import skdownloader.core.Common;
import skdownloader.core.UserIOUtils;
import skdownloader.core.MyFileWriter;

public class FileDownloader
{
	UserIOUtils ioutils;
	private static Logger logger = Logger.getLogger(FileDownloader.class);
	
	public FileDownloader(UserIOUtils l) {
		ioutils=l;
	}
	
	public int Download(String filename, String dirname) {
		long fileSize;
		File infile;

		int NUMFILES = 4;
		int MIN_WRITE_CHUNK = 2048;
		
		long splitFileSize = 0;
		long totalAllocated = 0;
		int nSplits=0;
		int splitSize=0;
		int amountRead=0;
		byte[] readBuffer = new byte[MIN_WRITE_CHUNK];
		MyFileWriter writer;
		long progressOffset = 0;
		
		//Check if input file exists
		if(Common.CheckFileExists(filename, logger)!=Common.OK) {
			//File does not exist
			logger.error("File \""+filename+"\"does not exist");
			return Common.ERROR;
		}
		
		//Check if output dir exists
		if(Common.CheckFileExists(dirname, logger)!=Common.OK) {
			//Dir does not exist
			logger.error("Output directory \""+dirname+"\"does not exist");
			return Common.ERROR;
		}
		
		
		infile = new File(filename);
		fileSize=infile.length();		
		String outFileName = dirname+File.separator+infile.getName();
		logger.debug("file size = "+fileSize);
		if(Common.CreateFileWithSize(outFileName, fileSize, logger) != Common.OK) {
			return Common.ERROR;
		}
		
		//Create writer object
		try {
			writer = new MyFileWriter(outFileName,  ioutils);
		} catch(Exception e) {
			logger.error("Caught exception while writing to \""+filename+"\".");
			logger.error("Exception:", e);
			return Common.ERROR;
		}
		
		FileInputStream inStream ;
		//FileOutputStream outStream;
		
		try {
			//Create empty file;
			inStream = new FileInputStream(infile);
			
			for(int i=1; i<=NUMFILES; i++) {
				//newFileName = outputDir+File.separator+inputFileName+"_sksplitter_"+i;
				splitFileSize = (i<NUMFILES? (long)fileSize/NUMFILES: fileSize-totalAllocated);
				totalAllocated += splitFileSize;
				logger.debug("Splitsize - "+splitFileSize);

				nSplits=((splitFileSize%MIN_WRITE_CHUNK)>0? (int)(splitFileSize/MIN_WRITE_CHUNK)+1 : (int)(splitFileSize/MIN_WRITE_CHUNK));
				try {
					//outStream = new FileOutputStream(outfile);
					for(int j=1; j<=nSplits; j++) {
						splitSize=(int)(j<nSplits? MIN_WRITE_CHUNK : (splitFileSize-(MIN_WRITE_CHUNK*(j-1))));
						try {
								amountRead = inStream.read(readBuffer, 0, splitSize);
								
								writer.WriteToFileAtOffset(readBuffer, progressOffset, amountRead);
								progressOffset += amountRead;
								
						} catch (Exception e) {
							logger.error("Error writing to file ");
							logger.error("Exceptino:", e);
							return Common.ERROR;
						}
					}
					//outStream.close();
				} catch (Exception e) {
					logger.error("Exception", e);
					inStream.close();
					return Common.ERROR;
				}
			}
			inStream.close();
		}catch (Exception e) {
			logger.error("Exception", e);
		}

		writer.Close();
		return Common.OK;
	}
}