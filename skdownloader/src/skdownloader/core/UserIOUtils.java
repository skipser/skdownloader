/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Interface for logger functions
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.core;

public interface UserIOUtils 
{
	public void PrintError(String str);
	
	public void PrintMessage(String str);
	
	public boolean GetFromYesNoMessage(Object c, String str);
	
	public boolean GetFromOkCancelMessage(Object c, String str);
	
	public String GetFileFromUser(Object c);
}
