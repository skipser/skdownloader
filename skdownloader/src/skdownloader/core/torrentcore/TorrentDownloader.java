package skdownloader.core.torrentcore;

import java.io.File;
import java.util.BitSet;
import java.util.Date;
import java.util.List;

import javax.swing.event.EventListenerList;

import org.apache.log4j.Logger;
import org.gudy.azureus2.core3.disk.DiskManagerFileInfo;
import org.gudy.azureus2.core3.download.DownloadManager;
import org.gudy.azureus2.core3.download.DownloadManagerListener;
import org.gudy.azureus2.core3.global.GlobalManager;
import org.gudy.azureus2.core3.torrent.TOTorrentException;

import com.aelitis.azureus.core.AzureusCore;

import skdownloader.core.Common;
import skdownloader.core.DownloadListener;
import skdownloader.core.DownloadStateHolder;
import skdownloader.core.DownloaderInterface;
import skdownloader.core.UserIOUtils;
import skdownloader.core.urlcore.UrlDownloader;
import skdownloader.downloadmanager.GUICommon;
import skdownloader.downloadmanager.SystemInfoHolder;


public class TorrentDownloader extends Thread implements DownloaderInterface {
	UserIOUtils ioutils;
	
	AzureusCore core;
	
	TorrentDownloadProgressAdminThread progressAdminThread;
	
	private boolean needTorrentDownload = false;;

	//Progress updater thread updates state holder in regular intervals
	
	//Stateholder to hold download status/info
	DownloadStateHolder stateHolder;
	private DownloadManager fileDownloadManager = null;
	private final EventListenerList listeners = new EventListenerList();
	private boolean newDownload = false;
	private TorrentDownloadProgressAdminThread adminThread;
	private TorrentDownloader me;
	
	SystemInfoHolder sysInfoHolder;
	
	private static Logger logger = Logger.getLogger(TorrentDownloader.class);
	
	public TorrentDownloader() {
		me=this;
	}

	public TorrentDownloader(String torrentname, String dirname, UserIOUtils ioutils, AzureusCore core, TorrentDownloadProgressAdminThread adminThread, SystemInfoHolder sysInfoHolder, boolean needDownload, boolean newDownload)	{
		this.ioutils=ioutils;
		stateHolder = new DownloadStateHolder(this, ioutils);
		stateHolder.SetDirName(dirname);
		stateHolder.SetUrlName(torrentname);
		this.sysInfoHolder = sysInfoHolder;
		this.needTorrentDownload=needDownload;
		this.newDownload=newDownload;
		this.core=core;
		this.adminThread=adminThread;
		me=this;
		if(! needTorrentDownload)
			init();
	}
	
	public DownloadStateHolder GetStateHolder() {
		return stateHolder;
	}
	
	public void run() {
		if(this.needTorrentDownload) {
			stateHolder.SetInfoText("Fetching torrent...");
			UrlDownloader dl = new UrlDownloader(ioutils, sysInfoHolder);
			String torrent=sysInfoHolder.GetTmpDownloadDir()+File.separator+(new Date()).getTime()+".torrent";
			if(dl.SingleDownload(stateHolder.GetUrlName(), torrent) == Common.OK) {
				this.stateHolder.SetUrlName(torrent);
				if(init() != Common.OK) {
					if(fileDownloadManager.getState() == DownloadManager.STATE_ERROR) {
						stateHolder.SetDownloadStatus(DownloadStateHolder.ERROR);
						//logger.PrintError(fileDownloadManager.getErrorDetails());
						return;
					}
				}
				stateHolder.SetDownloadStatus(DownloadStateHolder.STARTING);
			}
			else {
				stateHolder.SetInfoText("Could not fetch torrent...");
				ioutils.PrintError("Could not download the torrent file\""+stateHolder.GetUrlName()+"\" properly."+Common.LINESEPERATOR+dl.GetStateHolder().GetErrorMessage());
				stateHolder.SetDownloadStatus(DownloadStateHolder.ERROR);
				return;
			}
		}
		Download();
	}

	@SuppressWarnings("unchecked")
	public int init() {
		GlobalManager globalManager = core.getGlobalManager();
		
		//Core would have initialized download managers from it's config. We don't want them. They will error
		//out if the file has been deleted. So just create fresh. There will only be a file check override.
		List<DownloadManager> downloadManagers = core.getGlobalManager().getDownloadManagers();
		fileDownloadManager=null;
		for(DownloadManager manager : downloadManagers){
			if(Common.GetFileNameFromAbsPath(manager.getTorrentFileName()).equals(Common.GetFileNameFromAbsPath(stateHolder.GetUrlName()))) {
				//We got a stored torrent existing. Just use the existing download manager initialized by core.
				if(manager.filesExist(false)) {
					fileDownloadManager=manager;
				}
				else {
					//Just check if this was a download that was already done and the file was removed. In that case, we
					//need not re-download it again.
					if(stateHolder.GetStatus()==DownloadStateHolder.SEEDING) {
						stateHolder.SetDownloadStatus(DownloadStateHolder.ERROR);
						manager.stopIt(DownloadManager.STATE_STOPPED, true, false);
						return Common.ERROR;
					}
					try {
						manager.stopIt(DownloadManager.STATE_STOPPED, true, false);
						core.getGlobalManager().removeDownloadManager(manager);
					} catch (Exception e) {
						logger.error("Exception", e);
					}
				}
			}
		}
		if(fileDownloadManager==null) {
			//We don't have an existing file manager. Initialize a new one checking for torrent.
			if((new File(stateHolder.GetUrlName())).exists()) {
				fileDownloadManager = globalManager.addDownloadManager(stateHolder.GetUrlName(), stateHolder.GetDirName());
			}
			else {
				stateHolder.SetErrorMessage("Can't find torrent file "+stateHolder.GetUrlName());
				return Common.ERROR;
			}
		}
		if(fileDownloadManager.getState() == DownloadManager.STATE_ERROR) {
			stateHolder.SetDownloadStatus(DownloadStateHolder.ERROR);
			stateHolder.SetErrorMessage("Error initializing torrent");
			if(newDownload)
				ioutils.PrintError("Torrent error encountered. Details.."+GUICommon.LINESEPERATOR+fileDownloadManager.getErrorDetails());
			return Common.ERROR;
		}
		
		try {
			stateHolder.SetTorrentHash(fileDownloadManager.getTorrent().getHashWrapper().toBase32String());
			stateHolder.SetTorrentSize(Common.SizetoString(fileDownloadManager.getTorrent().getSize()));
			stateHolder.SetTorrentPrivacy(fileDownloadManager.getTorrent().getPrivate());
		} catch (TOTorrentException e) {
			stateHolder.SetDownloadStatus(DownloadStateHolder.ERROR);
			stateHolder.SetErrorMessage("Error initializing torrent");
			logger.error("Exception", e);
		}
		stateHolder.SetTorrentCreator(fileDownloadManager.getTorrentCreatedBy());
		stateHolder.SetTorrentCreatedDate(fileDownloadManager.getCreationTime());
		stateHolder.SetTorrentComment(fileDownloadManager.getTorrentComment());
		stateHolder.SetNumberOfPieces(fileDownloadManager.getNbPieces());
		stateHolder.SetFileSize(fileDownloadManager.getSize());
		
		//Initialize exclude bitset.
		BitSet excludeList = stateHolder.GetExcludedFiles();
		DiskManagerFileInfo[] fileList = fileDownloadManager.getDiskManagerFileInfoSet().getFiles();
		if(excludeList != null) {
			for(int i=0; i< fileList.length; i++) {
				if(excludeList.get(i))
					fileList[i].setSkipped(true);
			}
		}
		else {
			stateHolder.InitExcludedList(fileList.length);
		}

		return Common.OK;
	}
	
	public int Download() {
		//First check if in downloadable state. Return if not.
		if((stateHolder.GetStatus() == DownloadStateHolder.COMPLETED) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.ERROR) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.CANCELLED)){
			return Common.OK;
		}
		
		//Set the state to starting if it is to be downloaded.
		if((stateHolder.GetStatus() == DownloadStateHolder.PAUSEDFOREXIT) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.PAUSEDFORERROR) ||
		   (stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING)) {
			if(stateHolder.GetStatus() == DownloadStateHolder.PAUSEDFOREXIT ||
			   stateHolder.GetStatus() == DownloadStateHolder.PAUSEDFORERROR) {
				stateHolder.SetPausedTime(stateHolder.GetPausedTime()+(int)(Common.GetCurrentTime() - stateHolder.GetPauseStartTime()));
				stateHolder.SetPauseStartTime(0);
			}
			stateHolder.SetDownloadStatus(DownloadStateHolder.STARTING);
		}
		

		//Just a double check. We should not reach here if we have an error in download manager...
		if(fileDownloadManager.getState() == DownloadManager.STATE_ERROR) {
			stateHolder.SetFileName(Common.GetFileNameFromAbsPath(stateHolder.GetUrlName()));
			Cancel(fileDownloadManager.getErrorDetails(), DownloadStateHolder.ERROR);
			return Common.ERROR;
		}
		stateHolder.SetTorrentDownloadManager(fileDownloadManager);
		stateHolder.announceFileInfoSetChange(); // This will cause file list to be populated in details frame for first time.

		//Set download manager save location
		File saveLocation = fileDownloadManager.getSaveLocation();
		
		//If This file already exists on disk,attempt to delete it if the size does not match the torrent's
		if(saveLocation.exists() == true){
			long torrentFileSize = fileDownloadManager.getSize();
			long actualFileSize = saveLocation.length();
			if(torrentFileSize != actualFileSize){
				saveLocation.delete();
			}
		}

		//Add download manager listener.
		fileDownloadManager.addListener(new DownloadManagerListener(){
			public void completionChanged(DownloadManager manager, boolean bCompleted) {}
			public void downloadComplete(DownloadManager manager) {
				stateHolder.setTorrentDownloadPercent(fileDownloadManager.getStats().getDownloadCompleted(true)/10);
				stateHolder.SetTorrentDownloadedBitsToFull();
				if(stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) {
					stateHolder.SetDownloadStatus(DownloadStateHolder.SEEDING);
				}
				if(stateHolder.GetDownloadTime() ==0) {
					me.fireDownloadComplete();
					stateHolder.SetDownloadTime(Common.GetCurrentTime() - stateHolder.GetStartTime()-stateHolder.GetPausedTime());
				}
			}
			public void filePriorityChanged(DownloadManager download, DiskManagerFileInfo file) {}
			public void positionChanged(DownloadManager download, int oldPosition, int newPosition) {}
			public void stateChanged(DownloadManager manager, int state) {
				switch(state){
					case DownloadManager.STATE_ALLOCATING:
						stateHolder.SetInfoText("Allocating...");
					break;
					case DownloadManager.STATE_CHECKING:
						stateHolder.SetInfoText("Checking...");
					break;
					case DownloadManager.STATE_DOWNLOADING:
						stateHolder.SetInfoText("Downloading...");
					break;
					case DownloadManager.STATE_SEEDING:
						stateHolder.SetTorrentDownloadedBitsToFull();
						stateHolder.SetDownloadStatus(DownloadStateHolder.SEEDING);
						stateHolder.SetInfoText("Seeding...");
					break;
					case DownloadManager.STATE_WAITING:
						stateHolder.SetInfoText("Waiting...");
					break;
					case DownloadManager.STATE_QUEUED:
						stateHolder.SetInfoText("Queued...");
					break;
					case DownloadManager.STATE_INITIALIZED:
						stateHolder.SetInfoText("Initialised...");
					break;
					case DownloadManager.STATE_INITIALIZING:
						stateHolder.SetInfoText("Initialising...");
					break;
					case DownloadManager.STATE_ERROR:
						stateHolder.SetInfoText("Error");
						Cancel(manager.getErrorDetails(), DownloadStateHolder.ERROR);
					break;
					case DownloadManager.STATE_CLOSED:
						stateHolder.SetInfoText("Torrent Closed...");
					break;
					case DownloadManager.STATE_STOPPED:
						stateHolder.SetInfoText("Torrent Stopped...");
					break;
				}
			}
				
		});
		
		//Set save location. These can't be set during init..
		stateHolder.SetFileName(fileDownloadManager.getAbsoluteSaveLocation().getName());
		if(stateHolder.GetStartTime() == 0)
			stateHolder.SetStartTime(Common.GetCurrentTime());

		//Add ourselves to admin thread to update
		adminThread.AddDownload(fileDownloadManager, stateHolder);
		
		// Start the download by making it in queue state.
		fileDownloadManager.stopIt( DownloadManager.STATE_QUEUED, false, false );
		
		if(stateHolder.GetStatus() == DownloadStateHolder.PAUSED){
			stateHolder.setTimeRemaining(0);
			if(fileDownloadManager != null)
				fileDownloadManager.pause();
		}
		
		//Check if a completed download and change to seeding
		if(fileDownloadManager.isDownloadComplete(false)) {
			if(stateHolder.GetStatus() != DownloadStateHolder.PAUSED)
				stateHolder.SetDownloadStatus(DownloadStateHolder.SEEDING);
			stateHolder.SetTorrentDownloadedBitsToFull();
		}
		
		//Listener could change to seeding if we are adding a completed torrent. So check once.
		else if((stateHolder.GetStatus() != DownloadStateHolder.SEEDING) &&
				(stateHolder.GetStatus() != DownloadStateHolder.PAUSED))
			stateHolder.SetDownloadStatus(DownloadStateHolder.DOWNLOADING);

		return Common.OK;
	}
	
	public void Pause()	{
		stateHolder.SetDownloadStatus(DownloadStateHolder.PAUSED);
		//Set pause start time
		stateHolder.SetPauseStartTime(Common.GetCurrentTime());
		//Re-set remaining time.
		stateHolder.setTimeRemaining(0);
		if(fileDownloadManager != null) {
			fileDownloadManager.pause();
		}
	}
	
	public void PauseForExit() {
		//Set global pause
		if(stateHolder.GetStatus() != DownloadStateHolder.SEEDING)
			stateHolder.SetDownloadStatus(DownloadStateHolder.PAUSEDFOREXIT);
		
		//Set pause start time
		stateHolder.SetPauseStartTime(Common.GetCurrentTime());
		stateHolder.setTimeRemaining(0);
		if(fileDownloadManager != null) {
			fileDownloadManager.pause();
		}
	}
	
	public void PauseForError()	{
		//Set global pause
		if(stateHolder.GetStatus() != DownloadStateHolder.SEEDING)
			stateHolder.SetDownloadStatus(DownloadStateHolder.PAUSEDFORERROR);
		
		//Set pause start time
		stateHolder.SetPauseStartTime(Common.GetCurrentTime());
		stateHolder.setTimeRemaining(0);
		if(fileDownloadManager != null) {
			fileDownloadManager.pause();
		}
	}
	
	public void Resume() {
		//Set global resume
		if(fileDownloadManager.isDownloadComplete(false)) {
			stateHolder.SetDownloadStatus(DownloadStateHolder.SEEDING);
		}
		else {
			stateHolder.SetDownloadStatus(DownloadStateHolder.DOWNLOADING);
		}

		//Calculate pause end time
		//stateHolder.SetPauseEndTime(Common.GetCurrentTime());
		stateHolder.SetPausedTime(stateHolder.GetPausedTime()+(int)(Common.GetCurrentTime() - stateHolder.GetPauseStartTime()));
		stateHolder.SetPauseStartTime(0);

		if(fileDownloadManager != null) {
			fileDownloadManager.requestTrackerAnnounce(true);
			fileDownloadManager.resume();
		}
	}

	public void Cancel(String msg, int state) {
		stateHolder.SetErrorMessage(msg);
		if(stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING ||
			stateHolder.GetStatus() == DownloadStateHolder.STARTING ||
			stateHolder.GetStatus() == DownloadStateHolder.PAUSED ||
			stateHolder.GetStatus() == DownloadStateHolder.SEEDING){
			if(fileDownloadManager != null) {
				try {
					fileDownloadManager.stopIt(DownloadManager.STATE_STOPPED,true,false);
				} catch (Exception e) {
					logger.error("Exception", e);
				}
				try {
					core.getGlobalManager().removeDownloadManager(fileDownloadManager, true, false);
				} catch (Exception e) {
					logger.error("Exception", e);
				}
			}
			
		}
		if(state != DownloadStateHolder.EXITING)
			stateHolder.SetDownloadStatus(state);
		
		adminThread.RemoveDownload(stateHolder);
	}
	
    public void AddDownloadListener(DownloadListener listener) {
        listeners.add(DownloadListener.class, listener);
    }

    public void RemoveDownloadListener(DownloadListener listener) {
        listeners.remove(DownloadListener.class, listener);
    }

    public DownloadListener[] GetDownloadListener() {
        return listeners.getListeners(DownloadListener.class);
    }

    private void fireDownloadComplete() {
        for (DownloadListener listener : GetDownloadListener()) {
            listener.DownloadComplete(this.stateHolder);
        }
    }
    

}