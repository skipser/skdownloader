package skdownloader.core.torrentcore;

import skdownloader.core.Common;
import skdownloader.core.DownloadStateHolder;
import skdownloader.downloadmanager.SystemInfoHolder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.Timer;

import org.apache.log4j.Logger;
import org.gudy.azureus2.core3.disk.DiskManagerPiece;
import org.gudy.azureus2.core3.download.DownloadManager;

public class TorrentDownloadProgressAdminThread extends Thread{

	//private DownloadManager fileDownloadManager;
	//private DownloadStateHolder stateHolder;
	private HashMap<DownloadStateHolder, DownloadManager> downloaderList = new HashMap<DownloadStateHolder, DownloadManager>();
	private BitSet tmpset;
	private DiskManagerPiece[] arr;
	Timer timer1Sec;
	Timer timer2Sec;
	Timer timer5Sec;
	SystemInfoHolder sysInfoHolder;
	
	private Logger logger = Logger.getLogger(TorrentDownloadProgressAdminThread.class);
	
	/*TorrentDownloadProgressAdminThread(DownloadManager fileDownloadManager, DownloadStateHolder downloadStateHolder) {
		this.fileDownloadManager=fileDownloadManager;
		this.stateHolder=downloadStateHolder;
	}*/
	public TorrentDownloadProgressAdminThread(SystemInfoHolder sysInfoHolder) {
		this.sysInfoHolder=sysInfoHolder;
	}
	
	public void AddDownload(DownloadManager fileDownloadManager, DownloadStateHolder downloadStateHolder) {
		downloaderList.put(downloadStateHolder, fileDownloadManager);
	}
	public void RemoveDownload(DownloadStateHolder downloadStateHolder) {
		downloaderList.remove(downloadStateHolder);
	}
	
	public DownloadManager GetDownloadManager(DownloadStateHolder downloadStateHolder) {
		return (DownloadManager)downloaderList.get(downloadStateHolder);
	}
	
	
	ActionListener taskPerformer1Sec = new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			//Set download speed for all statuses.
			updateTask1Sec();
		}
	};
	@SuppressWarnings("unchecked")
	public void updateTask1Sec() {
		Iterator<?> itr =downloaderList.entrySet().iterator();
		while(itr.hasNext()) {
			Map.Entry<DownloadStateHolder, DownloadManager> me = (Entry<DownloadStateHolder, DownloadManager>)itr.next();
			DownloadStateHolder stateHolder = me.getKey();
			DownloadManager fileDownloadManager=me.getValue();
			stateHolder.SetDownloadSpeed(fileDownloadManager.getStats().getDataReceiveRate());
			stateHolder.SetUploadSpeed(fileDownloadManager.getStats().getDataSendRate());
			if(stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) {
				stateHolder.SetElapsedTime();
			}
		
			if((stateHolder.GetStatus() == DownloadStateHolder.SEEDING)) {
				stateHolder.SetElapsedTime();
				stateHolder.setTimeRemaining(0);
				if(stateHolder.GetSeedingStartTime() == 0)
					stateHolder.SetSeedingStartTime(Common.GetCurrentTime());
			}
		}
	}
	
	ActionListener taskPerformer2Sec = new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			updateTask2Sec();
		}
	};

	@SuppressWarnings("unchecked")
	public void updateTask2Sec() {
		Iterator<?> itr =downloaderList.entrySet().iterator();
		while(itr.hasNext()) {
			Map.Entry<DownloadStateHolder, DownloadManager> me = (Entry<DownloadStateHolder, DownloadManager>)itr.next();
			DownloadStateHolder stateHolder = me.getKey();
			DownloadManager fileDownloadManager=me.getValue();
			stateHolder.SetTorrentDownloadedSize(fileDownloadManager.getStats().getTotalGoodDataBytesReceived());
			stateHolder.SetUploadedSize(fileDownloadManager.getStats().getTotalDataBytesSent());
			stateHolder.setTorrentDownloadPercent(fileDownloadManager.getStats().getDownloadCompleted(true)/10);
			if(fileDownloadManager.getState() == DownloadManager.STATE_DOWNLOADING)
				stateHolder.setTimeRemaining((int) fileDownloadManager.getStats().getETA());
			stateHolder.SetShareRatio(Common.MyRound((float)fileDownloadManager.getStats().getShareRatio()/1000,2));
		
			//Testing
			if(stateHolder.GetStatus() == DownloadStateHolder.DOWNLOADING) {
				if(fileDownloadManager.getDiskManager() != null) {
					arr = fileDownloadManager.getDiskManager().getPieces();
					tmpset = new BitSet(stateHolder.GetNumberOfPieces());
					for(int i=0; i<arr.length; i++) {
						if(arr[i].isDone())
							tmpset.set(i);
					}
					stateHolder.SetTorrentDownloadedBitsFromBitset(tmpset);
				}
			}
		}
	}
	
	ActionListener taskPerformer5Sec = new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			updateTask5Sec();
		}
	};
	@SuppressWarnings("unchecked")
	public void updateTask5Sec() {
		Iterator<?> itr =downloaderList.entrySet().iterator();
		while(itr.hasNext()) {
			Map.Entry<DownloadStateHolder, DownloadManager> me = (Entry<DownloadStateHolder, DownloadManager>)itr.next();
			DownloadStateHolder stateHolder = me.getKey();
			DownloadManager fileDownloadManager=me.getValue();
			stateHolder.SetNumberOfLeechers(fileDownloadManager.getNbPeers());
			stateHolder.SetNumberOfSeeders(fileDownloadManager.getNbSeeds());
			
			if(stateHolder.GetStatus() == DownloadStateHolder.SEEDING) {
				if(sysInfoHolder.TORRENTREMOVEONSHARERATIOOPTION) {
					if(sysInfoHolder.SHARERATIOTOREMOVETORRENT > 0 && stateHolder.GetShareRatio() >= sysInfoHolder.SHARERATIOTOREMOVETORRENT) {
						stateHolder.SelfDestroy();
					}
				}
				
				if(sysInfoHolder.TORRENTREMOVEONSEEDTIMEOPTION) {
					long seedCheckTime = (sysInfoHolder.SEEDTIMETOREMOVETORRENT);
					if(seedCheckTime > 0 && stateHolder.GetSeedingTime() >= seedCheckTime*60000) {
						stateHolder.SelfDestroy();
					}
				}
			}
		}
		//Check if seeding and if there are any matching removal rules.
	}
	
	public void run() {
		//Start a timer to check download speed.
		timer1Sec = new Timer(1000, taskPerformer1Sec);
		timer1Sec.start();
		timer2Sec = new Timer(2000, taskPerformer2Sec);
		timer2Sec.start();
		timer5Sec = new Timer(5000, taskPerformer5Sec);
		timer5Sec.start();
		while(true) {
			//Sleep for PROGRESS_POLL_INTERVAL
			Common.Sleep(Common.PROGRESS_POLL_INTERVAL, logger);
		}
	}
}
