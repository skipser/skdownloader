/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Common functions
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         06/08/09 - Add comments for all functions
 *
 ****************************************************************************************/

package skdownloader.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class Common {
	public static int ERROR = 1;
	public static int OK = 0;
	
	public static final String LINESEPERATOR = System.getProperty("line.separator");
	
	// Min amount of data wrote in one go
	public static int MIN_WRITE_CHUNK = 10240;
	
	// Minimun amount of data Read in one go
	public static int MIN_READ_CHUNK=10240;
	
	//Interval for updating progress
	public static int PROGRESS_POLL_INTERVAL=1000;
	
	//Interval for updating speed
	public static int SPEED_POLL_INTERVAL=2000;
	
	//Interval for updating speed of torrent download
	public static int TORRENT_SPEED_POLL_INTERVAL=1000;
	public static int TORRENT_BAD_PEER_RETRY_INTERVAL = 60000; // 1 minute
	public static int PEER_CONNECTION_POOL_INTERVAL = 15000; 
	
	//Interval for http connction timeout
	public static int HTTP_SOCKET_TIMEOUT=20000;
	public static int HTTP_CONNECTION_TIMEOUT=20000;
	
	public static int TYPE_URL = 1;
	public static int TYPE_TORRENT = 2;
	
	public static short TORRENT_LISTEN_PORT_MIN=6880;
	public static short TORRENT_LISTEN_PORT_MAX=6980;
	
	public static int YES=1;
	public static int NO=2;
	public static int CANCEL=3;
	/*************************************************************
	 * 
	 * Function  : CheckFileExists
	 * Arguments : file/dir name
	 * Return    : OK or ERROR
	 * Notes     : Checks for file of dir existence
	 * 
	 *************************************************************/
	public static int CheckFileExists(String filename, Logger logger) {
		try {
			File file = new File(filename);
			if (!file.exists()) {
				return ERROR;
			}
		} catch (Exception e) {
			logger.error("Exception", e);
			return ERROR;
		}
		return OK;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetStackTrace
	 * Arguments : exception
	 * Return    : exception as a string
	 * Notes     : Returns exception as a string
	 * 
	 *************************************************************/
	public static String GetStackTrace(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		t.printStackTrace(pw);
		pw.flush();
		sw.flush();
		return sw.toString();
	}
	
	/*************************************************************
	 * 
	 * Function  : MyRound
	 * Arguments : value to be rounded, digits to round
	 * Return    : rounded value
	 * Notes     : This function rounds off Rval to Rpl decimal digits.
	 * 
	 *************************************************************/
	public static float MyRound(float Rval, int Rpl) {
		float p = (float)Math.pow(10,Rpl);
		Rval = Rval * p;
		float tmp = Math.round(Rval);
		return (float)tmp/p;
	}
	
	/*************************************************************
	 * 
	 * Function  : CreateFileWithSize
	 * Arguments : file name, size
	 * Return    : OK/ERROR
	 * Notes     : Creates a file with a fixed size passed.
	 * 
	 *************************************************************/
	public static int CreateFileWithSize(String name, long size, Logger logger) {
		byte a[] = new byte[1];
		a[0]=5;
		
		RandomAccessFile outFile;
		try {
			outFile = new RandomAccessFile(name, "rw");
			outFile.setLength(size);
			outFile.close();
		}
		catch (Exception e) {
			logger.error("Exception", e);
			return Common.ERROR;
		}

		return Common.OK;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetPercent
	 * Arguments : total, done
	 * Return    : calculated percent
	 * Notes     : Returns percent calculated from total and done
	 * 
	 *************************************************************/
	public static int GetPercent(long fileSize, long downloadedSize) {
		if(fileSize == 0)
			return 0;
		//If done has reached limit for int, multiply by 100 could cause overflow issue.
		long done1=downloadedSize;
		int percent = (int)(done1*100/fileSize);
		return percent;
	}
	
	/*************************************************************
	 * 
	 * Function  : FileSizeFromURL
	 * Arguments : url as string
	 * Return    : Size of file pointed by url
	 * Notes     : returns size of file pointed by url
	 * 
	 *************************************************************/
	public static long FileSizeFromURL (String urlstring, Logger logger) {
		URL url;
		URLConnection conn;
		long size=-1;
		
		if(urlstring.length() != 1) {
			return -1;
		}

		try {
			url = new URL(urlstring);
			conn = url.openConnection();
			size = conn.getContentLength();
			if(size < 0)
				return -1;
			conn.getInputStream().close();
		} 
		catch(Exception e) {
			logger.error("Exception", e);
		}
		return size;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetDownloadSpeed
	 * Arguments : bits downloaded, time taken in millisecs
	 * Return    : download speed in bits/sec
	 * Notes     : Computes download speed
	 * 
	 *************************************************************/
	public static float GetDownloadSpeed(long bits, int time) {
		long bits1=bits;
		float speed = (float)(bits1*1000/time);
		return MyRound(speed,2);
	}
	
	/*************************************************************
	 * 
	 * Function  : GetFileNameFromUrl
	 * Arguments : url string
	 * Return    : Name of file from url
	 * Notes     : Finds name of file from url
	 * 
	 *************************************************************/
	public static String GetFileNameFromUrl(String urlname, Logger logger) {
		URL url;
		try {
			url = new URL(urlname);
		} catch (Exception e) {
			logger.error("Exception", e);
			return "";
		}
		File tmpfile = new File(url.getFile());
		return(tmpfile.getName());
	}
	
	/*************************************************************
	 * 
	 * Function  : GetFileNameFromAbsPath
	 * Arguments : full file name including path
	 * Return    : file name with out path
	 * Notes     : Seperates name of file from path
	 * 
	 *************************************************************/
	public static String GetFileNameFromAbsPath(String path) {
		File file = new File(path);
		return file.getName();
	}
	
	/*************************************************************
	 * 
	 * Function  : GetPathNameFromAbsPath
	 * Arguments : file/dir name
	 * Return    : Parent directory name
	 * Notes     : Checks for file of dir existence
	 * 
	 *************************************************************/
	public static String GetPathNameFromAbsPath(String path) {
		File file = new File(path);
		return file.getParent();
	}
	
	/*************************************************************
	 * 
	 * Function  : SizetoString
	 * Arguments : size in int
	 * Return    : Converted size string with unit.
	 * Notes     : 
	 * 
	 *************************************************************/
	public static String SizetoString(long size) {
		float sizeVal;
		if(size == 0) {
			//return "Unknown";
			return "0";
		}
		if(size < 10240) {
			sizeVal=(float)size/1024;
			sizeVal=Common.MyRound(sizeVal, 2);
			return(sizeVal+"Kb");
		}
		else if ((long)size < 1073741824) {
			sizeVal=(float)size/1048576;
			sizeVal=Common.MyRound(sizeVal, 2);
			return(sizeVal+"Mb");
		}
		else {
			sizeVal=(float)size/1073741824;
			sizeVal=Common.MyRound(sizeVal, 2);
			return(sizeVal+"Gb");
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : Sleep
	 * Arguments : time in millisecs to sleep
	 * Return    : null after sleeping for specified time
	 * Notes     : This is a generic sleep to avoid putting
	 *             try/catch blocks everywhere
	 * 
	 *************************************************************/
	public static void Sleep(int time, Logger logger) {
		try{
			Thread.sleep(time);
		} catch (Exception e) {
			logger.error("Exception", e);
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : GetCurrentTime
	 * Arguments : 
	 * Return    : Returns current time in millisecs
	 * Notes     : This is a generic time function to avoid creating
	 *             Date object every time.
	 * 
	 *************************************************************/
	public static long GetCurrentTime() {
		return (new Date()).getTime();
	}
	
	/*************************************************************
	 * 
	 * Function  : CreateFile
	 * Arguments : File name to create
	 * Return    : 
	 * Notes     : This is a generic touch function to create a file
	 * 
	 *************************************************************/
	public static File CreateFile(String fileName, Logger logger) {
		File file = new File(fileName);
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			out.write("");
			out.close();
		} catch (Exception e) {
			logger.error("Exception", e);
		}
		return file;
	}
	
	/*************************************************************
	 * 
	 * Function  : DeleteFile
	 * Arguments : File name to delete
	 * Return    : 
	 * Notes     : Deletes a file
	 * 
	 *************************************************************/
	public static void DeleteFile(String file) {
		File file1 = new File(file);
		if(file1.exists()) {
			file1.delete();
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : deleteDirectory
	 * Arguments : Dir name to delete
	 * Return    : 
	 * Notes     : Deletes a directory
	 * 
	 *************************************************************/
	static public boolean deleteDirectory(String dir, Logger logger) {
		File path = new File(dir);
		if( path.exists() ) {
			File[] files = path.listFiles();
			for(int i=0; i<files.length; i++) {
				if(files[i].isDirectory()) {
					try {
						deleteDirectory(files[i].getCanonicalPath(), logger);
					} catch (Exception e) {
						logger.error("Exception", e);
					}
				}
				else {
					files[i].delete();
				}
			}
		}
		return( path.delete() );
	}
	
	/*************************************************************
	 * 
	 * Function  : deleteDirectory
	 * Arguments : Dir name to delete
	 * Return    : 
	 * Notes     : Deletes a directory
	 * 
	 *************************************************************/
	static public boolean deleteFileOrDirectory(String dir, Logger logger) {
		File file1 = new File(dir);
		if(file1.exists() && ! file1.isDirectory()) {
			file1.delete();
			return true;
		}
		else if(file1.exists() && file1.isDirectory()) {
			return deleteDirectory(dir, logger);
		}
		return true;
	}
	
	/*************************************************************
	 * 
	 * Function  : CopyFile
	 * Arguments : file to copy and destination file.
	 * Return    : 
	 * Notes     : Copies one file to another
	 * 
	 *************************************************************/
	public static void CopyFile(String fromFileName, String toFileName, Logger logger) {
		File fromFile = new File(fromFileName);
		File toFile = new File(toFileName);

		if (!fromFile.exists()) {
			return;
		}
		if (!fromFile.isFile()) {
			return;
		}
        if (!fromFile.canRead()) {
        	return;
        }

        if (toFile.exists()) {
        	if (!toFile.canWrite()) {
        		return;
        	}
        }

        //First create directory for toFile
        File parentDir = new File(toFile.getParent());
        if(! parentDir.exists()) {
        	if(toFile.getParent() != null)
        		new File(toFile.getParent()).mkdir();
        }

        FileInputStream from = null;
        FileOutputStream to = null;
        try {
        	from = new FileInputStream(fromFile);
        	to = new FileOutputStream(toFile);
        	byte[] buffer = new byte[4096];
        	int bytesRead;

        	while ((bytesRead = from.read(buffer)) != -1)
        		to.write(buffer, 0, bytesRead); // write
        	
        	to.close();
        	from.close();
        }
        catch(Exception e) {
        	logger.error("Exception", e);
        }
        finally {
        	if (from != null) {
        		try {
        			from.close();
        		} catch (IOException e) {
        			logger.error("Exception", e);
        		}
        	}
        	if (to != null) {
        		try {
        			to.close();
        		} catch (IOException e) {
        			logger.error("Exception", e);
        		}
        	}
        }
	}
	
	/*************************************************************
	 * 
	 * Function  : MoveFile
	 * Arguments : file to move and destination file.
	 * Return    : 
	 * Notes     : Moves one file to another
	 * 
	 *************************************************************/
	public static void MoveFile(String fromFileName, String toFileName, Logger logger) {
		CopyFile(fromFileName, toFileName, logger);
		File file = new File(fromFileName);
		file.delete();
	}
	
	/*************************************************************
	 * 
	 * Function  : ReplaceChar
	 * Arguments : 1. Character to replace
	 *             2. String to replace with
	 *             3. String to check for replace character
	 * Return    : 
	 * Notes     : Replaces a character in string with another string globally.
	 * 
	 *************************************************************/
	public static String ReplaceChar(char replaceChar, String replaceWithString, String string ) {
		char[] strArr = string.toCharArray();
		String finalString="";
		for(int i=0; i<strArr.length; i++) {
			if(strArr[i] == replaceChar) {
				finalString = finalString+replaceWithString;
			}
			else {
				finalString +=strArr[i];
			}
		}
		return finalString;
	}
	
	/*************************************************************
	 * 
	 * Function  : ReplaceString
	 * Arguments : 1. String to replace
	 *             2. String to replace with
	 *             3. String to check for replace string
	 * Return    : 
	 * Notes     : Replaces a substring in string with another string globally.
	 * 
	 *************************************************************/
	public static String ReplaceString(String replaceString, String replaceWithString, String string ) {
		char[] strArr = string.toCharArray();
		char[] replaceStrArr = replaceString.toCharArray();
		
		String finalString="";
		int gotMatch;
		int orig_i;
		for(int i=0; i<strArr.length; i++) {
			gotMatch=0;
			if(strArr[i] == replaceStrArr[0]) {
				orig_i=i;
				gotMatch=1;
				for(int j=1; j<replaceStrArr.length; j++) {
					i++;
					if((i == strArr.length) ||
					   (strArr[i] != replaceStrArr[j])) {
						gotMatch=0;
						break;
					}
				}
				if(gotMatch ==1) {
					finalString+=replaceWithString;
				}
				else {
					i=orig_i;
					finalString += strArr[i];
				}
			}
			else {
				finalString +=strArr[i];
			}
		}
		return finalString;
	}
	
	public static void BlockForState(int state, DownloadStateHolder stateHolder, Logger logger) {
		//Just check if we are in paused state and block if paused
    	while(stateHolder.GetStatus() == state) {
			//Check for sleep in 500ms intervals
			Sleep(500, logger);
		}
	}
	
	public static String BitSet2String(BitSet bs, int size) {
		String bsString = "";
		for(int i=0; i< size; i++) {
			if(bs.get(i))
				bsString += "1";
			else
				bsString+= "0";
		}
		return bsString;
	}
	
	public static BitSet String2BitSet(String bsString, Logger logger){
		StringReader reader = new StringReader(bsString);
		BitSet bs = new BitSet(bsString.length());
		
		int bit;
		int index=0;
		try {
			while((bit=reader.read()) != -1) {
				if(((char)bit != '1') &&((char)bit != '0'))
					continue;
				
				if((char)bit == '1') {
					bs.set(index);
				}
				index++;
			}
		} catch (IOException e) {
			logger.error("Exception", e);
		}
		return bs;
	}
	
	public static int GetBitsetSize(String bsString, Logger logger) {
		int index=0;
		int bit;
		try {
			StringReader reader = new StringReader(bsString);
			while((bit=reader.read()) != -1) {
				if(((char)bit != '1') &&((char)bit != '0'))
					continue;
				index++;
			}
		} catch (IOException e) {
			logger.error("Exception", e);
		}
		return index;
	}
	
	public static String ShortenString(String str,String pos, int maxChars) {
		if(str.length() <=maxChars) {
			return str;
		}
		//int charsToTrim=str.length()-maxChars;
		String start=str.substring(0, maxChars/2);
		String end=str.substring(str.length()-maxChars/2, str.length());
		return start+"..."+end;
	}
	
	public static String InputStreamToString(InputStream is, Logger logger) {
		String content = "";
		try {
			if(is != null) {
				Writer writer = new StringWriter();
				char[] buffer = new char[1024];
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
				content = writer.toString();
			}
		}
		catch(Exception e) {
			logger.error("Exception", e);
			content = "";
		}
		return content;
	}
	
	public static String IntArr2String(int[] arr) {
		String ret = "";
		for(int i=0; i<arr.length; i++)
			ret += arr[i] + " ";
		return ret;
	}
	
	public static  int[] String2IntArr(String str) {
		Pattern p = Pattern.compile("(\\d+)\\s*");
		Matcher m = p.matcher(str);
		ArrayList<Integer> list = new ArrayList<Integer>();
		while(m.find()) {
			list.add(Integer.parseInt(m.group(1)));
		}
		int [] arr = new int[list.size()];		
		for(int i=0; i< list.size(); i++) {
			arr[i]=list.get(i);
		}
		return arr;
	}
	
	public static void CopyIntArray(int[] src, int[] dest) {
		for(int i=0; i< src.length; i++)
			dest[i]=src[i];
	}
	
	public static int GetIndexInIntArr(int[] arr, int i) {
		for(int j=0; j< arr.length; j++) {
			if(arr[j] == i)
				return j;
		}
		return -1;
	}
	
	public static String forHTML(String aText){
	     final StringBuilder result = new StringBuilder();
	     final StringCharacterIterator iterator = new StringCharacterIterator(aText);
	     char character =  iterator.current();
	     while (character != CharacterIterator.DONE ){
	       if (character == '!') {
	    	   result.append("%21");
	       }
	       else if (character == '#') {
	    	   result.append("%23");
	       }
	       else if (character == '$') {
	    	   result.append("%24");
	       }
	       else if (character == '\'') {
	    	   result.append("%27");
	       }
	       else if (character == '(') {
	    	   result.append("%28");
	       }
	       else if (character == ')') {
	    	   result.append("%29");
	       }
	       else if (character == '*') {
	    	   result.append("%2A");
	       }
	       else if (character == '+') {
	    	   result.append("%2B");
	       }
	       else if (character == ',') {
	    	   result.append("%2C");
	       }
	       else if (character == '-') {
	    	   result.append("%2D");
	       }
	       else if (character == ';') {
	    	   result.append("%3B");
	       }
	       else if (character == '@') {
	    	   result.append("%40");
	       }
	       else if (character == '[') {
	    	   result.append("%5B");
	       }
	       else if (character == '\\') {
	    	   result.append("%5C");
	       }
	       else if (character == ']') {
	    	   result.append("%5D");
	       }
	       else if (character == '^') {
	    	   result.append("%5E");
	       }
	       else if (character == '_') {
	    	   result.append("%5F");
	       }
	       else if (character == '`') {
	    	   result.append("%60");
	       }
	       else if (character == '{') {
	    	   result.append("%7B");
	       }
	       else if (character == '|') {
	    	   result.append("%7C");
	       }
	       else if (character == '}') {
	    	   result.append("%7D");
	       }
	       else if (character == '~') {
	    	   result.append("%7E");
	       }
	       else {
	         //the char is not a special one
	         //add it to the result as is
	         result.append(character);
	       }
	       character = iterator.next();
	     }
	     return result.toString();
	  }
}
