/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Interface for logger functions
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.core;

public interface DownloaderThread
{
	public void Pause();
	
	public void PauseForExit();
	
	public void PauseForError();
	
	public void Resume();
	
	public void Cancel();
	
	public int GetStatus();
	
	public long GetTotalRead();
	
	public int GetDownloadSize();
}