/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Stateholder holding state of download
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *   arun         06/08/09 - Add comments to all functions 
 *
 ****************************************************************************************/

package skdownloader.core;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Observable;
import java.util.Date;

import org.gudy.azureus2.core3.disk.DiskManagerPiece;
import org.gudy.azureus2.core3.download.DownloadManager;

import skdownloader.core.Common;
import skdownloader.core.UserIOUtils;
import skdownloader.core.torrentcore.TorrentDownloader;
import skdownloader.core.urlcore.UrlDownloadThreadStateHolder;
import skdownloader.core.urlcore.UrlDownloader;

public class DownloadStateHolder extends Observable
{
	private String urlname;
	private int type;
	private String dirname;
	private int numConnections;
	private int minAcclelerateFileSize;
	private int retryLimit;
	private long fileSize;
	private String fileName;
	private long downloadedSize;
	private long uploadedSize;
	private long lastDownloadedSize; //This is used to increase percent calculation efficiency
	private int minPercentChunkSize; //Percent will be calculated only after last downloaded size exceeds this size.
	private long remainingSize;
	private long startTime;
	private long seedingStartTime;
	private long endTime;
	private long downloadTime;
	private int pausedTime;
	private long pauseStartTime;
	private long pauseEndTime;
	private int elapsedTime;
	private int status;
	private int percentDone;
	private String errorMessage;
	//private UrlDownloader myDownloader;
	private DownloaderInterface myDownloader;
	@SuppressWarnings("unused")
	private UserIOUtils ioutils;
	private float downloadSpeed;
	private float uploadSpeed;
	private ArrayList<UrlDownloadThreadStateHolder> threadStatusHolder = new ArrayList<UrlDownloadThreadStateHolder>();
	//private ArrayList<TaskRepresenter> downloadTaskList = new ArrayList<TaskRepresenter>();  // For torrent downloads
	private Object parentComponent; // This is for gui cases to display messages relative to position of gui.
	private int protocol;
	private boolean useCache = true;
	private int timeRemaining = 0;
	private BitSet torrentCompletedBits = new BitSet();
	private BitSet excludedFiles = new BitSet();
	private int numberOfPieces = 0;
	private int numberOfSeeders = 0;
	private int numberOfLeechers = 0;
	private int BLOCK_SIZE;
	public int filesInTorrent=0;
	
	// Proxy variables
	private boolean useProxy =false;
	private String httpProxyServer;
	private int httpProxyPort;
	private String httpAuthenUser;
	private String httpAuthenPassword;
	private String socksProxyServer;
	private String socksProxyPort;
	private String socksAuthenUser;
	private String socksAuthenPassword;
	
	private String torrentUniqName;
	private String torrentHash;
	private String torrentCreator;
	private String torrentComment;
	private String torrentCreateDate;
	private String torrentSize;
	private String torrentPrivacy="--";
	private String infoText;
	private DownloadManager fileDownloadManager;
	private boolean managedTorrent;
	
	private float shareRatio;
	
	public static final String STATUSES[] = {"Starting", "Downloading", "Paused", "Pausedforexit", "Pausedforerror", "Completed", "Cancelled", "Error", "Seeding"};
    
	// These are the status codes.
	public static final int STARTING = 0;
	public static final int DOWNLOADING = 1;
	public static final int PAUSED = 2;
	public static final int PAUSEDFOREXIT = 3;
	public static final int PAUSEDFORERROR = 4;
	public static final int COMPLETED = 5;
	public static final int CANCELLED = 6;
	public static final int ERROR = 7;
	public static final int SEEDING = 8;
	public static final int EXITING=100;
	
	//Protocols supported
	public static final int HTTP=0;
	public static final int HTTPS=1;
	public static final int FTP=2;
	
	/*************************************************************
	 * 
	 * Function  : DownloadStateHolder
	 * Arguments : downloaderobject, logger
	 * Return    : void
	 * Notes     : Initializer
	 * 
	 *************************************************************/
	public DownloadStateHolder(UrlDownloader d, UserIOUtils l) {
		type = Common.TYPE_URL;
		fileSize=-1;
		downloadedSize=0;
		uploadedSize = 0;
		lastDownloadedSize=0;
		downloadSpeed=0;
		remainingSize=0;
		percentDone=0;
		myDownloader=d;
		this.ioutils = l;
		status=STARTING;
		pausedTime=0;
		pauseStartTime=0;
		pauseEndTime=0;
		startTime=0;
		seedingStartTime=0;
		endTime=0;
		downloadTime=0;
		errorMessage="";
		parentComponent = null;
		this.fileDownloadManager=null;
		infoText="";
	}
	
	public DownloadStateHolder(TorrentDownloader d, UserIOUtils l)
	{
		type = Common.TYPE_TORRENT;
		fileSize=-1;
		downloadedSize=0;
		uploadedSize = 0;
		lastDownloadedSize=0;
		downloadSpeed=0;
		remainingSize=0;
		percentDone=0;
		myDownloader=d;
		this.ioutils = l;
		status=STARTING;
		pausedTime=0;
		pauseStartTime=0;
		pauseEndTime=0;
		startTime=0;
		seedingStartTime=0;
		endTime=0;
		downloadTime=0;
		errorMessage="";
		parentComponent = null;
		numberOfLeechers=0;
		numberOfSeeders=0;
		this.torrentUniqName="aaa";
		this.fileDownloadManager=null;
		infoText="";
		managedTorrent=false;
		shareRatio=0;
	}
	
	public boolean IsManagedTorrent() {
		return managedTorrent;
	}
	
	public void SetManagedTorrent(boolean val) {
		managedTorrent = val;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetDownloadType
	 * Arguments : void
	 * Return    : void
	 * Notes     : Returns stype of download (torrent, url etc)
	 * 
	 *************************************************************/
	public int GetDownloadType() {
		return this.type;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetDownloadType
	 * Arguments : void
	 * Return    : void
	 * Notes     : Set the type of this download (torrent, url etc)
	 * 
	 *************************************************************/
	public void SetDownloadType(int type) {
		this.type = type;
	}
	
	public void SetInfoText(String s){
		this.infoText=s;
		setChanged();
		notifyObservers("StatusTextChanged");
	}
	public String GetInfoText() {
		return this.infoText;
	}
	
	public void SetShareRatio(float r) {
		this.shareRatio=r;
	}
	
	public float GetShareRatio(){
		return this.shareRatio;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetStartTime
	 * Arguments : void
	 * Return    : void
	 * Notes     : Returns starting time of download
	 * 
	 *************************************************************/
	public long GetStartTime()
	{
		return startTime;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetStartTime
	 * Arguments : time
	 * Return    : void
	 * Notes     : Set starting time of download. Also sets starting
	 *             time of each thread
	 * 
	 *************************************************************/
	public void SetStartTime(long time)
	{
		startTime=time;
		for(int i=1; i< numConnections; i++)
		{
			SetThreadStartTime(i, time);
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : GetEndTime
	 * Arguments : void
	 * Return    : void
	 * Notes     : Get finish time of download
	 * 
	 *************************************************************/
	public long GetEndTime()
	{
		return endTime;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetEndTime
	 * Arguments : time
	 * Return    : void
	 * Notes     : Set ending time of download
	 * 
	 *************************************************************/
	public void SetEndTime(long time)
	{
		endTime=time;
		for(int i=1; i< numConnections; i++)
		{
			SetThreadEndTime(i, time);
		}		
	}
	
	/*************************************************************
	 * 
	 * Function  : SetParentComponent
	 * Arguments : Object
	 * Return    : void
	 * Notes     : Parent component object represents the gui component that
	 *             manages the download. Any gui message displays could use
	 *             this to display frames relative to the parent gui
	 * 
	 *************************************************************/
	public void SetParentComponent(Object o)
	{
		parentComponent = o;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetParentComponent
	 * Arguments : void
	 * Return    : void
	 * Notes     : Returns object representing parent gui component
	 * 
	 *************************************************************/
	public Object GetParentComponent()
	{
		return parentComponent;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetFileSize
	 * Arguments : size
	 * Return    : void
	 * Notes     : Sets size of file. Also sets minPercentChunkSize which
	 *             is used to optimize percent calculation. Percent is
	 *             calculated only when a chunk greater than this size is downloaded.
	 * 
	 *************************************************************/
	public void SetFileSize(long totalLength)
	{
		fileSize = totalLength;
		minPercentChunkSize = (int) Common.MyRound(totalLength/200, 0);
		minPercentChunkSize=(minPercentChunkSize>=Common.MIN_READ_CHUNK)?minPercentChunkSize:Common.MIN_READ_CHUNK;
		setChanged();
		notifyObservers("FileSize");
	}
	
	/*************************************************************
	 * 
	 * Function  : SetThreadFileSize
	 * Arguments : thread index, size
	 * Return    : void
	 * Notes     : Sets size of chunk downloaded by a thread.
	 * 
	 *************************************************************/
	public void SetThreadFileSize(int index, int size)
	{
		threadStatusHolder.get(index).SetSize(size);
		setChanged();
		notifyObservers("ThreadFileSize");		
		//notifyObservers("FileSize");
	}
	
	/*************************************************************
	 * 
	 * Function  : GetFileSize
	 * Arguments : void
	 * Return    : void
	 * Notes     : Returns file size
	 * 
	 *************************************************************/
	public long GetFileSize()
	{
		return fileSize;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetThreadFileSize
	 * Arguments : index
	 * Return    : void
	 * Notes     : Returns chunk size downloaded in a thread
	 * 
	 *************************************************************/
	public int GetThreadFileSize(int index)
	{
		return threadStatusHolder.get(index).GetSize();
	}
	
	/*************************************************************
	 * 
	 * Function  : SetDownloadSpeed
	 * Arguments : speed
	 * Return    : void
	 * Notes     : Sets download speed.
	 * 
	 *************************************************************/
	public void SetDownloadSpeed(float speed)
	{
		this.downloadSpeed=speed;
		setChanged();
		notifyObservers("DownloadSpeed");
	}
	
	public void SetUploadSpeed(float speed) {
		this.uploadSpeed=speed;
		setChanged();
		notifyObservers("UploadSpeed");
	}

	/*************************************************************
	 * 
	 * Function  : SetThreadDownloadSpeed
	 * Arguments : thread index, speed
	 * Return    : void
	 * Notes     : Sets download speed of each thread.
	 * 
	 *************************************************************/
	public void SetThreadDownloadSpeed(int index, float speed)
	{
		threadStatusHolder.get(index).SetSpeed(speed);
	}
	
	/*************************************************************
	 * 
	 * Function  : GetDownloadSpeed
	 * Arguments : void
	 * Return    : download speed
	 * Notes     : Returns download speed
	 * 
	 *************************************************************/
	public float GetDownloadSpeed()
	{
		return downloadSpeed;
	}
	
	public float GetUploadSpeed(){
		return uploadSpeed;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetThreadDownloadSpeed
	 * Arguments : thread inex
	 * Return    : download speed of a thread
	 * Notes     : Returns download speed of thread
	 * 
	 *************************************************************/
	public float GetThreadDownloadSpeed(int index)
	{
		return threadStatusHolder.get(index).Getspeed();
	}

	/*************************************************************
	 * 
	 * Function  : GetDownloadSpeedString
	 * Arguments : void
	 * Return    : String representation of download speed
	 * Notes     : Returns string representation of download speed
	 * 
	 *************************************************************/
	public String GetDownloadSpeedString()
	{
		if(downloadSpeed > 0)
		{
			return SpeedToString(downloadSpeed,1);
		}
		else
		{
			return "--";
		}
	}
	
	public String GetUploadSpeedString()
	{
		if(uploadSpeed > 0) {
			return SpeedToString(uploadSpeed,1);
		} 
		else {
			return "--";
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : GetThreadDownloadSpeedString
	 * Arguments : thread index
	 * Return    : String representation of thread download speed
	 * Notes     : Returns string representation of thread download speed
	 * 
	 *************************************************************/
	public String GetThreadDownloadSpeedString(int index)
	{
		return SpeedToString(threadStatusHolder.get(index).Getspeed(),2);
	}
	
	/*************************************************************
	 * 
	 * Function  : SpeedToString
	 * Arguments : downloadspeed
	 * Return    : String representation of speed
	 * Notes     : Converts speed to string representation
	 * 
	 *************************************************************/
	public static String SpeedToString(float downloadSpeed, int round)
	{
		if(downloadSpeed < 1000)
		{
			return(""+downloadSpeed+"bits/s");
		}
		else if(downloadSpeed <1000000)
		{
			return(Common.MyRound(downloadSpeed/1000, round)+"Kb/s");
		}
		else
		{
			return(Common.MyRound(downloadSpeed/1000000, round)+"Mb/s");
		}
	}
	
	public void SetUploadedSize(long uploaded) {
		this.uploadedSize=uploaded;
		setChanged();
		notifyObservers("UploadedSize");
	}
	
	public Long GetUploadedSize() {
		return this.uploadedSize;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetDownloadedSize
	 * Arguments : size in bytes
	 * Return    : void
	 * Notes     : Sets current downloaded size
	 * 
	 *************************************************************/
	public void SetDownloadedSize(long l)
	{
		downloadedSize = l;
		setChanged();
		notifyObservers("DownloadedSize");

		if(((downloadedSize - lastDownloadedSize) >= minPercentChunkSize) ||
			(downloadedSize == fileSize))
		{
			lastDownloadedSize=downloadedSize;
			int tmpPercent=Common.GetPercent(fileSize, downloadedSize);
			if (percentDone < tmpPercent)
			{
				if(tmpPercent > 100)
					tmpPercent=100;
				setChanged();
				percentDone = tmpPercent;
				notifyObservers("DownloadedPercent");
			}
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : GetDownloadedSize
	 * Arguments : void
	 * Return    : downloaded size
	 * Notes     : Returns current downloaded size
	 * 
	 *************************************************************/
	public long GetDownloadedSize()
	{
		return downloadedSize;
	}
	
	/*************************************************************
	 * 
	 * Function  : setThreadDownloadedSize
	 * Arguments : index, size
	 * Return    : void
	 * Notes     : Sets total size downloaded in each thread
	 * 
	 *************************************************************/
	public void setThreadDownloadedSize(int index, int size)
	{
		//logger.PrintLog("DEBUG", "Setting downloaded size for thread "+index+" to "+size);
		threadStatusHolder.get(index).SetDownloaded(size);
		int tmpPercent=Common.GetPercent(threadStatusHolder.get(index).GetSize(), size);
		//logger.PrintLog("DEBUG", "Setting percent donefor thread "+index+" to "+tmpPercent);
		if (threadStatusHolder.get(index).GetPercentDone() <= tmpPercent)
		{
			//We only need to notify once the last thread status is updated.
			if(index == numConnections-1)
			{
				setChanged();
				notifyObservers("ThreadDownloadedSize");
			}
			threadStatusHolder.get(index).SetPercentDone(tmpPercent);
			//logger.PrintLog("DEBUG", "Filesize, downloadedsize - "+fileSize+", "+downloadedSize);
		}
		
		if(size >= threadStatusHolder.get(index).GetSize())
		{
			//((DownloadThreadStateHolder)threadStatusHolder.get(index)).SetState(COMPLETED);
			SetThreadStatus(index,COMPLETED);
			SetThreadEndTime(index,(new Date()).getTime());
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : GetThreadDownloadedSize
	 * Arguments : void
	 * Return    : downloaded size
	 * Notes     : Returns current downloaded size of thread
	 * 
	 *************************************************************/
	public int GetThreadDownloadedSize(int index)
	{
		return threadStatusHolder.get(index).GetDownloaded();
	}
	
	/*************************************************************
	 * 
	 * Function  : SetRemainingSize
	 * Arguments : void
	 * Return    : void
	 * Notes     : Sets remaining download size
	 * 
	 *************************************************************/
	public void SetRemainingSize(int size)
	{
		remainingSize = size;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetRemainingSize
	 * Arguments : void
	 * Return    : void
	 * Notes     : Get remaining download size
	 * 
	 *************************************************************/
	public long GetRemainingSize()
	{
		return remainingSize;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetUrlName
	 * Arguments : url
	 * Return    : void
	 * Notes     : Sets url name
	 * 
	 *************************************************************/
	public void SetUrlName(String urlname)
	{
		this.urlname = urlname;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetUrlName
	 * Arguments : void
	 * Return    : url name
	 * Notes     : Get url name
	 * 
	 *************************************************************/
	public String GetUrlName()
	{
		return urlname;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetFileName
	 * Arguments : file name
	 * Return    : void
	 * Notes     : Sets download file name
	 * 
	 *************************************************************/
	public void SetFileName(String name)
	{
		this.fileName = name;
		setChanged();
		notifyObservers("FileName");
	}
	
	/*************************************************************
	 * 
	 * Function  : GetFileName
	 * Arguments : void
	 * Return    : file name
	 * Notes     : Returns download file name
	 * 
	 *************************************************************/
	public String GetFileName()
	{
		return fileName;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetDirName
	 * Arguments : dir name
	 * Return    : void
	 * Notes     : Returns download directory name
	 * 
	 *************************************************************/
	public void SetDirName(String dirname)
	{
		this.dirname = dirname;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetDirName
	 * Arguments : void
	 * Return    : dir name
	 * Notes     : Get download directory name
	 * 
	 *************************************************************/
	public String GetDirName()
	{
		return dirname;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetNumConnections
	 * Arguments : number of connections
	 * Return    : void
	 * Notes     : Set number of connections
	 * 
	 *************************************************************/
	public void SetNumConnections(int numConnections)
	{
		this.numConnections=numConnections;
		
		//Instantiate arraylist for thread data.
		threadStatusHolder.clear();
		for(int i=0; i< numConnections; i++)
		{
			threadStatusHolder.add(new UrlDownloadThreadStateHolder());
		}
		setChanged();
		notifyObservers("NumConnections");
	}
	
	/*************************************************************
	 * 
	 * Function  : GetNumConnections
	 * Arguments : number of connections
	 * Return    : void
	 * Notes     : Get number of connections
	 * 
	 *************************************************************/
	public int GetNumConnections()
	{
		return numConnections;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetProgress
	 * Arguments : noid
	 * Return    : percentage done
	 * Notes     : Get percentage completed
	 * 
	 *************************************************************/
	public int GetProgress()
	{
		return percentDone;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetProgress
	 * Arguments : percent
	 * Return    : void
	 * Notes     : Set percentage completed
	 * 
	 *************************************************************/
	public void SetProgress(int percent)
	{
		percentDone=percent;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetThreadProgress
	 * Arguments : thread index
	 * Return    : percentage done
	 * Notes     : Get percentage completed for a thread
	 * 
	 *************************************************************/
	public int GetThreadProgress(int index)
	{
		return threadStatusHolder.get(index).GetPercentDone();
	}
	
	/*************************************************************
	 * 
	 * Function  : SetDownloadStatus
	 * Arguments : status
	 * Return    : void
	 * Notes     : Set download status
	 * 
	 *************************************************************/
	public void SetDownloadStatus(int status)
	{
		this.status=status;
		//this.SetDownloadedSize(0);
		//this.SetProgress(0);
		setChanged();
		notifyObservers("DownloadStatus");
		//logger.PrintLog("DEBUG", "Changing state to "+DownloadStateHolder.STATUSES[status]);
	}
	
	/*************************************************************
	 * 
	 * Function  : GetStatusString
	 * Arguments : void
	 * Return    : status as string
	 * Notes     : Get download status as string
	 * 
	 *************************************************************/
	public String GetStatusString()
	{
		return STATUSES[status];
	}
	
	/*************************************************************
	 * 
	 * Function  : GetStatus
	 * Arguments : void
	 * Return    : download status
	 * Notes     : Get download status
	 * 
	 *************************************************************/
	public int GetStatus()
	{
		return status;
	}
	
	public void SelfDestroy() {
		setChanged();
		notifyObservers("DestroyMe");
	}
	
	/*************************************************************
	 * 
	 * Function  : GetDownloaderObject
	 * Arguments : void
	 * Return    : downloader object
	 * Notes     : Get downloader object
	 * 
	 *************************************************************/
	//public UrlDownloader GetDownloaderObject()
	public DownloaderInterface GetDownloaderObject()
	{
		return myDownloader;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetElapsedTime
	 * Arguments : void
	 * Return    : void
	 * Notes     : Set time elapsed
	 * 
	 *************************************************************/
	public void SetElapsedTime()
	{
		if(this.GetEndTime() == 0) {
			if(this.GetPauseStartTime()!=0)
				elapsedTime=(int) ((this.GetPauseStartTime()-this.startTime-this.pausedTime)/1000);
			else
				elapsedTime = (int) ((Common.GetCurrentTime()-this.startTime-this.pausedTime)/1000);
		}
		else
			elapsedTime = (int) ((this.endTime-this.startTime-this.pausedTime)/1000);
	}
	
	/*************************************************************
	 * 
	 * Function  : GetElapsedTimeString
	 * Arguments : void
	 * Return    : time elapsed string
	 * Notes     : Get time elapsed string
	 * 
	 *************************************************************/
	public String GetElapsedTimeString()
	{
		if(this.startTime==0)
		{
			return "00:00:00";
		}
		
		return(SecsToTimeString(elapsedTime));
	}
	
	/*************************************************************
	 * 
	 * Function  : SecsToTimeString
	 * Arguments : void
	 * Return    : time elapsed string
	 * Notes     : Converts time in seconds to hour:min:sec format
	 * 
	 *************************************************************/
	public String SecsToTimeString(int elapsedTime)
	{
		String hour1 = "0h";
		int days= 0;
		String days1="0d";
		String min1 = "0m";
		String secs1="";
		int remaining=elapsedTime;
		
		if(elapsedTime > 86400){
			days = (int) (elapsedTime/86400);
			days1=days+"d";
			remaining = (int) (elapsedTime%86400);
		}
		
		if(remaining > 3600)
		{
			int hour = (int) (remaining/3600);
			hour1 = Integer.toString(hour);
			//if(hour <10)
			hour1=hour1+"h";
			remaining = (int) (elapsedTime%3600);
		}
		
		if(remaining > 60)
		{
			int min = (int) (remaining/60);
			min1 = Integer.toString(min);
			//if(min<10)
			min1=min1+"m";
		}
		
		if(days==0) {
			int secs = (int) (remaining%60);
			secs1 = Integer.toString(secs);
			//if(secs<10)
			secs1=secs1+"s";
		}
		
		if(days>0)
			return(days1+" "+hour1+" "+min1);
		else
			return(hour1+" "+min1+" "+secs1);
	}
	
	/*************************************************************
	 * 
	 * Function  : setTimeRemaining
	 * Arguments : seconds
	 * Return    : void
	 * Notes     : sets time remaining in seconds
	 * 
	 *************************************************************/
	public void setTimeRemaining(int secs)
	{
		if(secs < 8640000)
			this.timeRemaining = secs;
		else
			this.timeRemaining=0;
	}
	
	public String getTimeRemainingString()
	{
		String time;
		if(this.timeRemaining == 0) {
			time = "--";
		}
		else {
			time = SecsToTimeString(this.timeRemaining);
		}
		return time;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetErrorMessage
	 * Arguments : message string
	 * Return    : void
	 * Notes     : Set error message
	 * 
	 *************************************************************/
	public void SetErrorMessage(String msg) {
		errorMessage = msg;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetErrorMessage
	 * Arguments : void
	 * Return    : message string
	 * Notes     : Get error message
	 * 
	 *************************************************************/
	public String GetErrorMessage() {
		return(errorMessage);
	}
	
	/*************************************************************
	 * 
	 * Function  : SetThreadStartTime
	 * Arguments : index, time
	 * Return    : void
	 * Notes     : Sets starting time of a thread.
	 * 
	 *************************************************************/
	public void SetThreadStartTime(int index, long time)
	{
		threadStatusHolder.get(index).SetStartTime(time);
	}
	
	/*************************************************************
	 * 
	 * Function  : GetThreadStartTime
	 * Arguments : index
	 * Return    : thread starting time
	 * Notes     : Get starting time of a thread.
	 * 
	 *************************************************************/
	public long GetThreadStartTime(int index)
	{
		return threadStatusHolder.get(index).GetStartTime();

	}
	
	/*************************************************************
	 * 
	 * Function  : SetThreadEndTime
	 * Arguments : index, thread download end time
	 * Return    : void
	 * Notes     : Set finishing time of a thread.
	 * 
	 *************************************************************/
	public void SetThreadEndTime(int index, long time)
	{
		threadStatusHolder.get(index).SetEndTime(time);
	}
	
	/*************************************************************
	 * 
	 * Function  : GetThreadEndTime
	 * Arguments : index
	 * Return    : thread download end time
	 * Notes     : Get finishing time of a thread.
	 * 
	 *************************************************************/
	public long GetThreadEndTime(int index)
	{
		return threadStatusHolder.get(index).GetEndTime();
	}
	
	/*************************************************************
	 * 
	 * Function  : SetThreadStatus
	 * Arguments : index, status
	 * Return    : void
	 * Notes     : Set status of thread
	 * 
	 *************************************************************/
	public void SetThreadStatus(int index, int state)
	{
		threadStatusHolder.get(index).SetState(state);
	}
	
	/*************************************************************
	 * 
	 * Function  : GetThreadStatus
	 * Arguments : index
	 * Return    : status
	 * Notes     : Get status of thread
	 * 
	 *************************************************************/
	public int GetThreadStatus(int index)
	{
		return threadStatusHolder.get(index).GetState();
	}
	
	/*************************************************************
	 * 
	 * Function  : ValidateDownload
	 * Arguments : void
	 * Return    : void
	 * Notes     : Validates a download in case of cancelled or error states. All
	 *             associated values are made zero or nullified.
	 * 
	 *************************************************************/
	public void ValidateDownload()
	{
		if((this.status == CANCELLED) ||
		   (this.status == ERROR))
		{
			//Reset the progress an download speeds.
			this.SetDownloadedSize(0);
			this.SetProgress(0);
			this.SetDownloadSpeed(0);
			this.SetEndTime(Common.GetCurrentTime());
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : SetProxy
	 * Arguments : proxy arguments
	 * Return    : void
	 * Notes     : Sets proxy variables to values
	 * 
	 *************************************************************/
	public void SetProxy(String httpProxyServer, int httpProxyPort, String httpAuthenUser, String httpAuthenPassword,
						 String socksProxyServer, String socksProxyPort, String socksAuthenUser, String socksAuthenPassword)
	{
		useProxy =true;
		this.httpProxyServer=httpProxyServer;
		this. httpProxyPort= httpProxyPort;
		this.httpAuthenUser=httpAuthenUser;
		this.httpAuthenPassword=httpAuthenPassword;
		this.socksProxyServer=socksProxyServer;
		this.socksProxyPort=socksProxyPort;
		this.socksAuthenUser=socksAuthenUser;
		this.socksAuthenPassword=socksAuthenPassword;
	}
	
	/*************************************************************
	 * 
	 * Function  : UseProxy
	 * Arguments : void
	 * Return    : true/false
	 * Notes     : Checks wheter to use proxy or not
	 * 
	 *************************************************************/
	public boolean UseProxy()
	{
		return useProxy;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetHttpProxyServer
	 * Arguments : void
	 * Return    : http proxy server name
	 * Notes     : Gets http proxy server
	 * 
	 *************************************************************/
	public String GetHttpProxyServer()
	{
		return httpProxyServer;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetHttpProxyPort
	 * Arguments : void
	 * Return    : http proxy server port
	 * Notes     : Gets http proxy server port
	 * 
	 *************************************************************/
	public int GetHttpProxyPort()
	{
		return httpProxyPort;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetHttpAuthenUser
	 * Arguments : void
	 * Return    : http proxy authentication user name
	 * Notes     : Gets http proxy authentication user name
	 * 
	 *************************************************************/
	public String GetHttpAuthenUser()
	{
		return httpAuthenUser;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetHttpAuthenPassword
	 * Arguments : void
	 * Return    : http proxy authentication password
	 * Notes     : Gets http proxy authentication password
	 * 
	 *************************************************************/
	public String GetHttpAuthenPassword()
	{
		return httpAuthenPassword;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetSocksProxyServer
	 * Arguments : void
	 * Return    : socks proxy server name
	 * Notes     : Gets socks proxy server name
	 * 
	 *************************************************************/
	public String GetSocksProxyServer()
	{
		return socksProxyServer;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetSocksProxyPort
	 * Arguments : void
	 * Return    : socks proxy server port
	 * Notes     : Gets socks proxy server port
	 * 
	 *************************************************************/
	public String GetSocksProxyPort()
	{
		return socksProxyPort;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetSocksAuthenUser
	 * Arguments : void
	 * Return    : socks proxy authentication user name
	 * Notes     : Gets socks proxy authentication user name
	 * 
	 *************************************************************/
	public String GetSocksAuthenUser()
	{
		return socksAuthenUser;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetSocksAuthenPassword
	 * Arguments : void
	 * Return    : socks proxy authentication password
	 * Notes     : Gets socks proxy authentication password
	 * 
	 *************************************************************/
	public String GetSocksAuthenPassword()
	{
		return socksAuthenPassword;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetMinAccelerateFileSize
	 * Arguments : size
	 * Return    : void
	 * Notes     : Sets minimum size for auto acceleration to apply
	 * 
	 *************************************************************/
	public void SetMinAccelerateFileSize(int size)
	{
		minAcclelerateFileSize=size;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetMinAccelerateFileSize
	 * Arguments : void
	 * Return    : min file size for acceleration
	 * Notes     : Get min file size for auto acceleration
	 * 
	 *************************************************************/
	public int GetMinAccelerateFileSize()
	{
		return minAcclelerateFileSize;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetRetryLimit
	 * Arguments : limit
	 * Return    : void
	 * Notes     : Sets retry limit for attempting connections
	 * 
	 *************************************************************/
	public void SetRetryLimit(int limit)
	{
		retryLimit=limit;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetRetryLimit
	 * Arguments : void
	 * Return    : limit
	 * Notes     : Get retry limit for attempting connections
	 * 
	 *************************************************************/
	public int GetRetryLimit()
	{
		return retryLimit;
	}

	/*************************************************************
	 * 
	 * Function  : GetThreadChunkOffset
	 * Arguments : index
	 * Return    : Chunk offset for thread
	 * Notes     : Get chunk offset for thread
	 * 
	 *************************************************************/
	public int GetThreadChunkOffset(int index)
	{
		return threadStatusHolder.get(index).GetChunkOffset();
	}
	
	/*************************************************************
	 * 
	 * Function  : SetThreadChunkOffset
	 * Arguments : index, offset
	 * Return    : Chunk offset for thread
	 * Notes     : Set chunk offset for thread
	 * 
	 *************************************************************/
	public void SetThreadChunkOffset(int index, int offset)
	{
		threadStatusHolder.get(index).SetChunkOffset(offset);
	}
	
	/*************************************************************
	 * 
	 * Function  : GetThreadWriteRangeStart
	 * Arguments : index
	 * Return    : write range start of thread
	 * Notes     : Get write range start of thread
	 * 
	 *************************************************************/
	public int GetThreadWriteRangeStart(int index)
	{
		return threadStatusHolder.get(index).GetWriteRangeStart();
	}
	
	/*************************************************************
	 * 
	 * Function  : GetThreadWriteRangeStart
	 * Arguments : index, range
	 * Return    : Void
	 * Notes     : Set write range start of thread
	 * 
	 *************************************************************/
	public void SetThreadWriteRangeStart(int index, int range)
	{
		threadStatusHolder.get(index).SetWriteRangeStart(range);
	}
	
	/*************************************************************
	 * 
	 * Function  : SetPauseStartTime
	 * Arguments : time
	 * Return    : Void
	 * Notes     : Set Pausing start time
	 * 
	 *************************************************************/
	public void SetPauseStartTime(long time)
	{
		pauseStartTime=time;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetPauseEndTime
	 * Arguments : time
	 * Return    : Void
	 * Notes     : Set Pausing end time
	 * 
	 *************************************************************/
	public void SetPauseEndTime(long time)
	{
		pauseEndTime=time;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetPausedTime
	 * Arguments : time
	 * Return    : Void
	 * Notes     : Set total time paused
	 * 
	 *************************************************************/
	public void SetPausedTime(int time)
	{
		pausedTime=time;
		SetElapsedTime();
	}
	
	/*************************************************************
	 * 
	 * Function  : GetPauseStartTime
	 * Arguments : void
	 * Return    : pause start time
	 * Notes     : Get pause start time
	 * 
	 *************************************************************/
	public long GetPauseStartTime()
	{
		return pauseStartTime;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetPauseEndTime
	 * Arguments : void
	 * Return    : pause end time
	 * Notes     : Get pause end time
	 * 
	 *************************************************************/
	public long GetPauseEndTime()
	{
		return pauseEndTime;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetPausedTime
	 * Arguments : void
	 * Return    : total time paused
	 * Notes     : Get total time paused
	 * 
	 *************************************************************/
	public int GetPausedTime()
	{
		return pausedTime;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetConnectionProtocol
	 * Arguments : connection type
	 * Return    : void
	 * Notes     : Set connection type
	 * 
	 *************************************************************/
	public void SetConnectionProtocol(int type)
	{
		this.protocol=type;
	}
	
	/*************************************************************
	 * 
	 * Function  : GetConnectionProtocol
	 * Arguments : void
	 * Return    : connection type
	 * Notes     : Get connection type
	 * 
	 *************************************************************/
	public int GetConnectionProtocol()
	{
		 return protocol;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetUseCache
	 * Arguments : true/false
	 * Return    : void
	 * Notes     : Sets whether to use cache
	 * 
	 *************************************************************/
	public void SetUseCache(boolean val)
	{
		useCache=val;
	}
	
	/*************************************************************
	 * 
	 * Function  : UseCache
	 * Arguments : true/false
	 * Return    : void
	 * Notes     : Checks whether to use cache or not
	 * 
	 *************************************************************/
	public boolean UseCache()
	{
		return useCache;
	}
	
	
/* The below functions are for torrents */
	
	public void SetTorrentUniqName(String name) {
		this.torrentUniqName=name;
	}
	
	public String GetTorrentUniqName() {
		return this.torrentUniqName;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetNumberOfPieces
	 * Arguments : bitset
	 * Return    : void
	 * Notes     : Sets the number of pieces in this torrent
	 * 
	 *************************************************************/
	public void SetNumberOfPieces(int n) {
		this.numberOfPieces = n;
		setChanged();
		notifyObservers("NumberPices");
	}
	
	/*************************************************************
	 * 
	 * Function  : GetNumberOfPieces
	 * Arguments : bitset
	 * Return    : void
	 * Notes     : returns the number of pieces in this torrent
	 * 
	 *************************************************************/
	public int GetNumberOfPieces() {
		return numberOfPieces;
	}
	
	/*************************************************************
	 * 
	 * Function  : SetTorrentDownloadedSize
	 * Arguments : size in bytes
	 * Return    : void
	 * Notes     : Sets current downloaded size. This one only sets the download size
	 *             and does not update percent. For torrent, we need to update percent
	 *             separately as there would be bytes discarded in the downloaded size.
	 * 
	 *************************************************************/
	public void SetTorrentDownloadedSize(long l)
	{
		downloadedSize = l;
		setChanged();
		notifyObservers("DownloadedSize");
	}	
	
	public void setTorrentDownloadPercent(int percent) {
		if (percent > this.GetProgress()) {
			setChanged();
			this.SetProgress(percent);
			notifyObservers("DownloadedPercent");
		}
	}	
	
	/*************************************************************
	 * 
	 * Function  : SetTorrentDownloadedBits
	 * Arguments : bitset
	 * Return    : void
	 * Notes     : Sets the torrent downloaded bitset
	 * @deprecated
	 * 
	 *************************************************************/
	public void SetTorrentDownloadedBitsFromBitset_old(BitSet bs, int size) {
		float tmp;
		int roundtmp;
		float tmp1;
		int roundtmp1;
		BitSet oldSet = (BitSet)this.torrentCompletedBits.clone(); 
		if(bs.size() == 100) {
			this.torrentCompletedBits = bs.get(0, 100);
		}
		else {
			for(int i=bs.nextSetBit(0); i>=0; i=bs.nextSetBit(i+1)) {
				tmp = i*100/size;
				roundtmp=(int) Common.MyRound(tmp, 0);
				if(roundtmp > tmp)
					roundtmp--;
				tmp1 = (i+1)*100/size;
				if(tmp1 >= 100) {
					roundtmp1 = 100;
				}
				else {
					roundtmp1=(int) Common.MyRound(tmp1, 0);
					if(roundtmp1 > tmp1)
						roundtmp1--;
				}
				this.torrentCompletedBits.set(roundtmp, roundtmp1);
			}
		}
		//Notify only if bitset has changed.
		if(! oldSet.equals(this.torrentCompletedBits)) {
			setChanged();
			notifyObservers("DownloadedPiecesSetChange");
		}
	}
	
	/*************************************************************
	 * 
	 * Function  : SetTorrentDownloadedBits
	 * Arguments : bitset
	 * Return    : void
	 * Notes     : Sets the torrent downloaded bitset
	 * 
	 *************************************************************/
	public void SetTorrentDownloadedBitsFromBitset(BitSet bs) {
		BitSet oldSet = (BitSet)this.torrentCompletedBits.clone(); 
		for(int i=0; i<this.numberOfPieces; i++) {
			if(bs.get(i))
				this.torrentCompletedBits.set(i);
		}
		//Notify only if bitset has changed.
		if(! oldSet.equals(this.torrentCompletedBits)) {
			setChanged();
			notifyObservers("DownloadedPiecesSetChange");
		}
		oldSet=null;
	}
	

	/*************************************************************
	 * 
	 * Function  : SetTorrentDownloadedBits
	 * Arguments : bitset
	 * Return    : void
	 * Notes     : Sets the torrent downloaded bitset
	 * 
	 *************************************************************/
	public void SetTorrentDownloadedBitsToFull() {
		if(fileDownloadManager.getDiskManager() != null) {
			//If all files are downloaded(including dnd ones), set expliclitely to all bits
			if(fileDownloadManager.isDownloadComplete(true)) {
				this.torrentCompletedBits.set(0,this.numberOfPieces);
			}
			else {
				DiskManagerPiece[] arr  = fileDownloadManager.getDiskManager().getPieces();
				BitSet tmpset = new BitSet(GetNumberOfPieces());
				for(int i=0; i<arr.length; i++) {
					if(arr[i].isDone())
						tmpset.set(i);
				}
				SetTorrentDownloadedBitsFromBitset(tmpset);
			}
		}
		setChanged();
		notifyObservers("DownloadedPiecesSetChange");
	}
	
	/*************************************************************
	 * 
	 * Function  : GetTorrentDownloadedBits
	 * Arguments : void
	 * Return    : torrent downloaded bitset
	 * Notes     : returns the torrent downloaded bitset
	 * 
	 *************************************************************/
	public BitSet GetTorrentDownloadedBits() {
		return this.torrentCompletedBits;
	}
	
	public String GetTorrentDownloadedBitsAsString() {
		return Common.BitSet2String(this.torrentCompletedBits, this.numberOfPieces);
	}
	
	public void SetNumberOfSeeders(int n) {
		this.numberOfSeeders=n;
		setChanged();
		notifyObservers("SeedersUpdated");
	}
	public int GetNumberOfSeeders() {
		return this.numberOfSeeders;
	}
	
	public void SetNumberOfLeechers(int n) {
		this.numberOfLeechers=n;
		setChanged();
		notifyObservers("LeechersUpdated");
	}
	
	public int GetNumberOfLeechers() {
		return this.numberOfLeechers;
	}
	
	public void SetBlockSize(int size) {
		this.BLOCK_SIZE=size;
	}
	
	public int GetBlockSize() {
		return this.BLOCK_SIZE;
	}
	
	public void SetTorrentDownloadManager(DownloadManager fileDownloadManager) {
		this.fileDownloadManager=fileDownloadManager;
	}
	
	public DownloadManager GetTorrentDownloadManager() {
		return this.fileDownloadManager;
	}
	
	public void SetTorrentHash(String hash) {
		this.torrentHash=hash;
	}
	public String GetTorrentHash() {
		return this.torrentHash;
	}
	
	public void SetTorrentCreator(String creator) {
		this.torrentCreator=creator;
	}
	public String GetTorrentCreator() {
		return this.torrentCreator;
	}
	
	public void SetTorrentCreatedDate(long date) {
		this.torrentCreateDate=(new Date(date)).toString();
	}
	public String GetTorrentCreatedDate() {
		return this.torrentCreateDate;
	}
	
	public void SetTorrentComment(String comment) {
		this.torrentComment=comment;
	}
	public String GetTorrentComment() {
		return this.torrentComment;
	}
	
	public void SetTorrentSize(String size) {
		this.torrentSize=size;
	}
	public String GetTorrentSize() {
		return this.torrentSize;
	}
	
	public void SetTorrentPrivacy(boolean priv) {
		if(priv) {
			this.torrentPrivacy="Private Torrent";
		}
		else {
			this.torrentPrivacy="Public Torrent";
		}
	}
	public String GetTorrentPrivacy() {
		return this.torrentPrivacy;
	}
	
	public void SetSeedingStartTime(long time) {
		this.seedingStartTime=time;
	}
	public long GetSeedingStartTime() {
		return this.seedingStartTime;
	}
	
	public void SetDownloadTime(long time) {
		this.downloadTime=time;
	}
	public long GetDownloadTime() {
		return this.downloadTime;
	}
	
	public long GetSeedingTime() {
		return(Common.GetCurrentTime()-startTime -pausedTime-downloadTime); 
	}
	
	//We do not want to store the info set in stateholder. What we need is for anybody to tell
	//infoholder that the file info has changed and info holder will update all it's listeners.
	//Since there won't be any file additions/removals, we need not take care abour row additions
	//or row deletions.
	public void announceFileInfoSetChange() {
		setChanged();
		notifyObservers("FileInfoUpdated");
	}
	
	public void SetExcludedList(BitSet b, int size) {
		filesInTorrent=size;
		excludedFiles=new BitSet(size);
		for(int i=0; i< size; i++) {
			if(b.get(i))
				excludedFiles.set(i);
			else
				excludedFiles.clear(i);
		}
	}
	
	public void SetInExcludedList(int i, boolean val) {
		if(val)
			excludedFiles.set(i);
		else
			excludedFiles.clear(i);
	}
	
	public void InitExcludedList(int size) {
		filesInTorrent=size;
		excludedFiles = new BitSet(size);
		excludedFiles.clear();
	}
	
	public BitSet GetExcludedFiles() {
		if(excludedFiles.isEmpty())
			return null;
		
		return excludedFiles;
	}
	
	public void SetExcludedFile(int index) {
		this.excludedFiles.set(index);
	}
	
	public String GetExcludedListAsString() {
		if(filesInTorrent == 0) 
			return "";
		
		return Common.BitSet2String(this.excludedFiles, filesInTorrent);
	}
}
