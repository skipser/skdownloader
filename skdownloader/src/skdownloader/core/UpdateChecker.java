/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    Update checker
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.core;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import skdownloader.core.urlcore.UrlDownloader;
import skdownloader.downloadmanager.GuiUserIOUtils;
import skdownloader.downloadmanager.SystemInfoHolder;

public class UpdateChecker extends Thread
{
	private String updatesBaseUrl;
	//private String instDir;
	private String updatesDir;
	private String version;
	private DocumentBuilderFactory dbf;
	private Document doc;
	GuiUserIOUtils ioutils;
	private SystemInfoHolder sysInfoHolder;
	private boolean immediate;
	
	private static Logger logger = Logger.getLogger(UpdateChecker.class);
	
	public UpdateChecker(SystemInfoHolder sysInfoHolder, GuiUserIOUtils ioutils, boolean immediate) {
		this.sysInfoHolder=sysInfoHolder;
		this.updatesBaseUrl=sysInfoHolder.UPDATESURL;
		//this.instDir=sysInfoHolder.GetInstallDir();
		this.updatesDir = sysInfoHolder.getUpdatesDir();
		this.version=sysInfoHolder.VERSION;
		this.ioutils=ioutils;
		this.immediate=immediate; // This indicates if this was an auto update or an user initiated one.
	}
	
	public void run() {
		CheckUpdate();
	}
	
	public void CheckUpdate() {
		logger.info("Starting updat check");
		//Download latest_version.xml
		UrlDownloader dl = new UrlDownloader(ioutils, sysInfoHolder);
		if(sysInfoHolder.USEPROXY)
			dl.GetStateHolder().SetProxy(sysInfoHolder.HTTPPROXYSERVER, sysInfoHolder.HTTPPROXYPORT,sysInfoHolder.HTTPPROXYUSER, sysInfoHolder.HTTPPROXYPASSWORD,"", "", "", "");
			logger.info("Downloading "+updatesBaseUrl+"/latest_version.xml");
		if(dl.SingleDownload(updatesBaseUrl+"/latest_version.xml", updatesDir+File.separator+"latest_version.xml") != Common.OK) {
			logger.error("Failed download");
			sysInfoHolder.isUpdateChecking=false;
			return;
		}
		
		//xml was downloaded properly. Proceed with checking for file updates.
		dbf= DocumentBuilderFactory.newInstance();
		dbf.setIgnoringElementContentWhitespace(false);
		try {
			DocumentBuilder dBuilder = dbf.newDocumentBuilder();
			doc = dBuilder.parse(new File(updatesDir+File.separator+"latest_version.xml"));
			String latestVer = GetNodeValue("version");

			//Return if no version change.
			if(version.equals(latestVer)) {
				if(immediate) {
					ioutils.PrintMessage("You are already on the latest version!!!");
				}
				sysInfoHolder.isUpdateChecking=false;
				return;
			}
			
			// We are now just showing a message that an update is needed... No autoupdate to prevent
			// huge loads on skipser server.
			if (! sysInfoHolder.HIDEUPDATEREMINDERFORVERSION.equals(latestVer)) {
				if (! ioutils.GetFromCustomButtonsMessage(null, "An update is available. Please upgrade using this link.<br/><a href=\"https://sourceforge.net/projects/skdownloader/files/?source=navbar\">https://sourceforge.net/projects/skdownloader/files/?source=navbar</a>", "Later", "Don't show again")) {
					sysInfoHolder.HIDEUPDATEREMINDERFORVERSION=latestVer;
				}
			}
			sysInfoHolder.isUpdateChecking=false;
			return;

			/*//Find out files to download/remove from latest.xml files.
			ArrayList<String> newDownloadList = new ArrayList<String>();
			ArrayList<String> newDownloadUrlList = new ArrayList<String>();
			ArrayList<String> newChksumList = new ArrayList<String>();
			ArrayList<String> removeFilesList = new ArrayList<String>();
			if(CheckFilesToDownload(instDir+File.separator+"latest.xml",
					             updatesDir+File.separator+"latest.xml",
					             newDownloadList,
					             newDownloadUrlList,
					             newChksumList,
					             removeFilesList))
			{
				//Process the arrays and download files.
				//1. Remove items from removeFilesList;
				if((removeFilesList != null )&&(removeFilesList.size() > 0)) {
					//Write xml file with remove list.
					try {
						BufferedWriter out = new BufferedWriter(new FileWriter(updatesDir+File.separator+"remove.xml"));
						out.write("<remove>");
						out.newLine();
						for(int i=0; i< removeFilesList.size(); i++) {
							out.write("<file name=\""+removeFilesList.get(i)+"\"/>");
							out.newLine();
							//File file = new File(instDir+File.separator+removeFilesList.get(i));
							//file.delete();
						}
						out.write("</remove>");
						out.close();
					} catch (Exception e) {
						logger.error("Exception:", e);
					}

				}
				
				//2. Download items from addfiles list.
				if((newDownloadList.size() > 0) && (newDownloadUrlList.size() > 0)) {
					for(int i=0; i< newDownloadList.size(); i++){
						//Remove file if exists.
						String fileName = updatesDir+File.separator+"skdownloader"+File.separator+newDownloadList.get(i);
						File file = new File(fileName);
						if(file.exists()){
							file.delete();
						}
						//Parent dir may not exist. Create it if necessary.
						file.getParentFile().mkdirs();
						
						//Download the file
						dl.SingleDownload(updatesBaseUrl+"/"+newDownloadUrlList.get(i), fileName);
						if( GUICommon.getChecksumValue(new CRC32(), fileName) != Long.parseLong(newChksumList.get(i))) {
							sysInfoHolder.isUpdateChecking = false;
							return;
						}
						//SingleDownload(updatesBaseUrl+"/"+newDownloadUrlList.get(i), updatesDir+File.separator+"skdownloader"+File.separator+newDownloadList.get(i));
					}
				}
				
				//Create updatechecker.available to indicate availability of a new update
				BufferedWriter out = new BufferedWriter(new FileWriter(updatesDir+File.separator+"updates.available"));
				out.write("Found new version");
				out.close();
				
				//Set last updated time..
				sysInfoHolder.LASTUDATEDAT=(new Date()).getTime();
				
				//Window informing user about update.
				Toolkit.getDefaultToolkit().beep();
				logger.PrintMessage("New version of SKDownloader is available."+System.getProperty("line.separator")+"Program will be updated to new version next time SKDownloader is started");
			}*/
		} catch (Exception e) {
			//Remove everything in updates dir, create update dir and leave.
			//Common.deleteDirectory(updatesDir);
			//(new File(updatesDir)).mkdirs();
			sysInfoHolder.isUpdateChecking=false;
			return;
		}
		
	}
	
	public String GetNodeValue(String nodeName)
	{
		return (String)doc.getElementsByTagName(nodeName).item(0).getFirstChild().getNodeValue();
	}
	
	public boolean CheckFilesToDownload(String oldXml,
            						 String newXml,
            						 ArrayList<String> newDownloadList,
            						 ArrayList<String> newDownloadUrlList,
            						 ArrayList<String> newChksumList,
            						 ArrayList<String> removeFilesList)
	{
		//Get all files in oldXml into array
		ArrayList<String> oldFilesArr = new ArrayList<String>();
		ArrayList<String> oldChksumArr = new ArrayList<String>();
		ArrayList<String> newFilesArr = new ArrayList<String>();
		ArrayList<String> newChksumArr = new ArrayList<String>();
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setIgnoringElementContentWhitespace(false);
		try {
			Document doc = dbf.newDocumentBuilder().parse(new File(oldXml));
			NodeList nl = doc.getElementsByTagName("file");
			for(int i=0; i< nl.getLength(); i++) {
				oldFilesArr.add(((Element)nl.item(i)).getAttribute("name"));
				oldChksumArr.add(((Element)nl.item(i)).getAttribute("checksum"));
			}
			
			doc = dbf.newDocumentBuilder().parse(new File(newXml));
			nl = doc.getElementsByTagName("file");
			for(int i=0; i< nl.getLength(); i++) {
				newFilesArr.add(((Element)nl.item(i)).getAttribute("name"));
				newChksumArr.add(((Element)nl.item(i)).getAttribute("checksum"));
			}
			
			//Find new files to download
			int index=0;
			for(int i=0; i<newFilesArr.size(); i++) {
				//if(oldFilesArr.contains(newFilesArr.get(i)))
				index=oldFilesArr.indexOf(newFilesArr.get(i));
				if(index > -1) {
					if(! oldChksumArr.get(index).equals(newChksumArr.get(i))) {
						String fileName = newFilesArr.get(i);
						String urlName = newFilesArr.get(i);
						if(fileName.contains("|")) {
							fileName=Common.ReplaceChar('|', File.separator.toString(), fileName);
							urlName=Common.ReplaceChar('|', "/", urlName);
						}
						newDownloadList.add(fileName);
						newDownloadUrlList.add(urlName);
						newChksumList.add(newChksumArr.get(i));
					}
				}
				else {
					String fileName = newFilesArr.get(i);
					String urlName = newFilesArr.get(i);
					if(fileName.contains("|")) {
						fileName=Common.ReplaceChar('|', File.separator.toString(), fileName);
						urlName=Common.ReplaceChar('|', "/", urlName);
					}
					newDownloadList.add(fileName);
					newDownloadUrlList.add(urlName);
					newChksumList.add(newChksumArr.get(i));
				}
			}
			
			//Find files to be removed
			for(int i=0; i<oldFilesArr.size(); i++) {
				if(! newFilesArr.contains(oldFilesArr.get(i))) {
					String fileName = oldFilesArr.get(i);
					fileName=Common.ReplaceChar('|', File.separator.toString(), fileName);
					removeFilesList.add(fileName);
				}
			}
		} catch (Exception e) {
			logger.error("Exception:", e);
			return false;
		}
		return true;
	}
	
	
	
/*	public int SingleDownload(String url, String outFile)
	{
		//create an instance of HttpClient
		HttpClient httpClient = new HttpClient();

		//create a method instance
		GetMethod getMethod = new GetMethod(url);
		
		try
		{
			//execute the method
			int statusCode = httpClient.executeMethod(getMethod);

			if(statusCode != 200)
			{
				return Common.ERROR;
			}
			//get the response as an InputStream
			InputStream in = getMethod.getResponseBodyAsStream();
			FileOutputStream fos = new FileOutputStream(outFile);

			byte[] b = new byte[1024];
			int len;

			while ((len = in.read(b)) != -1) 
			{
				fos.write(b, 0, len);
			}

			in.close();
			fos.close();
			getMethod.releaseConnection();
			return Common.OK;
			
		}catch(HttpException e){
			//do something
			return Common.ERROR;
		}catch(IOException e){
			//do something else
			return Common.ERROR;
		}
	}*/
}



