package skdownloader.core;

public interface DownloaderInterface {
	public DownloadStateHolder GetStateHolder();
	public int Download();
	public void Pause();
	public void PauseForExit();
	public void PauseForError();
	void Cancel(String string, int cancelled);
	public void Resume();
}