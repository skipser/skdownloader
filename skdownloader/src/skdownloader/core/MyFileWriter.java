/*************************************************************************************
 * Copyright (c) 2008, toolsbysk.com and/or its affiliates. All rights reserved.
 *
 *   DESCRIPTION
 *    File writer that writes downloaded bits to file without 
 *    loss and mixing.
 *
 *   PRIVATE CLASSES
 *    <list of private classes defined - with one-line descriptions>
 *
 *   NOTES
 *    <other useful comments, qualifications, etc.>
 * 
 *   MODIFIED    (MM/DD/YY)
 *   arun         05/17/09 - Creation
 *
 ****************************************************************************************/

package skdownloader.core;

import java.io.RandomAccessFile;
import java.io.IOException;
import java.io.File;

import org.apache.log4j.Logger;

public class MyFileWriter {
	private String fileName = "";
	private String writeFileName = "";
	private RandomAccessFile outStream = null;
	public int CANCELWRITE=0; //If set to 1, no further writes will be done.
	//private FileOutputStream ostream;
	private static Logger logger = Logger.getLogger(MyFileWriter.class);
	private UserIOUtils ioutils; 
	
	public MyFileWriter(String name, UserIOUtils ioutils)  throws IOException {
		fileName=name;
		this.ioutils=ioutils;
		writeFileName=fileName+"_skdownloader";
		RemoveFile(fileName);
		RemoveFile(writeFileName);
		try {
			outStream = new RandomAccessFile(new File(writeFileName),"rw");
		}catch (Exception e) {
			logger.debug("Specified file \""+fileName+"\" does not exist");
			logger.debug(Common.GetStackTrace(e));
			throw new IOException("Could not open file");
		}
	}
	
	public void WriteToFile(byte[] arr, int size) throws IOException {
		if(CANCELWRITE==1) {
			return;
		}
		
		try {
			outStream.write(arr, 0, size);
		} catch (Exception e) {
			throw new IOException("Could not write to output file");
		}
	}

	public synchronized void WriteToFileAtOffset(byte[] arr, long offset, int size) throws IOException {
		if(CANCELWRITE==1) {
			return;
		}
		try {
			//logger.PrintLog("DEBUG", "myfilewriter: size, offset- "+size+","+offset);
			outStream.seek(offset); // this basically reads n bytes in the file
			outStream.write(arr, 0, size);
			//String value = new String(arr);
			//logger.PrintLog("DEBUG", value);
			
		}catch (Exception e) {
			logger.error("WriteToFile: Could not write to output file");
			logger.error("Exception", e);
			throw new IOException("Could not write to output file");
		}
	}
	
	public void RemoveFile(String filename) {
		try {
			if(outStream != null) {
				outStream.close();
			}
			File f = new File(fileName);
			f.delete();
		} catch (Exception e) {
			logger.error("Exception", e);
		}
		
	}
	
	public void Close() {
		CANCELWRITE=1;
		try {
			outStream.close();
		}catch (Exception e) {
			ioutils.PrintError("Could not close file handle for "+Common.ShortenString(writeFileName, "middle",50 ));
			logger.error("Exception", e);
		}
		File f = new File(writeFileName);
		f.renameTo(new File(fileName));
	}
	
	public void Cancel() {
		CANCELWRITE=1;
		try {
			outStream.close();
		}catch (Exception e) {
			ioutils.PrintError("Could not close file handle for "+Common.ShortenString(writeFileName, "middle",50 ));
			logger.error("Exception", e);
		}
		File f = new File(writeFileName);
		f.delete();
	}
};
