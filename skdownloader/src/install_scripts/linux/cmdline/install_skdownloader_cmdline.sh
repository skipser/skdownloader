#!/bin/sh

##
## Check for valid java installation. Also check whether greater than REQUIRED_VERSION
##
check_java_version()
{

    REQUIRED_VERSION=$1

    # Transform the required version string into a number that can be used in comparisons
    REQUIRED_VERSION=`echo $REQUIRED_VERSION | sed -e 's;\.;0;g'`

    ## First check if java is there in path
    java -version > tmp.ver 2>&1
    if [ $? = 0 ]; then
        VERSION=`cat tmp.ver | grep "java version" | awk '{ print substr($3, 2, length($3)-2); }'`
        VERSION=`echo $VERSION | awk '{ print substr($1, 1, 3); }' | sed -e 's;\.;0;g'`
        rm tmp.ver
        if [ $VERSION ]; then
            if [ $VERSION -ge $REQUIRED_VERSION ]; then
                JAVA_EXE=`which java`
            fi
        fi
    else
        for JAVA_ in `locate bin/java | grep java$ | xargs echo`; do
            $JAVA_ -version > tmp.ver 2>&1
            VERSION=`cat tmp.ver | grep "java version" | awk '{ print substr($3, 2, length($3)-2); }'`
            rm tmp.ver
            VERSION=`echo $VERSION | awk '{ print substr($1, 1, 3); }' | sed -e 's;\.;0;g'`
            if [ $VERSION ]; then
                if [ $VERSION -ge $REQUIRED_VERSION ]; then
                    JAVA_EXE=`echo $JAVA_ | awk '{ print substr($1, 1, length($1)-9); }'`
                fi
            fi
        done
    fi
    echo $JAVA_EXE
    return
}

##
## Check for java
##
REQ_JAVA_VERSION=1.6
if [ "$OVERRIDE_JAVA_EXE" != '' ]; then
  JAVA_EXE=$OVERRIDE_JAVA_EXE;
else
  JAVA_EXE=$(check_java_version "$REQ_JAVA_VERSION")
fi
echo "Checking java version"
if [ $JAVA_EXE ]; then
    echo "Found java at $JAVA_EXE"
else
    echo "Could not find java > $REQ_JAVA_VERSION in your system"
    echo "Please install Java from \"http://www.java.com\""
    exit 1;
fi

##
## Find an install dir. Ask User for alternate dirs.
##
INST_DIR="/usr/local"
if [ -e "$INST_DIR" ]; then
    echo "skdownloader will be installed to \"$INST_DIR\" by default"
    CHANGE_WISH="n"
    while [ 1 ]; do
        echo -e "Do you want to change this [y/n]: \c"
        read CHANGE_WISH
        if [ "$CHANGE_WISH" = "y" -o "$CHANGE_WISH" = "n" ]; then
            break
        else
            echo "Incorrect option. Please re-enter"
        fi
    done

    if [ "$CHANGE_WISH" = "y" ]; then
         while [ 1 ]; do
            echo -e "Enter new location:\c"
            read INST_DIR
            if [ ! -d $INST_DIR ]; then
                echo "Entered location does not exist. Please re-enter"
            else
                break;
            fi
         done
    fi
fi

##
## Start installation
##

## Make skdownloader directory in installation dir.
INST_DIR=$INST_DIR/skdownloader
#dos2unix $INST_DIR/*

#echo "INST_DIR=$INST_DIR" >> env_file
if [ ! -d "$INST_DIR" ]; then
    mkdir $INST_DIR
    chmod -R 777 $INST_DIR
fi

##
##Copy over files. Change variables for each copy.
##
#cd `dirname $0`
tmpdir=`dirname $0`/skdownloader_$$
mkdir $tmpdir
trap 'rm -rf $tmpdir; exit 1' HUP INT QUIT TERM
sed -n '145,$p' $0 | tar -x -C $tmpdir

#1. Jar file
cp $tmpdir/skdownloader.jar $INST_DIR/

#2. Lib dirs
cp -r $tmpdir/lib  $INST_DIR/

#4. Prefs dirs
cp -r $tmpdir/prefs  $INST_DIR/

#4. Launcher file
sed "s|\%INSTDIR\%|$INST_DIR|g" $tmpdir/skdownloader > $INST_DIR/skdownloader.tmp
sed "s|\%JAVA_EXE\%|$JAVA_EXE|g" $INST_DIR/skdownloader.tmp > $INST_DIR/skdownloader
rm -rf $INST_DIR/skdownloader.tmp

#5. Create link in /usr/bin dir
rm -rf /usr/bin/skdownloader
ln -s $INST_DIR/skdownloader /usr/bin/skdownloader

#6. Create permissions
chmod -R 777 $INST_DIR
chmod 777 $INST_DIR/skdownloader
chmod 777 $INST_DIR/skdownloader.jar
chmod 777 /usr/bin/skdownloader

rm -rf $tmpdir

echo ""
echo "SKDownloader(cmdline version) is installed successfully."
echo "A link has beed added as /usr/local/bin/skdownloader."
echo "Please use above path or have /usr/local/bin in PATH to invoke skdownloader."
echo ""
exit 0
