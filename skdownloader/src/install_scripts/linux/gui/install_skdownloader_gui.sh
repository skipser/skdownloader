#!/bin/sh

##
## Check for valid java installation. Also check whether greater than REQUIRED_VERSION
##
check_java_version()
{

    REQUIRED_VERSION=$1

    # Transform the required version string into a number that can be used in comparisons
    REQUIRED_VERSION=`echo $REQUIRED_VERSION | sed -e 's;\.;0;g'`

    ## First check if java is there in path
    java -version 2> tmp.ver
    if [ $? = 0 ]; then
        VERSION=`cat tmp.ver | grep "java version" | awk '{ print substr($3, 2, length($3)-2); }'`
        VERSION=`echo $VERSION | awk '{ print substr($1, 1, 3); }' | sed -e 's;\.;0;g'`
        rm tmp.ver
        if [ $VERSION ]; then
            if [ $VERSION -ge $REQUIRED_VERSION ]; then
                JAVA_EXE=`which java`
            fi
        fi
    else
        for JAVA_ in `locate bin/java | grep java$ | xargs echo`; do
            $JAVA_ -version 2> tmp.ver 1> /dev/null
            VERSION=`cat tmp.ver | grep "java version" | awk '{ print substr($3, 2, length($3)-2); }'`
            rm tmp.ver
            VERSION=`echo $VERSION | awk '{ print substr($1, 1, 3); }' | sed -e 's;\.;0;g'`
            if [ $VERSION ]; then
                if [ $VERSION -ge $REQUIRED_VERSION ]; then
                    JAVA_EXE=`echo $JAVA_ | awk '{ print substr($1, 1, length($1)-9); }'`
                fi
            fi
        done
    fi
    echo $JAVA_EXE
    return
}

##
## Check for java
##
REQ_JAVA_VERSION=1.6
if [ "$OVERRIDE_JAVA_EXE" != '' ]; then
  JAVA_EXE=$OVERRIDE_JAVA_EXE;
else
  JAVA_EXE=$(check_java_version "$REQ_JAVA_VERSION")
fi
export JAVA_EXE

echo "Checking java version"
if [ $JAVA_EXE ]; then
    echo "Found java at $JAVA_EXE"
else
    echo "Could not find java > $REQ_JAVA_VERSION in your system"
    echo "Please install Java from \"http://www.java.com\""
    exit 1;
fi

outname=skdownloader_installer.jar
rm -rf $outname
trap 'rm -f $outname; exit 1' HUP INT QUIT TERM
echo "Unpacking..."
sed -n '71,$p' $0 | tar -x

$JAVA_EXE -jar $outname
rm -rf $outname
exit 0
