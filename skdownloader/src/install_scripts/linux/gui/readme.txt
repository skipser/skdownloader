Thank you for choosing SKDownloader.

SKDownloader supports firefox. But the plugin cannot
not get automatically installed with this installation
due to firefox limitations.

To have the "Download with SKDownloader" in firefox
right click menu, please manually install this 
extension from SKDownloader download page below.

www.skdownloader.com/downloads
