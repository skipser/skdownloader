#!/bin/sh

INST_DIR=%{INSTALL_PATH}

#1. Create link in /usr/bin dir
echo "ln -s $INST_DIR/skdownloader /usr/bin/skdownloader"
rm -rf /usr/bin/skdownloader
ln -s $INST_DIR/skdownloader /usr/bin/skdownloader
rm -rf /usr/bin/download_adder
ln -s $INST_DIR/download_adder /usr/bin/download_adder


#2. Create launcher in application menu if menu exists
sed "s|\%INSTDIR\%|$INST_DIR|g" $INST_DIR/skdownloader.desktop > $INST_DIR/skdownloader.desktop.tmp
if [ -d /usr/share/applications ]; then
    cp $INST_DIR/skdownloader.desktop.tmp /usr/share/applications/skdownloader.desktop
    chmod 777 /usr/share/applications/skdownloader.desktop
    
    echo "rm -rf  /usr/share/applications/skdownloader.desktop" >> $INST_DIR/uninstall_skdownloader.sh
fi

#3. Create launcher in desktop optionally
if [ "$USER" ]; then
    DESKTOP_DIR=/home/$USER/Desktop
fi
if [ "$USERNAME" ]; then
    DESKTOP_DIR=/home/$USERNAME/Desktop
fi

if [ -d $DESKTOP_DIR ]; then
    cp $INST_DIR/skdownloader.desktop.tmp $DESKTOP_DIR/skdownloader.desktop
    chmod 777 $DESKTOP_DIR/skdownloader.desktop

    echo "rm -rf  $DESKTOP_DIR/skdownloader.desktop" >> $INST_DIR/uninstall_skdownloader.sh
fi

#4. Remove /usr/bin link during uninstall
echo "rm -rf /usr/bin/skdownloader" >> $INST_DIR/uninstall_skdownloader.sh

#5. Give write permissions for prefs dir
chmod -R 777 $INST_DIR

#6. Remove tmp files.
rm -rf $INST_DIR/skdownloader.desktop.tmp



