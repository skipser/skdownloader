Thank you for choosing SKDownloader.

SKDownloader supports both Internet Explorer and firefox.

If you are using Firefox 3.0 or greater, the Firefox
integration plugin will be automatically installed. 

If not and you don't find "Download with SKDownloader"
in firefox right click menu, please manually install
this extension from SKDownloader download page below.

www.skdownloader.com/downloads